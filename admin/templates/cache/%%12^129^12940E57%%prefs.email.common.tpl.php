<?php /* Smarty version 2.6.28, created on 2020-09-29 12:10:37
         compiled from prefs.email.common.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.email.common.tpl', 3, false),)), $this); ?>
<form action="prefs.email.php?save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'common'), $this);?>
</legend>
	
		<table>
			<tr>
				<td width="40" valign="top" rowspan="3"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_email.png" border="0" alt="" width="32" height="32" /></td>				
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'storein'), $this);?>
:</td>
				<td class="td2"><select name="blobstorage_provider">
					<option value="0"<?php if ($this->_tpl_vars['bm_prefs']['blobstorage_provider'] == 0): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'filesystem'), $this);?>
 (<?php echo TemplateLang(array('p' => 'separatefiles'), $this);?>
)</option>
					<option value="1"<?php if ($this->_tpl_vars['bm_prefs']['blobstorage_provider'] == 1): ?> selected="selected"<?php endif; ?><?php if (! $this->_tpl_vars['bsUserDBAvailable']): ?> disabled="disabled"<?php endif; ?>><?php echo TemplateLang(array('p' => 'filesystem'), $this);?>
 (<?php echo TemplateLang(array('p' => 'userdb'), $this);?>
)</option>
				</select></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'blobcompress'), $this);?>
?</td>
				<td class="td2">
					<label>
						<input name="blobstorage_compress"<?php if ($this->_tpl_vars['bm_prefs']['blobstorage_compress'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" />
						<?php echo TemplateLang(array('p' => 'enable'), $this);?>

					</label>
					<small style="margin-left:1em;color:#666;"><?php echo TemplateLang(array('p' => 'onlyfor'), $this);?>
 &quot;<?php echo TemplateLang(array('p' => 'filesystem'), $this);?>
 (<?php echo TemplateLang(array('p' => 'userdb'), $this);?>
)&quot;</small>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'ftsearch'), $this);?>
:</td>
				<td class="td2">
					<label>
						<input name="fts_bg_indexing"<?php if ($this->_tpl_vars['bm_prefs']['fts_bg_indexing'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" />
						<?php echo TemplateLang(array('p' => 'fts_bg_indexing'), $this);?>

					</label>
				</td>
			</tr>
		</table>
	</fieldset>
	
	<p>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>