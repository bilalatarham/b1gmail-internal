<?php /* Smarty version 2.6.28, created on 2020-09-29 11:32:11
         compiled from plugins.install.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'plugins.install.tpl', 2, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'installplugin'), $this);?>
</legend>
	
	<form action="plugins.php?action=install&do=uploadPlugin&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" enctype="multipart/form-data" onsubmit="spin(this)">
		<p>
			<?php echo TemplateLang(array('p' => 'install_desc'), $this);?>

		</p>
		
		<table>
			<tr>
				<td width="40" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/plugin_add.png" border="0" alt="" width="32" height="32" /></td>
				<td><?php echo TemplateLang(array('p' => 'plugpackage'), $this);?>
:<br />
					<input type="file" name="package" accept=".bmplugin" style="width:440px;" /></td>
			</tr>
		</table>
		
		<p>
			<div style="float:left;">
				<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/warning.png" border="0" alt="" align="absmiddle" width="16" height="16" />
				<?php echo TemplateLang(array('p' => 'sourcewarning'), $this);?>

			</div>
			<div style="float:right;">
				<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'install'), $this);?>
 " />
			</div>
		</p>
	</form>
</fieldset>