<?php /* Smarty version 2.6.28, created on 2020-09-29 11:27:16
         compiled from login.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'login.tpl', 6, false),array('function', 'fileDateSig', 'login.tpl', 13, false),array('function', 'text', 'login.tpl', 23, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>b1gMail - <?php echo TemplateLang(array('p' => 'acp'), $this);?>
</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $this->_tpl_vars['charset']; ?>
" />
	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="<?php echo $this->_tpl_vars['tpldir']; ?>
style/common.css?<?php echo TemplateFileDateSig(array('file' => "style/common.css"), $this);?>
" rel="stylesheet" type="text/css" />
	
	<!-- client scripts -->
	<script src="../clientlang.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
" type="text/javascript" language="javascript"></script>
	<script src="<?php echo $this->_tpl_vars['tpldir']; ?>
js/common.js?<?php echo TemplateFileDateSig(array('file' => "js/common.js"), $this);?>
" type="text/javascript" language="javascript"></script>
</head>

<body onload="EBID('username').focus();" id="loginBody">
	
	<form action="index.php?action=login" method="post" autocomplete="off">
		<?php if ($this->_tpl_vars['jump']): ?><input type="hidden" id="jump" name="jump" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['jump'],'allowEmpty' => true), $this);?>
" /><?php endif; ?>
		<input type="hidden" name="timezone" id="timezone" value="<?php echo $this->_tpl_vars['timezone']; ?>
" />
		
		<div id="loginBox1">
			<div id="loginBox2">
				<div id="loginBox3">
					<?php if ($this->_tpl_vars['error']): ?><div class="loginError"><?php echo $this->_tpl_vars['error']; ?>
</div><?php endif; ?>
				
					<div id="loginLogo">
						<img src="templates/images/logo_letter.png" style="width:90px;height:53px;" border="0" alt="" />
					</div>
					
					<div id="loginForm">
						<?php echo TemplateLang(array('p' => 'username'), $this);?>
:<br />
						<input id="username" type="text" name="username" value="" style="width:200px;" />
						<br /><br />
						
						<?php echo TemplateLang(array('p' => 'password'), $this);?>
:<br />
						<input id="pw" type="password" name="password" value="" style="width:200px;" />
						<br /><br />
						
						<div style="float:right;">
						<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'login'), $this);?>
 &raquo; " />
						</div>
					</div>
					
					<br class="clear" />
				</div>
			</div>
		</div>
	</form>
	
	<script language="javascript">
	<!--
		EBID('timezone').value = (new Date()).getTimezoneOffset() * (-60);
	//-->
	</script>

</body>

</html>