<?php /* Smarty version 2.6.28, created on 2020-09-30 13:00:52
         compiled from maintenance.inactive.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'maintenance.inactive.tpl', 2, false),array('function', 'text', 'maintenance.inactive.tpl', 25, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'inactiveusers'), $this);?>
</legend>
	
	<form action="maintenance.php?do=exec&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
		<table>
			<tr>
				<td width="40" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/user_inactive32.png" border="0" alt="" width="32" height="32" /></td>
				<td valign="top">
					<p>
						<?php echo TemplateLang(array('p' => 'activity_desc1'), $this);?>

					</p>
					
					<blockquote>
						<table>
							<tr>
								<td width="20" valign="top"><input type="checkbox" id="queryTypeLogin" name="queryTypeLogin" checked="checked" /></td>
								<td><label for="queryTypeLogin"><b><?php echo TemplateLang(array('p' => 'notloggedinsince'), $this);?>
</b></label><br />
									<input type="text" size="6" name="loginDays" value="90" /> <?php echo TemplateLang(array('p' => 'days'), $this);?>
<br /><br /></td>
							</tr>
							<tr>
								<td width="20" valign="top"><input type="checkbox" id="queryTypeGroups" name="queryTypeGroups" checked="checked" /></td>
								<td><label for="queryTypeGroups"><b><?php echo TemplateLang(array('p' => 'whobelongtogrps'), $this);?>
</b></label><br />
									<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['groupID'] => $this->_tpl_vars['group']):
?>
										<input type="checkbox" name="groups[<?php echo $this->_tpl_vars['groupID']; ?>
]" id="group_<?php echo $this->_tpl_vars['groupID']; ?>
" checked="checked" />
											<label for="group_<?php echo $this->_tpl_vars['groupID']; ?>
"><b><?php echo TemplateText(array('value' => $this->_tpl_vars['group']['title']), $this);?>
</b></label><br />
									<?php endforeach; endif; unset($_from); ?></td>
							</tr>
						</table>
					</blockquote>
					
					<p>
						<?php echo TemplateLang(array('p' => 'activity_desc2'), $this);?>

					</p>
					
					<blockquote>
						<table>
							<tr>
								<td width="20" valign="top"><input type="radio" id="queryActionShow" name="queryAction" value="show" checked="checked" /></td>
								<td><label for="queryActionShow"><b><?php echo TemplateLang(array('p' => 'showlist'), $this);?>
</b></label></td>
							</tr>
							<tr>
								<td width="20" valign="top"><input type="radio" id="queryActionLock" name="queryAction" value="lock" /></td>
								<td><label for="queryActionLock"><b><?php echo TemplateLang(array('p' => 'lock'), $this);?>
</b></label></td>
							</tr>
							<tr>
								<td width="20" valign="top"><input type="radio" id="queryActionMove" name="queryAction" value="move" /></td>
								<td><label for="queryActionMove"><b><?php echo TemplateLang(array('p' => 'movetogroup'), $this);?>
:</b></label><br />
									<select name="moveGroup">
									<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['groupItem']):
?>
										<option value="<?php echo $this->_tpl_vars['groupItem']['id']; ?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['groupItem']['title']), $this);?>
</option>
									<?php endforeach; endif; unset($_from); ?>
									</select></td>
							</tr>
							<tr>
								<td width="20" valign="top"><input type="radio" id="queryActionDelete" name="queryAction" value="delete" /></td>
								<td><label for="queryActionDelete"><b><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</b></label></td>
							</tr>
						</table>
					</blockquote>
				</td>
			</tr>
		</table>
		
		<p>
			<div style="float:left;">
				<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/warning.png" border="0" alt="" width="16" height="16" align="absmiddle" />
				<?php echo TemplateLang(array('p' => 'undowarn'), $this);?>

			</div>
			<div style="float:right;">
				<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'execute'), $this);?>
 " />
			</div>
		</p>
	</form>
</fieldset>