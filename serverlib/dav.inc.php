<?php
/*
 * b1gMail
 * (c) 2002-2016 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 */

if(!defined('B1GMAIL_INIT'))
	die('Directly calling this file is not supported');

if(PHPNumVersion() < 540)
{
	DisplayError(0x18, 'PHP version too old', 'b1gMail DAV features require PHP 5.4 or newer.',
		sprintf("Installed PHP version:\n%s", phpversion()),
		__FILE__,
		__LINE__);
	exit();
}

include(B1GMAIL_DIR . 'serverlib/dav-real.inc.php');
