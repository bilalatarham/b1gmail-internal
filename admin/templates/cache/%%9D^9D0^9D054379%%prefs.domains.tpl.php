<?php /* Smarty version 2.6.28, created on 2020-09-29 11:35:56
         compiled from prefs.domains.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.domains.tpl', 2, false),array('function', 'cycle', 'prefs.domains.tpl', 18, false),array('function', 'domain', 'prefs.domains.tpl', 22, false),array('function', 'text', 'prefs.domains.tpl', 26, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'domains'), $this);?>
</legend>
	
	<form action="prefs.common.php?action=domains&sid=<?php echo $this->_tpl_vars['sid']; ?>
" name="f1" method="post" onsubmit="spin(this)">
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="25" style="text-align:center;"><a href="javascript:invertSelection2(document.forms.f1,'domains[','[del]');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/dot.png" border="0" alt="" width="10" height="8" /></a></th>
			<th><?php echo TemplateLang(array('p' => 'domain'), $this);?>
</th>
			<th style="text-align:center;" width="100"><?php echo TemplateLang(array('p' => 'login'), $this);?>
</th>
			<th style="text-align:center;" width="100"><?php echo TemplateLang(array('p' => 'signup'), $this);?>
</th>
			<th style="text-align:center;" width="100"><?php echo TemplateLang(array('p' => 'aliases'), $this);?>
</th>
			<th width="80"><?php echo TemplateLang(array('p' => 'pos'), $this);?>
</th>
			<th width="35">&nbsp;</th>
		</tr>
		
		<?php $_from = $this->_tpl_vars['domains']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domain']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/domain.png" border="0" alt="" width="16" height="16" /></td>
			<td><input type="checkbox" name="domains[<?php echo $this->_tpl_vars['domain']['domain']; ?>
][del]" /></td>
			<td><?php echo TemplateDomain(array('value' => $this->_tpl_vars['domain']['domain']), $this);?>
</td>
			<td style="text-align:center;"><input type="checkbox" name="domains[<?php echo $this->_tpl_vars['domain']['domain']; ?>
][in_login]"<?php if ($this->_tpl_vars['domain']['in_login']): ?> checked="checked"<?php endif; ?> /></td>
			<td style="text-align:center;"><input type="checkbox" name="domains[<?php echo $this->_tpl_vars['domain']['domain']; ?>
][in_signup]"<?php if ($this->_tpl_vars['domain']['in_signup']): ?> checked="checked"<?php endif; ?> /></td>
			<td style="text-align:center;"><input type="checkbox" name="domains[<?php echo $this->_tpl_vars['domain']['domain']; ?>
][in_aliases]"<?php if ($this->_tpl_vars['domain']['in_aliases']): ?> checked="checked"<?php endif; ?> /></td>
			<td><input type="text" name="domains[<?php echo $this->_tpl_vars['domain']['domain']; ?>
][pos]" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['domain']['pos'],'allowEmpty' => true), $this);?>
" size="6" /></td>
			<td>
				<a href="prefs.common.php?action=domains&delete=<?php echo $this->_tpl_vars['domain']['urlDomain']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onclick="return confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/delete.png" border="0" alt="<?php echo TemplateLang(array('p' => 'edit'), $this);?>
" width="16" height="16" /></a>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
		
		<tr>
			<td class="footer" colspan="8">
				<div style="float:left;">
					<?php echo TemplateLang(array('p' => 'action'), $this);?>
: <select name="massAction" class="smallInput">
						<option value="-">------------</option>
						
						<optgroup label="<?php echo TemplateLang(array('p' => 'actions'), $this);?>
">
							<option value="delete"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option>
						</optgroup>
					</select>&nbsp;
				</div>
				<div style="float:left;">
					<input type="submit" name="executeMassAction" value=" <?php echo TemplateLang(array('p' => 'execute'), $this);?>
 " class="smallInput" />
				</div>
				<div style="float:right;">
					<input type="submit" name="save" class="button" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />&nbsp; 
				</div>
			</td>
		</tr>
	</table>
	</form>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'adddomain'), $this);?>
</legend>
	
	<form action="prefs.common.php?action=domains&add=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
		<table width="100%">
			<tr>
				<td width="40" valign="top" rowspan="3"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/domain32.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="150"><?php echo TemplateLang(array('p' => 'domain'), $this);?>
:</td>
				<td class="td2"><input type="text" style="width:85%;" name="domain" value="" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'show_at'), $this);?>
:</td>
				<td class="td2"><input type="checkbox" name="in_login" id="in_login" checked="checked" />
								<label for="in_login"><?php echo TemplateLang(array('p' => 'login'), $this);?>
</label>
								<br />
								<input type="checkbox" name="in_signup" id="in_signup" checked="checked" />
								<label for="in_signup"><?php echo TemplateLang(array('p' => 'signup'), $this);?>
</label>
								<br />
								<input type="checkbox" name="in_aliases" id="in_aliases" checked="checked" />
								<label for="in_aliases"><?php echo TemplateLang(array('p' => 'aliases'), $this);?>
</label>
								</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'pos'), $this);?>
:</td>
				<td class="td2"><input type="text" name="pos" value="0" size="6" /></td>
			</tr>
		</table>
	
		<p align="right">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'add'), $this);?>
 " />
		</p>
	</form>
</fieldset>