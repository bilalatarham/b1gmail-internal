<?php /* Smarty version 2.6.28, created on 2020-09-29 12:10:38
         compiled from prefs.email.send.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.email.send.tpl', 3, false),array('function', 'text', 'prefs.email.send.tpl', 25, false),array('function', 'email', 'prefs.email.send.tpl', 65, false),)), $this); ?>
<form action="prefs.email.php?action=send&save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'sendmethod'), $this);?>
</legend>

		<table>
			<tr>
				<td width="40" valign="top" rowspan="1"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_sending.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'sendmethod'), $this);?>
:</td>
				<td class="td2"><select name="send_method">
					<option value="smtp"<?php if ($this->_tpl_vars['bm_prefs']['send_method'] == 'smtp'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'smtp'), $this);?>
</option>
					<option value="php"<?php if ($this->_tpl_vars['bm_prefs']['send_method'] == 'php'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'phpmail'), $this);?>
</option>
					<option value="sendmail"<?php if ($this->_tpl_vars['bm_prefs']['send_method'] == 'sendmail'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'sendmail2'), $this);?>
</option>
				</select></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'smtp'), $this);?>
</legend>

		<table>
			<tr>
				<td width="40" valign="top" rowspan="5"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_login.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'smtphost'), $this);?>
:</td>
				<td class="td2"><input type="text" name="smtp_host" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['smtp_host']), $this);?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'smtpport'), $this);?>
:</td>
				<td class="td2"><input type="text" name="smtp_port" value="<?php echo $this->_tpl_vars['bm_prefs']['smtp_port']; ?>
" size="6" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'smtpauth'), $this);?>
?</td>
				<td class="td2"><input name="smtp_auth"<?php if ($this->_tpl_vars['bm_prefs']['smtp_auth'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'smtpuser'), $this);?>
:</td>
				<td class="td2"><input type="text" name="smtp_user" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['smtp_user']), $this);?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'smtppass'), $this);?>
:</td>
				<td class="td2"><input type="password" autocomplete="off" name="smtp_pass" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['smtp_pass']), $this);?>
" size="36" /></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'sendmail2'), $this);?>
</legend>

		<table>
			<tr>
				<td width="40" valign="top" rowspan="1"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_cmd.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'sendmailpath'), $this);?>
:</td>
				<td class="td2"><input type="text" name="sendmail_path" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['sendmail_path']), $this);?>
" size="36" /></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'miscprefs'), $this);?>
</legend>

		<table width="90%">
			<tr>
				<td width="40" valign="top" rowspan="5"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_misc.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'sysmailsender'), $this);?>
:</td>
				<td class="td2"><input type="text" name="passmail_abs" value="<?php echo TemplateEMail(array('value' => $this->_tpl_vars['bm_prefs']['passmail_abs']), $this);?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'blockedrecps'), $this);?>
:</td>
				<td class="td2">
					<textarea style="width:100%;height:80px;" name="blocked"><?php echo TemplateText(array('value' => $this->_tpl_vars['bm_prefs']['blocked'],'allowEmpty' => true), $this);?>
</textarea>
					<small><?php echo TemplateLang(array('p' => 'altmailsepby'), $this);?>
</small>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'certmaillife'), $this);?>
:</td>
				<td class="td2"><input type="text" name="einsch_life" value="<?php echo $this->_tpl_vars['bm_prefs']['einsch_life']/86400; ?>
" size="4" />
								<?php echo TemplateLang(array('p' => 'days'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'min_draft_save'), $this);?>
:</td>
				<td class="td2"><input type="text" name="min_draft_save_interval" value="<?php echo $this->_tpl_vars['bm_prefs']['min_draft_save_interval']; ?>
" size="4" />
								<?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'write_xsenderip'), $this);?>
?</td>
				<td class="td2"><input name="write_xsenderip"<?php if ($this->_tpl_vars['bm_prefs']['write_xsenderip'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
		</table>
	</fieldset>

	<p>
		<div style="float:right;" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />&nbsp;
		</div>
	</p>
</form>