<?php /* Smarty version 2.6.28, created on 2020-09-29 11:27:21
         compiled from welcome.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'welcome.tpl', 7, false),array('function', 'text', 'welcome.tpl', 34, false),array('function', 'size', 'welcome.tpl', 80, false),)), $this); ?>
<fieldset>
	<legend>b1gMail</legend>
	
	<table width="100%">
		<tr>
			<td rowspan="2" width="40" align="center" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_b1gmail.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td1" width="24%"><?php echo TemplateLang(array('p' => 'version'), $this);?>
:</td>
			<td class="td2" width="26%"><?php echo $this->_tpl_vars['version']; ?>
<?php if ($this->_tpl_vars['patchlevel'] > 0): ?> (PL <?php echo $this->_tpl_vars['patchlevel']; ?>
)<?php endif; ?></td>
			
			<td rowspan="2" width="40" align="center" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_license.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td1" width="24%"><?php echo TemplateLang(array('p' => 'licensedto'), $this);?>
:</td>
			<td class="td2" width="26%"><?php echo $this->_tpl_vars['licenseDomain']; ?>
</td>
		</tr>
		<?php if ($this->_tpl_vars['adminRow']['type'] == 0): ?><tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'licenseno'), $this);?>
:</td>
			<td class="td2"><?php echo $this->_tpl_vars['licenseNo']; ?>
</td>
			
			<td class="td1"><?php echo TemplateLang(array('p' => 'serial'), $this);?>
:</td>
			<td class="td2"><?php echo $this->_tpl_vars['licenseSerial']; ?>
</td>
		</tr><?php endif; ?>
	</table>
</fieldset>
<?php if ($this->_tpl_vars['copyrightError']): ?>
<fieldset style="background-color:#FFB2B2;border:1px solid #FF0000;">
	<legend><?php echo TemplateLang(array('p' => 'copyrightnote'), $this);?>
</legend>
	
	<table width="100%">
		<tr>
			<td width="40" align="center" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/warning32.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td2">
				<p><?php echo TemplateLang(array('p' => 'c_err_1'), $this);?>
</p>
				<p><?php echo TemplateLang(array('p' => 'c_err_2'), $this);?>
</p>
				<blockquote>
					<code><?php echo TemplateText(array('value' => $this->_tpl_vars['copyrightCode']), $this);?>
</code>
				</blockquote>
				<p><?php echo TemplateLang(array('p' => 'c_err_3'), $this);?>
</p>
			</td>
		</tr>
	</table>
</fieldset>
<?php endif; ?>

<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['overview']): ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'overview'), $this);?>
</legend>
	
	<table width="100%">
		<!-- user stuff -->
		<tr>
			<td rowspan="3" width="40" align="center" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_users.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td1" width="24%"><a href="users.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateLang(array('p' => 'users'), $this);?>
</a>:</td>
			<td class="td2" width="26%"><?php echo $this->_tpl_vars['userCount']; ?>
</td>
			
			<td rowspan="3" width="40" align="center" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_system.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td1" width="24%"><?php echo TemplateLang(array('p' => 'phpversion'), $this);?>
:</td>
			<td class="td2" width="26%"><?php echo $this->_tpl_vars['phpVersion']; ?>
</td>
		</tr>
		<tr>
			<td class="td1"><a href="users.php?filter=true&statusNotActivated=true&allGroups=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateLang(array('p' => 'notactivated'), $this);?>
</a>:</td>
			<td class="td2"><?php echo $this->_tpl_vars['notActivatedUserCount']; ?>
</td>
			
			<td class="td1"><?php echo TemplateLang(array('p' => 'webserver'), $this);?>
:</td>
			<td class="td2"><?php echo $this->_tpl_vars['webserver']; ?>
</td>
		</tr>
		<tr>
			<td class="td1"><a href="users.php?filter=true&statusLocked=true&allGroups=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateLang(array('p' => 'locked'), $this);?>
</a>:</td>
			<td class="td2"><?php echo $this->_tpl_vars['lockedUserCount']; ?>
</td>
			
			<td class="td1"><?php echo TemplateLang(array('p' => 'load'), $this);?>
:</td>
			<td class="td2"><?php echo $this->_tpl_vars['loadAvg']; ?>
</td>
		</tr>
		
		<!-- mail stuff -->
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>
		<tr>
			<td rowspan="3" width="40" align="center" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_email.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td1"><?php echo TemplateLang(array('p' => 'emailsize'), $this);?>
:</td>
			<td class="td2"><?php if ($this->_tpl_vars['emailSize'] !== false): ?><?php echo TemplateSize(array('bytes' => $this->_tpl_vars['emailSize']), $this);?>
<?php else: ?>-<?php endif; ?></td>
			
			<td rowspan="3" width="40" align="center" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_data.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td1"><?php echo TemplateLang(array('p' => 'mysqlversion'), $this);?>
:</td>
			<td class="td2"><?php echo $this->_tpl_vars['mysqlVersion']; ?>
</td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'emails'), $this);?>
:</td>
			<td class="td2"><?php echo $this->_tpl_vars['emailCount']; ?>
</td>
			
			<td class="td1"><?php echo TemplateLang(array('p' => 'tables'), $this);?>
:</td>
			<td class="td2"><?php echo $this->_tpl_vars['tableCount']; ?>
</td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'folders'), $this);?>
:</td>
			<td class="td2"><?php echo $this->_tpl_vars['folderCount']; ?>
</td>
			
			<td class="td1"><?php echo TemplateLang(array('p' => 'dbsize'), $this);?>
:</td>
			<td class="td2"><?php echo TemplateSize(array('bytes' => $this->_tpl_vars['dbSize']), $this);?>
</td>
		</tr>
		
		<!-- webdisk stuff -->
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>
		<tr>
			<td rowspan="3" width="40" align="center" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_disk.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td1"><?php echo TemplateLang(array('p' => 'disksize'), $this);?>
:</td>
			<td class="td2" colspan="3"><?php if ($this->_tpl_vars['diskSize'] !== false): ?><?php echo TemplateSize(array('bytes' => $this->_tpl_vars['diskSize']), $this);?>
<?php else: ?>-<?php endif; ?></td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'files'), $this);?>
:</td>
			<td class="td2" colspan="3"><?php echo $this->_tpl_vars['diskFileCount']; ?>
</td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'folders'), $this);?>
:</td>
			<td class="td2" colspan="3"><?php echo $this->_tpl_vars['diskFolderCount']; ?>
</td>
		</tr>
	</table>
</fieldset>
<?php endif; ?>

<?php if ($this->_tpl_vars['showActivation']): ?>
<table width="100%" style="border-collapse:collapse;">
	<tr>
		<td style="width:50%;vertical-align:top;">
			<?php endif; ?>
			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'notes'), $this);?>
</legend>
				<form action="welcome.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&do=saveNotes" method="post" onsubmit="spin(this)">
					<textarea style="width:100%;height:94px;" name="notes"><?php echo TemplateText(array('value' => $this->_tpl_vars['notes'],'allowEmpty' => true), $this);?>
</textarea>
					<p align="right"><input type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " class="button" /></p>
				</form>
			</fieldset>
			<?php if ($this->_tpl_vars['showActivation']): ?>
		</td>
		<td style="width:50%;vertical-align:top;">
			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'activatepayment'), $this);?>
</legend>
				
				<table>
					<tr>
						<td align="left" rowspan="3" valign="top" width="40"><img src="templates/images/ico_prefs_payments.png" border="0" alt="" width="32" height="32" /></td>
						<td colspan="2"><?php echo TemplateLang(array('p' => 'activate_desc'), $this);?>
</td>
					</tr>
					<tr>
						<td class="td1" width="120"><?php echo TemplateLang(array('p' => 'vkcode'), $this);?>
:</td>
						<td class="td2"><input type="text" name="vkCode" id="vkCode" value="VK-" size="26" onkeypress="return handleActivatePaymentInput(event, 0);"  /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'amount'), $this);?>
:</td>
						<td class="td2"><input type="text" name="amount" id="amount" value="" size="10" onkeypress="return handleActivatePaymentInput(event, 1);" /> <?php echo TemplateText(array('value' => $this->_tpl_vars['bm_prefs']['currency']), $this);?>
</td>
					</tr>
				</table>
				
				<p>
					<div style="float:left;font-weight:bold;padding-top:4px;" id="activationResult">&nbsp;</div>
					<div style="float:right">
						<input class="button" type="button" onclick="activatePayment()" id="activateButton" value=" <?php echo TemplateLang(array('p' => 'activate'), $this);?>
 " />
					</div>
				</p>
			</fieldset>
		</td>
	</tr>
</table>
<?php endif; ?>

<?php if ($this->_tpl_vars['adminRow']['type'] == 0): ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'notices'), $this);?>
</legend>
	
	<table width="100%" id="noticeTable">
	<?php $_from = $this->_tpl_vars['notices']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['notice']):
?>
		<tr>
			<td width="20" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php echo $this->_tpl_vars['notice']['type']; ?>
.png" width="16" height="16" border="0" alt="" align="absmiddle" /></td>
			<td valign="top"><?php echo $this->_tpl_vars['notice']['text']; ?>
</td>
			<td align="right" valign="top" width="20"><?php if ($this->_tpl_vars['notice']['link']): ?><a href="<?php echo $this->_tpl_vars['notice']['link']; ?>
sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/go.png" border="0" alt="" width="16" height="16" /></a><?php else: ?>&nbsp;<?php endif; ?></td>
		</tr>
	<?php endforeach; endif; unset($_from); ?>
	</table>
</fieldset>

<script language="javascript">
<!--
	registerLoadAction('checkForUpdates()');
//-->
</script>
<script language="javascript" src="https://ssl.b1g.de/service.b1gmail.com/updates/?do=noticeJS&version=<?php echo $this->_tpl_vars['version']; ?>
&key=<?php echo $this->_tpl_vars['serial']; ?>
&lic=<?php echo $this->_tpl_vars['licnr']; ?>
&lang=<?php echo $this->_tpl_vars['lang']; ?>
"></script>
<?php endif; ?>