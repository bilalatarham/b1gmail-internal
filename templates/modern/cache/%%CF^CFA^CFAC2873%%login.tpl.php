<?php /* Smarty version 2.6.28, created on 2020-09-30 09:57:18
         compiled from nli/login.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'nli/login.tpl', 5, false),array('function', 'domain', 'nli/login.tpl', 24, false),)), $this); ?>
<div class="jumbotron splash"	style="background-image: url(<?php echo $this->_tpl_vars['tpldir']; ?>
images/nli/<?php echo $this->_tpl_vars['templatePrefs']['splashImage']; ?>
);">
	<div class="container">
		<div class="panel panel-primary login">
			<div class="panel-heading">
				<?php echo TemplateLang(array('p' => 'welcome'), $this);?>

				<div class="lost-pw">
					<a href="#" data-toggle="modal" data-target="#lostPW"><?php echo TemplateLang(array('p' => 'lostpw'), $this);?>
?</a>
				</div>
			</div>
			<div class="panel-body">
				<form action="<?php if ($this->_tpl_vars['ssl_login_enable']): ?><?php echo $this->_tpl_vars['ssl_url']; ?>
<?php endif; ?>index.php?action=login" method="post" id="loginFormMain">
					<input type="hidden" name="do" value="login" />
					<input type="hidden" name="timezone" value="<?php echo $this->_tpl_vars['timezone']; ?>
" />

					<div class="alert alert-danger" style="display:none;"></div>

					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<?php if ($this->_tpl_vars['domain_combobox']): ?>
							<label class="sr-only" for="email_local"><?php echo TemplateLang(array('p' => 'email'), $this);?>
</label>
							<input type="text" name="email_local" id="email_local" class="form-control" placeholder="<?php echo TemplateLang(array('p' => 'email'), $this);?>
" required="true" />
							<div class="input-group-btn">
								<input type="hidden" name="email_domain" data-bind="email-domain" value="<?php echo TemplateDomain(array('value' => $this->_tpl_vars['domainList'][0]), $this);?>
" />
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span data-bind="label">@<?php echo TemplateDomain(array('value' => $this->_tpl_vars['domainList'][0]), $this);?>
</span> <span class="caret"></span></button>
								<ul class="dropdown-menu dropdown-menu-right domainMenu" role="menu">
									<?php $_from = $this->_tpl_vars['domainList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['domain']):
?><li<?php if ($this->_tpl_vars['key'] == 0): ?> class="active"<?php endif; ?>><a href="#">@<?php echo TemplateDomain(array('value' => $this->_tpl_vars['domain']), $this);?>
</a></li><?php endforeach; endif; unset($_from); ?>
								</ul>
							</div>
							<?php else: ?>
							<label class="sr-only" for="email_full"><?php echo TemplateLang(array('p' => 'email'), $this);?>
</label>
							<input type="email" name="email_full" id="email_full" class="form-control" placeholder="<?php echo TemplateLang(array('p' => 'email'), $this);?>
" required="true" />
							<?php endif; ?>
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<label class="sr-only" for="password"><?php echo TemplateLang(array('p' => 'password'), $this);?>
</label>
							<input type="password" name="password" id="password" class="form-control" placeholder="<?php echo TemplateLang(array('p' => 'password'), $this);?>
" required="true" />
						</div>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="savelogin" id="savelogin" />
							<?php echo TemplateLang(array('p' => 'savelogin'), $this);?>

						</label>
					</div>
					<?php if ($this->_tpl_vars['ssl_login_option']): ?><div class="checkbox">
						<label>
							<input type="checkbox" id="ssl"<?php if ($this->_tpl_vars['ssl_login_enable']): ?> checked="checked"<?php endif; ?> onchange="updateFormSSL(this)" onclick="updateFormSSL(this)" />
							<?php echo TemplateLang(array('p' => 'ssl'), $this);?>

						</label>
					</div><?php endif; ?>
					<div class="form-group">
						<button type="submit" class="btn btn-success"><?php echo TemplateLang(array('p' => 'login'), $this);?>
</button>
					</div>
				</form>
			</div>
			<?php if ($this->_tpl_vars['_regEnabled'] || ( ! $this->_tpl_vars['templatePrefs']['hideSignup'] )): ?><div class="panel-footer">
				<?php echo TemplateLang(array('p' => 'notmember'), $this);?>
?
				<a href="<?php if ($this->_tpl_vars['ssl_signup_enable']): ?><?php echo $this->_tpl_vars['ssl_url']; ?>
<?php endif; ?>index.php?action=signup"><?php echo TemplateLang(array('p' => 'signup'), $this);?>
</a>
			</div><?php endif; ?>
		</div>
		<div class="bottom">
			<footer class="row">
				<div class="col-xs-4">
					&copy; <?php echo $this->_tpl_vars['year']; ?>
 <?php echo $this->_tpl_vars['service_title']; ?>

				</div>
				<div class="col-xs-4" style="text-align:center;">
					<a href="<?php echo $this->_tpl_vars['mobileURL']; ?>
"><?php echo TemplateLang(array('p' => 'mobilepda'), $this);?>
</a>
				<?php $_from = $this->_tpl_vars['pluginUserPages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?><?php if (! $this->_tpl_vars['item']['top']): ?>
				|	<a href="<?php echo $this->_tpl_vars['item']['link']; ?>
"><?php echo $this->_tpl_vars['item']['text']; ?>
</a>
				<?php endif; ?><?php endforeach; endif; unset($_from); ?>
				</div>
				<div class="col-xs-4" style="text-align:right;">
					<?php echo '&nbsp;'; ?>

				</div>
			</footer>
		</div>
	</div>
</div>