<?php /* Smarty version 2.6.28, created on 2020-09-29 11:44:17
         compiled from welcome.phpinfo.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'welcome.phpinfo.tpl', 2, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'phpinfo'), $this);?>
</legend>

	<iframe src="welcome.php?action=phpinfo&do=phpinfo&sid=<?php echo $this->_tpl_vars['sid']; ?>
" style="width:100%;height:440px;border:1px inset #CCC;"></iframe>
	<p align="right">
		<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_download.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		<a href="welcome.php?action=phpinfo&do=phpinfo&download=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" target="_blank"><?php echo TemplateLang(array('p' => 'download'), $this);?>
</a>
	</p>
</fieldset>