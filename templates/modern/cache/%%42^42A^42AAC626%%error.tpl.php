<?php /* Smarty version 2.6.28, created on 2020-09-30 10:12:24
         compiled from li/error.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'li/error.tpl', 6, false),array('function', 'text', 'li/error.tpl', 10, false),)), $this); ?>
<br />
<table>
	<tr>
		<td valign="top" width="64" align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/error.png" width="48" height="48" border="0" alt="" /></td>
		<td valign="top">
			<b><?php echo TemplateLang(array('p' => 'error'), $this);?>
</b>
			<br /><?php echo $this->_tpl_vars['msg']; ?>

			<br /><br />
			<input type="button" value="&laquo; <?php echo TemplateLang(array('p' => 'back'), $this);?>
" onclick="<?php if (! $this->_tpl_vars['backLink']): ?>history.back()<?php else: ?>document.location.href='<?php echo $this->_tpl_vars['backLink']; ?>
'<?php endif; ?>;" />
			<?php if ($this->_tpl_vars['otherButton']): ?><input type="button" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['otherButton']['caption']), $this);?>
" onclick="document.location.href='<?php echo $this->_tpl_vars['otherButton']['href']; ?>
';" /><?php endif; ?>
		</td>
	</tr>
</table>