<?php /* Smarty version 2.6.28, created on 2020-09-29 11:27:03
         compiled from /var/www/html/plugins/templates/widget.email.tpl */ ?>
<div class="innerWidget">
<table cellspacing="0" width="100%" style="table-layout:fixed;">
	<?php $_from = $this->_tpl_vars['bmwidget_email_items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['folderID'] => $this->_tpl_vars['folder']):
?>
<tr>
	<td width="20" align="center"><img width="16" height="16" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_<?php echo $this->_tpl_vars['folder']['icon']; ?>
.png" border="0" alt="" align="absmiddle" /></td>
	<td style="text-overflow:ellipsis;overflow:hidden;"><a href="email.php?folder=<?php echo $this->_tpl_vars['folderID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo $this->_tpl_vars['folder']['text']; ?>
</a></td>
	<td align="left" width="50"><i class="fa fa-envelope-o"></i> <?php echo $this->_tpl_vars['folder']['allMails']; ?>
</td>
	<td align="left" width="45"><i class="fa fa-flag-o"></i> <?php if ($this->_tpl_vars['folder']['flaggedMails'] > 0): ?><b><?php echo $this->_tpl_vars['folder']['flaggedMails']; ?>
</b><?php else: ?>-<?php endif; ?></td>
	<td align="left" width="45"><i class="fa fa-envelope"></i> <?php if ($this->_tpl_vars['folder']['unreadMails'] > 0): ?><b><?php echo $this->_tpl_vars['folder']['unreadMails']; ?>
</b><?php else: ?>-<?php endif; ?></td>
</tr>
	<?php endforeach; endif; unset($_from); ?>
</table>
</div>