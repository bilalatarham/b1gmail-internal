<?php /* Smarty version 2.6.28, created on 2020-09-29 11:34:55
         compiled from /var/www/html/plugins/templates/bms.admin.smtp.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', '/var/www/html/plugins/templates/bms.admin.smtp.tpl', 3, false),array('function', 'text', '/var/www/html/plugins/templates/bms.admin.smtp.tpl', 9, false),)), $this); ?>
<form action="<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=smtp&save=true" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'common'), $this);?>
</legend>

		<table width="100%">
			<tr>
				<td align="left" rowspan="8" valign="top" width="40"><img src="../plugins/templates/images/bms_common.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_greeting'), $this);?>
:</td>
				<td class="td2"><input type="text" name="smtpgreeting" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['smtpgreeting'],'allowEmpty' => true), $this);?>
" size="32" style="width:95%;" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_timeout'), $this);?>
:</td>
				<td class="td2"><input type="text" name="smtp_timeout" value="<?php echo $this->_tpl_vars['bms_prefs']['smtp_timeout']; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'mailmax'), $this);?>
:</td>
				<td class="td2"><input type="text" name="smtp_size_limit" value="<?php echo $this->_tpl_vars['bms_prefs']['smtp_size_limit']/1024; ?>
" size="6" />
								KB</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_recipient_limit'), $this);?>
:</td>
				<td class="td2"><input type="text" name="smtp_recipient_limit" value="<?php echo $this->_tpl_vars['bms_prefs']['smtp_recipient_limit']; ?>
" size="6" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_hop_limit'), $this);?>
:</td>
				<td class="td2"><input type="text" name="smtp_hop_limit" value="<?php echo $this->_tpl_vars['bms_prefs']['smtp_hop_limit']; ?>
" size="6" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_smtp_auth'), $this);?>
?</td>
				<td class="td2"><input type="checkbox" name="smtp_auth_enabled"<?php if ($this->_tpl_vars['bms_prefs']['smtp_auth_enabled'] == 1): ?> checked="checked"<?php endif; ?> id="smtp_auth_enabled" />
					<label for="smtp_auth_enabled"><b><?php echo TemplateLang(array('p' => 'activate'), $this);?>
</b></label><br />
					<input type="checkbox" name="smtp_auth_no_received"<?php if ($this->_tpl_vars['bms_prefs']['smtp_auth_no_received'] == 1): ?> checked="checked"<?php endif; ?> id="smtp_auth_no_received" />
					<label for="smtp_auth_no_received"><b><?php echo TemplateLang(array('p' => 'bms_auth_no_received'), $this);?>
</b></label></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_reversedns'), $this);?>
?</td>
				<td class="td2">
					<input type="checkbox" name="smtp_reversedns"<?php if ($this->_tpl_vars['bms_prefs']['smtp_reversedns'] == 1): ?> checked="checked"<?php endif; ?> id="smtp_reversedns" />
					<label for="smtp_reversedns"><b><?php echo TemplateLang(array('p' => 'activate'), $this);?>
</b></label><br />
					<input type="checkbox" name="smtp_reject_noreversedns"<?php if ($this->_tpl_vars['bms_prefs']['smtp_reject_noreversedns'] == 1): ?> checked="checked"<?php endif; ?> id="smtp_reject_noreversedns" />
					<label for="smtp_reject_noreversedns"><b><?php echo TemplateLang(array('p' => 'bms_reject_norevdns'), $this);?>
</b></label>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_helo_check'), $this);?>
:</td>
				<td class="td2">
					<select name="smtp_check_helo">
						<option value="0"<?php if ($this->_tpl_vars['bms_prefs']['smtp_check_helo'] == 0): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_helo_disabled'), $this);?>
</option>
						<option value="1"<?php if ($this->_tpl_vars['bms_prefs']['smtp_check_helo'] == 1): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_helo_exact'), $this);?>
</option>
						<option value="2"<?php if ($this->_tpl_vars['bms_prefs']['smtp_check_helo'] == 2): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_helo_fuzzy'), $this);?>
</option>
					</select>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_peer_classification'), $this);?>
</legend>

		<table width="100%">
			<tr>
				<td align="left" rowspan="2" valign="top" width="40"><img src="../plugins/templates/images/bms_classification.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_subnet_rules'), $this);?>
:</td>
				<td class="td2"><?php echo $this->_tpl_vars['subnetCount']; ?>
 <?php echo TemplateLang(array('p' => 'entries'), $this);?>
 <input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'edit'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=smtp&do=subnetRules';" /></td>
			</tr>
			<tr>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_dnsbl_rules'), $this);?>
:</td>
				<td class="td2"><?php echo $this->_tpl_vars['dnsblCount']; ?>
 <?php echo TemplateLang(array('p' => 'entries'), $this);?>
 <input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'edit'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=smtp&do=dnsblRules';" /></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_untrusted_limits'), $this);?>
</legend>

		<table width="100%">
			<tr>
				<td align="left" rowspan="4" valign="top" width="40"><img src="../plugins/templates/images/bms_untrusted.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_greetingdelay'), $this);?>
:</td>
				<td class="td2"><input type="text" name="smtp_greeting_delay" value="<?php echo $this->_tpl_vars['bms_prefs']['smtp_greeting_delay']; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_error_delay'), $this);?>
:</td>
				<td class="td2"><input type="text" name="smtp_error_delay" value="<?php echo $this->_tpl_vars['bms_prefs']['smtp_error_delay']; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_error_softlimit'), $this);?>
:</td>
				<td class="td2"><input type="text" name="smtp_error_softlimit" value="<?php echo $this->_tpl_vars['bms_prefs']['smtp_error_softlimit']; ?>
" size="6" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_error_hardlimit'), $this);?>
:</td>
				<td class="td2"><input type="text" name="smtp_error_hardlimit" value="<?php echo $this->_tpl_vars['bms_prefs']['smtp_error_hardlimit']; ?>
" size="6" /></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_greylisting'), $this);?>
</legend>

		<table width="100%">
			<tr>
				<td align="left" rowspan="5" valign="top" width="40"><img src="../plugins/templates/images/bms_greylisting.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'activate'), $this);?>
?</td>
				<td class="td2"><input type="checkbox" name="grey_enabled"<?php if ($this->_tpl_vars['bms_prefs']['grey_enabled'] == 1): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_grey_interval'), $this);?>
:</td>
				<td class="td2"><input type="text" name="grey_interval" value="<?php echo $this->_tpl_vars['bms_prefs']['grey_interval']; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_grey_wait_time'), $this);?>
:</td>
				<td class="td2"><input type="text" name="grey_wait_time" value="<?php echo $this->_tpl_vars['bms_prefs']['grey_wait_time']/3600; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'bms_hours'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_grey_good_time'), $this);?>
:</td>
				<td class="td2"><input type="text" name="grey_good_time" value="<?php echo $this->_tpl_vars['bms_prefs']['grey_good_time']/3600; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'bms_hours'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_list'), $this);?>
:</td>
				<td class="td2"><?php echo $this->_tpl_vars['greyCount']; ?>
 <?php echo TemplateLang(array('p' => 'entries'), $this);?>
 <input class="button" type="button"<?php if ($this->_tpl_vars['greyCount'] == 0): ?> disabled="disabled"<?php endif; ?> value=" <?php echo TemplateLang(array('p' => 'show'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=smtp&do=greylist';" /> <input<?php if ($this->_tpl_vars['greyCount'] == 0): ?> disabled="disabled"<?php endif; ?> class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'reset'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&action=smtp&resetGreyList=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
';" /></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_spf'), $this);?>
</legend>

		<table width="100%">
			<tr>
				<td align="left" rowspan="4" valign="top" width="40"><img src="../plugins/templates/images/bms_spf.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'activate'), $this);?>
?</td>
				<td class="td2"><input type="checkbox" name="spf_enable"<?php if ($this->_tpl_vars['bms_prefs']['spf_enable'] == 1): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_spf_injectheader'), $this);?>
?</td>
				<td class="td2"><input type="checkbox" name="spf_inject_header"<?php if ($this->_tpl_vars['bms_prefs']['spf_inject_header'] == 1): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_spf_onpass'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" id="spf_disable_greylisting" name="spf_disable_greylisting"<?php if ($this->_tpl_vars['bms_prefs']['spf_disable_greylisting'] == 1): ?> checked="checked"<?php endif; ?> />
					<label for="spf_disable_greylisting"><strong><?php echo TemplateLang(array('p' => 'bms_spf_disgrey'), $this);?>
</strong></label>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_spf_onfail'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" id="spf_reject_mails" name="spf_reject_mails"<?php if ($this->_tpl_vars['bms_prefs']['spf_reject_mails'] == 1): ?> checked="checked"<?php endif; ?> />
					<label for="spf_reject_mails"><strong><?php echo TemplateLang(array('p' => 'bms_spf_reject'), $this);?>
</strong></label>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_advanced'), $this);?>
</legend>

		<table width="100%">
			<tr>
				<td align="left" rowspan="1" valign="top" width="40"><img src="../plugins/templates/images/bms_common.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_milters'), $this);?>
:</td>
				<td class="td2">
					<input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'edit'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=smtp&do=milters';" />
				</td>
			</tr>
		</table>
	</fieldset>

	<p>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>