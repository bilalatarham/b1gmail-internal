<?php /* Smarty version 2.6.28, created on 2020-09-29 11:37:40
         compiled from li/email.compose.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'li/email.compose.tpl', 6, false),array('function', 'text', 'li/email.compose.tpl', 37, false),array('function', 'email', 'li/email.compose.tpl', 54, false),array('function', 'fileDateSig', 'li/email.compose.tpl', 138, false),array('function', 'hook', 'li/email.compose.tpl', 159, false),)), $this); ?>
<form name="f1" method="post" action="email.compose.php?action=sendMail&sid=<?php echo $this->_tpl_vars['sid']; ?>
" autocomplete="off" onreset="if(!askReset()) return(false);editor.reset();">

<div id="contentHeader">
	<div class="left">
		<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/send_mail.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		<?php echo TemplateLang(array('p' => 'sendmail'), $this);?>

	</div>
	
	<div class="right">
		<select name="newTextMode" id="textMode" onchange="return editor.switchMode(this.value)">
			<option value="text"<?php if (! $this->_tpl_vars['mail'] || $this->_tpl_vars['mail']['textMode'] == 'text'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'plaintext'), $this);?>
</option>
			<option value="html"<?php if ($this->_tpl_vars['mail']['textMode'] == 'html'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'htmltext'), $this);?>
</option>
		</select>
		
		&nbsp;
		
		<i class="fa fa-flag"></i> <select name="priority" id="priority">
			<option value="1"<?php if ($this->_tpl_vars['mail']['priority'] == 1): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'prio_1'), $this);?>
</option>
			<option value="0"<?php if (! $this->_tpl_vars['mail'] || $this->_tpl_vars['mail']['priority'] == 0): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'prio_0'), $this);?>
</option>
			<option value="-1"<?php if ($this->_tpl_vars['mail']['priority'] == -1): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => "prio_-1"), $this);?>
</option>
		</select>
	</div>
</div>

<div class="bigForm withBottomBar" style="overflow-y:auto">
	<input type="hidden" name="actionToken" value="<?php echo $this->_tpl_vars['actionToken']; ?>
" />
	<input type="hidden" name="do" id="do" value="" />
	<input type="hidden" name="reference" id="reference" value="<?php echo $this->_tpl_vars['reference']; ?>
" />
	<input type="hidden" name="baseDraftID" id="baseDraftID" value="<?php if ($this->_tpl_vars['mail']['isAutoSavedDraft']): ?><?php echo $this->_tpl_vars['mail']['baseDraftID']; ?>
<?php endif; ?>" />
	
	<?php if ($this->_tpl_vars['latestDraft']): ?>
	<div class="draftNote" id="draftNote">
		<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_drafts.png" width="16" height="16" />
		<div>
			<?php echo TemplateLang(array('p' => 'drafttext'), $this);?>

			<ul>
				<?php if ($this->_tpl_vars['latestDraft']['subject']): ?><li><label><?php echo TemplateLang(array('p' => 'subject'), $this);?>
:</label> <?php echo TemplateText(array('value' => $this->_tpl_vars['latestDraft']['subject'],'cut' => 100), $this);?>
</li><?php endif; ?>
				<?php if ($this->_tpl_vars['latestDraft']['to']): ?><li><label><?php echo TemplateLang(array('p' => 'to'), $this);?>
:</label> <?php echo TemplateText(array('value' => $this->_tpl_vars['latestDraft']['to'],'cut' => 100), $this);?>
</li><?php endif; ?>
			</ul>
			<input type="button" class="primary" value=" <?php echo TemplateLang(array('p' => 'loaddraft'), $this);?>
 " onclick="loadDraft(<?php echo $this->_tpl_vars['latestDraft']['id']; ?>
)" />
			<input type="button" value=" <?php echo TemplateLang(array('p' => 'nothanks'), $this);?>
 " onclick="hideDraftNote(true,<?php echo $this->_tpl_vars['latestDraft']['id']; ?>
)" />
			<label for="deleteDraft" style="color:#666;"><input type="checkbox" id="deleteDraft" /> <?php echo TemplateLang(array('p' => 'deletedraft'), $this);?>
</label>
		</div>
		<br class="clear" />
	</div>
	<?php endif; ?>

	<div class="previewMailHeader" id="composeHeader">
		<table class="lightTable">
			<tr>
				<th width="120">* <label for="from"><?php echo TemplateLang(array('p' => 'from'), $this);?>
:</label></th>
				<td><select name="from" id="from" style="width:100%;">
					<?php $_from = $this->_tpl_vars['possibleSenders']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['senderID'] => $this->_tpl_vars['sender']):
?>
						<option value="<?php echo $this->_tpl_vars['senderID']; ?>
"<?php if ($this->_tpl_vars['senderID'] == $this->_tpl_vars['mail']['from']): ?> selected="selected"<?php endif; ?>><?php echo TemplateEMail(array('value' => $this->_tpl_vars['sender']), $this);?>
</option>
					<?php endforeach; endif; unset($_from); ?>
					</select></td>
				<td width="140">&nbsp;</td>
			</tr>
			<tr>
				<th>* <label for="to"><?php echo TemplateLang(array('p' => 'to'), $this);?>
:</label></th>
				<td><input type="text" name="to" id="to" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['mail']['to']), $this);?>
" style="width:100%;" /></td>
				<td>
					<span id="addrDiv_to">
						<button onclick="javascript:openAddressbook('<?php echo $this->_tpl_vars['sid']; ?>
','email')" type="button">
							<i class="fa fa-users"></i>
							<?php echo TemplateLang(array('p' => 'fromaddr'), $this);?>

						</button>
					</span>
				</td>
			</tr>
			<tr>
				<th>
					<a href="javascript:advancedOptions('fields', 'right', 'bottom', '<?php echo $this->_tpl_vars['tpldir']; ?>
');composeSizer(true);"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mini_arrow_<?php if (! $this->_tpl_vars['mail']['replyto'] && ! $this->_tpl_vars['mail']['bcc']): ?>right<?php else: ?>bottom<?php endif; ?>.png" width="13" height="13" align="absmiddle" border="0" alt="" id="advanced_fields_arrow" /></a> &nbsp;
					<label for="cc"><?php echo TemplateLang(array('p' => 'cc'), $this);?>
:</label></th>
				<td><input type="text" name="cc" id="cc" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['mail']['cc']), $this);?>
" style="width:100%;" /></td>
				<td>&nbsp;</td>
			</tr>
			
			<tbody id="advanced_fields_body" style="display:<?php if (! $this->_tpl_vars['mail']['replyto'] && ! $this->_tpl_vars['mail']['bcc']): ?>none<?php endif; ?>;">
			<tr>
				<th><label for="bcc"><?php echo TemplateLang(array('p' => 'bcc'), $this);?>
:</label></th>
				<td><input type="text" name="bcc" id="bcc" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['mail']['bcc']), $this);?>
" style="width:100%;" /></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<th><label for="replyto"><?php echo TemplateLang(array('p' => 'replyto'), $this);?>
:</label></th>
				<td><input type="text" name="replyto" id="replyto" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['mail']['replyto']), $this);?>
" style="width:100%;" /></td>
				<td>&nbsp;</td>
			</tr>
			</tbody>
			
			<tr>
				<th>* <label for="subject"><?php echo TemplateLang(array('p' => 'subject'), $this);?>
:</label></th>
				<td><input type="text" name="subject" id="subject" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['mail']['subject']), $this);?>
" onchange="beginDraftAutoSave()" style="width:100%;" /></td>
				<td>&nbsp;</td>
			</tr>
			
			<tr>
				<th><?php echo TemplateLang(array('p' => 'attachments'), $this);?>
:</th>
				<td>
					<input type="hidden" name="attachments" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['mail']['attachments'],'allowEmpty' => true), $this);?>
" id="attachments" />
					<div id="attachmentList"></div>
				</td>
				<td valign="top">
					<button onclick="javascript:addAttachment('<?php echo $this->_tpl_vars['sid']; ?>
')" type="button">
						<i class="fa fa-plus-circle"></i>
						<?php echo TemplateLang(array('p' => 'add'), $this);?>

					</button>
				</td>
			</tr>
			
			<tr>
				<th>&nbsp;</th>
				<td class="mailSendOptions" colspan="2">
					<div><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/export_vcf.png" width="16" height="16" border="0" alt="" align="absmiddle" /><input type="checkbox" name="attachVCard" id="attachVCard"<?php if ($this->_tpl_vars['mail']['attachVCard']): ?> checked="checked"<?php endif; ?> /><label for="attachVCard"><?php echo TemplateLang(array('p' => 'attachvc'), $this);?>
</label></div>
					
					<div><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_cert.png" width="16" height="16" border="0" alt="" align="absmiddle" /><input type="checkbox" name="certMail" id="certMail"<?php if ($this->_tpl_vars['mail']['certMail']): ?> checked="checked"<?php endif; ?> onchange="EBID('smimeEncrypt').disabled=this.checked;if(this.checked)EBID('smimeEncrypt').checked=false;" /><label for="certMail"><?php echo TemplateLang(array('p' => 'certmail'), $this);?>
</label></div>
					
					<div><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_confirm.png" width="16" height="16" border="0" alt="" align="absmiddle" /><input type="checkbox" name="mailConfirmation" id="mailConfirmation"<?php if ($this->_tpl_vars['mail']['mailConfirmation']): ?> checked="checked"<?php endif; ?> /><label for="mailConfirmation"><?php echo TemplateLang(array('p' => 'mailconfirmation'), $this);?>
</label></div>
					
					<?php if ($this->_tpl_vars['smime']): ?>
					<div><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_signed_ok.png" width="16" height="16" border="0" alt="" align="absmiddle" /><input type="checkbox" name="smimeSign" id="smimeSign"<?php if ($this->_tpl_vars['mail']['smimeSign']): ?> checked="checked"<?php endif; ?> /><label for="smimeSign"><?php echo TemplateLang(array('p' => 'sign'), $this);?>
</label></div>
					
					<div><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_encrypted.png" width="16" height="16" border="0" alt="" align="absmiddle" /><input type="checkbox" name="smimeEncrypt" id="smimeEncrypt"<?php if ($this->_tpl_vars['mail']['smimeEncrypt']): ?> checked="checked"<?php endif; ?> onchange="EBID('certMail').disabled=this.checked;if(this.checked)EBID('certMail').checked=false;" /><label for="smimeEncrypt"><?php echo TemplateLang(array('p' => 'encrypt'), $this);?>
</label></div>
					<?php endif; ?>
				</td>
			</tr>
		</table>
	</div>

	<div id="composeText" style="width:100%;position:absolute;">
		<textarea class="composeTextarea<?php if ($this->_tpl_vars['lineSep']): ?> lineSep<?php endif; ?>" name="emailText" id="emailText" style="width:100%;height:100%;<?php if ($this->_tpl_vars['useCourier']): ?>font-family:courier;<?php endif; ?>"><?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['mail']['text']), $this);?>
</textarea>
		<?php if (! $this->_tpl_vars['mail'] || $this->_tpl_vars['mail']['textMode'] == 'text'): ?>
		<input type="hidden" name="textMode" value="text" />
		<?php else: ?>
		<input type="hidden" name="textMode" value="html" />
		<?php endif; ?>
		<script language="javascript" src="./clientlib/wysiwyg.js?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/wysiwyg.js"), $this);?>
"></script>
		<script type="text/javascript" src="./clientlib/ckeditor/ckeditor.js?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/ckeditor/ckeditor.js"), $this);?>
"></script>
		<script language="javascript">
		<!--
			var autoSaveDrafts = <?php if ($this->_tpl_vars['autoSaveDrafts']): ?>true<?php else: ?>false<?php endif; ?>;
			var autoSaveDraftsInterval = <?php if ($this->_tpl_vars['autoSaveDraftsInterval']): ?><?php echo $this->_tpl_vars['autoSaveDraftsInterval']; ?>
<?php else: ?>0<?php endif; ?>;
			
			var editor = new htmlEditor('emailText', '<?php echo $this->_tpl_vars['tpldir']; ?>
/images/editor/');
			editor.modeField = 'textMode';
			editor.onReady = function()
			<?php echo '{'; ?>

				editor.start();
				editor.switchMode("<?php if (! $this->_tpl_vars['mail'] || $this->_tpl_vars['mail']['textMode'] == 'text'): ?>text<?php else: ?>html<?php endif; ?>", true);
			<?php echo '}'; ?>

			<?php if ($this->_tpl_vars['autoSaveDrafts']): ?>editor.onChange = beginDraftAutoSave;<?php endif; ?>
			editor.init();
		//-->
		</script>
	</div>
</div>

<?php echo TemplateHook(array('id' => "email.compose.tpl:foot"), $this);?>


<?php if ($this->_tpl_vars['captchaInfo']): ?>
<div id="safecodeFooter"<?php if ($this->_tpl_vars['captchaInfo']['heightHint']): ?> style="height:<?php echo $this->_tpl_vars['captchaInfo']['heightHint']; ?>
;"<?php endif; ?>>
	<table cellpadding="0" style="margin-left:auto;margin-right:auto;">
		<tr>
			<td>
				<label for="safecode"><?php echo TemplateLang(array('p' => 'safecode'), $this);?>
:</label>
			</td>
			<td style="padding-left:2em;" id="captchaContainer"><?php echo $this->_tpl_vars['captchaHTML']; ?>
</td>
			<?php if (! $this->_tpl_vars['captchaInfo']['hasOwnInput']): ?>
			<td style="padding-left:2em;">
				<input type="text" size="20" style="text-align:center;width:212px;" name="safecode" id="safecode" />
			</td>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['captchaInfo']['showNotReadable']): ?><td style="padding-left:2em;"><small><?php echo TemplateLang(array('p' => 'notreadable'), $this);?>
</small></td><?php endif; ?>
		</tr>
	</table>
</div>
<?php endif; ?>

<div id="contentFooter">
	<div class="left">
		<i class="fa fa-folder-o"></i> <label for="savecopy"><?php echo TemplateLang(array('p' => 'savecopy'), $this);?>
</label> <select name="savecopy" id="savecopy">
				<option value="-128">-</option>
			<?php $_from = $this->_tpl_vars['dropdownFolderList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['dFolderID'] => $this->_tpl_vars['dFolderTitle']):
?>
				<option value="<?php echo $this->_tpl_vars['dFolderID']; ?>
" style="font-family:courier;"<?php if (( ! $this->_tpl_vars['composeDefaults']['savecopy'] && $this->_tpl_vars['composeDefaults']['savecopy'] !== '0' && $this->_tpl_vars['dFolderID'] == -2 ) || $this->_tpl_vars['composeDefaults']['savecopy'] == $this->_tpl_vars['dFolderID']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['dFolderTitle']; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
		</select>
		
		<?php if ($this->_tpl_vars['signatures']): ?>
		&nbsp;
		
		<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_signatures.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <select name="signature" id="signature">
		<?php $_from = $this->_tpl_vars['signatures']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['signature']):
?>
			<option value="<?php echo $this->_tpl_vars['signature']['id']; ?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['signature']['titel'],'cut' => 15), $this);?>
</option>
		<?php endforeach; endif; unset($_from); ?>
		</select> <button type="button" onclick="placeSignature(EBID('signature').value)">&raquo;</button>
		<?php endif; ?>
	</div>
	<div class="center" style="line-height:2em;" id="autoSaveNote">
	</div>
	<div class="right">
		<button type="button" onclick="EBID('do').value='saveDraft';editor.submit();document.forms.f1.submit();" />
			<i class="fa fa-save"></i>
			<?php echo TemplateLang(array('p' => 'savedraft'), $this);?>

		</button>
		<button class="primary" type="button" id="sendButton" onclick="if(!checkComposeForm(document.forms.f1, <?php if ($this->_tpl_vars['attCheck']): ?>true<?php else: ?>false<?php endif; ?>, '<?php echo TemplateLang(array('p' => 'att_keywords'), $this);?>
')) return(false); EBID('do').value='sendMail';editor.submit();checkSMIME('<?php if ($this->_tpl_vars['captchaInfo'] && ! $this->_tpl_vars['captchaInfo']['hasOwnAJAXCheck']): ?>checkSafeCode(\'<?php echo $this->_tpl_vars['captchaInfo']['failAction']; ?>
\',\'submitComposeForm();\');<?php else: ?>submitComposeForm();<?php endif; ?>');">
			<i class="fa fa-send"></i>
			<?php echo TemplateLang(array('p' => 'sendmail2'), $this);?>

		</button>
	</div>
</div>

</form>

<div id="composeLoading" style="display:none"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/load_32.gif" border="0" alt="" /></div>

<script src="./clientlib/dndupload.js?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/dndupload.js"), $this);?>
" language="javascript" type="text/javascript"></script>

<script language="javascript">
<!--
	registerLoadAction(initComposeAutoComplete);
	registerLoadAction(generateAttachmentList);
	registerLoadAction(composeSizer);
	initDnDUpload(EBID('mainContent'), 'email.compose.php?action=uploadDnDAttachment&sid=' + currentSID, false, dndAttachmentUploaded, dndAttachmentURLAddition);
//-->
</script>