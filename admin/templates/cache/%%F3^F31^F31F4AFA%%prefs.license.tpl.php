<?php /* Smarty version 2.6.28, created on 2020-09-29 11:36:12
         compiled from prefs.license.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.license.tpl', 3, false),)), $this); ?>
<form action="prefs.common.php?action=license&save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'licensedetails'), $this);?>
</legend>
	
		<table>
			<tr>
				<td align="left" rowspan="6" valign="top" width="40"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_license.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'licenseno'), $this);?>
:</td>
				<td class="td2"><?php echo $this->_tpl_vars['licenseNo']; ?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'licensedto'), $this);?>
:</td>
				<td class="td2"><?php echo $this->_tpl_vars['licenseDomain']; ?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'dldate'), $this);?>
:</td>
				<td class="td2"><?php echo $this->_tpl_vars['licenseDate']; ?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'updateaccess'), $this);?>
:</td>
				<td class="td2"><div id="updateAccess"><?php echo TemplateLang(array('p' => 'pleasewait'), $this);?>
</div></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'copyrightnote'), $this);?>
:</td>
				<td class="td2"><div id="copyrightNote"><?php echo TemplateLang(array('p' => 'pleasewait'), $this);?>
</div></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'serial'), $this);?>
:</td>
				<td class="td2"><input type="text" name="serial_1" value="<?php echo $this->_tpl_vars['serial_1']; ?>
" size="5" maxlength="4" /> -
								<input type="text" name="serial_2" value="<?php echo $this->_tpl_vars['serial_2']; ?>
" size="5" maxlength="4" /> -
								<input type="text" name="serial_3" value="<?php echo $this->_tpl_vars['serial_3']; ?>
" size="5" maxlength="4" /> -
								<input type="text" name="serial_4" value="<?php echo $this->_tpl_vars['serial_4']; ?>
" size="5" maxlength="4" /></td>
			</tr>
		</table>
	</fieldset>
	
	<p>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>

<script language="javascript" src="https://ssl.b1g.de/service.b1gmail.com/licenseinfo/?do=js&version=<?php echo $this->_tpl_vars['version']; ?>
&key=<?php echo $this->_tpl_vars['serial']; ?>
&lic=<?php echo $this->_tpl_vars['licenseNo']; ?>
&lang=<?php echo $this->_tpl_vars['lang']; ?>
"></script>