<?php /* Smarty version 2.6.28, created on 2020-09-29 11:27:03
         compiled from li/index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'li/index.tpl', 2, false),array('function', 'text', 'li/index.tpl', 5, false),array('function', 'fileDateSig', 'li/index.tpl', 12, false),array('function', 'hook', 'li/index.tpl', 41, false),array('function', 'number', 'li/index.tpl', 58, false),array('function', 'comment', 'li/index.tpl', 68, false),array('function', 'math', 'li/index.tpl', 90, false),array('function', 'banner', 'li/index.tpl', 125, false),)), $this); ?>
<!DOCTYPE html>
<html lang="<?php echo TemplateLang(array('p' => 'langCode'), $this);?>
">

<head>
    <title><?php if ($this->_tpl_vars['pageTitle']): ?><?php echo TemplateText(array('value' => $this->_tpl_vars['pageTitle']), $this);?>
 - <?php endif; ?><?php echo $this->_tpl_vars['service_title']; ?>
</title>
    
	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $this->_tpl_vars['charset']; ?>
" />
	
	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="<?php echo $this->_tpl_vars['tpldir']; ?>
style/loggedin.css?<?php echo TemplateFileDateSig(array('file' => "style/loggedin.css"), $this);?>
" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->_tpl_vars['tpldir']; ?>
style/dtree.css?<?php echo TemplateFileDateSig(array('file' => "style/dtree.css"), $this);?>
" rel="stylesheet" type="text/css" />
	<link href="clientlib/fontawesome/css/font-awesome.min.css?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/fontawesome/css/font-awesome.min.css"), $this);?>
" rel="stylesheet" type="text/css" />
	<link href="clientlib/fontawesome/css/font-awesome-animation.min.css?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/fontawesome/css/font-awesome-animation.min.css"), $this);?>
" rel="stylesheet" type="text/css" />
<?php $_from = $this->_tpl_vars['_cssFiles']['li']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_file']):
?>	<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['_file']; ?>
" />
<?php endforeach; endif; unset($_from); ?>

	<!-- client scripts -->
	<script language="javascript" type="text/javascript">
	<!--
		var currentSID = '<?php echo $this->_tpl_vars['sid']; ?>
', tplDir = '<?php echo $this->_tpl_vars['tpldir']; ?>
', serverTZ = <?php echo $this->_tpl_vars['serverTZ']; ?>
, ftsBGIndexing = <?php if ($this->_tpl_vars['ftsBGIndexing']): ?>true<?php else: ?>false<?php endif; ?><?php if ($this->_tpl_vars['bmNotifyInterval']): ?>,
			notifyInterval = <?php echo $this->_tpl_vars['bmNotifyInterval']; ?>
, notifySound = <?php if ($this->_tpl_vars['bmNotifySound']): ?>true<?php else: ?>false<?php endif; ?><?php endif; ?>;
	//-->
	</script>
	<script src="clientlang.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
" type="text/javascript" language="javascript"></script>
	<script src="<?php echo $this->_tpl_vars['tpldir']; ?>
js/common.js?<?php echo TemplateFileDateSig(array('file' => "js/common.js"), $this);?>
" type="text/javascript" language="javascript"></script>
	<script src="<?php echo $this->_tpl_vars['tpldir']; ?>
js/loggedin.js?<?php echo TemplateFileDateSig(array('file' => "js/loggedin.js"), $this);?>
" type="text/javascript" language="javascript"></script>
	<script src="clientlib/dtree.js?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/dtree.js"), $this);?>
" type="text/javascript" language="javascript"></script>
	<script src="clientlib/overlay.js?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/overlay.js"), $this);?>
" type="text/javascript" language="javascript"></script>
	<script src="clientlib/autocomplete.js?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/autocomplete.js"), $this);?>
" type="text/javascript" language="javascript"></script>
	<!--[if lt IE 9]>
	<script defer type="text/javascript" src="clientlib/IE9.js"></script>
	<![endif]-->
	<!--[if IE]>
	<meta http-equiv="Page-Enter" content="blendTrans(duration=0)" />
	<meta http-equiv="Page-Exit" content="blendTrans(duration=0)" />
	<![endif]-->
<?php $_from = $this->_tpl_vars['_jsFiles']['li']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_file']):
?>	<script type="text/javascript" src="<?php echo $this->_tpl_vars['_file']; ?>
"></script>
<?php endforeach; endif; unset($_from); ?>
	<?php echo TemplateHook(array('id' => "li:index.tpl:head"), $this);?>

</head>

<body onload="documentLoader()">
	<?php echo TemplateHook(array('id' => "li:index.tpl:beforeContent"), $this);?>


	<div id="main">
		<div class="dropdownNavbar">
			<a class="logo" href="#"<?php if ($this->_tpl_vars['templatePrefs']['navPos'] == 'top'): ?> onclick="toggleDropdownNavMenu()"<?php endif; ?>>
				<?php if ($this->_tpl_vars['activeTab'] == '_search'): ?><i class="fa fa-search"></i><?php else: ?><?php $_from = $this->_tpl_vars['pageTabs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tabID'] => $this->_tpl_vars['tab']):
?><?php if ($this->_tpl_vars['activeTab'] == $this->_tpl_vars['tabID']): ?>
				<i class="fa <?php echo $this->_tpl_vars['tab']['faIcon']; ?>
"></i>
				<?php endif; ?><?php endforeach; endif; unset($_from); ?><?php endif; ?>
				<?php echo $this->_tpl_vars['service_title']; ?>

				<?php if ($this->_tpl_vars['templatePrefs']['navPos'] == 'top'): ?><span style="">| <i class="fa fa-angle-down"></i></span><?php endif; ?>
			</a>

			<div class="toolbar right">
				<?php if ($this->_tpl_vars['bmNotifyInterval'] > 0): ?><a href="#" onclick="showNotifications(this)" title="<?php echo TemplateLang(array('p' => 'notifications'), $this);?>
" style="position:relative;"><i id="notifyIcon" class="fa fa-bell faa-ring"></i><div class="noBadge" id="notifyCount"<?php if ($this->_tpl_vars['bmUnreadNotifications'] == 0): ?> style="display:none;"<?php endif; ?>><?php echo TemplateNumber(array('value' => $this->_tpl_vars['bmUnreadNotifications'],'min' => 0,'max' => 99), $this);?>
</div></a><?php endif; ?>
				<a href="#" onclick="showNewMenu(this)" title="<?php echo TemplateLang(array('p' => 'new'), $this);?>
"><i class="fa fa-plus-square fa-lg"></i> <?php echo TemplateLang(array('p' => 'new'), $this);?>

							| <i class="fa fa-angle-down"></i></a>
				<a href="#" onclick="showSearchPopup(this)" title="<?php echo TemplateLang(array('p' => 'search'), $this);?>
"><i class="fa fa-search"></i></a>
				<a href="prefs.php?action=faq&sid=<?php echo $this->_tpl_vars['sid']; ?>
" title="<?php echo TemplateLang(array('p' => 'faq'), $this);?>
"><i class="fa fa-question fa-lg"></i></a>
				<a href="start.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=logout" onclick="return confirm('<?php echo TemplateLang(array('p' => 'logoutquestion'), $this);?>
');" title="<?php echo TemplateLang(array('p' => 'logout'), $this);?>
"><i class="fa fa-sign-out fa-lg"></i></a>
			</div>

			<div class="toolbar">
				<?php if ($this->_tpl_vars['pageToolbarFile']): ?>
				<?php echo TemplateComment(array('text' => "including ".($this->_tpl_vars['pageToolbarFile'])), $this);?>

				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['pageToolbarFile']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				<?php elseif ($this->_tpl_vars['pageToolbar']): ?>
				<?php echo $this->_tpl_vars['pageToolbar']; ?>

				<?php else: ?>
				&nbsp;
				<?php endif; ?>
			</div>

			<div class="menu fade" id="dropdownNavMenu" style="display:none;">
				<div class="arrow"></div>
				<?php $_from = $this->_tpl_vars['pageTabs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tabID'] => $this->_tpl_vars['tab']):
?>
				<?php echo TemplateComment(array('text' => "tab ".($this->_tpl_vars['tabID'])), $this);?>

				<a href="<?php echo $this->_tpl_vars['tab']['link']; ?>
<?php echo $this->_tpl_vars['sid']; ?>
" title="<?php echo $this->_tpl_vars['tab']['text']; ?>
"<?php if ($this->_tpl_vars['activeTab'] == $this->_tpl_vars['tabID']): ?> class="active"<?php endif; ?>>
					<i class="fa <?php echo $this->_tpl_vars['tab']['faIcon']; ?>
"></i>
					<?php echo $this->_tpl_vars['tab']['text']; ?>

				</a>
				<?php endforeach; endif; unset($_from); ?>
			</div>
		</div>
		
		<div id="mainMenu" class="up">
			<div id="mainMenuContainer"<?php if ($this->_tpl_vars['templatePrefs']['navPos'] == 'left'): ?> style="bottom:<?php echo smarty_function_math(array('equation' => "x*29",'x' => $this->_tpl_vars['pageTabsCount']), $this);?>
px;"<?php endif; ?>>
	            <?php if ($this->_tpl_vars['pageMenuFile']): ?>
	            <?php echo TemplateComment(array('text' => "including ".($this->_tpl_vars['pageMenuFile'])), $this);?>

	            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['pageMenuFile']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	            <?php else: ?>
	            <?php $_from = $this->_tpl_vars['pageMenu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['menuID'] => $this->_tpl_vars['menu']):
?>
	            <?php echo TemplateComment(array('text' => "menuitem ".($this->_tpl_vars['menuID'])), $this);?>

	           	<a href="<?php echo $this->_tpl_vars['menu']['link']; ?>
">
		            <img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_<?php echo $this->_tpl_vars['menu']['icon']; ?>
.png" width="16" height="16" border="0" alt="" align="absmiddle" />
		            <?php echo $this->_tpl_vars['menu']['text']; ?>

	            </a>
	            <?php if ($this->_tpl_vars['menu']['addText']): ?>
	            <span class="menuAddText"><?php echo $this->_tpl_vars['menu']['addText']; ?>
</span>
	            <?php endif; ?>
	            <br />
	        	<?php endforeach; endif; unset($_from); ?>
	            <?php endif; ?>
            </div>
            
			<?php if ($this->_tpl_vars['templatePrefs']['navPos'] == 'left'): ?>
			<ul id="menuTabItems">
	            <?php $_from = $this->_tpl_vars['pageTabs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tabID'] => $this->_tpl_vars['tab']):
?>
	            <?php echo TemplateComment(array('text' => "tab ".($this->_tpl_vars['tabID'])), $this);?>

	            <li<?php if ($this->_tpl_vars['activeTab'] == $this->_tpl_vars['tabID']): ?> class="active"<?php endif; ?>>
	            	<a href="<?php echo $this->_tpl_vars['tab']['link']; ?>
<?php echo $this->_tpl_vars['sid']; ?>
">
	            		<i class="fa <?php echo $this->_tpl_vars['tab']['faIcon']; ?>
"></i>
	                    <?php if ($this->_tpl_vars['tab']['text']): ?>&nbsp;<?php echo $this->_tpl_vars['tab']['text']; ?>
<?php endif; ?>
	                </a>
	            </li>
	            <?php endforeach; endif; unset($_from); ?>
			</ul>
			<?php endif; ?>
		</div>

		<div id="mainBanner" style="display:none;">
			<?php echo TemplateBanner(array(), $this);?>

		</div>

		<div id="mainContent" class="up">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['pageContent']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</div>
		
		<div id="mainStatusBar">
			<?php echo '&nbsp;'; ?>
 
		</div>
		
	    <?php echo TemplateComment(array('text' => 'search popup'), $this);?>

	    <div class="headerBox" id="searchPopup" style="display:none">
			<div class="arrow"></div>
			<div class="inner">
				<table width="100%" cellspacing="0" cellpadding="0" class="up" onmouseover="disableHide=true;" onmouseout="disableHide=false;">
					<tr>
						<td>
							<?php if ($this->_tpl_vars['templatePrefs']['navPos'] == 'top'): ?><div class="arrow"></div><?php endif; ?>
							<table cellspacing="0" cellpadding="0" width="100%">
								<tr>
									<td width="22" height="26" align="right"><img id="searchSpinner" style="display:none;" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/load_16.gif" border="0" alt="" width="16" height="16" align="absmiddle" /></td>
									<td align="right" width="70"><?php echo TemplateLang(array('p' => 'search'), $this);?>
: &nbsp;</td>
									<td align="center">
										<input id="searchField" name="searchField" style="width:90%" onkeypress="searchFieldKeyPress(event,<?php if ($this->_tpl_vars['searchDetailsDefault']): ?>true<?php else: ?>false<?php endif; ?>)" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tbody id="searchResultBody" style="display:none">
					<tr>
						<td id="searchResults"></td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
			
	    <?php echo TemplateComment(array('text' => 'new menu'), $this);?>

		<div class="headerBox" id="newMenu" style="display:none;">
			<div class="arrow"></div>
			<div class="inner">
			<?php $_from = $this->_tpl_vars['newMenu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
				<?php if ($this->_tpl_vars['item']['sep']): ?>
				<div class="mailMenuSep"></div>
				<?php else: ?>
				<a class="mailMenuItem" href="<?php echo $this->_tpl_vars['item']['link']; ?>
<?php echo $this->_tpl_vars['sid']; ?>
"><img align="absmiddle" src="<?php if (! $this->_tpl_vars['item']['iconDir']): ?><?php echo $this->_tpl_vars['tpldir']; ?>
images/li/<?php else: ?><?php echo $this->_tpl_vars['item']['iconDir']; ?>
<?php endif; ?><?php echo $this->_tpl_vars['item']['icon']; ?>
.png" width="16" height="16" border="0" alt="" /> <?php echo $this->_tpl_vars['item']['text']; ?>
...</a>
				<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
			</div>
		</div>
		
		<?php echo TemplateComment(array('text' => 'notifications'), $this);?>

		<div class="headerBox" id="notifyBox" style="display:none;">
			<div class="arrow"></div>
			<div class="inner" id="notifyInner"></div>
		</div>

	</div>

	<?php echo TemplateHook(array('id' => "li:index.tpl:afterContent"), $this);?>

</body>

</html>