<?php /* Smarty version 2.6.28, created on 2020-09-29 11:27:35
         compiled from updates.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'updates.tpl', 2, false),array('function', 'cycle', 'updates.tpl', 43, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'updates'), $this);?>
</legend>
	
	<table width="100%">
	<tr>
		<td align="left" valign="top" width="40"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php if ($this->_tpl_vars['error']): ?>error32<?php else: ?>updates<?php endif; ?>.png" border="0" alt="" width="32" height="32" /></td>
		<td>
			<?php if ($this->_tpl_vars['error']): ?>
				<b><?php echo TemplateLang(array('p' => 'error'), $this);?>
</b><br /><br />
				<?php echo TemplateLang(array('p' => 'upderrordesc'), $this);?>
<br /><br />
				<?php echo $this->_tpl_vars['error_desc']; ?>

			<?php else: ?>
				<form action="updates.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
				
				<?php if (! $this->_tpl_vars['step']): ?>
				
					<input type="hidden" name="step" value="1" />
					<?php echo TemplateLang(array('p' => 'updatesdesc'), $this);?>

			
					<div align="center">
						<br />
						<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'searchupdatesnow'), $this);?>
 &raquo; " />
					</div>
					
				<?php elseif ($this->_tpl_vars['step'] == 1): ?>
				
					<?php if ($this->_tpl_vars['updates']): ?>
					
						<input type="hidden" name="step" value="2" />
						
						<p>
							<?php echo TemplateLang(array('p' => 'updatesfound'), $this);?>

						</p>
						
						<p>
							<table class="list">
								<tr>
									<th width="20">&nbsp;</th>
									<th><?php echo TemplateLang(array('p' => 'patchlevel'), $this);?>
</th>
									<th width="150"><?php echo TemplateLang(array('p' => 'version'), $this);?>
</th>
								</tr>
								<?php $_from = $this->_tpl_vars['updates']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['update']):
?>
								<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

								<?php if ($this->_tpl_vars['update'] > $this->_tpl_vars['pl']): ?><tr class="<?php echo $this->_tpl_vars['class']; ?>
">
									<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/patchlevel.png" border="0" width="16" height="16" alt="" /></td>
									<td><?php echo $this->_tpl_vars['update']; ?>
</td>
									<td><?php echo $this->_tpl_vars['version']; ?>
</td>
								</tr><?php endif; ?>
								<?php endforeach; endif; unset($_from); ?>
							</table>
						</p>
						
						<p>
							<?php echo TemplateLang(array('p' => 'clicktoupdate'), $this);?>

						</p>
						
						<p>
							<div style="float:right">
								<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'next'), $this);?>
 &raquo; " />
							</div>
						</p>
					
					<?php else: ?>
					
						<?php echo TemplateLang(array('p' => 'noupdatesfound'), $this);?>

					
					<?php endif; ?>
					
				<?php elseif ($this->_tpl_vars['step'] == 2): ?>
				
					<input type="hidden" name="step" value="3" />
						
					<p>
						<?php echo TemplateLang(array('p' => 'pleasereadme'), $this);?>

					</p>
					
					<p>
						<div style="background-color: #FFFFFF;overflow:auto;border: 1px solid #A0A0A0;height: 120px;padding: 5px;">
							<?php echo $this->_tpl_vars['readme']; ?>

						</div>
					</p>
					
					<p>
						<br />
						<?php echo TemplateLang(array('p' => 'changedfiles'), $this);?>

					</p>
				
					<p>
						<table class="list">
							<tr>
								<th width="20">&nbsp;</th>
								<th><?php echo TemplateLang(array('p' => 'filename'), $this);?>
</th>
								<th width="140"><?php echo TemplateLang(array('p' => 'writeable'), $this);?>
?</th>
							</tr>
							<?php $_from = $this->_tpl_vars['files']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['file']):
?>
							<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

							<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
								<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/file.png" border="0" width="16" height="16" alt="" /></td>
								<td><?php echo $this->_tpl_vars['file']['file']; ?>
</td>
								<td><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php if ($this->_tpl_vars['file']['writeable']): ?>ok<?php else: ?>error<?php endif; ?>.png" width="16" height="16" alt="" border="0" /></td>
							</tr>
							<?php endforeach; endif; unset($_from); ?>
						</table>
					</p>
					
					<p>
						<br />
						<div style="float:right">
							<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'next'), $this);?>
 &raquo; " />
						</div>
					</p>
					
				<?php elseif ($this->_tpl_vars['step'] == 3): ?>
				
					<input type="hidden" name="step" value="<?php if ($this->_tpl_vars['moreUpdates']): ?>2<?php else: ?>0<?php endif; ?>" />
					
					<p>
						<?php echo TemplateLang(array('p' => 'updating'), $this);?>

					</p>
					
					<p>
						<table class="list">
							<tr>
								<th width="20">&nbsp;</th>
								<th><?php echo TemplateLang(array('p' => 'filename'), $this);?>
</th>
								<th width="100"><?php echo TemplateLang(array('p' => 'status'), $this);?>
</th>
							</tr>
							<?php $_from = $this->_tpl_vars['status']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['fileName'] => $this->_tpl_vars['statusCode']):
?>
							<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

							<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
								<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/file.png" border="0" width="16" height="16" alt="" /></td>
								<td><?php echo $this->_tpl_vars['fileName']; ?>
</td>
								<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php if ($this->_tpl_vars['statusCode'] == 0): ?>ok<?php else: ?>error<?php endif; ?>.png" border="0" width="16" height="16" alt="" align="absmiddle" /><?php if ($this->_tpl_vars['statusCode'] != 0): ?> (<?php echo $this->_tpl_vars['statusCode']; ?>
)<?php endif; ?></td>
							</tr>
							<?php endforeach; endif; unset($_from); ?>
						</table>
					</p>
					
					<p>
						<?php echo TemplateLang(array('p' => 'updateinstalled'), $this);?>

						<?php if ($this->_tpl_vars['moreUpdates']): ?><?php echo TemplateLang(array('p' => 'moreupdates'), $this);?>
<?php endif; ?>
					</p>
					
					<?php if ($this->_tpl_vars['moreUpdates']): ?><p>
						<div style="float:right">
							<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'next'), $this);?>
 &raquo; " />
						</div>
					</p><?php endif; ?>
					
				<?php endif; ?>
			
				</form>
			<?php endif; ?>
		</td>
	</tr>
	</table>
</fieldset>