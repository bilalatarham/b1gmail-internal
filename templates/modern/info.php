<?php 
/*
 * b1gMail
 * (c) 2002-2016 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 */

$templateInfo = array(
	'title'			=> 'b1gMail ' . $lang_admin['default'],
	'author'		=> 'B1G Software',
	'website'		=> 'http://www.b1g.de/',
	'for_b1gmail'	=> B1GMAIL_VERSION,
	
	'prefs'	=> array(
		'splashImage'	=> array(
			'title'		=> $lang_admin['splashimage'] . ':',
			'type'		=> FIELD_DROPDOWN,
			'options'	=> array(	'login_bg_1.jpg'	=> $lang_admin['login_bg_1']),
			'default'	=> 'login_bg_1.jpg'
		),
		'hideSignup'	=> array(
			'title'		=> $lang_admin['hidesignup'] . '?',
			'type'		=> FIELD_CHECKBOX,
			'default'	=> false
		),
		'navPos'		=> array(
			'title'		=> $lang_admin['navpos'] . ':',
			'type'		=> FIELD_DROPDOWN,
			'options'	=> array(	'top'			=> $lang_admin['top'],
									'left'			=> $lang_admin['left']),
			'default'	=> 'top'
		),
		'prefsLayout'	=> array(
			'title'		=> $lang_admin['prefslayout'] . ':',
			'type'		=> FIELD_DROPDOWN,
			'options'	=> array('onecolumn'		=> $lang_admin['onecolumn'],
									'twocolumns'	=> $lang_admin['twocolumns']),
			'default'	=> 'onecolumn'
		),
		'showUserEmail'	=> array(
			'title'		=> $lang_admin['showuseremail'] . '?',
			'type'		=> FIELD_CHECKBOX,
			'default'	=> false
		),
		'showCheckboxes'=> array(
			'title'		=> $lang_admin['showcheckboxes'] . '?',
			'type'		=> FIELD_CHECKBOX,
			'default'	=> false
		)
	)
);
