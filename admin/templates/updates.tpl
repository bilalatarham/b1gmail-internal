<fieldset>
	<legend>{lng p="updates"}</legend>
	
	<table width="100%">
	<tr>
		<td align="left" valign="top" width="40"><img src="{$tpldir}images/{if $error}error32{else}updates{/if}.png" border="0" alt="" width="32" height="32" /></td>
		<td>
			{if $error}
				<b>{lng p="error"}</b><br /><br />
				{lng p="upderrordesc"}<br /><br />
				{$error_desc}
			{else}
				<form action="updates.php?sid={$sid}" method="post" onsubmit="spin(this)">
				
				{if !$step}
				
					<input type="hidden" name="step" value="1" />
					{lng p="updatesdesc"}
			
					<div align="center">
						<br />
						<input class="button" type="submit" value=" {lng p="searchupdatesnow"} &raquo; " />
					</div>
					
				{elseif $step==1}
				
					{if $updates}
					
						<input type="hidden" name="step" value="2" />
						
						<p>
							{lng p="updatesfound"}
						</p>
						
						<p>
							<table class="list">
								<tr>
									<th width="20">&nbsp;</th>
									<th>{lng p="patchlevel"}</th>
									<th width="150">{lng p="version"}</th>
								</tr>
								{foreach from=$updates item=update}
								{cycle name=class values="td1,td2" assign=class}
								{if $update>$pl}<tr class="{$class}">
									<td align="center"><img src="{$tpldir}images/patchlevel.png" border="0" width="16" height="16" alt="" /></td>
									<td>{$update}</td>
									<td>{$version}</td>
								</tr>{/if}
								{/foreach}
							</table>
						</p>
						
						<p>
							{lng p="clicktoupdate"}
						</p>
						
						<p>
							<div style="float:right">
								<input class="button" type="submit" value=" {lng p="next"} &raquo; " />
							</div>
						</p>
					
					{else}
					
						{lng p="noupdatesfound"}
					
					{/if}
					
				{elseif $step==2}
				
					<input type="hidden" name="step" value="3" />
						
					<p>
						{lng p="pleasereadme"}
					</p>
					
					<p>
						<div style="background-color: #FFFFFF;overflow:auto;border: 1px solid #A0A0A0;height: 120px;padding: 5px;">
							{$readme}
						</div>
					</p>
					
					<p>
						<br />
						{lng p="changedfiles"}
					</p>
				
					<p>
						<table class="list">
							<tr>
								<th width="20">&nbsp;</th>
								<th>{lng p="filename"}</th>
								<th width="140">{lng p="writeable"}?</th>
							</tr>
							{foreach from=$files item=file}
							{cycle name=class values="td1,td2" assign=class}
							<tr class="{$class}">
								<td align="center"><img src="{$tpldir}images/file.png" border="0" width="16" height="16" alt="" /></td>
								<td>{$file.file}</td>
								<td><img align="absmiddle" src="{$tpldir}images/{if $file.writeable}ok{else}error{/if}.png" width="16" height="16" alt="" border="0" /></td>
							</tr>
							{/foreach}
						</table>
					</p>
					
					<p>
						<br />
						<div style="float:right">
							<input class="button" type="submit" value=" {lng p="next"} &raquo; " />
						</div>
					</p>
					
				{elseif $step==3}
				
					<input type="hidden" name="step" value="{if $moreUpdates}2{else}0{/if}" />
					
					<p>
						{lng p="updating"}
					</p>
					
					<p>
						<table class="list">
							<tr>
								<th width="20">&nbsp;</th>
								<th>{lng p="filename"}</th>
								<th width="100">{lng p="status}</th>
							</tr>
							{foreach from=$status item=statusCode key=fileName}
							{cycle name=class values="td1,td2" assign=class}
							<tr class="{$class}">
								<td align="center"><img src="{$tpldir}images/file.png" border="0" width="16" height="16" alt="" /></td>
								<td>{$fileName}</td>
								<td><img src="{$tpldir}images/{if $statusCode==0}ok{else}error{/if}.png" border="0" width="16" height="16" alt="" align="absmiddle" />{if $statusCode!=0} ({$statusCode}){/if}</td>
							</tr>
							{/foreach}
						</table>
					</p>
					
					<p>
						{lng p="updateinstalled"}
						{if $moreUpdates}{lng p="moreupdates"}{/if}
					</p>
					
					{if $moreUpdates}<p>
						<div style="float:right">
							<input class="button" type="submit" value=" {lng p="next"} &raquo; " />
						</div>
					</p>{/if}
					
				{/if}
			
				</form>
			{/if}
		</td>
	</tr>
	</table>
</fieldset>
