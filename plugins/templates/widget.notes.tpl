<div class="innerWidget notePreview" id="notePreview">
	{lng p="clicknote"}
</div>
<div class="innerWidget" style="max-height: 79px; overflow-y: auto; border-top: 1px solid #DDDDDD;">
{foreach from=$bmwidget_notes_items key=noteID item=note}
	<a href="javascript:previewNote('{$sid}', '{$noteID}');">
	<img width="16" height="16" src="{$tpldir}images/li/ico_notes.png" border="0" alt="" align="absmiddle" />
	{text value=$note.text cut=30}</a><br />
{/foreach}
</div>