<?php /* Smarty version 2.6.28, created on 2020-09-29 11:44:18
         compiled from about.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'about.tpl', 9, false),)), $this); ?>
<p align="center">
	<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/about_logo.png" width="242" height="171" border="0" alt="" />
</p>

<p align="center">
	<br />
	<b>b1gMail</b><br />
	<i>
		<?php echo TemplateLang(array('p' => 'version'), $this);?>
 <?php echo $this->_tpl_vars['version']; ?>

		<?php if ($this->_tpl_vars['patchlevel']): ?><small><br /><?php echo TemplateLang(array('p' => 'patchlevel'), $this);?>
 <?php echo $this->_tpl_vars['patchlevel']; ?>
</small><?php endif; ?>
	</i>
</p>

<p align="center">
	Copyright &copy; 2002-2018 <a target="_blank" href="http://www.b1g.de"><img src="./templates/images/b1gsoftware.png" align="absmiddle" style="padding-bottom:2px;" border="0" alt="B1G Software" width="87" height="22" /></a>
</p>

<p align="center">
	<small>
		<?php echo TemplateLang(array('p' => 'acpiconsfrom'), $this);?>
 <a href="http://www.fatcow.com/free-icons" target="_blank" rel="noreferrer">FatCow Web Hosting</a><br />
		<?php echo TemplateLang(array('p' => 'acpbgfrom'), $this);?>
 <a href="http://subtlepatterns.com" target="_blank" rel="noreferrer">subtlepatterns.com</a>
	</small>
</p>