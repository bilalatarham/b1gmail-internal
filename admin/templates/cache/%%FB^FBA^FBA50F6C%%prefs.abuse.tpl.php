<?php /* Smarty version 2.6.28, created on 2020-09-30 13:04:03
         compiled from prefs.abuse.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.abuse.tpl', 3, false),array('function', 'text', 'prefs.abuse.tpl', 11, false),array('function', 'email', 'prefs.abuse.tpl', 46, false),array('function', 'cycle', 'prefs.abuse.tpl', 62, false),)), $this); ?>
<form action="prefs.abuse.php?save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'common'), $this);?>
</legend>

		<table>
			<tr>
				<td width="40" valign="top" rowspan="6"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/abuse32.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'ap_medium_limit'), $this);?>
:</td>
				<td class="td2">
					<img src="templates/images/indicator_yellow.png" border="0" alt="" align="absmiddle" />
					<input type="number" min="1" step="1" name="ap_medium_limit" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['ap_medium_limit']), $this);?>
" style="width:60px;" />
					<?php echo TemplateLang(array('p' => 'points'), $this);?>

				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'ap_hard_limit'), $this);?>
:</td>
				<td class="td2">
					<img src="templates/images/indicator_red.png" border="0" alt="" align="absmiddle" />
					<input type="number" min="1" step="1" name="ap_hard_limit" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['ap_hard_limit']), $this);?>
" style="width:60px;" />
					<?php echo TemplateLang(array('p' => 'points'), $this);?>

				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'ap_expire_time'), $this);?>
:</td>
				<td class="td2"><input type="number" min="1" step="1" name="ap_expire_time" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['ap_expire_time']/3600), $this);?>
" style="width:80px;" /> <?php echo TemplateLang(array('p' => 'hours'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'ap_expire_mode'), $this);?>
:</td>
				<td class="td2">
					<input type="radio" name="ap_expire_mode" value="dynamic" id="ap_expire_mode_dynamic"<?php if ($this->_tpl_vars['bm_prefs']['ap_expire_mode'] == 'dynamic'): ?> checked="checked"<?php endif; ?> />
					<label for="ap_expire_mode_dynamic"><?php echo TemplateLang(array('p' => 'ap_dynamic'), $this);?>
</label><br />
					<input type="radio" name="ap_expire_mode" value="static" id="ap_expire_mode_static"<?php if ($this->_tpl_vars['bm_prefs']['ap_expire_mode'] == 'static'): ?> checked="checked"<?php endif; ?> />
					<label for="ap_expire_mode_static"><?php echo TemplateLang(array('p' => 'ap_static'), $this);?>
</label>
				</select></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'ap_autolock'), $this);?>
?</td>
				<td class="td2">
					<input name="ap_autolock"<?php if ($this->_tpl_vars['bm_prefs']['ap_autolock'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" id="ap_autolock" />
					<label for="ap_autolock"><?php echo TemplateLang(array('p' => 'ap_athardlimit'), $this);?>
</label>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'ap_autolock_notify'), $this);?>
?</td>
				<td class="td2"><input name="ap_autolock_notify" id="ap_autolock_notify"<?php if ($this->_tpl_vars['bm_prefs']['ap_autolock_notify'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" />
					<label for="ap_autolock_notify"> <?php echo TemplateLang(array('p' => 'to2'), $this);?>
: </label><input type="text" name="ap_autolock_notify_to" value="<?php echo TemplateEMail(array('value' => $this->_tpl_vars['bm_prefs']['ap_autolock_notify_to']), $this);?>
" size="24" /></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'pointtypes'), $this);?>
</legend>

		<table class="list">
			<tr>
				<th width="20">&nbsp;</th>
				<th><?php echo TemplateLang(array('p' => 'title'), $this);?>
</th>
				<th width="80"><?php echo TemplateLang(array('p' => 'points'), $this);?>
</th>
			</tr>

			<?php $_from = $this->_tpl_vars['apTypes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['apTypeID'] => $this->_tpl_vars['apType']):
?>
			<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

			<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
				<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
/images/abuse.png" border="0" alt="" width="16" height="16" /></td>
				<td>
					<?php echo $this->_tpl_vars['apType']['title']; ?>

					<?php if ($this->_tpl_vars['apType']['prefs']): ?>
					<div>
						<table class="subTable">
							<?php $_from = $this->_tpl_vars['apType']['prefs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['prefKey'] => $this->_tpl_vars['prefDetails']):
?>
							<tr>
								<td><?php echo $this->_tpl_vars['prefDetails']['title']; ?>
</td>
								<td>
									<?php if ($this->_tpl_vars['prefDetails']['type'] == 1): ?>
									<input type="text" name="types[<?php echo $this->_tpl_vars['apTypeID']; ?>
][prefs][<?php echo $this->_tpl_vars['prefKey']; ?>
]" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['prefDetails']['value']), $this);?>
" style="width:100px;" class="smallInput" />
									<?php endif; ?>
								</td>
							</tr>
							<?php endforeach; endif; unset($_from); ?>
						</table>
					</div>
					<?php endif; ?>
				</td>
				<td><input type="text" name="types[<?php echo $this->_tpl_vars['apTypeID']; ?>
][points]" value="<?php echo $this->_tpl_vars['apType']['points']; ?>
" size="6" /></td>
			</tr>
			<?php endforeach; endif; unset($_from); ?>
		</table>
	</fieldset>

	<p>
		<div style="float:right;" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>