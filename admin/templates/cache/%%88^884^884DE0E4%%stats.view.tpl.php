<?php /* Smarty version 2.6.28, created on 2020-09-29 15:07:30
         compiled from stats.view.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'stats.view.tpl', 11, false),array('function', 'html_select_date', 'stats.view.tpl', 16, false),array('function', 'text', 'stats.view.tpl', 33, false),array('function', 'implode', 'stats.view.tpl', 66, false),array('function', 'math', 'stats.view.tpl', 74, false),)), $this); ?>
<fieldset>
	<legend><?php echo $this->_tpl_vars['modeTitle']; ?>
</legend>
	
	<form action="stats.php?action=<?php echo $this->_tpl_vars['mode']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post">
		<table width="100%">
			<tr>
				<td align="left">
					<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/stats.png" border="0" alt="" width="16" height="16" align="absmiddle" />
					<select name="statType">
					<?php $_from = $this->_tpl_vars['statTypes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['type']):
?>
						<option value="<?php echo $this->_tpl_vars['type']; ?>
"<?php if ($this->_tpl_vars['statType'] == $this->_tpl_vars['type']): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => "stat_".($this->_tpl_vars['type'])), $this);?>
</option>
					<?php endforeach; endif; unset($_from); ?>
					</select>
					&nbsp;&nbsp;
					<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/calendar.png" border="0" alt="" width="16" height="16" align="absmiddle" />
					<?php echo smarty_function_html_select_date(array('prefix' => 'time','start_year' => "-5",'time' => $this->_tpl_vars['time'],'display_days' => false), $this);?>

				</td>
				<td align="right">
					<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'show'), $this);?>
 &raquo; " />
				</td>
			</tr>
		</table>
	</form>
</fieldset>
	
<?php $_from = $this->_tpl_vars['stats']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['stat']):
?>
<fieldset>
	<legend><?php echo $this->_tpl_vars['stat']['title']; ?>
</legend>
	
	<center>
		<table class="statsTable">
			<tr>
				<th colspan="<?php echo $this->_tpl_vars['stat']['count']+1; ?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['stat']['title']), $this);?>
</th>
			</tr>
			<tr style="height:250px;">
				<td width="30" class="yScale"><?php $_from = $this->_tpl_vars['stat']['yScale']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['val']):
?><div><?php echo $this->_tpl_vars['val']; ?>
&nbsp;</div><?php endforeach; endif; unset($_from); ?></td>
				<?php $_from = $this->_tpl_vars['stat']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['day'] => $this->_tpl_vars['values']):
?>
				<td class="bar"><?php if ($this->_tpl_vars['values'][$this->_tpl_vars['stat']['key']] !== false): ?><div title="<?php echo $this->_tpl_vars['values'][$this->_tpl_vars['stat']['key']]; ?>
" style="height:<?php if ($this->_tpl_vars['stat']['heights'][$this->_tpl_vars['day']] == 0): ?>1<?php else: ?><?php echo $this->_tpl_vars['stat']['heights'][$this->_tpl_vars['day']]; ?>
<?php endif; ?>px;"></div><?php endif; ?></td>
				<?php endforeach; endif; unset($_from); ?>
			</tr>
			<tr>
				<td rowspan="2"></td>
				<?php $_from = $this->_tpl_vars['stat']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['day'] => $this->_tpl_vars['values']):
?><td class="xLines"></td><?php endforeach; endif; unset($_from); ?>
			</tr>
			<tr>
				<?php $_from = $this->_tpl_vars['stat']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['day'] => $this->_tpl_vars['values']):
?><td class="xScale"><?php echo $this->_tpl_vars['day']; ?>
</td><?php endforeach; endif; unset($_from); ?>
			</tr>
		</table>

		<table class="list" style="width:692px;">
			<tr>
				<th width="60"><?php echo TemplateLang(array('p' => 'day'), $this);?>
</th>
				<th><?php echo TemplateLang(array('p' => 'value'), $this);?>
</th>
				<th width="60"><?php echo TemplateLang(array('p' => 'day'), $this);?>
</th>
				<th><?php echo TemplateLang(array('p' => 'value'), $this);?>
</th>
				<th width="60"><?php echo TemplateLang(array('p' => 'day'), $this);?>
</th>
				<th><?php echo TemplateLang(array('p' => 'value'), $this);?>
</th>
				<th width="60"><?php echo TemplateLang(array('p' => 'day'), $this);?>
</th>
				<th><?php echo TemplateLang(array('p' => 'value'), $this);?>
</th>
			</tr>
			<tr>
		<?php $this->assign('i', 0); ?>
		<?php $_from = $this->_tpl_vars['stat']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['day'] => $this->_tpl_vars['values']):
?>
		<?php $this->assign('i', $this->_tpl_vars['i']+1); ?>
				<td class="td2"><?php echo $this->_tpl_vars['day']; ?>
</td>
				<td class="td1"<?php if ($this->_tpl_vars['i'] != 4): ?> style="border-right: 1px solid #BBBBBB;"<?php endif; ?>><?php echo TemplateImplode(array('pieces' => $this->_tpl_vars['values'],'glue' => " / "), $this);?>
</td>
		<?php if ($this->_tpl_vars['i'] == 4): ?>
		<?php $this->assign('i', 0); ?>
			</tr>
			<tr>
		<?php endif; ?>
		<?php endforeach; endif; unset($_from); ?>
		<?php if ($this->_tpl_vars['i'] < 4): ?>
		<?php echo smarty_function_math(array('assign' => 'i','equation' => "(x - y)",'x' => 4,'y' => $this->_tpl_vars['i']), $this);?>

		<?php unset($this->_sections['rest']);
$this->_sections['rest']['loop'] = is_array($_loop=$this->_tpl_vars['i']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['rest']['name'] = 'rest';
$this->_sections['rest']['show'] = true;
$this->_sections['rest']['max'] = $this->_sections['rest']['loop'];
$this->_sections['rest']['step'] = 1;
$this->_sections['rest']['start'] = $this->_sections['rest']['step'] > 0 ? 0 : $this->_sections['rest']['loop']-1;
if ($this->_sections['rest']['show']) {
    $this->_sections['rest']['total'] = $this->_sections['rest']['loop'];
    if ($this->_sections['rest']['total'] == 0)
        $this->_sections['rest']['show'] = false;
} else
    $this->_sections['rest']['total'] = 0;
if ($this->_sections['rest']['show']):

            for ($this->_sections['rest']['index'] = $this->_sections['rest']['start'], $this->_sections['rest']['iteration'] = 1;
                 $this->_sections['rest']['iteration'] <= $this->_sections['rest']['total'];
                 $this->_sections['rest']['index'] += $this->_sections['rest']['step'], $this->_sections['rest']['iteration']++):
$this->_sections['rest']['rownum'] = $this->_sections['rest']['iteration'];
$this->_sections['rest']['index_prev'] = $this->_sections['rest']['index'] - $this->_sections['rest']['step'];
$this->_sections['rest']['index_next'] = $this->_sections['rest']['index'] + $this->_sections['rest']['step'];
$this->_sections['rest']['first']      = ($this->_sections['rest']['iteration'] == 1);
$this->_sections['rest']['last']       = ($this->_sections['rest']['iteration'] == $this->_sections['rest']['total']);
?>
				<td class="td2">&nbsp;</td>
				<td class="td1"<?php if ($this->_sections['rest']['index'] != $this->_tpl_vars['i']-1): ?> style="border-right: 1px solid #BBBBBB;"<?php endif; ?>>&nbsp;</td>
		<?php endfor; endif; ?>
			</tr>
		<?php endif; ?>

			<tr>
				<td colspan="8" class="footer" style="text-align:center;">
					<?php echo TemplateLang(array('p' => 'sum'), $this);?>
:
					<?php echo $this->_tpl_vars['stat']['sum']; ?>

				</td>
			</tr>
		</table>

		<p>
			
		</p>
	</center>
</fieldset>
<?php endforeach; endif; unset($_from); ?>