<?php /* Smarty version 2.6.28, created on 2020-10-01 11:01:57
         compiled from prefs.languages.texts.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.languages.texts.tpl', 2, false),array('function', 'text', 'prefs.languages.texts.tpl', 11, false),array('function', 'fileDateSig', 'prefs.languages.texts.tpl', 22, false),array('function', 'cycle', 'prefs.languages.texts.tpl', 46, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'language'), $this);?>
</legend>
	
	<form action="prefs.languages.php?action=texts&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post">
	<center>
		<table>
			<tr>
				<td><?php echo TemplateLang(array('p' => 'language'), $this);?>
:</td>
				<td><select name="lang">
					<?php $_from = $this->_tpl_vars['languages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['langID'] => $this->_tpl_vars['lang']):
?>
						<option value="<?php echo $this->_tpl_vars['langID']; ?>
"<?php if ($this->_tpl_vars['langID'] == $this->_tpl_vars['selectedLang']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['lang']['title']), $this);?>
</option>
					<?php endforeach; endif; unset($_from); ?>
					</select></td>
				<td><input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'ok'), $this);?>
 &raquo; " /></td>
			</tr>
		</table>
	</center>
	</form>
</fieldset>

<?php if ($this->_tpl_vars['selectedLang']): ?>
<script language="javascript" src="../clientlib/wysiwyg.js?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/wysiwyg.js"), $this);?>
"></script>
<script type="text/javascript" src="../clientlib/ckeditor/ckeditor.js?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/ckeditor/ckeditor.js"), $this);?>
"></script>

<script language="javascript">
<!--
	var editors = [];
//-->
</script>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'customtexts'), $this);?>
</legend>
	
	<form action="prefs.languages.php?action=texts&lang=<?php echo $this->_tpl_vars['selectedLang']; ?>
&save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
	<p align="right">
		<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
	</p>
	
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="220"><?php echo TemplateLang(array('p' => 'title'), $this);?>
</th>
			<th><?php echo TemplateLang(array('p' => 'text'), $this);?>
</th>
		</tr>
		<?php $_from = $this->_tpl_vars['texts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['text']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','assign' => 'class','values' => "td1,td2"), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/phrases.png" border="0" alt="" width="16" height="16" /></td>
			<td><a name="<?php echo $this->_tpl_vars['text']['key']; ?>
" /><?php echo $this->_tpl_vars['text']['title']; ?>
<br /><small><?php echo TemplateText(array('value' => $this->_tpl_vars['text']['key']), $this);?>
</small></td>
			<td>
				<?php if ($this->_tpl_vars['customTextsHTML'][$this->_tpl_vars['text']['key']]): ?><div style="border: 1px solid #DDDDDD;background-color:#FFFFFF;"><?php endif; ?>
				<textarea onfocus="this.style.height='240px';" onblur="this.style.height='100px';" style="width:99%;height:<?php if ($this->_tpl_vars['customTextsHTML'][$this->_tpl_vars['text']['key']]): ?>350<?php else: ?>100<?php endif; ?>px;" name="text-<?php echo $this->_tpl_vars['text']['key']; ?>
" id="text-<?php echo $this->_tpl_vars['text']['key']; ?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['text']['text'],'allowEmpty' => true), $this);?>
</textarea>
				<?php if ($this->_tpl_vars['customTextsHTML'][$this->_tpl_vars['text']['key']]): ?>
				</div>
				<script language="javascript">
				<!--
					editors['<?php echo $this->_tpl_vars['text']['key']; ?>
'] = new htmlEditor('text-<?php echo $this->_tpl_vars['text']['key']; ?>
');
					editors['<?php echo $this->_tpl_vars['text']['key']; ?>
'].disableIntro = true;
					editors['<?php echo $this->_tpl_vars['text']['key']; ?>
'].init();
					registerLoadAction('editors[\'<?php echo $this->_tpl_vars['text']['key']; ?>
\'].start()');
				//-->
				</script>
				<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
	</table>
	
	<p align="right">
		<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
	</p>
	</form>
</fieldset>
<?php endif; ?>