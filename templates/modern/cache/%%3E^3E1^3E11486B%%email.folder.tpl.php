<?php /* Smarty version 2.6.28, created on 2020-09-29 11:27:05
         compiled from li/email.folder.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'li/email.folder.tpl', 16, false),array('function', 'hook', 'li/email.folder.tpl', 128, false),)), $this); ?>
<?php if ($this->_tpl_vars['enablePreview']): ?>
	<?php if ($this->_tpl_vars['narrow']): ?>
		<div id="hSep1">
			<div id="folderMailArea">
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "li/email.folder.contents.narrow.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			</div>
			<div id="folderLoading" style="display:none"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/load_32.gif" border="0" alt="" /></div>
		</div>
		<div id="hSepSep"></div>
		<div id="hSep2">
			<div class="scrollContainer withoutContentHeader" style="display:none;" id="previewArea"></div>
	
			<div class="scrollContainer withoutContentHeader" id="multiSelPreview">
				<div id="multiSelPreview_vCenter">
					<div id="multiSelPreview_inner">
						<div id="multiSelPreview_count"><?php echo TemplateLang(array('p' => 'nomailsselected'), $this);?>
</div>
					</div>
				</div>
			</div>
			
			<div id="previewLoading" style="display:none"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/load_32.gif" border="0" alt="" /></div>
		</div>
		
		<script language="javascript">
		<!--
			var folderNarrowView = true;
			registerLoadAction('initHSep()');
		//-->
		</script>
	<?php else: ?>
		<div id="vSep1">
			<div id="folderMailArea">
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "li/email.folder.contents.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			</div>
			<div id="folderLoading" style="display:none"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/load_32.gif" border="0" alt="" /></div>
		</div>
		<div id="vSepSep"></div>
		<div id="vSep2">
			<div class="scrollContainer withoutContentHeader" style="display:none;" id="previewArea"></div>
	
			<div class="scrollContainer withoutContentHeader" id="multiSelPreview">
				<div id="multiSelPreview_vCenter">
					<div id="multiSelPreview_inner">
						<div id="multiSelPreview_count"><?php echo TemplateLang(array('p' => 'nomailsselected'), $this);?>
</div>
					</div>
				</div>
			</div>
			
			<div id="previewLoading" style="display:none"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/load_32.gif" border="0" alt="" /></div>
		</div>
		
		<script language="javascript">
		<!--
			var folderNarrowView = false;
			registerLoadAction('initVSep()');
		//-->
		</script>
	<?php endif; ?>
<?php else: ?>
<div id="folderMailArea">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "li/email.folder.contents.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
<div id="folderLoading" style="display:none"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/load_32.gif" border="0" alt="" /></div>
<?php endif; ?>

<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/drag_email.png" style="display:none;" /><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/drag_emails.png" style="display:none;" />

<!-- mail menu -->
<div id="mailMenu" class="mailMenu" style="display:none;position:absolute;left:0px;top:0px;z-index:1000;" oncontextmenu="return(false);" onmousedown="if(event.button==2) return(false);">
	<a class="mailMenuItem" href="javascript:document.location.href='email.read.php?id='+currentID+'&sid='+currentSID;"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_read.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'mail_read'), $this);?>
</a>
	<a class="mailMenuItem" href="javascript:printMail(currentID, currentSID);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_print.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'print'), $this);?>
</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:document.location.href='email.compose.php?reply='+currentID+'&sid='+currentSID;"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_reply.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'reply'), $this);?>
</a>
	<a class="mailMenuItem" href="javascript:document.location.href='email.compose.php?forward='+currentID+'&sid='+currentSID;"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_forward.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'forward'), $this);?>
</a>
	<a class="mailMenuItem" href="javascript:document.location.href='email.compose.php?redirect='+currentID+'&sid='+currentSID;"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_redirect.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'redirect'), $this);?>
</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentID, 1, false);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_markread.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'markread'), $this);?>
</a>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentID, 1, true);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_markunread.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'markunread'), $this);?>
</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentID, 16, true);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_flagged.gif" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'mark'), $this);?>
</a>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentID, 16, false);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_empty.gif" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'unmark'), $this);?>
</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentID, 4096, true);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_done.gif" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'markdone'), $this);?>
</a>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentID, 4096, false);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_empty.gif" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'unmarkdone'), $this);?>
</a>
	<div class="mailMenuSep"></div>
	<div class="mailColorButtons">
		<span id="mailColorButton_0" onclick="javascript:folderColorMail(currentID, 0);"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/pixel.gif" /></span>
		<span id="mailColorButton_1" onclick="javascript:folderColorMail(currentID, 1);"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/pixel.gif" /></span>
		<span id="mailColorButton_2" onclick="javascript:folderColorMail(currentID, 2);"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/pixel.gif" /></span>
		<span id="mailColorButton_3" onclick="javascript:folderColorMail(currentID, 3);"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/pixel.gif" /></span>
		<span id="mailColorButton_4" onclick="javascript:folderColorMail(currentID, 4);"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/pixel.gif" /></span>
		<span id="mailColorButton_5" onclick="javascript:folderColorMail(currentID, 5);"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/pixel.gif" /></span>
		<span id="mailColorButton_6" onclick="javascript:folderColorMail(currentID, 6);"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/pixel.gif" /></span>
	</div>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:showMailSource(currentID);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_source.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'showsource'), $this);?>
</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:if(confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
')) deleteMail(currentID);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_delete.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'mail_del'), $this);?>
</a>
</div>

<!-- multi mail menu -->
<div id="multiMailMenu" class="mailMenu" style="display:none;position:absolute;left:0px;top:0px;z-index:1000;" oncontextmenu="return(false);" onmousedown="if(event.button==2) return(false);">
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentIDs, 1, false);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_markread.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'markread'), $this);?>
</a>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentIDs, 1, true);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_markunread.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'markunread'), $this);?>
</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentIDs, 16, true);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_flagged.gif" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'mark'), $this);?>
</a>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentIDs, 16, false);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_empty.gif" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'unmark'), $this);?>
</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentIDs, 4096, true);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_done.gif" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'markdone'), $this);?>
</a>
	<a class="mailMenuItem" href="javascript:folderFlagMail(currentIDs, 4096, false);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_empty.gif" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'unmarkdone'), $this);?>
</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:setMailSpamStatus(currentIDs, true, true);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_spam.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'markspam'), $this);?>
</a>
	<a class="mailMenuItem" href="javascript:setMailSpamStatus(currentIDs, false, true);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_empty.gif" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'marknonspam'), $this);?>
</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:if(confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
')) deleteMail(currentIDs);"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_delete.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'mails_del'), $this);?>
</a>
</div>

<!-- folder menu -->
<div id="folderMenu" class="mailMenu" style="display:none;position:absolute;left:0px;top:0px;">
	<a class="mailMenuItem" href="javascript:document.location.href='email.php?do=markAllAsRead&folder='+currentFolderID+'&sid=<?php echo $this->_tpl_vars['sid']; ?>
';"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_markread.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'markallasread'), $this);?>
</a>
	<a class="mailMenuItem" href="javascript:document.location.href='email.php?do=markAllAsRead&unread=true&folder='+currentFolderID+'&sid=<?php echo $this->_tpl_vars['sid']; ?>
';"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_markunread.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'markallasunread'), $this);?>
</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:document.location.href='email.php?do=downloadAll&folder='+currentFolderID+'&sid=<?php echo $this->_tpl_vars['sid']; ?>
';"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_download.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'downloadall'), $this);?>
</a>
	<div class="mailMenuSep"></div>
	<a class="mailMenuItem" href="javascript:void(0);" onclick="if(confirm('<?php echo TemplateLang(array('p' => 'realempty'), $this);?>
')) document.location.href='email.php?do=emptyFolder&folder='+currentFolderID+'&sid=<?php echo $this->_tpl_vars['sid']; ?>
';"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/folder_empty.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'emptyfolder'), $this);?>
</a>
</div>

<?php echo TemplateHook(array('id' => "email.folder.tpl:foot"), $this);?>


<script language="javascript">
<!--
	var currentFolderID = <?php echo $this->_tpl_vars['folderID']; ?>
;
<?php if ($this->_tpl_vars['refreshEnabled'] && $this->_tpl_vars['refreshInterval'] > 0): ?>
	initFolderRefresh(<?php echo $this->_tpl_vars['folderID']; ?>
, <?php echo $this->_tpl_vars['refreshInterval']; ?>
);
<?php endif; ?>
<?php if ($this->_tpl_vars['hotkeys']): ?>
	registerLoadAction('registerFolderHotkeyHandler()');
<?php endif; ?>
//-->
</script>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "li/email.addressmenu.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>