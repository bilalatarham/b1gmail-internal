<div class="innerWidget">
	<fieldset>
		<legend>{lng p="email"}</legend>
		<a href="email.php?sid={$sid}"><img src="{$tpldir}images/li/menu_ico_inbox.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="inbox"}</a><br />
		<a href="email.compose.php?sid={$sid}"><img src="{$tpldir}images/li/send_mail.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="sendmail"}</a><br />
		<a href="email.folders.php?sid={$sid}"><img src="{$tpldir}images/li/menu_ico_folder.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="folderadmin"}</a><br />
	</fieldset>
	<fieldset>
		<legend>{lng p="organizer"}</legend>
		<a href="organizer.php?sid={$sid}"><img src="{$tpldir}images/li/ico_overview.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="overview"}</a><br />
		<a href="organizer.calendar.php?sid={$sid}"><img src="{$tpldir}images/li/ico_calendar.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="calendar"}</a><br />
		<a href="organizer.todo.php?sid={$sid}"><img src="{$tpldir}images/li/ico_todo.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="tasks"}</a><br />
		<a href="organizer.addressbook.php?sid={$sid}"><img src="{$tpldir}images/li/ico_addressbook.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="addressbook"}</a><br />
		<a href="organizer.notes.php?sid={$sid}"><img src="{$tpldir}images/li/ico_notes.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="notes"}</a><br />
	</fieldset>
	{if $pageTabs.webdisk}<fieldset>
		<legend>{lng p="webdisk"}</legend>
		<a href="webdisk.php?sid={$sid}"><img src="{$tpldir}images/li/ico_share.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="webdisk"}</a><br />
		<a href="webdisk.php?sid={$sid}&do=uploadFilesForm"><img src="{$tpldir}images/li/webdisk_file.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="uploadfiles"}</a><br />
	</fieldset>{/if}
	<fieldset>
		<legend>{lng p="misc"}</legend>
		<a href="prefs.php?sid={$sid}"><img src="{$tpldir}images/li/ico_common.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="prefs"}</a><br />
		<a href="start.php?sid={$sid}&action=logout"><img src="{$tpldir}images/li/ico_logout.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										{lng p="logout"}</a><br />
	</fieldset>
</div>