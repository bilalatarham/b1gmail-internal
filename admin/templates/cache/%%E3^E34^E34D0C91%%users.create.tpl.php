<?php /* Smarty version 2.6.28, created on 2020-10-01 11:21:59
         compiled from users.create.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'users.create.tpl', 6, false),array('function', 'text', 'users.create.tpl', 11, false),array('function', 'domain', 'users.create.tpl', 14, false),array('function', 'html_select_date', 'users.create.tpl', 90, false),)), $this); ?>
<form method="post" action="users.php?action=create&create=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onsubmit="spin(this)">
	<table width="100%">
		<tr>
			<td width="50%" valign="top">
				<fieldset>
					<legend><?php echo TemplateLang(array('p' => 'profile'), $this);?>
</legend>
					
					<table width="100%">
						<tr>
							<td class="td1" width="115"><?php echo TemplateLang(array('p' => 'email'), $this);?>
:</td>
							<td class="td2"><input type="text" name="email" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['email'],'allowEmpty' => true), $this);?>
" style="width:40%;" />
											<select name="emailDomain">
											<?php $_from = $this->_tpl_vars['domainList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['domain']):
?>
												<option value="<?php echo $this->_tpl_vars['domain']; ?>
">@<?php echo TemplateDomain(array('value' => $this->_tpl_vars['domain']), $this);?>
</option>
											<?php endforeach; endif; unset($_from); ?>
											</select></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'salutation'), $this);?>
:</td>
							<td class="td2"><select name="anrede">
									<option value="">&nbsp;</option>
									<option value="herr"><?php echo TemplateLang(array('p' => 'mr'), $this);?>
</option>
									<option value="frau"><?php echo TemplateLang(array('p' => 'mrs'), $this);?>
</option>
								</select></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'firstname'), $this);?>
:</td>
							<td class="td2"><input type="text" name="vorname" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['vorname'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'lastname'), $this);?>
:</td>
							<td class="td2"><input type="text" name="nachname" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['nachname'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'streetno'), $this);?>
:</td>
							<td class="td2"><input type="text" name="strasse" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['strasse'],'allowEmpty' => true), $this);?>
" style="width:55%;" />
											<input type="text" name="hnr" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['hnr'],'allowEmpty' => true), $this);?>
" style="width:15%;" /></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'zipcity'), $this);?>
:</td>
							<td class="td2"><input type="text" name="plz" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['plz'],'allowEmpty' => true), $this);?>
" style="width:20%;" />
											<input type="text" name="ort" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['ort'],'allowEmpty' => true), $this);?>
" style="width:50%;" /></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'country'), $this);?>
:</td>
							<td class="td2"><select name="land">
							<?php $_from = $this->_tpl_vars['countries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['countryID'] => $this->_tpl_vars['countryName']):
?>
								<option value="<?php echo $this->_tpl_vars['countryID']; ?>
"<?php if ($this->_tpl_vars['countryID'] == $this->_tpl_vars['defaultCountry']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['countryName']), $this);?>
</option>
							<?php endforeach; endif; unset($_from); ?>
							</select></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'tel'), $this);?>
:</td>
							<td class="td2"><input type="text" name="tel" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['tel'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'fax'), $this);?>
:</td>
							<td class="td2"><input type="text" name="fax" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['fax'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'cellphone'), $this);?>
:</td>
							<td class="td2"><input type="text" name="mail2sms_nummer" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['mail2sms_nummer'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'altmail'), $this);?>
:</td>
							<td class="td2"><input type="text" name="altmail" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['altmail'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
						</tr>
						
						<?php $_from = $this->_tpl_vars['profileFields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['profileField']):
?>
						<?php $this->assign('fieldID', $this->_tpl_vars['profileField']['id']); ?>
						<tr>
							<td class="td1"><?php echo $this->_tpl_vars['profileField']['title']; ?>
:</td>
							<td class="td2">
								<?php if ($this->_tpl_vars['profileField']['type'] == 1): ?>
									<input type="text" name="field_<?php echo $this->_tpl_vars['profileField']['id']; ?>
" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['profileField']['value'],'allowEmpty' => true), $this);?>
" style="width:85%;" />
								<?php elseif ($this->_tpl_vars['profileField']['type'] == 2): ?>
									<input type="checkbox" name="field_<?php echo $this->_tpl_vars['profileField']['id']; ?>
"<?php if ($this->_tpl_vars['profileField']['value']): ?> checked="checked"<?php endif; ?> />
								<?php elseif ($this->_tpl_vars['profileField']['type'] == 4): ?>
									<select name="field_<?php echo $this->_tpl_vars['profileField']['id']; ?>
">
									<?php $_from = $this->_tpl_vars['profileField']['extra']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
										<option value="<?php echo TemplateText(array('value' => $this->_tpl_vars['item'],'allowEmpty' => true), $this);?>
"<?php if ($this->_tpl_vars['profileField']['value'] == $this->_tpl_vars['item']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['item'],'allowEmpty' => true), $this);?>
</option>
									<?php endforeach; endif; unset($_from); ?>
									</select>
								<?php elseif ($this->_tpl_vars['profileField']['type'] == 8): ?>
									<?php $_from = $this->_tpl_vars['profileField']['extra']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
										<input type="radio" id="field_<?php echo $this->_tpl_vars['profileField']['id']; ?>
_<?php echo $this->_tpl_vars['item']; ?>
" name="field_<?php echo $this->_tpl_vars['profileField']['id']; ?>
" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['item'],'allowEmpty' => true), $this);?>
"<?php if ($this->_tpl_vars['profileField']['value'] == $this->_tpl_vars['item']): ?> checked="checked"<?php endif; ?> />
										<label for="field_<?php echo $this->_tpl_vars['profileField']['id']; ?>
_<?php echo $this->_tpl_vars['item']; ?>
"><b><?php echo $this->_tpl_vars['item']; ?>
</b></label> &nbsp;
									<?php endforeach; endif; unset($_from); ?>
								<?php elseif ($this->_tpl_vars['profileField']['type'] == 32): ?>
									<?php echo smarty_function_html_select_date(array('time' => "---",'year_empty' => "---",'day_empty' => "---",'month_empty' => "---",'start_year' => "-120",'end_year' => "+0",'prefix' => "field_".($this->_tpl_vars['fieldID']),'field_order' => 'DMY'), $this);?>

								<?php endif; ?>
							</td>
						</tr>
						<?php endforeach; endif; unset($_from); ?>
					</table>
				</fieldset>
			</td>
			
			<td width="50%" valign="top">
				<fieldset>
					<legend><?php echo TemplateLang(array('p' => 'common'), $this);?>
</legend>
					
					<table width="100%">
						<tr>
							<td class="td1" width="115"><?php echo TemplateLang(array('p' => 'group'), $this);?>
:</td>
							<td class="td2"><select name="gruppe">
							<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['groupItem']):
?>
								<option value="<?php echo $this->_tpl_vars['groupItem']['id']; ?>
"<?php if ($this->_tpl_vars['groupItem']['id'] == $this->_tpl_vars['defaultGroup']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['groupItem']['title']), $this);?>
</option>
							<?php endforeach; endif; unset($_from); ?>
							</select></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'status'), $this);?>
:</td>
							<td class="td2"><select name="gesperrt">
								<option value="no"<?php if ($this->_tpl_vars['user']['gesperrt'] == 'no'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'active'), $this);?>
</option>
								<option value="yes"<?php if ($this->_tpl_vars['user']['gesperrt'] == 'yes'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'locked'), $this);?>
</option>
								<option value="locked"<?php if ($this->_tpl_vars['user']['gesperrt'] == 'locked'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'notactivated'), $this);?>
</option>
								<option value="delete"<?php if ($this->_tpl_vars['user']['gesperrt'] == 'delete'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'deleted'), $this);?>
</option>
							</select></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'password'), $this);?>
:</td>
							<td class="td2"><input type="text" name="passwort" value="" style="width:85%;" /></td>
						</tr>
					</table>
				</fieldset>
				
				<fieldset>
					<legend><?php echo TemplateLang(array('p' => 'notes'), $this);?>
</legend>
					<textarea style="width:100%;height:80px;" name="notes"><?php echo TemplateText(array('value' => $this->_tpl_vars['user']['notes'],'allowEmpty' => true), $this);?>
</textarea>
				</fieldset>
			</td>
		</tr>
	</table>
				
	<p>
		<div style="float:right;" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'create'), $this);?>
 " />
		</div>
	</p>
</form>