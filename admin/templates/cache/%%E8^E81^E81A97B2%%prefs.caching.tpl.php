<?php /* Smarty version 2.6.28, created on 2020-09-29 11:35:57
         compiled from prefs.caching.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.caching.tpl', 3, false),array('function', 'text', 'prefs.caching.tpl', 74, false),)), $this); ?>
<form action="prefs.common.php?action=caching&save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'caching'), $this);?>
</legend>
		
		<table>
			<tr>
				<td width="40" valign="top" rowspan="2"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_cache.png" border="0" alt="" width="32" height="32" /></td>
				<td><?php echo TemplateLang(array('p' => 'cachemanager'), $this);?>
:</td>
				<td><?php echo TemplateLang(array('p' => 'prefs'), $this);?>
:</td>
			</tr>
			<tr>
				<td valign="top" width="35%">
					<table>
						<tr>
							<td valign="top" width="20" align="center"><input type="radio" id="cache_disable" name="cache_type" value="0"<?php if ($this->_tpl_vars['bm_prefs']['cache_type'] == 0): ?> checked="checked"<?php endif; ?> onchange="cachePrefs()" /></td>
							<td><label for="cache_disable"><b><?php echo TemplateLang(array('p' => 'ce_disable'), $this);?>
</b></label><br />
								<?php echo TemplateLang(array('p' => 'ce_disable_desc'), $this);?>
</td>
						</tr>
						<tr>
							<td colspan="3">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td valign="top" width="20" align="center"><input type="radio" id="cache_b1gmail" name="cache_type" value="1"<?php if ($this->_tpl_vars['bm_prefs']['cache_type'] == 1): ?> checked="checked"<?php endif; ?> onchange="cachePrefs()" /></td>
							<td><label for="cache_b1gmail"><b><?php echo TemplateLang(array('p' => 'ce_b1gmail'), $this);?>
</b></label><br />
								<?php echo TemplateLang(array('p' => 'ce_b1gmail_desc'), $this);?>
</td>
						</tr>
						<tr>
							<td colspan="3">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td valign="top" width="20" align="center"><input type="radio" id="cache_memcache" name="cache_type" value="2"<?php if ($this->_tpl_vars['bm_prefs']['cache_type'] == 2): ?> checked="checked"<?php endif; ?><?php if (! $this->_tpl_vars['memcache']): ?> disabled="disabled"<?php endif; ?> onchange="cachePrefs()" /></td>
							<td><label for="cache_memcache"><b><?php echo TemplateLang(array('p' => 'ce_memcache'), $this);?>
</b></label><br />
								<?php echo TemplateLang(array('p' => 'ce_memcache_desc'), $this);?>
</td>
						</tr>
					</table>
				</td>
				<td valign="top">
					<div id="prefs_0" style="display:<?php if ($this->_tpl_vars['bm_prefs']['cache_type'] != 0): ?>none<?php endif; ?>;">
						<i>(<?php echo TemplateLang(array('p' => 'none'), $this);?>
)</i>
					</div>
					
					<div id="prefs_3" style="display:<?php if ($this->_tpl_vars['bm_prefs']['cache_type'] == 0): ?>none<?php endif; ?>;">
						<table>
							<tr>
								<td class="td1" width="180"><?php echo TemplateLang(array('p' => 'parseonly'), $this);?>
?</td>
								<td class="td2"><input type="checkbox" name="cache_parseonly"<?php if ($this->_tpl_vars['bm_prefs']['cache_parseonly'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
							</tr>
						</table>
					</div>
					
					<div id="prefs_1" style="display:<?php if ($this->_tpl_vars['bm_prefs']['cache_type'] != 1): ?>none<?php endif; ?>;">
						<table>
							<tr>
								<td class="td1" width="180"><?php echo TemplateLang(array('p' => 'cachesize'), $this);?>
:</td>
								<td class="td2"><input type="text" name="filecache_size" value="<?php echo $this->_tpl_vars['bm_prefs']['filecache_size']/1024/1024; ?>
" size="6" />
												MB <!--<small>(<?php echo TemplateLang(array('p' => 'inactiveonly'), $this);?>
)</small>--></td>
							</tr>
						</table>
					</div>
					
					<div id="prefs_2" style="display:<?php if ($this->_tpl_vars['bm_prefs']['cache_type'] != 2): ?>none<?php endif; ?>;">
						<table>
							<tr>
								<td class="td1" width="180"><?php echo TemplateLang(array('p' => 'persistent'), $this);?>
?</td>
								<td class="td2"><input type="checkbox" name="memcache_persistent"<?php if ($this->_tpl_vars['bm_prefs']['memcache_persistent'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
							</tr>
							<tr>
								<td class="td1" width="180"><?php echo TemplateLang(array('p' => 'servers'), $this);?>
:</td>
								<td class="td2">
									<textarea style="width:100%;height:80px;" name="memcache_servers"><?php echo TemplateText(array('value' => $this->_tpl_vars['bm_prefs']['memcache_servers'],'allowEmpty' => true), $this);?>
</textarea>
									<small><?php echo TemplateLang(array('p' => 'memcachesepby'), $this);?>
</small>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</fieldset>
	
	<p>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>
	