<?php /* Smarty version 2.6.28, created on 2020-09-30 13:00:06
         compiled from users.edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'users.edit.tpl', 15, false),array('function', 'email', 'users.edit.tpl', 20, false),array('function', 'text', 'users.edit.tpl', 32, false),array('function', 'html_select_date', 'users.edit.tpl', 95, false),array('function', 'progressBar', 'users.edit.tpl', 172, false),array('function', 'size', 'users.edit.tpl', 173, false),array('function', 'date', 'users.edit.tpl', 225, false),array('function', 'cycle', 'users.edit.tpl', 325, false),)), $this); ?>
<form method="post" action="users.php?do=edit&id=<?php echo $this->_tpl_vars['user']['id']; ?>
&save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onsubmit="spin(this)">

<?php if ($this->_tpl_vars['msg']): ?>
<center style="margin:1em;">
	<div class="note">
		<?php echo $this->_tpl_vars['msg']; ?>

	</div>
</center>
<?php endif; ?>

<table width="100%" cellspacing="2" cellpadding="0">
	<tr>
		<td valign="top" width="50%">
			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'profile'), $this);?>
</legend>

				<table width="100%">
					<tr>
						<td class="td1" width="160"><?php echo TemplateLang(array('p' => 'email'), $this);?>
:</td>
						<td class="td2"><input type="text" name="email" value="<?php echo TemplateEMail(array('value' => $this->_tpl_vars['user']['email']), $this);?>
" style="width:85%;" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'salutation'), $this);?>
:</td>
						<td class="td2"><select name="anrede">
								<option value="">&nbsp;</option>
								<option value="herr"<?php if ($this->_tpl_vars['user']['anrede'] == 'herr'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'mr'), $this);?>
</option>
								<option value="frau"<?php if ($this->_tpl_vars['user']['anrede'] == 'frau'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'mrs'), $this);?>
</option>
							</select></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'firstname'), $this);?>
:</td>
						<td class="td2"><input type="text" name="vorname" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['vorname'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'lastname'), $this);?>
:</td>
						<td class="td2"><input type="text" name="nachname" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['nachname'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'streetno'), $this);?>
:</td>
						<td class="td2"><input type="text" name="strasse" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['strasse'],'allowEmpty' => true), $this);?>
" style="width:55%;" />
										<input type="text" name="hnr" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['hnr'],'allowEmpty' => true), $this);?>
" style="width:15%;" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'zipcity'), $this);?>
:</td>
						<td class="td2"><input type="text" name="plz" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['plz'],'allowEmpty' => true), $this);?>
" style="width:20%;" />
										<input type="text" name="ort" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['ort'],'allowEmpty' => true), $this);?>
" style="width:50%;" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'country'), $this);?>
:</td>
						<td class="td2"><select name="land">
						<?php $_from = $this->_tpl_vars['countries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['countryID'] => $this->_tpl_vars['countryName']):
?>
							<option value="<?php echo $this->_tpl_vars['countryID']; ?>
"<?php if ($this->_tpl_vars['countryID'] == $this->_tpl_vars['user']['land']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['countryName']), $this);?>
</option>
						<?php endforeach; endif; unset($_from); ?>
						</select></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'tel'), $this);?>
:</td>
						<td class="td2"><input type="text" name="tel" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['tel'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'fax'), $this);?>
:</td>
						<td class="td2"><input type="text" name="fax" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['fax'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'cellphone'), $this);?>
:</td>
						<td class="td2"><input type="text" name="mail2sms_nummer" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['mail2sms_nummer'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'altmail'), $this);?>
:</td>
						<td class="td2"><input type="text" name="altmail" value="<?php echo TemplateEMail(array('value' => $this->_tpl_vars['user']['altmail']), $this);?>
" style="width:85%;" /></td>
					</tr>

					<?php $_from = $this->_tpl_vars['profileFields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['profileField']):
?>
					<?php $this->assign('fieldID', $this->_tpl_vars['profileField']['id']); ?>
					<tr>
						<td class="td1"><?php echo $this->_tpl_vars['profileField']['title']; ?>
:</td>
						<td class="td2">
							<?php if ($this->_tpl_vars['profileField']['type'] == 1): ?>
								<input type="text" name="field_<?php echo $this->_tpl_vars['profileField']['id']; ?>
" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['profileField']['value'],'allowEmpty' => true), $this);?>
" style="width:85%;" />
							<?php elseif ($this->_tpl_vars['profileField']['type'] == 2): ?>
								<input type="checkbox" name="field_<?php echo $this->_tpl_vars['profileField']['id']; ?>
"<?php if ($this->_tpl_vars['profileField']['value']): ?> checked="checked"<?php endif; ?> />
							<?php elseif ($this->_tpl_vars['profileField']['type'] == 4): ?>
								<select name="field_<?php echo $this->_tpl_vars['profileField']['id']; ?>
">
								<?php $_from = $this->_tpl_vars['profileField']['extra']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									<option value="<?php echo TemplateText(array('value' => $this->_tpl_vars['item'],'allowEmpty' => true), $this);?>
"<?php if ($this->_tpl_vars['profileField']['value'] == $this->_tpl_vars['item']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['item'],'allowEmpty' => true), $this);?>
</option>
								<?php endforeach; endif; unset($_from); ?>
								</select>
							<?php elseif ($this->_tpl_vars['profileField']['type'] == 8): ?>
								<?php $_from = $this->_tpl_vars['profileField']['extra']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									<input type="radio" id="field_<?php echo $this->_tpl_vars['profileField']['id']; ?>
_<?php echo $this->_tpl_vars['item']; ?>
" name="field_<?php echo $this->_tpl_vars['profileField']['id']; ?>
" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['item'],'allowEmpty' => true), $this);?>
"<?php if ($this->_tpl_vars['profileField']['value'] == $this->_tpl_vars['item']): ?> checked="checked"<?php endif; ?> />
									<label for="field_<?php echo $this->_tpl_vars['profileField']['id']; ?>
_<?php echo $this->_tpl_vars['item']; ?>
"><b><?php echo $this->_tpl_vars['item']; ?>
</b></label> &nbsp;
								<?php endforeach; endif; unset($_from); ?>
							<?php elseif ($this->_tpl_vars['profileField']['type'] == 32): ?>
								<?php if ($this->_tpl_vars['profileField']['value']): ?>
								<?php echo smarty_function_html_select_date(array('time' => $this->_tpl_vars['profileField']['value'],'year_empty' => "---",'day_empty' => "---",'month_empty' => "---",'start_year' => "-120",'end_year' => "+0",'prefix' => "field_".($this->_tpl_vars['fieldID']),'field_order' => 'DMY'), $this);?>

								<?php else: ?>
								<?php echo smarty_function_html_select_date(array('time' => "---",'year_empty' => "---",'day_empty' => "---",'month_empty' => "---",'start_year' => "-120",'end_year' => "+0",'prefix' => "field_".($this->_tpl_vars['fieldID']),'field_order' => 'DMY'), $this);?>

								<?php endif; ?>
							<?php endif; ?>
						</td>
					</tr>
					<?php endforeach; endif; unset($_from); ?>
				</table>
				<?php if ($this->_tpl_vars['historyCount']): ?>
				<br /><div class="note">
					<?php echo $this->_tpl_vars['historyCount']; ?>
 <?php echo TemplateLang(array('p' => 'oldcontacts'), $this);?>

					<a href="users.php?do=contactHistory&id=<?php echo $this->_tpl_vars['user']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateLang(array('p' => 'show'), $this);?>
 &raquo;</a>
				</div>
				<?php endif; ?>
			</fieldset>

			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'common'), $this);?>
</legend>

				<table width="100%">
					<tr>
						<td class="td1" width="160"><?php echo TemplateLang(array('p' => 'group'), $this);?>
:</td>
						<td class="td2"><select name="gruppe">
						<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['groupItem']):
?>
							<option value="<?php echo $this->_tpl_vars['groupItem']['id']; ?>
"<?php if ($this->_tpl_vars['groupItem']['id'] == $this->_tpl_vars['group']['id']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['groupItem']['title']), $this);?>
</option>
						<?php endforeach; endif; unset($_from); ?>
						</select></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'status'), $this);?>
:</td>
						<td class="td2"><select name="gesperrt">
							<option value="no"<?php if ($this->_tpl_vars['user']['gesperrt'] == 'no'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'active'), $this);?>
</option>
							<option value="yes"<?php if ($this->_tpl_vars['user']['gesperrt'] == 'yes'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'locked'), $this);?>
</option>
							<option value="locked"<?php if ($this->_tpl_vars['user']['gesperrt'] == 'locked'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'notactivated'), $this);?>
</option>
							<option value="delete"<?php if ($this->_tpl_vars['user']['gesperrt'] == 'delete'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'deleted'), $this);?>
</option>
						</select></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'assets'), $this);?>
:</td>
						<td class="td2"><a href="users.php?do=transactions&id=<?php echo $this->_tpl_vars['user']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo $this->_tpl_vars['staticBalance']; ?>
 <?php echo TemplateLang(array('p' => 'credits'), $this);?>
</a></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'newpassword'), $this);?>
:</td>
						<td class="td2"><input type="text" name="passwort" value="" style="width:85%;" /></td>
					</tr>
				</table>
			</fieldset>

			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'addservices'), $this);?>
</legend>

				<table width="100%">
					<tr>
						<td class="td1" width="160"><?php echo TemplateLang(array('p' => 'mailspace_add'), $this);?>
:</td>
						<td class="td2"><input type="text" name="mailspace_add" value="<?php echo $this->_tpl_vars['user']['mailspace_add']/1024/1024; ?>
" size="8" /> MB</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'diskspace_add'), $this);?>
:</td>
						<td class="td2"><input type="text" name="diskspace_add" value="<?php echo $this->_tpl_vars['user']['diskspace_add']/1024/1024; ?>
" size="8" /> MB</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'traffic_add'), $this);?>
:</td>
						<td class="td2"><input type="text" name="traffic_add" value="<?php echo $this->_tpl_vars['user']['traffic_add']/1024/1024; ?>
" size="8" /> MB</td>
					</tr>
				</table>
			</fieldset>
		</td>
		<td valign="top">
			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'usage'), $this);?>
</legend>

				<table>
					<tr>
						<td class="td1" width="160"><?php echo TemplateLang(array('p' => 'email'), $this);?>
:</td>
						<td class="td2">
							<?php echo $this->_tpl_vars['emailMails']; ?>
 <?php echo TemplateLang(array('p' => 'emails'), $this);?>
, <?php echo $this->_tpl_vars['emailFolders']; ?>
 <?php echo TemplateLang(array('p' => 'folders'), $this);?>

							<?php echo TemplateProgressBar(array('value' => $this->_tpl_vars['user']['mailspace_used'],'max' => $this->_tpl_vars['group']['storage'],'width' => 200), $this);?>

							<small><?php echo TemplateSize(array('bytes' => $this->_tpl_vars['user']['mailspace_used']), $this);?>
 / <?php echo TemplateSize(array('bytes' => $this->_tpl_vars['group']['storage']), $this);?>
 <?php echo TemplateLang(array('p' => 'used'), $this);?>
</small>
						</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'webdisk'), $this);?>
:</td>
						<td class="td2">
							<?php echo $this->_tpl_vars['diskFiles']; ?>
 <?php echo TemplateLang(array('p' => 'files'), $this);?>
, <?php echo $this->_tpl_vars['diskFolders']; ?>
 <?php echo TemplateLang(array('p' => 'folders'), $this);?>

							<?php echo TemplateProgressBar(array('value' => $this->_tpl_vars['user']['diskspace_used'],'max' => $this->_tpl_vars['group']['webdisk'],'width' => 200), $this);?>

							<small><?php echo TemplateSize(array('bytes' => $this->_tpl_vars['user']['diskspace_used']), $this);?>
 / <?php echo TemplateSize(array('bytes' => $this->_tpl_vars['group']['webdisk']), $this);?>
 <?php echo TemplateLang(array('p' => 'used'), $this);?>
</small>
						</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'wdtraffic'), $this);?>
:</td>
						<td class="td2">
							<?php if ($this->_tpl_vars['group']['traffic'] > 0): ?><?php echo TemplateProgressBar(array('value' => $this->_tpl_vars['user']['traffic_down']+$this->_tpl_vars['user']['traffic_up'],'max' => $this->_tpl_vars['group']['traffic'],'width' => 200), $this);?>
<?php endif; ?>
							<small><?php echo TemplateSize(array('bytes' => $this->_tpl_vars['user']['traffic_down']+$this->_tpl_vars['user']['traffic_up']), $this);?>
<?php if ($this->_tpl_vars['group']['traffic'] > 0): ?> / <?php echo TemplateSize(array('bytes' => $this->_tpl_vars['group']['traffic']), $this);?>
<?php endif; ?> <?php echo TemplateLang(array('p' => 'used2'), $this);?>
</small>
						</td>
					</tr>

					<?php if ($this->_tpl_vars['group']['sms_monat'] > 0): ?>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'monthasset'), $this);?>
:</td>
						<td class="td2">
							<?php echo TemplateProgressBar(array('value' => $this->_tpl_vars['usedMonthSMS'],'max' => $this->_tpl_vars['group']['sms_monat'],'width' => 200), $this);?>

							<small><?php echo $this->_tpl_vars['usedMonthSMS']; ?>
 / <?php echo $this->_tpl_vars['group']['sms_monat']; ?>
 <?php echo TemplateLang(array('p' => 'credits'), $this);?>
 <?php echo TemplateLang(array('p' => 'used2'), $this);?>
</small>
						</td>
					</tr>
					<?php endif; ?>

					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'abuseprotect'), $this);?>
:</td>
						<td class="td2">
							<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
/images/indicator_<?php echo $this->_tpl_vars['abuseIndicator']; ?>
.png" alt="" border="0" align="absmiddle" />
								<a href="abuse.php?do=show&userid=<?php echo $this->_tpl_vars['user']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
">
								<?php if ($this->_tpl_vars['abuseIndicator'] != 'grey'): ?>
									<?php echo $this->_tpl_vars['abusePoints']; ?>

									<?php echo TemplateLang(array('p' => 'points'), $this);?>

								<?php else: ?>
									(<?php echo TemplateLang(array('p' => 'disabled'), $this);?>
)
								<?php endif; ?>
								</a>
						</td>
					</tr>
				</table>
			</fieldset>

			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'misc'), $this);?>
</legend>

				<table>
					<tr>
						<td class="td1" width="160"><?php echo TemplateLang(array('p' => 'lastlogin'), $this);?>
:</td>
						<td class="td2"><?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['user']['lastlogin'],'nice' => true,'nozero' => true), $this);?>
</td>
					</tr>
					<tr>
						<td class="td1"><small><?php echo TemplateLang(array('p' => 'ip'), $this);?>
:</small></td>
						<td class="td2"><small><?php echo TemplateText(array('value' => $this->_tpl_vars['user']['ip']), $this);?>
</small></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'regdate'), $this);?>
:</td>
						<td class="td2"><?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['user']['reg_date'],'nice' => true,'nozero' => true), $this);?>
</td>
					</tr>
					<tr>
						<td class="td1"><small><?php echo TemplateLang(array('p' => 'ip'), $this);?>
:</small></td>
						<td class="td2"><small><?php echo TemplateText(array('value' => $this->_tpl_vars['user']['reg_ip']), $this);?>
</small></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'lastpop3'), $this);?>
:</td>
						<td class="td2"><?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['user']['last_pop3'],'nice' => true,'nozero' => true), $this);?>
</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'lastsmtp'), $this);?>
:</td>
						<td class="td2"><?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['user']['last_smtp'],'nice' => true,'nozero' => true), $this);?>
</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'lastimap'), $this);?>
:</td>
						<td class="td2"><?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['user']['last_imap'],'nice' => true,'nozero' => true), $this);?>
</td>
					</tr>
				</table>
			</fieldset>

			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'notes'), $this);?>
</legend>
				<textarea style="width:100%;height:150px;" name="notes"><?php echo TemplateText(array('value' => $this->_tpl_vars['user']['notes'],'allowEmpty' => true), $this);?>
</textarea>
			</fieldset>

			<fieldset class="collapsed">
				<legend><a href="javascript:;" onclick="toggleFieldset(this)"><?php echo TemplateLang(array('p' => 'prefs'), $this);?>
</a></legend>
				<div class="content">
					<table width="100%">
						<tr>
							<td class="td1" width="160"><?php echo TemplateLang(array('p' => 're'), $this);?>
/<?php echo TemplateLang(array('p' => 'fwd'), $this);?>
:</td>
							<td class="td2"><input type="text" name="re" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['re'],'allowEmpty' => true), $this);?>
" style="width:35%;" />
											<input type="text" name="fwd" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['fwd'],'allowEmpty' => true), $this);?>
" style="width:35%;" /></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'mail2sms'), $this);?>
:</td>
							<td class="td2"><select name="mail2sms">
								<option value="yes"<?php if ($this->_tpl_vars['user']['mail2sms'] == 'yes'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'yes'), $this);?>
</option>
								<option value="no"<?php if ($this->_tpl_vars['user']['mail2sms'] == 'no'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'no'), $this);?>
</option>
							</select></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'forward'), $this);?>
:</td>
							<td class="td2"><select name="forward">
								<option value="yes"<?php if ($this->_tpl_vars['user']['forward'] == 'yes'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'yes'), $this);?>
</option>
								<option value="no"<?php if ($this->_tpl_vars['user']['forward'] == 'no'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'no'), $this);?>
</option>
							</select></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'forwardto'), $this);?>
:</td>
							<td class="td2"><input type="text" name="forward_to" value="<?php echo TemplateEMail(array('value' => $this->_tpl_vars['user']['forward_to']), $this);?>
" style="width:85%;" /></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'newsletter'), $this);?>
:</td>
							<td class="td2"><select name="newsletter_optin">
								<option value="yes"<?php if ($this->_tpl_vars['user']['newsletter_optin'] == 'yes'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'yes'), $this);?>
</option>
								<option value="no"<?php if ($this->_tpl_vars['user']['newsletter_optin'] == 'no'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'no'), $this);?>
</option>
							</select></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'dateformat'), $this);?>
:</td>
							<td class="td2"><input type="text" name="datumsformat" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['datumsformat'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
						</tr>
						<tr>
							<td class="td1"><?php echo TemplateLang(array('p' => 'sendername'), $this);?>
:</td>
							<td class="td2"><input type="text" name="absendername" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['user']['absendername'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
						</tr>
					</table>
				</div>
			</fieldset>

			<fieldset class="collapsed">
				<legend><a href="javascript:;" onclick="toggleFieldset(this)"><?php echo TemplateLang(array('p' => 'aliasdomains'), $this);?>
</a></legend>
				<div class="content">
					<textarea style="width:100%;height:80px;" name="saliase"><?php echo TemplateText(array('value' => $this->_tpl_vars['user']['saliase'],'allowEmpty' => true), $this);?>
</textarea>
					<small><?php echo TemplateLang(array('p' => 'sepby'), $this);?>
</small>
				</div>
			</fieldset>

			<fieldset class="<?php if ($this->_tpl_vars['showAliases']): ?>un<?php endif; ?>collapsed">
				<legend><a href="javascript:;" onclick="toggleFieldset(this)"><?php echo TemplateLang(array('p' => 'aliases'), $this);?>
</a></legend>
				<div class="content">
					<table class="list">
						<tr>
							<th width="20">&nbsp;</th>
							<th><?php echo TemplateLang(array('p' => 'alias'), $this);?>
</th>
							<th width="130"><?php echo TemplateLang(array('p' => 'type'), $this);?>
</th>
							<th width="28">&nbsp;</th>
						</tr>

						<?php $_from = $this->_tpl_vars['aliases']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['alias']):
?>
						<?php echo smarty_function_cycle(array('values' => "td1,td2",'name' => 'class','assign' => 'class'), $this);?>

						<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
							<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/alias.png" border="0" alt="" width="16" height="16" /></td>
							<td><?php echo TemplateEMail(array('value' => $this->_tpl_vars['alias']['email'],'cut' => 30), $this);?>
</td>
							<td><?php echo $this->_tpl_vars['alias']['type']; ?>
</td>
							<td><a href="users.php?do=edit&id=<?php echo $this->_tpl_vars['user']['id']; ?>
&deleteAlias=<?php echo $this->_tpl_vars['alias']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onclick="return confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/delete.png" border="0" alt="<?php echo TemplateLang(array('p' => 'delete'), $this);?>
" width="16" height="16" /></a></td>
						</tr>
						<?php endforeach; endif; unset($_from); ?>
					</table>
				</div>
			</fieldset>

			<fieldset class="<?php if ($this->_tpl_vars['showPayments']): ?>un<?php endif; ?>collapsed">
				<legend><a href="javascript:;" onclick="toggleFieldset(this)"><?php echo TemplateLang(array('p' => 'payments'), $this);?>
</a> (<?php echo TemplateLang(array('p' => 'max'), $this);?>
 15)</legend>
				<div class="content">
					<table class="list">
						<tr>
							<th width="20">&nbsp;</th>
							<th><?php echo TemplateLang(array('p' => 'orderno'), $this);?>
</th>
							<th width="135"><?php echo TemplateLang(array('p' => 'amount'), $this);?>
</th>
							<th width="145"><?php echo TemplateLang(array('p' => 'date'), $this);?>
</th>
							<th width="65">&nbsp;</th>
						</tr>

						<?php $_from = $this->_tpl_vars['payments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['payment']):
?>
						<?php echo smarty_function_cycle(array('values' => "td1,td2",'name' => 'class','assign' => 'class'), $this);?>

						<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
							<td align="center"><img src="templates/images/<?php if ($this->_tpl_vars['payment']['status'] == 1): ?>yes<?php else: ?>no<?php endif; ?>.png" border="0" alt="" width="16" height="16" /></td>
							<td><?php echo TemplateText(array('value' => $this->_tpl_vars['payment']['invoiceNo']), $this);?>
<br /><small><?php echo TemplateText(array('value' => $this->_tpl_vars['payment']['customerNo']), $this);?>
</small></td>
							<td>
								<div style="float:left;">
									<?php echo $this->_tpl_vars['payment']['amount']; ?>
<br /><small><?php echo $this->_tpl_vars['payment']['method']; ?>
</small>
								</div>
								<?php if ($this->_tpl_vars['payment']['paymethod'] < 0): ?>
								<div style="float:right;">
									<a href="payments.php?do=details&orderid=<?php echo $this->_tpl_vars['payment']['orderid']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" title="<?php echo TemplateLang(array('p' => 'details'), $this);?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_payments.png" border="0" alt="<?php echo TemplateLang(array('p' => 'details'), $this);?>
" width="16" height="16" /></a>
								</div>
								<?php endif; ?>
							</td>
							<td><?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['payment']['created'],'nice' => true), $this);?>
</td>
							<td>
								<?php if ($this->_tpl_vars['payment']['hasInvoice']): ?><a href="javascript:void(0);" onclick="openWindow('payments.php?action=showInvoice&orderID=<?php echo $this->_tpl_vars['payment']['orderid']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
','invoice_<?php echo $this->_tpl_vars['payment']['orderid']; ?>
',640,480);" title="<?php echo TemplateLang(array('p' => 'showinvoice'), $this);?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/file.png" border="0" alt="<?php echo TemplateLang(array('p' => 'showinvoice'), $this);?>
" width="16" height="16" /></a><?php endif; ?>
								<?php if ($this->_tpl_vars['payment']['status'] == 0): ?><a href="<?php if ($this->_tpl_vars['payment']['paymethod'] < 0): ?>payments.php?do=details&orderid=<?php echo $this->_tpl_vars['payment']['orderid']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
<?php else: ?>users.php?do=edit&id=<?php echo $this->_tpl_vars['user']['id']; ?>
&activatePayment=<?php echo $this->_tpl_vars['payment']['orderid']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
<?php endif; ?>" title="<?php echo TemplateLang(array('p' => 'activatepayment'), $this);?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/unlock.png" border="0" alt="<?php echo TemplateLang(array('p' => 'activatepayment'), $this);?>
" width="16" height="16" /></a><?php endif; ?>
								<a href="users.php?do=edit&id=<?php echo $this->_tpl_vars['user']['id']; ?>
&deletePayment=<?php echo $this->_tpl_vars['payment']['orderid']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onclick="return confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
');" title="<?php echo TemplateLang(array('p' => 'delete'), $this);?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/delete.png" border="0" alt="<?php echo TemplateLang(array('p' => 'delete'), $this);?>
" width="16" height="16" /></a>
							</td>
						</tr>
						<?php endforeach; endif; unset($_from); ?>
					</table>
				</div>
			</fieldset>
		</td>
	</tr>
</table>
<p>
	<div style="float:left" class="buttons">
		<?php echo TemplateLang(array('p' => 'action'), $this);?>
:&nbsp;
		<select name="userAction" id="userAction">
			<optgroup label="<?php echo TemplateLang(array('p' => 'actions'), $this);?>
">
				<?php if ($this->_tpl_vars['user']['sms_validation_code'] != '' && $this->_tpl_vars['user']['gesperrt'] == 'locked'): ?>
				<?php if ($this->_tpl_vars['regValidation'] == 'email' && $this->_tpl_vars['user']['altmail'] != ''): ?>
				<option value="users.php?do=edit&id=<?php echo $this->_tpl_vars['user']['id']; ?>
&resendValidationEmail=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateLang(array('p' => 'resend_val_email'), $this);?>
</option>
				<?php elseif ($this->_tpl_vars['regValidation'] == 'sms' && $this->_tpl_vars['user']['mail2sms_nummer'] != ''): ?>
				<option value="users.php?do=edit&id=<?php echo $this->_tpl_vars['user']['id']; ?>
&resendValidationSMS=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateLang(array('p' => 'resend_val_sms'), $this);?>
</option>
				<?php endif; ?>
				<?php endif; ?>
				<option value="mailto:<?php echo TemplateEMail(array('value' => $this->_tpl_vars['user']['email']), $this);?>
"><?php echo TemplateLang(array('p' => 'sendmail'), $this);?>
</option>
				<?php if ($this->_tpl_vars['user']['altmail'] != ''): ?><option value="mailto:<?php echo TemplateEMail(array('value' => $this->_tpl_vars['user']['altmail']), $this);?>
"><?php echo TemplateLang(array('p' => 'sendmail'), $this);?>
 (<?php echo TemplateLang(array('p' => 'altmail'), $this);?>
)</option><?php endif; ?>
				<option value="popup;users.php?do=login&id=<?php echo $this->_tpl_vars['user']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateLang(array('p' => 'login'), $this);?>
</option>
				<option value="users.php?singleAction=emptyTrash&singleID=<?php echo $this->_tpl_vars['user']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateLang(array('p' => 'emptytrash'), $this);?>
</option>
				<option value="users.php?singleAction=<?php if ($this->_tpl_vars['user']['gesperrt'] == 'no'): ?>lock<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'yes'): ?>unlock<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'locked'): ?>activate<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'delete'): ?>recover<?php endif; ?>&singleID=<?php echo $this->_tpl_vars['user']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php if ($this->_tpl_vars['user']['gesperrt'] == 'no'): ?><?php echo TemplateLang(array('p' => 'lock'), $this);?>
<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'yes'): ?><?php echo TemplateLang(array('p' => 'unlock'), $this);?>
<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'locked'): ?><?php echo TemplateLang(array('p' => 'activate'), $this);?>
<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'delete'): ?><?php echo TemplateLang(array('p' => 'restore'), $this);?>
<?php endif; ?></option>
				<option value="users.php?singleAction=delete&singleID=<?php echo $this->_tpl_vars['user']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option>
			</optgroup>

			<optgroup label="<?php echo TemplateLang(array('p' => 'move'), $this);?>
">
			<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['groupID'] => $this->_tpl_vars['groupItem']):
?>
			<?php if ($this->_tpl_vars['groupID'] != $this->_tpl_vars['user']['gruppe']): ?>
				<option value="users.php?do=edit&id=<?php echo $this->_tpl_vars['user']['id']; ?>
&moveToGroup=<?php echo $this->_tpl_vars['groupID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateLang(array('p' => 'moveto'), $this);?>
 &quot;<?php echo TemplateText(array('value' => $this->_tpl_vars['groupItem']['title'],'cut' => 25), $this);?>
&quot;</option>
			<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
			</optgroup>
		</select>
	</div>
	<div style="float:left;padding-left:0.5em;">
		<input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'ok'), $this);?>
 " onclick="executeAction('userAction');" />
	</div>
	<div style="float:right" class="buttons">
		<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
	</div>
</p>
</form>