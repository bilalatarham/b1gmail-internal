<?php /* Smarty version 2.6.28, created on 2020-09-29 11:40:43
         compiled from /var/www/html/plugins/templates/bms.admin.plugins.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', '/var/www/html/plugins/templates/bms.admin.plugins.tpl', 2, false),array('function', 'cycle', '/var/www/html/plugins/templates/bms.admin.plugins.tpl', 21, false),array('function', 'text', '/var/www/html/plugins/templates/bms.admin.plugins.tpl', 24, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'installedplugins'), $this);?>
</legend>
	
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th><?php echo TemplateLang(array('p' => 'title'), $this);?>
</th>
			
			<?php if (! $this->_tpl_vars['updateCheck']): ?>
			<th><?php echo TemplateLang(array('p' => 'author'), $this);?>
</th>
			<th><?php echo TemplateLang(array('p' => 'info'), $this);?>
</th>
			<th><?php echo TemplateLang(array('p' => 'status'), $this);?>
</th>
			<th>&nbsp;</th>
			<?php else: ?>
			<th width="120"><?php echo TemplateLang(array('p' => 'installed'), $this);?>
</th>
			<th width="120"><?php echo TemplateLang(array('p' => 'current'), $this);?>
</th>
			<?php endif; ?>
		</tr>
		
		<?php $_from = $this->_tpl_vars['plugins']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['plugin']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/plugin_<?php if (! $this->_tpl_vars['plugin']['active']): ?>in<?php endif; ?>active.png" border="0" alt="" width="16" height="16" /></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['plugin']['title']), $this);?>
<br /><small><?php echo TemplateText(array('value' => $this->_tpl_vars['plugin']['name']), $this);?>
</small></td>
			<?php if (! $this->_tpl_vars['updateCheck']): ?>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['plugin']['author']), $this);?>
</td>
			<td><?php echo TemplateLang(array('p' => 'version'), $this);?>
: <?php echo TemplateText(array('value' => $this->_tpl_vars['plugin']['version']), $this);?>
<br /><small><?php echo $this->_tpl_vars['plugin']['filename']; ?>
</small></td>
			<td><?php if ($this->_tpl_vars['plugin']['active']): ?><?php echo TemplateLang(array('p' => 'active'), $this);?>
<?php else: ?><?php echo TemplateLang(array('p' => 'inactive'), $this);?>
<?php endif; ?></td>
			<td><a href="<?php echo $this->_tpl_vars['pageURL']; ?>
&action=plugins&sid=<?php echo $this->_tpl_vars['sid']; ?>
&do=<?php if ($this->_tpl_vars['plugin']['active']): ?>de<?php endif; ?>activatePlugin&filename=<?php echo $this->_tpl_vars['plugin']['filename']; ?>
" onclick="return confirm('<?php echo TemplateLang(array('p' => 'reallyplugin'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/plugin_switch.png" border="0" alt="<?php echo TemplateLang(array('p' => 'acdeactivate'), $this);?>
" border="0" width="16" height="16" /></a></td>
			<?php else: ?>
			<td><?php echo TemplateLang(array('p' => 'version'), $this);?>
: <?php echo TemplateText(array('value' => $this->_tpl_vars['plugin']['version']), $this);?>
</td>
			<td id="updates_<?php echo $this->_tpl_vars['plugin']['filename']; ?>
">
				<div align="center">
					<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/load_16.gif" border="0" alt="" />
				</div>
			</td>
			<?php endif; ?>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
	</table>
</fieldset>

<?php if (! $this->_tpl_vars['updateCheck']): ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'updates'), $this);?>
</legend>
	
	<table width="100%">
	<tr>
		<td align="left" valign="top" width="40"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/updates.png" border="0" alt="" width="32" height="32" /></td>
		<td>
			<?php echo TemplateLang(array('p' => 'bms_updatesdesc'), $this);?>

	
			<div align="center">
				<br />
				<input class="button" type="button" onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&action=plugins&updateCheck=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
';" value=" <?php echo TemplateLang(array('p' => 'searchupdatesnow'), $this);?>
 &raquo; " />
			</div>
		</td>
	</tr>
	</table>
</fieldset>
<?php else: ?>
<p>
	<input class="button" type="button" value=" &laquo; <?php echo TemplateLang(array('p' => 'back'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&action=plugins&sid=<?php echo $this->_tpl_vars['sid']; ?>
';" />
</p>
<script language="javascript">
<!--
<?php echo '
	function checkForBMSPluginUpdates(fileName)
	{
		MakeXMLRequest(\''; ?>
<?php echo $this->_tpl_vars['pageURL']; ?>
<?php echo '&action=plugins&do=updateCheck&sid=\' + currentSID 
							+ \'&do=updateCheck\'
							+ \'&filename=\' + fileName,
						_checkForPluginUpdates);
	}
'; ?>

<?php $_from = $this->_tpl_vars['plugins']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['plugin']):
?>
	checkForBMSPluginUpdates('<?php echo $this->_tpl_vars['plugin']['filename']; ?>
');
<?php endforeach; endif; unset($_from); ?>
//-->
</script>
<?php endif; ?>
					