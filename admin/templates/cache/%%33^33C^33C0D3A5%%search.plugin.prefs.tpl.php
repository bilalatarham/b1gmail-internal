<?php /* Smarty version 2.6.28, created on 2020-09-29 15:08:06
         compiled from /var/www/html/plugins/templates/search.plugin.prefs.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', '/var/www/html/plugins/templates/search.plugin.prefs.tpl', 2, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'searchprovider'), $this);?>
</legend>
	
	<form action="<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&do=save" method="post" onsubmit="spin(this)">
	<table>
		<tr>
			<td align="left" rowspan="3" valign="top" width="40"><img src="../plugins/templates/images/search32.png" border="0" alt="" width="32" height="32" /></td>
			<td>
				<?php echo TemplateLang(array('p' => 'includeinsearch'), $this);?>
:
				<blockquote>
					<input type="checkbox" name="searchIn[mails]" id="searchMails"<?php if ($this->_tpl_vars['searchIn']['mails']): ?> checked="checked"<?php endif; ?> />
						<label for="searchMails"><b><?php echo TemplateLang(array('p' => 'emails'), $this);?>
</b></label><br />
					<input type="checkbox" name="searchIn[attachments]" id="searchAtt"<?php if ($this->_tpl_vars['searchIn']['attachments']): ?> checked="checked"<?php endif; ?> />
						<label for="searchAtt"><b><?php echo TemplateLang(array('p' => 'attachments'), $this);?>
</b></label><br />
					<input type="checkbox" name="searchIn[sms]" id="searchSMS"<?php if ($this->_tpl_vars['searchIn']['sms']): ?> checked="checked"<?php endif; ?> />
						<label for="searchSMS"><b><?php echo TemplateLang(array('p' => 'smsoutbox'), $this);?>
</b></label><br />
					<input type="checkbox" name="searchIn[calendar]" id="searchCalendar"<?php if ($this->_tpl_vars['searchIn']['calendar']): ?> checked="checked"<?php endif; ?> />
						<label for="searchCalendar"><b><?php echo TemplateLang(array('p' => 'calendar'), $this);?>
</b></label><br />
					<input type="checkbox" name="searchIn[tasks]" id="searchTasks"<?php if ($this->_tpl_vars['searchIn']['tasks']): ?> checked="checked"<?php endif; ?> />
						<label for="searchTasks"><b><?php echo TemplateLang(array('p' => 'tasks'), $this);?>
</b></label><br />
					<input type="checkbox" name="searchIn[addressbook]" id="searchAddressbook"<?php if ($this->_tpl_vars['searchIn']['addressbook']): ?> checked="checked"<?php endif; ?> />
						<label for="searchAddressbook"><b><?php echo TemplateLang(array('p' => 'addressbook'), $this);?>
</b></label><br />
					<input type="checkbox" name="searchIn[notes]" id="searchNotes"<?php if ($this->_tpl_vars['searchIn']['notes']): ?> checked="checked"<?php endif; ?> />
						<label for="searchNotes"><b><?php echo TemplateLang(array('p' => 'notes'), $this);?>
</b></label><br />
					<input type="checkbox" name="searchIn[webdisk]" id="searchWebdisk"<?php if ($this->_tpl_vars['searchIn']['webdisk']): ?> checked="checked"<?php endif; ?> />
						<label for="searchWebdisk"><b><?php echo TemplateLang(array('p' => 'webdisk'), $this);?>
</b></label><br />
				</blockquote>
			</td>
		</tr>
	</table>
	<p>
		<div style="float:left;">
			<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/warning.png" border="0" alt="" width="16" height="16" align="absmiddle" />
			<?php echo TemplateLang(array('p' => 'mailsearchwarn'), $this);?>

		</div>
		<div style="float:right;">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
	</form>
</fieldset>