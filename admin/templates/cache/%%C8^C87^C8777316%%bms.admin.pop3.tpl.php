<?php /* Smarty version 2.6.28, created on 2020-09-29 11:34:55
         compiled from /var/www/html/plugins/templates/bms.admin.pop3.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', '/var/www/html/plugins/templates/bms.admin.pop3.tpl', 3, false),array('function', 'text', '/var/www/html/plugins/templates/bms.admin.pop3.tpl', 9, false),)), $this); ?>
<form action="<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=pop3&save=true" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'common'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="4" valign="top" width="40"><img src="../plugins/templates/images/bms_common.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_greeting'), $this);?>
:</td>
				<td class="td2"><input type="text" name="pop3greeting" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['pop3greeting'],'allowEmpty' => true), $this);?>
" size="32" style="width:95%;" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_timeout'), $this);?>
:</td>
				<td class="td2"><input type="text" name="pop3_timeout" value="<?php echo $this->_tpl_vars['bms_prefs']['pop3_timeout']; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_altpop3'), $this);?>
:</td>
				<td class="td2"><input type="checkbox" name="altpop3_enable"<?php if ($this->_tpl_vars['bms_prefs']['altpop3'] != 0): ?> checked="checked"<?php endif; ?> />
								<?php echo TemplateLang(array('p' => 'bms_toport'), $this);?>

								<input type="text" name="altpop3_port" value="<?php echo $this->_tpl_vars['bms_prefs']['altpop3']; ?>
" size="6" /></td>
			</tr>
		</table>
	</fieldset>
	
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_folderstofetch'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="2" valign="top" width="40"><img src="../plugins/templates/images/bms_folders.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_user_chosepop3folders'), $this);?>
?</td>
				<td class="td2"><input type="checkbox" name="user_chosepop3folders"<?php if ($this->_tpl_vars['bms_prefs']['user_chosepop3folders']): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'folders'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" name="pop3_folders[]" value="0" id="pop3_folders_0"<?php if ($this->_tpl_vars['pop3Folders']['0']): ?> checked="checked"<?php endif; ?> />
					<label for="pop3_folders_0"><?php echo TemplateLang(array('p' => 'bms_folder_inbox'), $this);?>
</label><br />

					<input type="checkbox" name="pop3_folders[]" value="-4" id="pop3_folders_-4"<?php if ($this->_tpl_vars['pop3Folders']['m4']): ?> checked="checked"<?php endif; ?> />
					<label for="pop3_folders_-4"><?php echo TemplateLang(array('p' => 'bms_folder_spam'), $this);?>
</label><br />

					<input type="checkbox" name="pop3_folders[]" value="-5" id="pop3_folders_-5"<?php if ($this->_tpl_vars['pop3Folders']['m5']): ?> checked="checked"<?php endif; ?> />
					<label for="pop3_folders_-5"><?php echo TemplateLang(array('p' => 'bms_folder_trash'), $this);?>
</label><br />

					<input type="checkbox" name="pop3_folders[]" value="-128" id="pop3_folders_-128"<?php if ($this->_tpl_vars['pop3Folders']['m128']): ?> checked="checked"<?php endif; ?> />
					<label for="pop3_folders_-128"><?php echo TemplateLang(array('p' => 'bms_userfolders'), $this);?>
</label><br />
				</td>
			</tr>
		</table>
	</fieldset>

	<p>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>