<?php /* Smarty version 2.6.28, created on 2020-09-29 11:32:44
         compiled from msg.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'msg.tpl', 17, false),)), $this); ?>
<fieldset>
	<legend><?php echo $this->_tpl_vars['msgTitle']; ?>
</legend>
	
	<?php if ($this->_tpl_vars['msgIcon']): ?>
	<table>
		<tr>
			<td width="36" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php echo $this->_tpl_vars['msgIcon']; ?>
.png" border="0" alt="" width="32" height="32" /></td>
			<td valign="top"><?php echo $this->_tpl_vars['msgText']; ?>
</td>
		</tr>
	</table>
	<?php else: ?>
	<?php echo $this->_tpl_vars['msgText']; ?>

	<?php endif; ?>
	
	<?php if ($this->_tpl_vars['backLink']): ?>
	<p align="right">
		<input class="button" type="button" onclick="document.location.href='<?php echo $this->_tpl_vars['backLink']; ?>
sid=<?php echo $this->_tpl_vars['sid']; ?>
';" value=" <?php echo TemplateLang(array('p' => 'back'), $this);?>
 " />
	</p>
	<?php else: ?>
	<p align="right">
		<input class="button" type="button" onclick="history.back(1);" value=" <?php echo TemplateLang(array('p' => 'back'), $this);?>
 " />
	</p>	
	<?php endif; ?>
</fieldset>

<?php if ($this->_tpl_vars['reloadMenu']): ?>
<script language="javascript">
<!--
	parent.frames['menu'].location.href = 'main.php?action=menu&item=4&sid=<?php echo $this->_tpl_vars['sid']; ?>
';
//-->
</script>
<?php endif; ?>