<?php /* Smarty version 2.6.28, created on 2020-09-29 11:38:28
         compiled from /var/www/html/plugins/templates/bms.admin.msgqueue.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', '/var/www/html/plugins/templates/bms.admin.msgqueue.tpl', 3, false),array('function', 'text', '/var/www/html/plugins/templates/bms.admin.msgqueue.tpl', 35, false),array('function', 'size', '/var/www/html/plugins/templates/bms.admin.msgqueue.tpl', 152, false),)), $this); ?>
<form action="<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=msgqueue&save=true" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_queue_prefs'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="10" valign="top" width="40"><img src="../plugins/templates/images/bms_queue.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_queue_interval'), $this);?>
:</td>
				<td class="td2"><input type="text" name="queue_interval" value="<?php echo $this->_tpl_vars['bms_prefs']['queue_interval']; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_queue_retry'), $this);?>
:</td>
				<td class="td2"><input type="text" name="queue_retry" value="<?php echo $this->_tpl_vars['bms_prefs']['queue_retry']/60; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'bms_minutes'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_queue_lifetime'), $this);?>
:</td>
				<td class="td2"><input type="text" name="queue_lifetime" value="<?php echo $this->_tpl_vars['bms_prefs']['queue_lifetime']/3600; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'bms_hours'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_queue_timeout'), $this);?>
:</td>
				<td class="td2"><input type="text" name="queue_timeout" value="<?php echo $this->_tpl_vars['bms_prefs']['queue_timeout']; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_queue_threads'), $this);?>
:</td>
				<td class="td2"><input type="text" name="queue_threads" value="<?php echo $this->_tpl_vars['bms_prefs']['queue_threads']; ?>
" size="6" />
								/ <input type="text" name="queue_maxthreads" value="<?php echo $this->_tpl_vars['bms_prefs']['queue_maxthreads']; ?>
" size="6" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_control_addr'), $this);?>
:</td>
				<td class="td2">
					<input type="text" name="control_addr" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['control_addr'],'allowEmpty' => true), $this);?>
" size="16" />
					<a href="#" onclick="alert('<?php echo TemplateLang(array('p' => 'bms_control_addr_help'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/info.png" border="0" alt="<?php echo TemplateLang(array('p' => 'help'), $this);?>
" /></a>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_mysqlconnection'), $this);?>
:</td>
				<td class="td2"><input type="checkbox" name="queue_mysqlclose"<?php if ($this->_tpl_vars['bms_prefs']['queue_mysqlclose'] == 1): ?> checked="checked"<?php endif; ?> id="queue_mysqlclose" />
					<label for="queue_mysqlclose"><b><?php echo TemplateLang(array('p' => 'bms_closewhenidle'), $this);?>
</b></label></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_random_queue_id'), $this);?>
?</td>
				<td class="td2"><input type="checkbox" name="random_queue_id"<?php if ($this->_tpl_vars['bms_prefs']['random_queue_id'] == 1): ?> checked="checked"<?php endif; ?> id="random_queue_id" />
					<label for="random_queue_id"><b><?php echo TemplateLang(array('p' => 'activate'), $this);?>
</b></label></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_received_header'), $this);?>
:</td>
				<td class="td2"><input type="checkbox" name="received_header_no_expose"<?php if ($this->_tpl_vars['bms_prefs']['received_header_no_expose'] == 1): ?> checked="checked"<?php endif; ?> id="received_header_no_expose" />
					<label for="received_header_no_expose"><b><?php echo TemplateLang(array('p' => 'bms_dont_expose'), $this);?>
</b></label></td>
			</tr>
		</table>
	</fieldset>
	
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_inbound'), $this);?>
</legend>
		
		<table width="100%">
			<tr>
				<td align="left" valign="top" width="40" rowspan="2"><img src="../plugins/templates/images/bms_inbound.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_php_path'), $this);?>
:</td>
				<td class="td2"><input type="text" name="php_path" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['php_path'],'allowEmpty' => true), $this);?>
" size="32" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_reuseprocess'), $this);?>
?</td>
				<td class="td2"><input id="inbound_reuse_process" name="inbound_reuse_process"<?php if ($this->_tpl_vars['bms_prefs']['inbound_reuse_process'] && $this->_tpl_vars['minV72']): ?> checked="checked"<?php endif; ?> type="checkbox"<?php if (! $this->_tpl_vars['minV72']): ?> disabled="disabled"<?php endif; ?> />
								<label for="inbound_reuse_process"><b><?php echo TemplateLang(array('p' => 'activate'), $this);?>
</b></label></td>
			</tr>
		</table>
	</fieldset>
	
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_outbound'), $this);?>
</legend>
		
		<table width="100%">
			<tr>
				<td align="left" valign="top" width="40"><img src="../plugins/templates/images/bms_outbound.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_processing'), $this);?>
:</td>
				<td class="td2"><select name="outbound_target" onchange="EBID('outbound_sendmail_prefs').style.display=this.value==0?'':'none';EBID('outbound_smtp_prefs').style.display=this.value==1?'':'none';EBID('outbound_smtpself_prefs').style.display=this.value!=0?'':'none';">
					<option value="0"<?php if ($this->_tpl_vars['bms_prefs']['outbound_target'] == 0): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_redirecttosendmail'), $this);?>
</option>
					<option value="1"<?php if ($this->_tpl_vars['bms_prefs']['outbound_target'] == 1): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_redirecttosmtprelay'), $this);?>
</option>
					<option value="2"<?php if ($this->_tpl_vars['bms_prefs']['outbound_target'] == 2): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_deliverself'), $this);?>
</option>
				</select></td>
			</tr>
			<tbody id="outbound_sendmail_prefs" style="display:<?php if ($this->_tpl_vars['bms_prefs']['outbound_target'] != 0): ?>none<?php endif; ?>;">
			<tr>
				<td>&nbsp;</td>
				<td class="td1"><?php echo TemplateLang(array('p' => 'sendmailpath'), $this);?>
:</td>
				<td class="td2"><input type="text" name="outbound_sendmail_path" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['outbound_sendmail_path'],'allowEmpty' => true), $this);?>
" size="32" /></td>
			</tr>
			</tbody>
			<tbody id="outbound_smtp_prefs" style="display:<?php if ($this->_tpl_vars['bms_prefs']['outbound_target'] != 1): ?>none<?php endif; ?>;">
			<tr>
				<td rowspan="5">&nbsp;</td>
				<td class="td1"><?php echo TemplateLang(array('p' => 'smtphost'), $this);?>
:</td>
				<td class="td2"><input type="text" name="outbound_smtp_relay_host" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['outbound_smtp_relay_host'],'allowEmpty' => true), $this);?>
" size="32" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'smtpport'), $this);?>
:</td>
				<td class="td2"><input type="text" name="outbound_smtp_relay_port" value="<?php echo $this->_tpl_vars['bms_prefs']['outbound_smtp_relay_port']; ?>
" size="6" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'smtpauth'), $this);?>
?</td>
				<td class="td2"><input name="outbound_smtp_relay_auth"<?php if ($this->_tpl_vars['bms_prefs']['outbound_smtp_relay_auth']): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'smtpuser'), $this);?>
:</td>
				<td class="td2"><input type="text" name="outbound_smtp_relay_user" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bms_prefs']['outbound_smtp_relay_user']), $this);?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'smtppass'), $this);?>
:</td>
				<td class="td2"><input type="password" autocomplete="off" name="outbound_smtp_relay_pass" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bms_prefs']['outbound_smtp_relay_pass']), $this);?>
" size="36" /></td>
			</tr>
			</tbody>
			<tbody id="outbound_smtpself_prefs" style="display:<?php if ($this->_tpl_vars['bms_prefs']['outbound_target'] == 0): ?>none<?php endif; ?>;">
			<tr>
				<td>&nbsp;</td>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_usetls'), $this);?>
?</td>
				<td class="td2"><input name="outbound_smtp_usetls"<?php if ($this->_tpl_vars['bms_prefs']['outbound_smtp_usetls']): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_usednssecdane'), $this);?>
?</td>
				<td class="td2"><input name="outbound_smtp_usedane"<?php if ($this->_tpl_vars['bms_prefs']['outbound_smtp_usedane']): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			</tbody>
		</table>
		<?php if ($this->_tpl_vars['haveSignatureSupport']): ?>
		<table width="100%">
			<tr>
				<td align="left" rowspan="2" valign="top" width="40"><img src="../plugins/templates/images/bms_signature.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_add_signature'), $this);?>
?</td>
				<td class="td2"><input type="checkbox" name="outbound_add_signature"<?php if ($this->_tpl_vars['bms_prefs']['outbound_add_signature'] == 1): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_signature_sep'), $this);?>
:</td>
				<td class="td2"><input type="text" name="outbound_signature_sep" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['outbound_signature_sep'],'allowEmpty' => true), $this);?>
" size="54" /></td>
			</tr>
		</table>
		<?php endif; ?>
	</fieldset>
	
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_queue'), $this);?>
</legend>
		
		<table width="100%">
			<tr>
				<td align="left" rowspan="2" valign="top" width="40"><img src="../plugins/templates/images/bms_queue.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_queue'), $this);?>
:</td>
				<td class="td2"><?php echo $this->_tpl_vars['queueCount']; ?>
 <?php echo TemplateLang(array('p' => 'entries'), $this);?>
 (<?php echo TemplateSize(array('bytes' => $this->_tpl_vars['queueSize']), $this);?>
)
					<input class="button" type="button"<?php if ($this->_tpl_vars['queueCount'] == 0): ?> disabled="disabled"<?php endif; ?> value=" <?php echo TemplateLang(array('p' => 'show'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=msgqueue&do=queue';" />
					<input class="button" type="button"<?php if ($this->_tpl_vars['queueCount'] == 0): ?> disabled="disabled"<?php endif; ?> value=" <?php echo TemplateLang(array('p' => 'bms_clearqueue'), $this);?>
 " onclick="if(confirm('<?php echo TemplateLang(array('p' => 'bms_clearquestion'), $this);?>
')) document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=msgqueue&clearQueue=true';" />
					<input class="button" type="button"<?php if ($this->_tpl_vars['queueCount'] == 0 || ! $this->_tpl_vars['allowFlush']): ?> disabled="disabled"<?php endif; ?> value=" <?php echo TemplateLang(array('p' => 'bms_flushqueue'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=msgqueue&flushQueue=true';" />
					<input class="button" type="button"<?php if (! $this->_tpl_vars['enableRestart']): ?> disabled="disabled"<?php endif; ?> value=" <?php echo TemplateLang(array('p' => 'bms_restartqueue'), $this);?>
 " onclick="if(confirm('<?php echo TemplateLang(array('p' => 'bms_reallyrestartqueue'), $this);?>
')) document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=msgqueue&restartQueue=true';" />
				</td>
			</tr>
			<?php if ($this->_tpl_vars['threadCount']): ?>
			<tr>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_threads'), $this);?>
:</td>
				<td class="td2"><?php echo $this->_tpl_vars['threadCount']; ?>
</td>
			</tr>
			<?php endif; ?>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_advanced'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="2" valign="top" width="40"><img src="../plugins/templates/images/bms_common.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_ownheaders'), $this);?>
:</td>
				<td class="td2">
					<input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'edit'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=msgqueue&do=headers';" />
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_deliveryrules'), $this);?>
:</td>
				<td class="td2">
					<input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'edit'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=msgqueue&do=deliveryRules';" />
				</td>
			</tr>
		</table>
	</fieldset>
	
	<p>
		<div style="float:left" class="buttons">
			<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/warning.png" border="0" alt="" width="16" height="16" align="absmiddle" />
			<?php echo TemplateLang(array('p' => 'bms_queuerestartnote'), $this);?>

		</div>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>