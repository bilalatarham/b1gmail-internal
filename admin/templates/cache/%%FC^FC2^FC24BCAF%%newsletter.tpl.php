<?php /* Smarty version 2.6.28, created on 2020-09-29 16:06:18
         compiled from newsletter.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'newsletter.tpl', 3, false),array('function', 'text', 'newsletter.tpl', 21, false),array('function', 'fileDateSig', 'newsletter.tpl', 95, false),)), $this); ?>
<form action="newsletter.php?do=send&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" id="newsletterForm" onsubmit="<?php echo 'if(newsletterMode==\'export\') { this.target=\'_blank\'; } else { if(EBID(\'subject\').value.length<3 && !confirm(window.lang[\'sendwosubject\'])) return(false); this.target=\'\';editor.submit();spin(this); }'; ?>
">
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'recipients'), $this);?>
</legend>
	
	<table width="100%">
		<tr>
			<td width="40" valign="top" rowspan="3"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/filter.png" border="0" alt="" width="32" height="32" /></td>
			<td class="td1" width="120"><?php echo TemplateLang(array('p' => 'status'), $this);?>
:</td>
			<td class="td2">
				<input type="checkbox" name="statusActive" id="statusActive" checked="checked" onclick="determineNewsletterRecipients()" />
					<label for="statusActive"><b><?php echo TemplateLang(array('p' => 'active'), $this);?>
</b></label><br />
				<input type="checkbox" name="statusLocked" id="statusLocked" checked="checked" onclick="determineNewsletterRecipients()" />
					<label for="statusLocked"><b><?php echo TemplateLang(array('p' => 'locked'), $this);?>
</b></label><br />
				<input type="checkbox" name="statusNotActivated" id="statusNotActivated" checked="checked" onclick="determineNewsletterRecipients()" />
					<label for="statusNotActivated"><b><?php echo TemplateLang(array('p' => 'notactivated'), $this);?>
</b></label><br />
			</td>
			<td class="td1" width="120"><?php echo TemplateLang(array('p' => 'groups'), $this);?>
:</td>
			<td class="td2">
				<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['groupID'] => $this->_tpl_vars['group']):
?>
					<input type="checkbox" name="group_<?php echo $this->_tpl_vars['groupID']; ?>
" id="group_<?php echo $this->_tpl_vars['groupID']; ?>
"<?php if (! $_GET['toGroup'] || $_GET['toGroup'] == $this->_tpl_vars['groupID']): ?> checked="checked"<?php endif; ?> onclick="determineNewsletterRecipients()" />
						<label for="group_<?php echo $this->_tpl_vars['groupID']; ?>
"><b><?php echo TemplateText(array('value' => $this->_tpl_vars['group']['title']), $this);?>
</b></label><br />
				<?php endforeach; endif; unset($_from); ?>
			</td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'countries'), $this);?>
:</td>
			<td class="td2">
				<div id="countrySelectBox" style="background-color:#FFF;border:1px solid #CCC;overflow-y:scroll;min-height:3em;max-height:80px;">
					<?php $_from = $this->_tpl_vars['countries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['countryID'] => $this->_tpl_vars['country']):
?>
					<div style="padding:1px;">
						<input type="checkbox" name="countries[]" value="<?php echo $this->_tpl_vars['countryID']; ?>
" id="country_<?php echo $this->_tpl_vars['countryID']; ?>
" checked="checked" onchange="determineNewsletterRecipients()" />
						<label for="country_<?php echo $this->_tpl_vars['countryID']; ?>
"><?php echo $this->_tpl_vars['country']; ?>
</label>
					</div>
					<?php endforeach; endif; unset($_from); ?>
				</div>
			</td>
			<td class="td1"><?php echo TemplateLang(array('p' => 'sendto'), $this);?>
:</td>
			<td class="td2">
				<input type="radio" name="sendto" value="mailboxes" id="sendto_mailboxes" checked="checked" onclick="determineNewsletterRecipients()" />
				<label for="sendto_mailboxes"><b><?php echo TemplateLang(array('p' => 'mailboxes'), $this);?>
</b></label><br />
				<input type="radio" name="sendto" value="altmails" id="sendto_altmails" onclick="determineNewsletterRecipients()" />
				<label for="sendto_altmails"><b><?php echo TemplateLang(array('p' => 'altmails'), $this);?>
</b></label>
			</td>
		</tr>
	</table>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'email'), $this);?>
</legend>
	
	<table width="100%">
		<tr>
			<td width="40" valign="top" rowspan="7"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/newsletter.png" border="0" alt="" width="32" height="32" /></td>
			<td class="td1" width="120"><?php echo TemplateLang(array('p' => 'template'), $this);?>
:</td>
			<td class="td2">
				<select name="template" id="template" onchange="loadNewsletterTemplate(this)">
					<option value="0" selected="selected">-</option>
					<?php $_from = $this->_tpl_vars['templates']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tplID'] => $this->_tpl_vars['tplTitle']):
?>
					<option value="<?php echo $this->_tpl_vars['tplID']; ?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['tplTitle']), $this);?>
</option>
					<?php endforeach; endif; unset($_from); ?>
				</select>
			</td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'mode'), $this);?>
:</td>
			<td class="td2">
				<input type="radio" name="mode" value="html" id="mode_html" checked="checked" onclick="if(this.checked) return editor.switchMode('html');" />
				<label for="mode_html"><b><?php echo TemplateLang(array('p' => 'htmltext'), $this);?>
</b></label>
				
				<input type="radio" name="mode" value="text" id="mode_text" onclick="if(this.checked) return editor.switchMode('text');" />
				<label for="mode_text"><b><?php echo TemplateLang(array('p' => 'plaintext'), $this);?>
</b></label>
			</td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'from'), $this);?>
:</td>
			<td class="td2"><input type="text" id="from" name="from" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['from']), $this);?>
" size="42" /></td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'subject'), $this);?>
:</td>
			<td class="td2"><input type="text" id="subject" name="subject" value="" size="42" /></td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'priority'), $this);?>
:</td>
			<td class="td2"><select name="priority" id="priority">
							<option value="1"><?php echo TemplateLang(array('p' => 'prio_1'), $this);?>
</option>
							<option value="0" selected="selected"><?php echo TemplateLang(array('p' => 'prio_0'), $this);?>
</option>
							<option value="-1"><?php echo TemplateLang(array('p' => "prio_-1"), $this);?>
</option>
						</select></td>
		</tr>
		<tr>
			<td colspan="2" style="border: 1px solid #DDDDDD;background-color:#FFFFFF;">
				<textarea name="emailText" id="emailText" class="plainTextArea" style="width:100%;height:400px;"></textarea>
				<script language="javascript" src="../clientlib/wysiwyg.js?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/wysiwyg.js"), $this);?>
"></script>
				<script type="text/javascript" src="../clientlib/ckeditor/ckeditor.js?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/ckeditor/ckeditor.js"), $this);?>
"></script>
				<script language="javascript">
				<!--
					var editor = new htmlEditor('emailText');
					editor.height = 400;
					editor.init();
					registerLoadAction('editor.start()');
				//-->
				</script>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<select class="smallInput" onchange="editor.insertText(this.value);">
					<option value="">-- <?php echo TemplateLang(array('p' => 'vars'), $this);?>
 --</option>
					<option value="%%email%%">%%email%% (<?php echo TemplateLang(array('p' => 'email'), $this);?>
)</option>
					<option value="%%greeting%%">%%greeting%% (<?php echo TemplateLang(array('p' => 'greeting'), $this);?>
)</option>
					<option value="%%salutation%%">%%salutation%% (<?php echo TemplateLang(array('p' => 'salutation'), $this);?>
)</option>
					<option value="%%firstname%%">%%firstname%% (<?php echo TemplateLang(array('p' => 'firstname'), $this);?>
)</option>
					<option value="%%lastname%%">%%lastname%% (<?php echo TemplateLang(array('p' => 'lastname'), $this);?>
)</option>
				</select>
			</td>
		</tr>
	</table>
</fieldset>

<p>
	<div style="float:left;" class="buttons">
		<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/user_active.png" border="0" alt="" width="16" height="16" align="absmiddle" />
		<span id="recpCount">0</span> <?php echo TemplateLang(array('p' => 'recpdetermined'), $this);?>

		<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'export'), $this);?>
 " id="exportButton" name="exportRecipients" disabled="disabled"
			onclick="newsletterMode='export';return true;" />
	</div>
	<div style="float:right;" class="buttons">
		<?php echo TemplateLang(array('p' => 'opsperpage'), $this);?>
:
		<input type="text" name="perpage" id="perpage" value="25" size="5" />
		<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'sendletter'), $this);?>
 " id="submitButton" disabled="disabled"
			onclick="newsletterMode='send';return true;" />
	</div>
</p>
</form>

<script language="javascript">
<!--
	var newsletterMode = 'export';
	registerLoadAction('determineNewsletterRecipients()');
//-->
</script>