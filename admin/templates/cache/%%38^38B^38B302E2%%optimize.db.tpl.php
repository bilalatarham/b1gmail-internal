<?php /* Smarty version 2.6.28, created on 2020-09-29 15:07:18
         compiled from optimize.db.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'optimize.db.tpl', 2, false),array('function', 'cycle', 'optimize.db.tpl', 13, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'db'), $this);?>
</legend>
	
	<?php if ($this->_tpl_vars['execute']): ?>
		<table class="list">
		<tr>
			<th>&nbsp;</th>
			<th><?php echo TemplateLang(array('p' => 'table'), $this);?>
</th>
			<th><?php echo TemplateLang(array('p' => 'query'), $this);?>
</th>
			<th width="100"><?php echo TemplateLang(array('p' => 'status'), $this);?>
</th>
		</tr>
		<?php $_from = $this->_tpl_vars['result']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['table']):
?>
		<?php echo smarty_function_cycle(array('values' => "td1,td2",'name' => 'class','assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td width="20"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/db_table.png" border="0" alt="" width="16" height="16" /></td>
			<td><?php echo $this->_tpl_vars['table']['table']; ?>
</td>
			<td><code><?php echo $this->_tpl_vars['table']['query']; ?>
</code></td>
			<td align="right"><?php if ($this->_tpl_vars['table']['type'] != 'status' && $this->_tpl_vars['table']['type'] != 'info' && $this->_tpl_vars['table']['type'] != 'note'): ?>
				<img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/error.png" width="16" height="16" alt="" border="0" />
				<?php echo TemplateLang(array('p' => 'error'), $this);?>

			<?php else: ?>
				<img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ok.png" width="16" height="16" alt="" border="0" />
				<?php echo TemplateLang(array('p' => 'success'), $this);?>

			<?php endif; ?></td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
		</table>
		
		<p align="right">
			<input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'back'), $this);?>
 " onclick="document.location.href='optimize.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
';" />
		</p>
	<?php elseif ($this->_tpl_vars['executeStruct']): ?>
	<form action="optimize.php?do=repairStruct&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
		<table class="list">
		<tr>
			<th>&nbsp;</th>
			<th><?php echo TemplateLang(array('p' => 'table'), $this);?>
</th>
			<th width="120"><?php echo TemplateLang(array('p' => 'exists'), $this);?>
</th>
			<th width="120"><?php echo TemplateLang(array('p' => 'structstate'), $this);?>
</th>
			<th width="120"><?php echo TemplateLang(array('p' => 'status'), $this);?>
</th>
		</tr>
		<?php $_from = $this->_tpl_vars['result']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['table']):
?>
		<?php echo smarty_function_cycle(array('values' => "td1,td2",'name' => 'class','assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td width="20"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/db_table.png" border="0" alt="" width="16" height="16" /></td>
			<td><?php echo $this->_tpl_vars['table']['table']; ?>
</td>
			<td><?php if ($this->_tpl_vars['table']['exists']): ?><?php echo TemplateLang(array('p' => 'yes'), $this);?>
<?php else: ?><?php echo TemplateLang(array('p' => 'no'), $this);?>
<?php endif; ?></td>
			<td><?php echo $this->_tpl_vars['table']['missing']; ?>
 / <?php echo $this->_tpl_vars['table']['invalid']; ?>
</td>
			<td align="right"><?php if (! $this->_tpl_vars['table']['exists'] || $this->_tpl_vars['table']['missing'] || $this->_tpl_vars['table']['invalid']): ?>
				<img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/error.png" width="16" height="16" alt="" border="0" />
				<?php echo TemplateLang(array('p' => 'error'), $this);?>

			<?php else: ?>
				<img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ok.png" width="16" height="16" alt="" border="0" />
				<?php echo TemplateLang(array('p' => 'ok'), $this);?>

			<?php endif; ?></td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
		</table>
		
		<p>
			<div style="float:left;">
				<?php if ($this->_tpl_vars['repair']): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/warning.png" border="0" alt="" width="16" height="16" align="absmiddle" />
				<?php echo TemplateLang(array('p' => 'dbwarn'), $this);?>
<?php endif; ?>
			</div>
			<div style="float:right;">
				<input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'back'), $this);?>
 " onclick="document.location.href='optimize.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
';" />
				<?php if ($this->_tpl_vars['repair']): ?><input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'repairstruct'), $this);?>
 " /><?php endif; ?>
			</div>
		</p>
	</form>
	<?php else: ?>
	<form action="optimize.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&do=execute" method="post" onsubmit="spin(this)">
		<table>
			<tr>
				<td><?php echo TemplateLang(array('p' => 'tables'), $this);?>
:</td>
				<td><?php echo TemplateLang(array('p' => 'action'), $this);?>
:</td>
			</tr>
			<tr>
				<td valign="top"><select size="10" name="tables[]" multiple="multiple">
				<?php $_from = $this->_tpl_vars['tables']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['table']):
?>
					<option value="<?php echo $this->_tpl_vars['table']; ?>
" selected="selected"><?php echo $this->_tpl_vars['table']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
				</select></td>
				<td valign="top">
					<table>
						<tr>
							<td valign="top" width="20" align="center"><input type="radio" id="op_optimize" name="operation" value="optimize" checked="checked" /></td>
							<td valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/db_optimize.png" border="0" alt="" width="32" height="32" /></td>
							<td><label for="op_optimize"><b><?php echo TemplateLang(array('p' => 'op_optimize'), $this);?>
</b></label><br />
								<?php echo TemplateLang(array('p' => 'op_optimize_desc'), $this);?>
</td>
						</tr>
						<tr>
							<td colspan="3">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td valign="top" width="20" align="center"><input type="radio" id="op_repair" name="operation" value="repair" /></td>
							<td valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/db_repair.png" border="0" alt="" width="32" height="32" /></td>
							<td><label for="op_repair"><b><?php echo TemplateLang(array('p' => 'op_repair'), $this);?>
</b></label><br />
								<?php echo TemplateLang(array('p' => 'op_repair_desc'), $this);?>
</td>
						</tr>
						<tr>
							<td colspan="3">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td valign="top" width="20" align="center"><input type="radio" id="op_struct" name="operation" value="struct" /></td>
							<td valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/db_struct.png" border="0" alt="" width="32" height="32" /></td>
							<td><label for="op_struct"><b><?php echo TemplateLang(array('p' => 'op_struct'), $this);?>
</b></label><br />
								<?php echo TemplateLang(array('p' => 'op_struct_desc'), $this);?>
</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<p>
			<div style="float:left;">
				<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/warning.png" border="0" alt="" width="16" height="16" align="absmiddle" />
				<?php echo TemplateLang(array('p' => 'dbwarn'), $this);?>

			</div>
			<div style="float:right;">
				<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'execute'), $this);?>
 " />
			</div>
		</p>
	</form>
	<?php endif; ?>
</fieldset>