<?php /* Smarty version 2.6.28, created on 2020-09-29 11:32:10
         compiled from plugins.list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'plugins.list.tpl', 2, false),array('function', 'text', 'plugins.list.tpl', 19, false),array('function', 'cycle', 'plugins.list.tpl', 29, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'installedplugins'), $this);?>
</legend>
	
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th><?php echo TemplateLang(array('p' => 'title'), $this);?>
</th>
			<th><?php echo TemplateLang(array('p' => 'author'), $this);?>
</th>
			<th><?php echo TemplateLang(array('p' => 'info'), $this);?>
</th>
			<th><?php echo TemplateLang(array('p' => 'status'), $this);?>
</th>
			<th>&nbsp;</th>
		</tr>
		
		<?php $_from = $this->_tpl_vars['plugins']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['packageSignature'] => $this->_tpl_vars['pluginPackage']):
?>
		
		<tr class="tableSubHead">
			<td colspan="5">
				<a href="javascript:togglePluginPackage('<?php echo $this->_tpl_vars['packageSignature']; ?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/contract.gif" border="0" alt="" align="absmiddle" id="packageImage_<?php echo $this->_tpl_vars['packageSignature']; ?>
" /></a>
				<?php if ($this->_tpl_vars['pluginPackage']['name']): ?><b><?php echo TemplateLang(array('p' => 'package'), $this);?>
:</b> <?php echo TemplateText(array('value' => $this->_tpl_vars['pluginPackage']['name']), $this);?>
<?php else: ?><?php echo TemplateLang(array('p' => 'withoutpackage'), $this);?>
<?php endif; ?>
			</td>
			<td>
				<?php if ($this->_tpl_vars['packageSignature']): ?><a href="plugins.php?action=<?php echo $this->_tpl_vars['action']; ?>
&do=deletePackage&package=<?php echo $this->_tpl_vars['packageSignature']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onclick="return confirm('<?php echo TemplateLang(array('p' => 'realpackage'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/delete.png" border="0" alt="<?php echo TemplateLang(array('p' => 'edit'), $this);?>
" width="16" height="16" /></a><?php else: ?>&nbsp;<?php endif; ?>
			</td>
		</tr>
		
		<tbody id="package_<?php echo $this->_tpl_vars['packageSignature']; ?>
" style="display:;">
		
		<?php $_from = $this->_tpl_vars['pluginPackage']['plugins']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['plugin']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/plugin_<?php if (! $this->_tpl_vars['plugin']['installed']): ?>in<?php endif; ?>active.png" border="0" alt="" width="16" height="16" /></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['plugin']['title']), $this);?>
<br /><small><?php echo TemplateText(array('value' => $this->_tpl_vars['plugin']['name']), $this);?>
</small></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['plugin']['author']), $this);?>
</td>
			<td><?php echo TemplateLang(array('p' => 'version'), $this);?>
: <?php echo TemplateText(array('value' => $this->_tpl_vars['plugin']['version']), $this);?>
<br /><small><?php echo TemplateLang(array('p' => 'type'), $this);?>
: <?php echo $this->_tpl_vars['plugin']['type']; ?>
</small></td>
			<td><?php if ($this->_tpl_vars['plugin']['installed']): ?><?php echo TemplateLang(array('p' => 'installed'), $this);?>
<?php else: ?><?php echo TemplateLang(array('p' => 'notinstalled'), $this);?>
<?php endif; ?><br />
				<?php if ($this->_tpl_vars['plugin']['paused']): ?><small><?php echo TemplateLang(array('p' => 'paused'), $this);?>
</small><?php endif; ?></td>
			<td>
				<a href="plugins.php?action=<?php echo $this->_tpl_vars['action']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&do=<?php if ($this->_tpl_vars['plugin']['installed']): ?>de<?php endif; ?>activatePlugin&plugin=<?php echo $this->_tpl_vars['plugin']['name']; ?>
" title="<?php echo TemplateLang(array('p' => 'acdeactivate'), $this);?>
" onclick="return confirm('<?php echo TemplateLang(array('p' => 'reallyplugin'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/plugin_switch.png" border="0" alt="<?php echo TemplateLang(array('p' => 'acdeactivate'), $this);?>
" border="0" width="16" height="16" /></a>
				<?php if ($this->_tpl_vars['plugin']['installed']): ?><a href="plugins.php?action=<?php echo $this->_tpl_vars['action']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&do=<?php if ($this->_tpl_vars['plugin']['paused']): ?>un<?php endif; ?>pausePlugin&plugin=<?php echo $this->_tpl_vars['plugin']['name']; ?>
" title="<?php if ($this->_tpl_vars['plugin']['paused']): ?><?php echo TemplateLang(array('p' => 'continue'), $this);?>
<?php else: ?><?php echo TemplateLang(array('p' => 'pause'), $this);?>
<?php endif; ?>"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php if ($this->_tpl_vars['plugin']['paused']): ?>error<?php else: ?>ok<?php endif; ?>.png" border="0" alt="<?php if ($this->_tpl_vars['plugin']['paused']): ?><?php echo TemplateLang(array('p' => 'continue'), $this);?>
<?php else: ?><?php echo TemplateLang(array('p' => 'pause'), $this);?>
<?php endif; ?>" border="0" width="16" height="16" /></a><?php endif; ?>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
		
		</tbody>
		
		<?php endforeach; endif; unset($_from); ?>
	</table>
</fieldset>

<?php if ($this->_tpl_vars['reloadMenu']): ?>
<script language="javascript">
<!--
	parent.frames['menu'].location.href = 'main.php?action=menu&item=4&sid=<?php echo $this->_tpl_vars['sid']; ?>
';
//-->
</script>
<?php endif; ?>