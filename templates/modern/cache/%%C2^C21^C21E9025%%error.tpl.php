<?php /* Smarty version 2.6.28, created on 2020-09-29 11:43:53
         compiled from nli/error.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'nli/error.tpl', 3, false),)), $this); ?>
<html>
<head>
	<title><?php echo $this->_tpl_vars['service_title']; ?>
: <?php echo TemplateLang(array('p' => 'error'), $this);?>
</title>
	<style>
	<!--
		<?php echo '*			{ font-family: tahoma, arial, verdana; font-size: 12px; }
		H1			{ font-size: 16px; font-weight: bold; border-bottom: 1px solid #DDDDDD; }
		H2			{ font-size: 14px; font-weight: normal; }
		.addInfo	{ font-family: courier, courier new; font-size: 10px; height: 100px; overflow: auto;
						border: 1px solid #DDDDDD; padding: 5px; }
		.box		{ width: 600px; border: 1px solid #CCC; border-radius: 10px; background-color: #FFF;
						padding: 30px 15px; margin-top: 3em; margin-left: auto; margin-right: auto; }'; ?>

	//-->
	</style>
</head>
<body bgcolor="#F1F2F6">
	
	<div class="box">
		<table width="100%">
			<tr>
				<td align="center" width="80" valign="top"><img src="./res/error_icon.png" border="0" alt="" /></td>
				<td valign="top" align="left">
				
					<h1><?php echo $this->_tpl_vars['title']; ?>
</h1>
					<h2><?php echo $this->_tpl_vars['description']; ?>
</h2>
					
					<hr size="1" color="#DDDDDD" width="100%" noshade="noshade" />
					<input type="button" value="&nbsp; <?php echo TemplateLang(array('p' => 'start'), $this);?>
 &nbsp;" onclick="document.location.href='./';" style="padding: 1px;" />
					
				</td>
			</tr>
		</table>
	</div>

</body>
</html>