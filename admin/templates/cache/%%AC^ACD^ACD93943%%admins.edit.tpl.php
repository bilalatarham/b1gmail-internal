<?php /* Smarty version 2.6.28, created on 2020-09-29 11:44:56
         compiled from admins.edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'admins.edit.tpl', 4, false),array('function', 'text', 'admins.edit.tpl', 4, false),)), $this); ?>
<form action="admins.php?action=admins&do=edit&id=<?php echo $this->_tpl_vars['admin']['adminid']; ?>
&save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
	
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'editadmin'), $this);?>
: <?php echo TemplateText(array('value' => $this->_tpl_vars['admin']['username']), $this);?>
</legend>
		
		<table width="100%">
			<tr>
				<td width="40" valign="top" rowspan="6"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_users.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'username'), $this);?>
:</td>
				<td class="td2"><input type="text" size="28" id="username" name="username" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['admin']['username']), $this);?>
" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'firstname'), $this);?>
:</td>
				<td class="td2"><input type="text" size="36" id="firstname" name="firstname" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['admin']['firstname'],'allowEmpty' => true), $this);?>
" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'lastname'), $this);?>
:</td>
				<td class="td2"><input type="text" size="36" id="lastname" name="lastname" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['admin']['lastname'],'allowEmpty' => true), $this);?>
" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'type'), $this);?>
:</td>
				<td class="td2"><select name="type"<?php if ($this->_tpl_vars['admin']['adminid'] == 1): ?> disabled="disabled"<?php endif; ?> onclick="EBID('perms').style.display=this.value==0?'none':'';">
					<option value="1"<?php if ($this->_tpl_vars['admin']['type'] == 1): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'admin'), $this);?>
</option>
					<option value="0"<?php if ($this->_tpl_vars['admin']['type'] == 0): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'superadmin'), $this);?>
</option>
				</select></td>
			</tr>
		</table>
	</fieldset>
	
	<fieldset id="perms" style="display:<?php if ($this->_tpl_vars['admin']['type'] == 0): ?>none<?php endif; ?>;">
		<legend><?php echo TemplateLang(array('p' => 'permissions'), $this);?>
</legend>
		
		<table width="100%">
			<tr>
				<td width="40" valign="top" rowspan="2"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_validation.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'areas'), $this);?>
:</td>
				<td class="td2">
					<?php $_from = $this->_tpl_vars['permsTable']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['permName'] => $this->_tpl_vars['permTitle']):
?>
					<input type="checkbox" name="perms[<?php echo $this->_tpl_vars['permName']; ?>
]" value="1" id="perm_<?php echo $this->_tpl_vars['permName']; ?>
"<?php if ($this->_tpl_vars['admin']['perms'][$this->_tpl_vars['permName']]): ?> checked="checked"<?php endif; ?> />
					<label for="perm_<?php echo $this->_tpl_vars['permName']; ?>
" style="font-weight:bold;"><?php echo $this->_tpl_vars['permTitle']; ?>
</label><br />
					<?php endforeach; endif; unset($_from); ?>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'plugins'), $this);?>
:</td>
				<td class="td2">
					<?php $_from = $this->_tpl_vars['pluginList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['pluginName'] => $this->_tpl_vars['pluginTitle']):
?>
					<input type="checkbox" name="perms[plugins][<?php echo $this->_tpl_vars['pluginName']; ?>
]" value="1" id="plugin_<?php echo $this->_tpl_vars['pluginName']; ?>
"<?php if ($this->_tpl_vars['admin']['perms']['plugins'][$this->_tpl_vars['pluginName']]): ?> checked="checked"<?php endif; ?> />
					<label for="plugin_<?php echo $this->_tpl_vars['pluginName']; ?>
" style="font-weight:bold;"><?php echo TemplateText(array('value' => $this->_tpl_vars['pluginTitle']), $this);?>
</label><br />
					<?php endforeach; endif; unset($_from); ?>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'password'), $this);?>
</legend>
	
		<table>
			<tr>
				<td width="40" valign="top" rowspan="2"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_login.png" border="0" alt="" width="32" height="32" /></td>				
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'newpassword'), $this);?>
:</td>
				<td class="td2"><input type="password" name="newpw1" size="36" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'newpassword'), $this);?>
 (<?php echo TemplateLang(array('p' => 'repeat'), $this);?>
):</td>
				<td class="td2"><input type="password" name="newpw2" size="36" /></td>
			</tr>
		</table>
	</fieldset>
	
	<p>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>

</form>