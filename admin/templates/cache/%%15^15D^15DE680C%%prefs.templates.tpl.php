<?php /* Smarty version 2.6.28, created on 2020-09-29 15:49:57
         compiled from prefs.templates.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.templates.tpl', 2, false),array('function', 'cycle', 'prefs.templates.tpl', 13, false),array('function', 'text', 'prefs.templates.tpl', 16, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'templates'), $this);?>
</legend>

	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th><?php echo TemplateLang(array('p' => 'template'), $this);?>
</th>
			<th><?php echo TemplateLang(array('p' => 'author'), $this);?>
</th>
			<th width="60">&nbsp;</th>
		</tr>
		
		<?php $_from = $this->_tpl_vars['templates']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['template'] => $this->_tpl_vars['templateInfo']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/template.png" border="0" alt="" width="16" height="16" /></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['templateInfo']['title']), $this);?>
<br /><small><?php echo TemplateText(array('value' => $this->_tpl_vars['template']), $this);?>
</small></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['templateInfo']['author']), $this);?>
<br /><small><?php echo TemplateText(array('value' => $this->_tpl_vars['templateInfo']['website'],'allowEmpty' => true), $this);?>
</small></td>
			<td>
				<?php if ($this->_tpl_vars['templateInfo']['prefs']): ?><a href="prefs.templates.php?do=prefs&template=<?php echo $this->_tpl_vars['template']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/prefs.png" border="0" alt="<?php echo TemplateLang(array('p' => 'prefs'), $this);?>
" width="16" height="16" /></a><?php endif; ?>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
	</table>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'defaultemplate'), $this);?>
</legend>
	
	<form action="prefs.templates.php?save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
		<table width="100%">
			<tr>
				<td width="40" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/template32.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="150"><?php echo TemplateLang(array('p' => 'template'), $this);?>
:</td>
				<td class="td2"><select name="template">
				<?php $_from = $this->_tpl_vars['templates']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['template'] => $this->_tpl_vars['templateInfo']):
?>
					<option value="<?php echo $this->_tpl_vars['template']; ?>
"<?php if ($this->_tpl_vars['defaultTemplate'] == $this->_tpl_vars['template']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['templateInfo']['title']), $this);?>
</option>
				<?php endforeach; endif; unset($_from); ?>
				</select></td>
			</tr>
		</table>
	
		<p align="right">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</p>
	</form>
</fieldset>