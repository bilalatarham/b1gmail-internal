<?php 
/*
 * b1gMail
 * (c) 2002-2016 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 */

include('./serverlib/init.inc.php');
include('./serverlib/dashboard.class.php');
RequestPrivileges(PRIVILEGES_USER);

/**
 * file handler for modules
 */
ModuleFunction('FileHandler',
	array(substr(__FILE__, strlen(dirname(__FILE__))+1),
	isset($_REQUEST['action']) ? $_REQUEST['action'] : ''));

/**
 * default action = start
 */
if(!isset($_REQUEST['action']))
	$_REQUEST['action'] = 'start';
$tpl->assign('activeTab', 'start');
$tpl->assign('pageTitle', $lang_user['start']);

/**
 * page sidebar
 */
$tpl->assign('pageMenuFile', 'li/start.sidebar.tpl');

/**
 * dashboard
 */
$dashboard = _new('BMDashboard', array(BMWIDGET_START));

/**
 * start page
 */
if($_REQUEST['action'] == 'start')
{
	$widgetOrder = $thisUser->GetPref('widgetOrderStart');
	if($widgetOrder === false || trim($widgetOrder) == '')
		$widgetOrder = $bm_prefs['widget_order_start'];
	
	$tpl->assign('autoSetPreviewPos', !$thisUser->GetPref('previewPosition'));
	$tpl->assign('pageTitle', $lang_user['welcome']);
	$tpl->assign('widgetOrder', $widgetOrder);
	$tpl->assign('widgets', $dashboard->getWidgetArray($widgetOrder));
	$tpl->assign('pageContent', 'li/start.page.tpl');
	$tpl->display('li/index.tpl');
}

/**
 * save widget order
 */
else if($_REQUEST['action'] == 'saveWidgetOrder'
			&& isset($_REQUEST['order']))
{
	$widgetOrder = $_REQUEST['order'];
	
	if($dashboard->checkWidgetOrder($widgetOrder))			
	{
		$thisUser->SetPref('widgetOrderStart', $widgetOrder);
		die('OK');
	}
	else 
	{
		die('Invalid order');
	}
}

/**
 * customize widgets
 */
else if($_REQUEST['action'] == 'customize')
{
	$widgetOrder = $thisUser->GetPref('widgetOrderStart');
	if($widgetOrder === false || trim($widgetOrder) == '')
		$widgetOrder = $bm_prefs['widget_order_start'];
		
	$tpl->assign('pageTitle', $lang_user['customize']);
	$tpl->assign('possibleWidgets', $dashboard->getPossibleWidgets($widgetOrder));
	$tpl->assign('pageContent', 'li/start.customize.tpl');
	$tpl->display('li/index.tpl');	
}

/**
 * save cutomization
 */
else if($_REQUEST['action'] == 'saveCustomize')
{
	$widgetOrder = $thisUser->GetPref('widgetOrderStart');
	if($widgetOrder === false || trim($widgetOrder) == '')
		$widgetOrder = $bm_prefs['widget_order_start'];
	$newOrder = $dashboard->generateOrderStringFromPostForm($widgetOrder);
	
	$thisUser->SetPref('widgetOrderStart', $newOrder);
	
	header('Location: start.php?sid=' . session_id());
	exit();
}

/**
 * search
 */
else if($_REQUEST['action'] == 'search'
		&& isset($_REQUEST['q']))
{
	$url = sprintf($bm_prefs['search_engine'], urlencode($_REQUEST['q']));
	header('Location: ' . $url);
	exit();
}

/**
 * widget preferences
 */
else if($_REQUEST['action'] == 'showWidgetPrefs'
		&& isset($_REQUEST['name']))
{
	$dashboard->showWidgetPrefs($_REQUEST['name']);
}

/**
 * safe code validation RPC
 */
else if($_REQUEST['action'] == 'checkSafeCode')
{
	if(!class_exists('BMCaptcha'))
		include(B1GMAIL_DIR . 'serverlib/captcha.class.php');
	$captcha = BMCaptcha::createDefaultProvider();
	if($captcha->check(false))
		echo '1';
	else
		echo '0';
	exit();
}

/**
 * notifications
 */
else if($_REQUEST['action'] == 'getNotifications')
{
	$tpl->assign('bmNotifications', $thisUser->GetNotifications());
	$tpl->display('li/notifications.tpl');
}

/**
 * notification count
 */
else if($_REQUEST['action'] == 'getNotificationCount')
{
	echo $thisUser->GetUnreadNotifications();
	exit();
}

/**
 * logout
 */
else if($_REQUEST['action'] == 'logout')
{
	BMUser::Logout();
	header('Location: ' . $bm_prefs['logouturl']);
	exit();
}
?>