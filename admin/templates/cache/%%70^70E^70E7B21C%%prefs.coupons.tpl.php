<?php /* Smarty version 2.6.28, created on 2020-09-30 13:03:48
         compiled from prefs.coupons.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.coupons.tpl', 2, false),array('function', 'cycle', 'prefs.coupons.tpl', 17, false),array('function', 'text', 'prefs.coupons.tpl', 21, false),array('function', 'date', 'prefs.coupons.tpl', 23, false),array('function', 'html_select_date', 'prefs.coupons.tpl', 123, false),array('function', 'html_select_time', 'prefs.coupons.tpl', 124, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'coupons'), $this);?>
</legend>
	
	<form action="prefs.coupons.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" name="f1" onsubmit="spin(this)">
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="25" style="text-align:center;"><a href="javascript:invertSelection(document.forms.f1,'coupon_');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/dot.png" border="0" alt="" width="10" height="8" /></a></th>
			<th><?php echo TemplateLang(array('p' => 'code'), $this);?>
</th>
			<th><?php echo TemplateLang(array('p' => 'validitytime'), $this);?>
</th>
			<th width="160"><?php echo TemplateLang(array('p' => 'validity'), $this);?>
</th>
			<th width="160"><?php echo TemplateLang(array('p' => 'used3'), $this);?>
 / <?php echo TemplateLang(array('p' => 'count'), $this);?>
</th>
			<th width="60">&nbsp;</th>
		</tr>
		
		<?php $_from = $this->_tpl_vars['coupons']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['coupon']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/coupon.png" border="0" alt="" width="16" height="16" /></td>
			<td align="center"><input type="checkbox" name="coupon_<?php echo $this->_tpl_vars['coupon']['id']; ?>
" /></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['coupon']['code']), $this);?>
<br />
				<small><?php if ($this->_tpl_vars['coupon']['ver']['sms']): ?><?php echo TemplateLang(array('p' => 'addcredits'), $this);?>
: <?php echo $this->_tpl_vars['coupon']['ver']['sms']; ?>
<?php endif; ?><?php if ($this->_tpl_vars['coupon']['ver']['gruppe']): ?><?php if ($this->_tpl_vars['coupon']['ver']['sms']): ?>, <?php endif; ?><?php echo TemplateLang(array('p' => 'movetogroup'), $this);?>
: <?php echo $this->_tpl_vars['groups'][$this->_tpl_vars['coupon']['ver']['gruppe']]['title']; ?>
<?php endif; ?></small></td>
			<td><?php echo TemplateLang(array('p' => 'to'), $this);?>
 <?php if ($this->_tpl_vars['coupon']['bis'] == -1): ?>(<?php echo TemplateLang(array('p' => 'unlimited'), $this);?>
)<?php else: ?><?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['coupon']['bis']), $this);?>
<?php endif; ?>
				<br /><small><?php echo TemplateLang(array('p' => 'from'), $this);?>
 <?php if ($this->_tpl_vars['coupon']['von'] == -1): ?>(<?php echo TemplateLang(array('p' => 'unlimited'), $this);?>
)<?php else: ?><?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['coupon']['von']), $this);?>
<?php endif; ?></small></td>
			<td>
				<?php if ($this->_tpl_vars['coupon']['valid_signup']): ?><?php echo TemplateLang(array('p' => 'signup'), $this);?>
<?php if ($this->_tpl_vars['coupon']['valid_loggedin']): ?>,<?php endif; ?><?php endif; ?>
				<?php if ($this->_tpl_vars['coupon']['valid_loggedin']): ?><?php echo TemplateLang(array('p' => 'li'), $this);?>
<?php endif; ?>
			</td>
			<td><?php echo $this->_tpl_vars['coupon']['used']; ?>

				/ <?php if ($this->_tpl_vars['coupon']['anzahl'] == -1): ?>(<?php echo TemplateLang(array('p' => 'unlimited'), $this);?>
)<?php else: ?><?php echo $this->_tpl_vars['coupon']['anzahl']; ?>
<?php endif; ?></td>
			<td>
				<a href="prefs.coupons.php?do=edit&id=<?php echo $this->_tpl_vars['coupon']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/edit.png" border="0" alt="<?php echo TemplateLang(array('p' => 'edit'), $this);?>
" width="16" height="16" /></a>
				<a href="prefs.coupons.php?delete=<?php echo $this->_tpl_vars['coupon']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onclick="return confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/delete.png" border="0" alt="<?php echo TemplateLang(array('p' => 'edit'), $this);?>
" width="16" height="16" /></a>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
		
		<tr>
			<td class="footer" colspan="8">
				<div style="float:left;">
					<?php echo TemplateLang(array('p' => 'action'), $this);?>
: <select name="massAction" class="smallInput">
						<option value="-">------------</option>
						
						<optgroup label="<?php echo TemplateLang(array('p' => 'actions'), $this);?>
">
							<option value="delete"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option>
						</optgroup>
					</select>&nbsp;
				</div>
				<div style="float:left;">
					<input type="submit" name="executeMassAction" value=" <?php echo TemplateLang(array('p' => 'execute'), $this);?>
 " class="smallInput" />
				</div>
			</td>
		</tr>
	</table>
	</form>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'addcoupon'), $this);?>
</legend>
	
	<form action="prefs.coupons.php?add=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
		<table width="100%">
			<tr>
				<td width="40" valign="top" rowspan="6"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/coupon32.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="150"><?php echo TemplateLang(array('p' => 'codes'), $this);?>
:</td>
				<td class="td2">
					<textarea style="width:100%;height:80px;" name="code" id="codes"></textarea>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td align="left"><small><?php echo TemplateLang(array('p' => 'sepby'), $this);?>
</small></td>
							<td align="right">[ <a href="javascript:void(0);" onclick="EBID('generator').style.display=EBID('generator').style.display==''?'none':'';"><?php echo TemplateLang(array('p' => 'generate'), $this);?>
</a> ]</td>
						</tr>
					</table>
					<div id="generator" style="display:none;">
						<fieldset>
							<legend><?php echo TemplateLang(array('p' => 'generate'), $this);?>
</legend>
							<table width="100%">
								<tr>
									<td class="td1" width="120"><?php echo TemplateLang(array('p' => 'count'), $this);?>
:</td>
									<td class="td2"><input type="text" id="generator_count" value="10" size="6" /></td>
								</tr>
								<tr>
									<td class="td1"><?php echo TemplateLang(array('p' => 'length'), $this);?>
:</td>
									<td class="td2"><input type="text" id="generator_length" value="10" size="6" /></td>
								</tr>
								<tr>
									<td class="td1"><?php echo TemplateLang(array('p' => 'chars'), $this);?>
:</td>
									<td class="td2">
										<input type="checkbox" checked="checked" id="generator_az" />
											<label for="generator_az"><b>a-z</b></label>
										<input type="checkbox" checked="checked" id="generator_az2" />
											<label for="generator_az2"><b>A-Z</b></label>
										<input type="checkbox" checked="checked" id="generator_09" />
											<label for="generator_09"><b>0-9</b></label>
										<input type="checkbox" id="generator_special" />
											<label for="generator_special"><b>.,_-&amp;$</b></label>
									</td>
								</tr>
								<tr>
									<td class="td1">&nbsp;</td>
									<td class="td2"><input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'generate'), $this);?>
 " onclick="generateCodes(EBID('codes'));EBID('generator').style.display='none';" /></td>
								</tr>
							</table>
						</fieldset>
					</div>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'count'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" onchange="EBID('count').value=this.checked?'-1':'0';" checked="checked" id="count_unlim" />
					<label for="count_unlim"><b><?php echo TemplateLang(array('p' => 'unlimited'), $this);?>
</b></label>
					<?php echo TemplateLang(array('p' => 'or'), $this);?>

					<input type="text" size="6" name="anzahl" id="count" value="-1" onkeyup="EBID('count_unlim').checked=this.value=='-1';" />
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'from'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" checked="checked" id="from_unlim" name="von_unlim" />
					<label for="from_unlim"><b><?php echo TemplateLang(array('p' => 'now'), $this);?>
</b></label>
					<?php echo TemplateLang(array('p' => 'or'), $this);?>

					<?php echo smarty_function_html_select_date(array('prefix' => 'von','start_year' => "-5",'field_order' => 'DMY','field_separator' => "."), $this);?>
, 
					<?php echo smarty_function_html_select_time(array('prefix' => 'von','display_seconds' => false), $this);?>

				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'to'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" checked="checked" id="to_unlim" name="bis_unlim" />
					<label for="to_unlim"><b><?php echo TemplateLang(array('p' => 'unlimited'), $this);?>
</b></label>
					<?php echo TemplateLang(array('p' => 'or'), $this);?>

					<?php echo smarty_function_html_select_date(array('prefix' => 'bis','end_year' => "+5",'field_order' => 'DMY','field_separator' => "."), $this);?>
, 
					<?php echo smarty_function_html_select_time(array('prefix' => 'bis','display_seconds' => false), $this);?>

				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'validity'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" checked="checked" id="valid_signup" name="valid_signup" />
					<label for="valid_signup"><b><?php echo TemplateLang(array('p' => 'signup'), $this);?>
</b></label><br />
					<input type="checkbox" checked="checked" id="valid_loggedin" name="valid_loggedin" />
					<label for="valid_loggedin"><b><?php echo TemplateLang(array('p' => 'li'), $this);?>
</b></label>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'benefit'), $this);?>
:</td>
				<td class="td2">
					<div>
						<input type="checkbox" name="ver_gruppe" id="ver_gruppe" />
						<label for="ver_gruppe"><b><?php echo TemplateLang(array('p' => 'movetogroup'), $this);?>
:</b></label>
						<select name="ver_gruppe_id">
						<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['group']):
?>
							<option value="<?php echo $this->_tpl_vars['group']['id']; ?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['group']['title']), $this);?>
</option>
						<?php endforeach; endif; unset($_from); ?>
						</select>
					</div>
					
					<div>
						<input type="checkbox" name="ver_credits" id="ver_credits" />
						<label for="ver_credits"><b><?php echo TemplateLang(array('p' => 'addcredits'), $this);?>
:</b></label>
						<input type="text" name="ver_credits_count" value="5" size="6" />
					</div>
				</td>
			</tr>
		</table>
	
		<p align="right">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'add'), $this);?>
 " />
		</p>
	</form>
</fieldset>