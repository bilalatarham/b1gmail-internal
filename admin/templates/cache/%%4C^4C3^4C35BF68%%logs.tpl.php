<?php /* Smarty version 2.6.28, created on 2020-09-29 11:36:26
         compiled from logs.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'logs.tpl', 2, false),array('function', 'date', 'logs.tpl', 2, false),array('function', 'cycle', 'logs.tpl', 12, false),array('function', 'text', 'logs.tpl', 15, false),array('function', 'html_select_date', 'logs.tpl', 35, false),array('function', 'html_select_time', 'logs.tpl', 36, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'logs'), $this);?>
 (<?php echo TemplateDate(array('nice' => true,'timestamp' => $this->_tpl_vars['start']), $this);?>
 - <?php echo TemplateDate(array('nice' => true,'timestamp' => $this->_tpl_vars['end']), $this);?>
)</legend>
	
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th><?php echo TemplateLang(array('p' => 'entry'), $this);?>
</th>
			<th width="150"><?php echo TemplateLang(array('p' => 'date'), $this);?>
</th>
		</tr>
		
		<?php $_from = $this->_tpl_vars['entries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['entry']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php echo $this->_tpl_vars['entry']['prioImg']; ?>
.png" border="0" alt="" width="16" height="16" /></td>
			<td><code><?php echo TemplateText(array('value' => $this->_tpl_vars['entry']['eintrag']), $this);?>
</code></td>
			<td><?php echo TemplateDate(array('nice' => true,'timestamp' => $this->_tpl_vars['entry']['zeitstempel']), $this);?>
</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
	</table>
	
	<p align="right">
		<input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'export'), $this);?>
 " onclick="parent.frames['top'].location.href='logs.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&do=export&start=<?php echo $this->_tpl_vars['start']; ?>
&end=<?php echo $this->_tpl_vars['end']; ?>
&q=<?php echo $this->_tpl_vars['ueQ']; ?>
<?php echo $this->_tpl_vars['prioQ']; ?>
';" />
	</p>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'filter'), $this);?>
</legend>
	
	<form action="logs.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
		<table>
			<tr>
				<td width="40" valign="top" rowspan="4"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/filter.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="80"><?php echo TemplateLang(array('p' => 'from'), $this);?>
:</td>
				<td class="td2">
						<?php echo smarty_function_html_select_date(array('prefix' => 'start','time' => $this->_tpl_vars['start'],'start_year' => "-5",'field_order' => 'DMY','field_separator' => "."), $this);?>
, 
						<?php echo smarty_function_html_select_time(array('prefix' => 'start','time' => $this->_tpl_vars['start'],'display_seconds' => false), $this);?>

				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'to'), $this);?>
:</td>
				<td class="td2">
						<?php echo smarty_function_html_select_date(array('prefix' => 'end','time' => $this->_tpl_vars['end'],'start_year' => "-5",'field_order' => 'DMY','field_separator' => "."), $this);?>
, 
						<?php echo smarty_function_html_select_time(array('prefix' => 'end','time' => $this->_tpl_vars['end'],'display_seconds' => false), $this);?>

				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'priority'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox"<?php if ($this->_tpl_vars['prio'][8]): ?> checked="checked"<?php endif; ?> name="prio[8]" id="prio8" />
					<label for="prio8"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/debug.png" border="0" alt="" width="16" height="16" /></label> &nbsp; 
					
					<input type="checkbox"<?php if ($this->_tpl_vars['prio'][2]): ?> checked="checked"<?php endif; ?> name="prio[2]" id="prio2" />
					<label for="prio2"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/info.png" border="0" alt="" width="16" height="16" /></label> &nbsp; 
					
					<input type="checkbox"<?php if ($this->_tpl_vars['prio'][1]): ?> checked="checked"<?php endif; ?> name="prio[1]" id="prio1" />
					<label for="prio1"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/warning.png" border="0" alt="" width="16" height="16" /></label> &nbsp; 
					
					<input type="checkbox"<?php if ($this->_tpl_vars['prio'][4]): ?> checked="checked"<?php endif; ?> name="prio[4]" id="prio4" />
					<label for="prio4"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/error.png" border="0" alt="" width="16" height="16" /></label> &nbsp; 
					
					<input type="checkbox"<?php if ($this->_tpl_vars['prio'][16]): ?> checked="checked"<?php endif; ?> name="prio[16]" id="prio16" />
					<label for="prio16"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/plugin.png" border="0" alt="" width="16" height="16" /></label>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'search'), $this);?>
:</td>
				<td class="td2">
						<input type="text" name="q" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['q'],'allowEmpty' => true), $this);?>
" size="36" style="width:85%;" />
				</td>
			</tr>
		</table>
		
		<p align="right">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'apply'), $this);?>
 " />
		</p>
	</form>
</fieldset>