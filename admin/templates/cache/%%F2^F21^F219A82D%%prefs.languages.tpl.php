<?php /* Smarty version 2.6.28, created on 2020-09-29 15:06:55
         compiled from prefs.languages.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.languages.tpl', 2, false),array('function', 'cycle', 'prefs.languages.tpl', 15, false),array('function', 'text', 'prefs.languages.tpl', 19, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'languages'), $this);?>
</legend>

	<form action="prefs.languages.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
" name="f1" method="post" onsubmit="spin(this)">
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="25" style="text-align:center;"><a href="javascript:invertSelection(document.forms.f1,'lang_');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/dot.png" border="0" alt="" width="10" height="8" /></a></th>
			<th><?php echo TemplateLang(array('p' => 'language'), $this);?>
</th>
			<th><?php echo TemplateLang(array('p' => 'author'), $this);?>
</th>
			<th width="60">&nbsp;</th>
		</tr>
		
		<?php $_from = $this->_tpl_vars['languages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['langID'] => $this->_tpl_vars['language']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/language.png" border="0" alt="" width="16" height="16" /></td>
			<td align="center"><?php if ($this->_tpl_vars['language']['writeable'] && ! $this->_tpl_vars['language']['default']): ?><input type="checkbox" name="lang_<?php echo $this->_tpl_vars['langID']; ?>
" /><?php endif; ?></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['language']['title']), $this);?>
<br /><small><?php echo TemplateText(array('value' => $this->_tpl_vars['language']['charset']), $this);?>
, <?php echo TemplateText(array('value' => $this->_tpl_vars['language']['locale']), $this);?>
</small></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['language']['author']), $this);?>
<br /><small><?php echo TemplateText(array('value' => $this->_tpl_vars['language']['authorWeb'],'allowEmpty' => true), $this);?>
</small></td>
			<td>
				<a href="prefs.languages.php?action=texts&lang=<?php echo $this->_tpl_vars['langID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/phrases.png" border="0" alt="<?php echo TemplateLang(array('p' => 'customtexts'), $this);?>
" width="16" height="16" /></a>
				<?php if ($this->_tpl_vars['language']['writeable'] && ! $this->_tpl_vars['language']['default']): ?><a href="prefs.languages.php?delete=<?php echo $this->_tpl_vars['langID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onclick="return confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/delete.png" border="0" alt="<?php echo TemplateLang(array('p' => 'delete'), $this);?>
" width="16" height="16" /></a><?php endif; ?>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
		
		<tr>
			<td class="footer" colspan="8">
				<div style="float:left;">
					<?php echo TemplateLang(array('p' => 'action'), $this);?>
: <select name="massAction" class="smallInput">
						<option value="-">------------</option>
						
						<optgroup label="<?php echo TemplateLang(array('p' => 'actions'), $this);?>
">
							<option value="delete"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option>
						</optgroup>
					</select>&nbsp;
				</div>
				<div style="float:left;">
					<input type="submit" name="executeMassAction" value=" <?php echo TemplateLang(array('p' => 'execute'), $this);?>
 " class="smallInput" />
				</div>
			</td>
		</tr>
	</table>
	</form>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'addlanguage'), $this);?>
</legend>
	
	<form action="prefs.languages.php?add=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" enctype="multipart/form-data" onsubmit="spin(this)">
		<p>
			<?php echo TemplateLang(array('p' => 'addlang_desc'), $this);?>

		</p>
		
		<table>
			<tr>
				<td width="40" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/lang32.png" border="0" alt="" width="32" height="32" /></td>
				<td><?php echo TemplateLang(array('p' => 'langfile'), $this);?>
:<br />
					<input type="file" name="langfile" style="width:440px;" accept=".lang.php" /></td>
			</tr>
		</table>
		
		<p>
			<div style="float:left;">
				<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/warning.png" border="0" alt="" align="absmiddle" width="16" height="16" />
				<?php echo TemplateLang(array('p' => 'sourcewarning'), $this);?>

			</div>
			<div style="float:right;">
				<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'install'), $this);?>
 " />
			</div>
		</p>
	</form>
</fieldset>