<?php /* Smarty version 2.6.28, created on 2020-09-29 11:37:33
         compiled from li/start.page.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'li/start.page.tpl', 3, false),array('function', 'hook', 'li/start.page.tpl', 18, false),array('function', 'text', 'li/start.page.tpl', 24, false),)), $this); ?>
<div id="contentHeader">
	<div class="left">
		<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_start.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'welcome'), $this);?>
!
	</div>
	
	<div class="right">
		<?php if ($this->_tpl_vars['templatePrefs']['showUserEmail']): ?>
		<em><?php echo $this->_tpl_vars['_userEmail']; ?>
</em>
		<?php endif; ?>
		<button onclick="document.location.href='start.php?action=customize&sid=<?php echo $this->_tpl_vars['sid']; ?>
';" type="button">
			<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_customize.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			<?php echo TemplateLang(array('p' => 'customize'), $this);?>

		</button>
	</div>
</div>

<div class="scrollContainer"><div class="pad">
	<?php echo TemplateHook(array('id' => "start.page.tpl:head"), $this);?>

	
	<div id="startBoxes">
	</div>
	<div id="startBoxes_elems" style="display:none">
	<?php $_from = $this->_tpl_vars['widgets']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['widget']):
?>
		<div title="<?php echo TemplateText(array('value' => $this->_tpl_vars['widget']['title']), $this);?>
" rel="<?php if ($this->_tpl_vars['widget']['hasPrefs']): ?>1<?php else: ?>0<?php endif; ?>,<?php echo $this->_tpl_vars['widget']['prefsW']; ?>
,<?php echo $this->_tpl_vars['widget']['prefsH']; ?>
,<?php if ($this->_tpl_vars['widget']['icon']): ?><?php echo $this->_tpl_vars['widget']['icon']; ?>
<?php else: ?>0<?php endif; ?>" id="<?php echo $this->_tpl_vars['key']; ?>
"><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['widget']['template'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
	<?php endforeach; endif; unset($_from); ?>
	</div>
	
	<?php echo TemplateHook(array('id' => "start.page.tpl:foot"), $this);?>

</div></div>

<script src="./clientlib/dragcontainer.js" language="javascript" type="text/javascript"></script>
<script language="javascript">
<!--
	currentSID = '<?php echo $this->_tpl_vars['sid']; ?>
';
	var dc = new dragContainer('startBoxes', 3, 'dc');
	dc.order = '<?php echo $this->_tpl_vars['widgetOrder']; ?>
';
	dc.onOrderChanged = startBoardOrderChanged;
	dc.run();
	<?php if ($this->_tpl_vars['autoSetPreviewPos']): ?>autoSetPreviewPos();<?php endif; ?>
//-->
</script>