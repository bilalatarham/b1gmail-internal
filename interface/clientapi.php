<?php
/*
 * b1gMail
 * (c) 2002-2016 B1G Software
 * 
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 */

define('INTERFACE_MODE', true);
include('../serverlib/init.inc.php');

$exportedClasses = array(
	'BMToolInterface'	=> array(
		'fileName'				=> '../serverlib/toolinterface.class.php',
		'constructorParams'		=> array(),
		'requiredPrivileges'	=> 0
	)
);

$response = array();

if(!isset($_REQUEST['class'])
	|| !isset($exportedClasses[$_REQUEST['class']]))
{
	$response['status'] = 'INVALID_CLASS';
}
else
{
	$classInfo = $exportedClasses[$_REQUEST['class']];
	
	// load class
	if(!class_exists($_REQUEST['class']))
		include($classInfo['fileName']);
	
	// check privileges
	if(!RequestPrivileges($classInfo['requiredPrivileges'] | PRIVILEGES_CLIENTAPI, true))
	{
		// return access error
		$response['status'] = 'ACCESS_DENIED';
	}
	else 
	{
		// instantiate
		$response['class'] = $_REQUEST['class'];
		if(isset($_REQUEST['method']))
			$response['method'] = $_REQUEST['method'];
		$instance = _new($_REQUEST['class'], $classInfo['constructorParams']);
	
		$params = isset($_REQUEST['params']) && is_array($_REQUEST['params']) ? $_REQUEST['params'] : array();
	
		// function
		if(!isset($_REQUEST['method']) || !method_exists($instance, $_REQUEST['method']))
		{
			$response['status'] = 'INVALID_METHOD';
			if(method_exists($instance, 'HandleNonexistentMethod'))
				call_user_func_array(array(&$instance, 'HandleNonexistentMethod'), array($_REQUEST['method'], $params, &$response));
		}
		else if(isset($_REQUEST['method']) && method_exists($instance, $_REQUEST['method'])) 
		{
			$response['status'] = 'OK';
			$response['result'] = call_user_func_array(array(&$instance, $_REQUEST['method']), $params);
		}
	}
}

NormalArray2XML($response, 'response');
