<?php /* Smarty version 2.6.28, created on 2020-09-29 11:27:03
         compiled from /var/www/html/plugins/templates/widget.mailspace.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'progressBar', '/var/www/html/plugins/templates/widget.mailspace.tpl', 4, false),array('function', 'size', '/var/www/html/plugins/templates/widget.mailspace.tpl', 7, false),array('function', 'lng', '/var/www/html/plugins/templates/widget.mailspace.tpl', 7, false),)), $this); ?>
<div class="innerWidget" style="text-align:center;">
	<table cellspacing="0" cellpadding="2" width="100%">
		<tr>
			<td align="center"><?php echo TemplateProgressBar(array('value' => $this->_tpl_vars['bmwidget_mailspace_spaceUsed'],'max' => $this->_tpl_vars['bmwidget_mailspace_spaceLimit'],'width' => 250), $this);?>
</td>
		</tr>
		<tr>
			<td><?php echo TemplateSize(array('bytes' => $this->_tpl_vars['bmwidget_mailspace_spaceUsed']), $this);?>
 / <?php echo TemplateSize(array('bytes' => $this->_tpl_vars['bmwidget_mailspace_spaceLimit']), $this);?>
 <?php echo TemplateLang(array('p' => 'used'), $this);?>
</td>
		</tr>
	</table>
</div>