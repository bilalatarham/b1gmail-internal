<?php /* Smarty version 2.6.28, created on 2020-09-30 10:10:28
         compiled from C:%5Cxampp%5Chtdocs%5Cb1gmail/plugins/templates/widget.quicklinks.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'C:\\xampp\\htdocs\\b1gmail/plugins/templates/widget.quicklinks.tpl', 3, false),)), $this); ?>
<div class="innerWidget">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'email'), $this);?>
</legend>
		<a href="email.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_inbox.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										<?php echo TemplateLang(array('p' => 'inbox'), $this);?>
</a><br />
		<a href="email.compose.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/send_mail.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										<?php echo TemplateLang(array('p' => 'sendmail'), $this);?>
</a><br />
		<a href="email.folders.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_folder.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										<?php echo TemplateLang(array('p' => 'folderadmin'), $this);?>
</a><br />
	</fieldset>
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'organizer'), $this);?>
</legend>
		<a href="organizer.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_overview.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										<?php echo TemplateLang(array('p' => 'overview'), $this);?>
</a><br />
		<a href="organizer.calendar.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_calendar.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										<?php echo TemplateLang(array('p' => 'calendar'), $this);?>
</a><br />
		<a href="organizer.todo.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_todo.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										<?php echo TemplateLang(array('p' => 'tasks'), $this);?>
</a><br />
		<a href="organizer.addressbook.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_addressbook.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										<?php echo TemplateLang(array('p' => 'addressbook'), $this);?>
</a><br />
		<a href="organizer.notes.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_notes.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										<?php echo TemplateLang(array('p' => 'notes'), $this);?>
</a><br />
	</fieldset>
	<?php if ($this->_tpl_vars['pageTabs']['webdisk']): ?><fieldset>
		<legend><?php echo TemplateLang(array('p' => 'webdisk'), $this);?>
</legend>
		<a href="webdisk.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_share.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										<?php echo TemplateLang(array('p' => 'webdisk'), $this);?>
</a><br />
		<a href="webdisk.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&do=uploadFilesForm"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/webdisk_file.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										<?php echo TemplateLang(array('p' => 'uploadfiles'), $this);?>
</a><br />
	</fieldset><?php endif; ?>
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'misc'), $this);?>
</legend>
		<a href="prefs.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_common.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										<?php echo TemplateLang(array('p' => 'prefs'), $this);?>
</a><br />
		<a href="start.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=logout"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_logout.png" width="16" height="16" border="0" alt="" align="absmiddle" />
										<?php echo TemplateLang(array('p' => 'logout'), $this);?>
</a><br />
	</fieldset>
</div>