<?php /* Smarty version 2.6.28, created on 2020-09-30 10:11:28
         compiled from li/email.folder.contents.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'li/email.folder.contents.tpl', 14, false),array('function', 'cycle', 'li/email.folder.contents.tpl', 66, false),array('function', 'date', 'li/email.folder.contents.tpl', 76, false),array('function', 'email', 'li/email.folder.contents.tpl', 94, false),array('function', 'text', 'li/email.folder.contents.tpl', 95, false),array('function', 'size', 'li/email.folder.contents.tpl', 110, false),array('function', 'hook', 'li/email.folder.contents.tpl', 135, false),array('function', 'pageNav', 'li/email.folder.contents.tpl', 173, false),)), $this); ?>
<form name="f1" action="email.php?do=action&<?php echo $this->_tpl_vars['folderString']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onsubmit="transferSelectedMailIDs()" method="post">
<input type="hidden" name="selectedMailIDs" id="selectedMailIDs" value="" />

<div id="contentHeader">
	<div class="left"<?php if ($this->_tpl_vars['templatePrefs']['showCheckboxes']): ?> style="padding-left:2px;"<?php endif; ?>>
		<?php if ($this->_tpl_vars['templatePrefs']['showCheckboxes']): ?><input type="checkbox" style="vertical-align:middle;" id="checkAllMails" onclick="if(this.checked) _mailSel.selectAll(); else _mailSel.unselectAll()||showMultiSelPreview(0);" /><?php endif; ?>
		<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_<?php echo $this->_tpl_vars['folderInfo']['type']; ?>
.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo $this->_tpl_vars['folderInfo']['title']; ?>

	</div>

	<div class="right">
		<?php if ($this->_tpl_vars['folderInfo']['type'] != 'intellifolder' && ! $this->_tpl_vars['folderInfo']['readonly']): ?>
		<button onclick="showFolderMenu(event);" type="button">
			<i class="fa fa-gears fa-lg"></i>
			<?php echo TemplateLang(array('p' => 'folderactions'), $this);?>

		</button>
		<?php endif; ?>

		<button onclick="switchPage(<?php echo $this->_tpl_vars['pageNo']; ?>
)" type="button">
			<i class="fa fa-refresh fa-lg"></i>
			<?php echo TemplateLang(array('p' => 'refresh'), $this);?>

		</button>

		<?php if (! $this->_tpl_vars['folderInfo']['readonly']): ?><button onclick="folderViewOptions(<?php echo $this->_tpl_vars['folderID']; ?>
);" type="button">
			<i class="fa fa-desktop fa-lg"></i>
			<?php echo TemplateLang(array('p' => 'viewoptions'), $this);?>

		</button><?php endif; ?>
	</div>
</div>

<div class="scrollContainer withBottomBar">
<table class="bigTable" id="mailTable">
	<thead>
	<tr>
		<?php if ($this->_tpl_vars['templatePrefs']['showCheckboxes']): ?>
		<th style="text-align:center;width:24px;">&nbsp;</th>
		<?php endif; ?>
		<th width="50"><i class="fa fa-envelope"></i></th>
		<th width="20%">
		<?php if ($this->_tpl_vars['folderID'] != -2): ?>
			<a href="email.php?folder=<?php echo $this->_tpl_vars['folderID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&sort=von&order=<?php echo $this->_tpl_vars['sortOrderInv']; ?>
"><?php echo TemplateLang(array('p' => 'from'), $this);?>
</a>
			<?php if ($this->_tpl_vars['sortColumn'] == 'von'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/<?php echo $this->_tpl_vars['sortOrder']; ?>
.gif" border="0" alt="" align="absmiddle" /><?php endif; ?>
		<?php else: ?>
			<a href="email.php?folder=<?php echo $this->_tpl_vars['folderID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&sort=an&order=<?php echo $this->_tpl_vars['sortOrderInv']; ?>
"><?php echo TemplateLang(array('p' => 'to'), $this);?>
</a>
			<?php if ($this->_tpl_vars['sortColumn'] == 'an'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/<?php echo $this->_tpl_vars['sortOrder']; ?>
.gif" border="0" alt="" align="absmiddle" /><?php endif; ?>
		<?php endif; ?></th>
		<th>
			<a href="email.php?folder=<?php echo $this->_tpl_vars['folderID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&sort=betreff&order=<?php echo $this->_tpl_vars['sortOrderInv']; ?>
"><?php echo TemplateLang(array('p' => 'subject'), $this);?>
</a>
			<?php if ($this->_tpl_vars['sortColumn'] == 'betreff'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/<?php echo $this->_tpl_vars['sortOrder']; ?>
.gif" border="0" alt="" align="absmiddle" /><?php endif; ?>
		</th>
		<th width="130">
			<a href="email.php?folder=<?php echo $this->_tpl_vars['folderID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&sort=fetched&order=<?php echo $this->_tpl_vars['sortOrderInv']; ?>
"><?php echo TemplateLang(array('p' => 'date'), $this);?>
</a>
			<?php if ($this->_tpl_vars['sortColumn'] == 'fetched'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/<?php echo $this->_tpl_vars['sortOrder']; ?>
.gif" border="0" alt="" align="absmiddle" /><?php endif; ?>
		</th>
		<th width="65">
			<a href="email.php?folder=<?php echo $this->_tpl_vars['folderID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&sort=size&order=<?php echo $this->_tpl_vars['sortOrderInv']; ?>
"><?php echo TemplateLang(array('p' => 'size'), $this);?>
</a>
			<?php if ($this->_tpl_vars['sortColumn'] == 'size'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/<?php echo $this->_tpl_vars['sortOrder']; ?>
.gif" border="0" alt="" align="absmiddle" /><?php endif; ?>
		</th>
		<th width="70">&nbsp;</th>
	</tr>
	</thead>

	<?php if ($this->_tpl_vars['mailList']): ?>
	<?php $this->assign('first', true); ?>
	<?php $_from = $this->_tpl_vars['mailList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['mailID'] => $this->_tpl_vars['mail']):
?>
	<?php $this->assign('mailGroupID', $this->_tpl_vars['mail']['groupID']); ?>
	<?php echo smarty_function_cycle(array('values' => "listTableTR,listTableTR2",'assign' => 'class'), $this);?>


	<?php if ($this->_tpl_vars['mailID'] < 0): ?>
	<?php echo smarty_function_cycle(array('values' => "listTableTR,listTableTR2",'assign' => 'class'), $this);?>

	<?php if (! $this->_tpl_vars['first']): ?>
	</tbody>
	<?php endif; ?>
	<tr>
		<td colspan="<?php if ($this->_tpl_vars['templatePrefs']['showCheckboxes']): ?>7<?php else: ?>6<?php endif; ?>" class="folderGroup">
			<a style="display:block;cursor:pointer;" onclick="toggleGroup(<?php echo $this->_tpl_vars['mailID']; ?>
,'<?php echo $this->_tpl_vars['mail']['groupID']; ?>
');">&nbsp;<img id="groupImage_<?php echo $this->_tpl_vars['mailID']; ?>
" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php if ($_COOKIE['toggleGroup'][$this->_tpl_vars['mailGroupID']] == 'closed'): ?>expand<?php else: ?>contract<?php endif; ?>.png" width="11" height="11" border="0" align="absmiddle" alt="" />
			&nbsp;<?php echo $this->_tpl_vars['mail']['text']; ?>
 <?php if ($this->_tpl_vars['mail']['date'] && $this->_tpl_vars['mail']['date'] != -1): ?>(<?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['mail']['date'],'dayonly' => true), $this);?>
)<?php endif; ?></a>
		</td>
	</tr>
	<tbody id="group_<?php echo $this->_tpl_vars['mailID']; ?>
" style="display:<?php if ($_COOKIE['toggleGroup'][$this->_tpl_vars['mailGroupID']] == 'closed'): ?>none<?php endif; ?>;">
	<?php $this->assign('first', false); ?>
	<?php else: ?>
	<tr _draggable="true" _ondragstart="mailDragStart(event,<?php echo $this->_tpl_vars['mailID']; ?>
)" class="<?php echo $this->_tpl_vars['class']; ?>
" id="mail_<?php echo $this->_tpl_vars['mailID']; ?>
_ntr" _onmousedown="return mailMouseDown(event,<?php echo $this->_tpl_vars['mailID']; ?>
);" _onmouseup="mailMouseUp(event,<?php echo $this->_tpl_vars['mailID']; ?>
);" <?php if ($this->_tpl_vars['folderID'] == -3): ?>_ondblclick="document.location.href='email.compose.php?redirect=<?php echo $this->_tpl_vars['mailID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
';"<?php else: ?>_ondblclick="document.location.href='email.read.php?id=<?php echo $this->_tpl_vars['mailID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
';"<?php endif; ?> _oncontextmenu="return(false);">
		<?php if ($this->_tpl_vars['templatePrefs']['showCheckboxes']): ?>
		<td style="text-align:center;width:24px;">
			<input type="checkbox" id="selecTable_<?php echo $this->_tpl_vars['mailID']; ?>
" />
		</td>
		<?php endif; ?>
		<td nowrap="nowrap">
			<i id="mail_<?php echo $this->_tpl_vars['mailID']; ?>
_flagimg" class="<?php if ($this->_tpl_vars['mail']['flags'] & 16): ?>fa fa-flag-o<?php elseif ($this->_tpl_vars['mail']['priority'] == 1): ?>fa fa-exclamation<?php elseif ($this->_tpl_vars['mail']['priority'] == -1): ?>fa fa-long-arrow-down<?php else: ?><?php endif; ?>"></i>
			<?php if ($this->_tpl_vars['mail']['flags'] & 64): ?><i class="fa fa-paperclip"></i><?php endif; ?>
			<?php if ($this->_tpl_vars['mail']['flags'] & 4 || $this->_tpl_vars['mail']['flags'] & 2): ?><i class="fa fa-mail-<?php if ($this->_tpl_vars['mail']['flags'] & 4): ?>forward<?php else: ?>reply<?php endif; ?>"></i><?php endif; ?>
		</td>
		<?php if ($this->_tpl_vars['folderID'] != -2): ?>
		<td id="mail_<?php echo $this->_tpl_vars['mailID']; ?>
_col2"<?php if ($this->_tpl_vars['sortColumn'] == 'von' && $this->_tpl_vars['mail']['color'] == 0): ?> class="listTableTDActive"<?php elseif ($this->_tpl_vars['mail']['color'] > 0): ?> class="mailColor_<?php echo $this->_tpl_vars['mail']['color']; ?>
"<?php endif; ?> nowrap="nowrap"><span id="mail_<?php echo $this->_tpl_vars['mailID']; ?>
_span1" class="<?php if ($this->_tpl_vars['mail']['flags'] & 1): ?>un<?php endif; ?>readMail"><a draggable="false" href="javascript:void(0);" onclick="currentEMail='<?php echo TemplateEMail(array('value' => $this->_tpl_vars['mail']['from_mail']), $this);?>
';currentEMailID=<?php echo $this->_tpl_vars['mailID']; ?>
;showAddressMenu(event,true);">
			&nbsp;<?php if ($this->_tpl_vars['mail']['flags'] & 8): ?><s><?php endif; ?><?php if ($this->_tpl_vars['mail']['from_name']): ?><?php echo TemplateText(array('value' => $this->_tpl_vars['mail']['from_name']), $this);?>
<?php else: ?><?php if ($this->_tpl_vars['mail']['from_mail']): ?><?php echo TemplateEMail(array('value' => $this->_tpl_vars['mail']['from_mail']), $this);?>
<?php else: ?>-<?php endif; ?><?php endif; ?><?php if ($this->_tpl_vars['mail']['flags'] & 8): ?></s><?php endif; ?>
		</a></span>&nbsp;</td>
		<?php else: ?>
		<td id="mail_<?php echo $this->_tpl_vars['mailID']; ?>
_col2"<?php if ($this->_tpl_vars['sortColumn'] == 'an' && $this->_tpl_vars['mail']['color'] == 0): ?> class="listTableTDActive"<?php elseif ($this->_tpl_vars['mail']['color'] > 0): ?> class="mailColor_<?php echo $this->_tpl_vars['mail']['color']; ?>
"<?php endif; ?> nowrap="nowrap"><span id="mail_<?php echo $this->_tpl_vars['mailID']; ?>
_span1" class="<?php if ($this->_tpl_vars['mail']['flags'] & 1): ?>un<?php endif; ?>readMail"><a draggable="false" href="javascript:void(0);" onclick="currentEMail='<?php echo TemplateEMail(array('value' => $this->_tpl_vars['mail']['to_mail']), $this);?>
';currentEMailID=<?php echo $this->_tpl_vars['mailID']; ?>
;showAddressMenu(event,true);">
			&nbsp;<?php if ($this->_tpl_vars['mail']['flags'] & 8): ?><s><?php endif; ?><?php if ($this->_tpl_vars['mail']['to_name']): ?><?php echo TemplateText(array('value' => $this->_tpl_vars['mail']['to_name']), $this);?>
<?php else: ?><?php if ($this->_tpl_vars['mail']['to_mail']): ?><?php echo TemplateEMail(array('value' => $this->_tpl_vars['mail']['to_mail']), $this);?>
<?php else: ?>-<?php endif; ?><?php endif; ?><?php if ($this->_tpl_vars['mail']['flags'] & 8): ?></s><?php endif; ?>
		</a></span>&nbsp;</td>
		<?php endif; ?>
		<td id="mail_<?php echo $this->_tpl_vars['mailID']; ?>
_col3"<?php if ($this->_tpl_vars['sortColumn'] == 'betreff' && $this->_tpl_vars['mail']['color'] == 0): ?> class="listTableTDActive"<?php elseif ($this->_tpl_vars['mail']['color'] > 0): ?> class="mailColor_<?php echo $this->_tpl_vars['mail']['color']; ?>
"<?php endif; ?> nowrap="nowrap">
			<a draggable="false" href="email.read.php?id=<?php echo $this->_tpl_vars['mailID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onclick="return(false)">
				&nbsp;
				<?php if ($this->_tpl_vars['mail']['flags'] & 8): ?><s><?php endif; ?><i id="maildone_<?php echo $this->_tpl_vars['mailID']; ?>
" class="<?php if ($this->_tpl_vars['mail']['flags'] & 4096): ?>fa fa-check<?php endif; ?>"></i> <?php if ($this->_tpl_vars['mail']['flags'] & 128): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/infected.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php endif; ?><?php if ($this->_tpl_vars['mail']['flags'] & 256): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/spam.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php endif; ?><span style="background:transparent;" id="mail_<?php echo $this->_tpl_vars['mailID']; ?>
_span2" class="<?php if ($this->_tpl_vars['mail']['flags'] & 1): ?>un<?php endif; ?>readMail"><?php echo TemplateText(array('value' => $this->_tpl_vars['mail']['subject']), $this);?>
</span>
				<?php if ($this->_tpl_vars['mail']['flags'] & 8): ?></s><?php endif; ?>
			</a>
		</td>
		<td id="mail_<?php echo $this->_tpl_vars['mailID']; ?>
_col4"<?php if ($this->_tpl_vars['sortColumn'] == 'fetched' && $this->_tpl_vars['mail']['color'] == 0): ?> class="listTableTDActive"<?php elseif ($this->_tpl_vars['mail']['color'] > 0): ?> class="mailColor_<?php echo $this->_tpl_vars['mail']['color']; ?>
"<?php endif; ?> nowrap="nowrap">&nbsp;<?php if ($this->_tpl_vars['mail']['flags'] & 8): ?><s><?php endif; ?><?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['mail']['timestamp'],'nice' => true), $this);?>
<?php if ($this->_tpl_vars['mail']['flags'] & 8): ?></s><?php endif; ?>&nbsp;</td>
		<td<?php if ($this->_tpl_vars['sortColumn'] == 'size' && $this->_tpl_vars['mail']['color'] == 0): ?> class="listTableTDActive"<?php endif; ?> nowrap="nowrap">&nbsp;<?php if ($this->_tpl_vars['mail']['flags'] & 8): ?><s><?php endif; ?><?php echo TemplateSize(array('bytes' => $this->_tpl_vars['mail']['size']), $this);?>
<?php if ($this->_tpl_vars['mail']['flags'] & 8): ?></s><?php endif; ?>&nbsp;</td>
		<td nowrap="nowrap">
			<a href="email.read.php?id=<?php echo $this->_tpl_vars['mailID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_read.png" width="16" height="16" border="0" alt="<?php echo TemplateLang(array('p' => 'read'), $this);?>
" /></a>
			<a href="javascript:void(0);" onclick="currentSID='<?php echo $this->_tpl_vars['sid']; ?>
';currentID=<?php echo $this->_tpl_vars['mailID']; ?>
;currentSortColumn='<?php echo $this->_tpl_vars['sortColumn']; ?>
';showMailMenu(event);"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_menu.png" width="16" height="16" border="0" alt="<?php echo TemplateLang(array('p' => 'mail_menu'), $this);?>
" /></a>
			<a href="email.php?do=deleteMail&id=<?php echo $this->_tpl_vars['mailID']; ?>
&<?php echo $this->_tpl_vars['folderString']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"<?php if ($this->_tpl_vars['folderID'] == -5): ?> onclick="return(confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
'));"<?php endif; ?>><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_delete.png" width="16" height="16" border="0" alt="<?php echo TemplateLang(array('p' => 'delete'), $this);?>
" /></a>
		</td>
	</tr>
	<?php endif; ?>
	<?php endforeach; endif; unset($_from); ?>
	<?php if (! $this->_tpl_vars['first']): ?>
	</tbody>
	<?php endif; ?>
	<?php endif; ?>
</table>
</div>

<div id="contentFooter">
	<div class="left">
		<select class="smallInput" name="massAction" id="massAction">
			<option value="-">------ <?php echo TemplateLang(array('p' => 'selaction'), $this);?>
 ------</option>

			<optgroup label="<?php echo TemplateLang(array('p' => 'actions'), $this);?>
">
				<?php if (! $this->_tpl_vars['folderInfo']['readonly']): ?><option value="delete"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option><?php endif; ?>
				<option value="forward"><?php echo TemplateLang(array('p' => 'forward'), $this);?>
</option>
				<option value="download"><?php echo TemplateLang(array('p' => 'download'), $this);?>
</option>
				<?php echo TemplateHook(array('id' => "email.folder.tpl:mailSelect.actions"), $this);?>

			</optgroup>

			<?php if (! $this->_tpl_vars['folderInfo']['readonly']): ?><optgroup label="<?php echo TemplateLang(array('p' => 'flags'), $this);?>
">
				<option value="markread"><?php echo TemplateLang(array('p' => 'markread'), $this);?>
</option>
				<option value="markunread"><?php echo TemplateLang(array('p' => 'markunread'), $this);?>
</option>
				<option value="mark"><?php echo TemplateLang(array('p' => 'mark'), $this);?>
</option>
				<option value="unmark"><?php echo TemplateLang(array('p' => 'unmark'), $this);?>
</option>
				<option value="done"><?php echo TemplateLang(array('p' => 'markdone'), $this);?>
</option>
				<option value="undone"><?php echo TemplateLang(array('p' => 'unmarkdone'), $this);?>
</option>
				<option value="markspam"><?php echo TemplateLang(array('p' => 'markspam'), $this);?>
</option>
				<option value="marknonspam"><?php echo TemplateLang(array('p' => 'marknonspam'), $this);?>
</option>
				<?php echo TemplateHook(array('id' => "email.folder.tpl:mailSelect.flags"), $this);?>

			</optgroup>

			<optgroup label="<?php echo TemplateLang(array('p' => 'setmailcolor'), $this);?>
">
				<option value="color_0" class="mailColor_0"><?php echo TemplateLang(array('p' => 'color_0'), $this);?>
</option>
				<option value="color_1" class="mailColor_1"><?php echo TemplateLang(array('p' => 'color_1'), $this);?>
</option>
				<option value="color_2" class="mailColor_2"><?php echo TemplateLang(array('p' => 'color_2'), $this);?>
</option>
				<option value="color_3" class="mailColor_3"><?php echo TemplateLang(array('p' => 'color_3'), $this);?>
</option>
				<option value="color_4" class="mailColor_4"><?php echo TemplateLang(array('p' => 'color_4'), $this);?>
</option>
				<option value="color_5" class="mailColor_5"><?php echo TemplateLang(array('p' => 'color_5'), $this);?>
</option>
				<option value="color_6" class="mailColor_6"><?php echo TemplateLang(array('p' => 'color_6'), $this);?>
</option>
			</optgroup>

			<optgroup label="<?php echo TemplateLang(array('p' => 'move'), $this);?>
 <?php echo TemplateLang(array('p' => 'moveto'), $this);?>
">
			<?php $_from = $this->_tpl_vars['dropdownFolderList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['dFolderID'] => $this->_tpl_vars['dFolderTitle']):
?>
			<option value="moveto_<?php echo $this->_tpl_vars['dFolderID']; ?>
" style="font-family:courier;"><?php echo $this->_tpl_vars['dFolderTitle']; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
			</optgroup><?php endif; ?>

			<?php echo TemplateHook(array('id' => "email.folder.tpl:mailSelect"), $this);?>

		</select>
		<input class="smallInput" type="submit" value="<?php echo TemplateLang(array('p' => 'ok'), $this);?>
" />
	</div>

	<div class="right">
		<?php echo TemplateLang(array('p' => 'pages'), $this);?>
:
		<?php echo TemplatePageNav(array('page' => $this->_tpl_vars['pageNo'],'pages' => $this->_tpl_vars['pageCount'],'on' => " <b>[.t]</b> ",'off' => " <a class=\"pageNav\" href=\"javascript:void(0);\" onclick=\"switchPage(.s);\">.t</a> "), $this);?>

		&nbsp;
		<select class="smallInput" onchange="switchPage(this.value)">
			<?php unset($this->_sections['page']);
$this->_sections['page']['name'] = 'page';
$this->_sections['page']['start'] = (int)0;
$this->_sections['page']['loop'] = is_array($_loop=$this->_tpl_vars['pageCount']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['page']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['page']['show'] = true;
$this->_sections['page']['max'] = $this->_sections['page']['loop'];
if ($this->_sections['page']['start'] < 0)
    $this->_sections['page']['start'] = max($this->_sections['page']['step'] > 0 ? 0 : -1, $this->_sections['page']['loop'] + $this->_sections['page']['start']);
else
    $this->_sections['page']['start'] = min($this->_sections['page']['start'], $this->_sections['page']['step'] > 0 ? $this->_sections['page']['loop'] : $this->_sections['page']['loop']-1);
if ($this->_sections['page']['show']) {
    $this->_sections['page']['total'] = min(ceil(($this->_sections['page']['step'] > 0 ? $this->_sections['page']['loop'] - $this->_sections['page']['start'] : $this->_sections['page']['start']+1)/abs($this->_sections['page']['step'])), $this->_sections['page']['max']);
    if ($this->_sections['page']['total'] == 0)
        $this->_sections['page']['show'] = false;
} else
    $this->_sections['page']['total'] = 0;
if ($this->_sections['page']['show']):

            for ($this->_sections['page']['index'] = $this->_sections['page']['start'], $this->_sections['page']['iteration'] = 1;
                 $this->_sections['page']['iteration'] <= $this->_sections['page']['total'];
                 $this->_sections['page']['index'] += $this->_sections['page']['step'], $this->_sections['page']['iteration']++):
$this->_sections['page']['rownum'] = $this->_sections['page']['iteration'];
$this->_sections['page']['index_prev'] = $this->_sections['page']['index'] - $this->_sections['page']['step'];
$this->_sections['page']['index_next'] = $this->_sections['page']['index'] + $this->_sections['page']['step'];
$this->_sections['page']['first']      = ($this->_sections['page']['iteration'] == 1);
$this->_sections['page']['last']       = ($this->_sections['page']['iteration'] == $this->_sections['page']['total']);
?>
				<option value="<?php echo $this->_sections['page']['index']+1; ?>
"<?php if ($this->_tpl_vars['pageNo'] == $this->_sections['page']['index']+1): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['page']['index']+1; ?>
</option>
			<?php endfor; endif; ?>
		</select>
	</div>
</div>

</form>


<script language="javascript">
<!--
	currentSortColumn = '<?php echo $this->_tpl_vars['sortColumn']; ?>
';
	currentSortOrder = '<?php echo $this->_tpl_vars['sortOrder']; ?>
';
	currentPageNo = <?php echo $this->_tpl_vars['pageNo']; ?>
;
	currentPageCount = <?php echo $this->_tpl_vars['pageCount']; ?>
;
	initMailSel();
//-->
</script>