<?php /* Smarty version 2.6.28, created on 2020-09-29 11:27:03
         compiled from li/start.sidebar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'hook', 'li/start.sidebar.tpl', 1, false),array('function', 'lng', 'li/start.sidebar.tpl', 3, false),)), $this); ?>
<?php echo TemplateHook(array('id' => "start.sidebar.tpl:head"), $this);?>


<div class="sidebarHeading"><?php echo TemplateLang(array('p' => 'start'), $this);?>
</div>
<div class="contentMenuIcons">
	<a href="start.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_start.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'start'), $this);?>
</a><br />
	<a href="start.php?action=customize&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_customize.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'customize'), $this);?>
</a><br />
	<?php echo TemplateHook(array('id' => "start.sidebar.tpl:start"), $this);?>

</div>

<div class="sidebarHeading"><?php echo TemplateLang(array('p' => 'misc'), $this);?>
</div>
<div class="contentMenuIcons">
	<a href="start.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=logout" onclick="return(confirm('<?php echo TemplateLang(array('p' => 'logoutquestion'), $this);?>
'));"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_logout.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'logout'), $this);?>
</a><br />
	<?php echo TemplateHook(array('id' => "start.sidebar.tpl:misc"), $this);?>

</div>

<?php echo TemplateHook(array('id' => "start.sidebar.tpl:foot"), $this);?>
