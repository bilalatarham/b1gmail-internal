<?php /* Smarty version 2.6.28, created on 2020-09-29 11:32:42
         compiled from plugin.install.info.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'plugin.install.info.tpl', 2, false),array('function', 'text', 'plugin.install.info.tpl', 2, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'installplugin'), $this);?>
: <?php echo TemplateText(array('value' => $this->_tpl_vars['meta']['name']), $this);?>
</legend>
	
	<form action="plugins.php?action=install&do=installPlugin&id=<?php echo $this->_tpl_vars['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
		<p>
			<?php echo TemplateLang(array('p' => 'install_desc2'), $this);?>

		</p>
		
		<table>
			<tr>
				<td width="40" valign="top" rowspan="5"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/plugin_add.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="150"><?php echo TemplateLang(array('p' => 'name'), $this);?>
:</td>
				<td class="td2"><b><?php echo TemplateText(array('value' => $this->_tpl_vars['meta']['name']), $this);?>
</b></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'version'), $this);?>
:</td>
				<td class="td2"><?php echo TemplateText(array('value' => $this->_tpl_vars['meta']['version']), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'vendor'), $this);?>
:</td>
				<td class="td2"><a target="_blank" href="<?php echo TemplateText(array('value' => $this->_tpl_vars['meta']['vendor_url'],'escape' => true), $this);?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['meta']['vendor']), $this);?>
</a> (<a href="mailto:<?php echo TemplateText(array('value' => $this->_tpl_vars['meta']['vendor_mail'],'escape' => true), $this);?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['meta']['vendor_mail']), $this);?>
</a>)</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'forb1gmail'), $this);?>
:</td>
				<td class="td2">
					<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php if (! $this->_tpl_vars['versionsMatch']): ?>warning<?php else: ?>ok<?php endif; ?>.png" border="0" alt="" align="absmiddle" width="16" height="16" />
					<?php echo TemplateText(array('value' => $this->_tpl_vars['meta']['for_b1gmail']), $this);?>

					<?php if (! $this->_tpl_vars['versionsMatch']): ?>(<?php echo TemplateLang(array('p' => 'yourversion'), $this);?>
: <?php echo $this->_tpl_vars['b1gmailVersion']; ?>
)<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td class="td2" colspan="2" align="center">
					<br />
					
					<div id="sigLayer">
						<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/load_16.gif" align="absmiddle" border="0" alt="" />
						<?php echo TemplateLang(array('p' => 'checkingsig'), $this);?>

					</div>
					
					<script language="javascript">
					<!--
						registerLoadAction('checkPluginSignature(\'<?php echo $this->_tpl_vars['signature']; ?>
\')');
					//-->
					</script>
				</td>
			</tr>
		</table>
		
		<p>
			<div style="float:right;">
				<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'install'), $this);?>
 " />
			</div>
		</p>
	</form>
</fieldset>