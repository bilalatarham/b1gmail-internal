<?php /* Smarty version 2.6.28, created on 2020-09-29 11:27:06
         compiled from li/email.addressmenu.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'li/email.addressmenu.tpl', 3, false),)), $this); ?>
<!-- address menu -->
<div id="addressMenu" class="mailMenu" style="display:none;position:absolute;left:0px;top:0px;" oncontextmenu="return(false);" onmousedown="if(event.button==2) return(false);">
	<a id="addressMenuReadItem" class="mailMenuItem" style="display:none;" href="javascript:<?php if ($this->_tpl_vars['preview']): ?>parent.<?php endif; ?>document.location.href='email.read.php?id='+encodeURIComponent(currentEMailID)+'&sid='+currentSID;"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_read.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'mail_read'), $this);?>
</a>
	<div class="mailMenuSep" id="addressMenuReadItemSep"></div>
	<a class="mailMenuItem" href="javascript:<?php if ($this->_tpl_vars['preview']): ?>parent.<?php endif; ?>document.location.href='email.compose.php?to='+encodeURIComponent(currentEMail)+'&sid='+currentSID;"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/send_mail.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'sendmail'), $this);?>
</a>
	<a class="mailMenuItem" href="javascript:<?php if ($this->_tpl_vars['preview']): ?>parent.<?php endif; ?>document.location.href='organizer.addressbook.php?action=addContact&email='+encodeURIComponent(currentEMail)+'&sid='+currentSID;"><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_addressbook.png" width="16" height="16" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'toaddr'), $this);?>
</a>
</div>