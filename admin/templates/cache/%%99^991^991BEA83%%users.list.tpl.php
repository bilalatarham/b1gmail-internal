<?php /* Smarty version 2.6.28, created on 2020-09-29 15:07:06
         compiled from users.list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'text', 'users.list.tpl', 7, false),array('function', 'lng', 'users.list.tpl', 11, false),array('function', 'cycle', 'users.list.tpl', 42, false),array('function', 'email', 'users.list.tpl', 47, false),array('function', 'fieldDate', 'users.list.tpl', 52, false),array('function', 'pageNav', 'users.list.tpl', 90, false),)), $this); ?>
<form action="users.php?filter=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="return userMassActionFormSubmit(this);" name="f1">
<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['pageNo']; ?>
" />
<input type="hidden" name="sortBy" id="sortBy" value="<?php echo $this->_tpl_vars['sortBy']; ?>
" />
<input type="hidden" name="sortOrder" id="sortOrder" value="<?php echo $this->_tpl_vars['sortOrder']; ?>
" />
<input type="hidden" name="singleAction" id="singleAction" value="" />
<input type="hidden" name="singleID" id="singleID" value="" />
<?php if ($this->_tpl_vars['queryString']): ?><input type="hidden" name="query" id="query" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['queryString']), $this);?>
" /><?php endif; ?>

<?php if ($this->_tpl_vars['searchQuery']): ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'search'), $this);?>
</legend>

	<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/user_searchingfor.png" align="absmiddle" border="0" width="16" height="16" />
		<?php echo TemplateLang(array('p' => 'searchingfor'), $this);?>
: <b><?php echo TemplateText(array('value' => $this->_tpl_vars['searchQuery']), $this);?>
</b>

	<a href="users.php?action=search&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/delete.png" align="absmiddle" border="0" width="16" height="16" /></a>
</fieldset>
<?php endif; ?>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'users'), $this);?>
</legend>

	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="25" style="text-align:center;"><a href="javascript:invertSelection(document.forms.f1,'user_');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/dot.png" border="0" alt="" width="10" height="8" /></a></th>
			<th><a href="javascript:updateSort('id');"><?php echo TemplateLang(array('p' => 'id'), $this);?>

				<?php if ($this->_tpl_vars['sortBy'] == 'id'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/sort_<?php echo $this->_tpl_vars['sortOrder']; ?>
.png" border="0" alt="" width="7" height="6" align="absmiddle" /><?php endif; ?></a></th>
			<th><a href="javascript:updateSort('email');"><?php echo TemplateLang(array('p' => 'email'), $this);?>

				<?php if ($this->_tpl_vars['sortBy'] == 'email'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/sort_<?php echo $this->_tpl_vars['sortOrder']; ?>
.png" border="0" alt="" width="7" height="6" align="absmiddle" /><?php endif; ?></a></th>
			<th><a href="javascript:updateSort('nachname');"><?php echo TemplateLang(array('p' => 'name'), $this);?>

				<?php if ($this->_tpl_vars['sortBy'] == 'nachname'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/sort_<?php echo $this->_tpl_vars['sortOrder']; ?>
.png" border="0" alt="" width="7" height="6" align="absmiddle" /><?php endif; ?></a></th>
			<?php $_from = $this->_tpl_vars['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['fieldID'] => $this->_tpl_vars['field']):
?><?php if ($this->_tpl_vars['field']['checked']): ?>
			<th<?php if ($this->_tpl_vars['field']['typ'] == 2): ?> style="text-align:center;"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['field']['feld']), $this);?>
</th>
			<?php endif; ?><?php endforeach; endif; unset($_from); ?>
			<th><a href="javascript:updateSort('gesperrt');"><?php echo TemplateLang(array('p' => 'status'), $this);?>

				<?php if ($this->_tpl_vars['sortBy'] == 'gesperrt'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/sort_<?php echo $this->_tpl_vars['sortOrder']; ?>
.png" border="0" alt="" width="7" height="6" align="absmiddle" /><?php endif; ?></a></th>
			<th width="95">&nbsp;</th>
		</tr>

		<?php $_from = $this->_tpl_vars['users']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['user']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/user_<?php echo $this->_tpl_vars['user']['statusImg']; ?>
.png" border="0" width="16" height="16" alt="" /></td>
			<td align="center"><input type="checkbox" name="user_<?php echo $this->_tpl_vars['user']['id']; ?>
" /></td>
			<td><?php echo $this->_tpl_vars['user']['id']; ?>
</td>
			<td><a href="users.php?do=edit&id=<?php echo $this->_tpl_vars['user']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateEMail(array('value' => $this->_tpl_vars['user']['email']), $this);?>
</a><br /><small><?php echo TemplateText(array('value' => $this->_tpl_vars['user']['aliases'],'cut' => 45,'allowEmpty' => true), $this);?>
</small></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['user']['nachname'],'cut' => 20), $this);?>
, <?php echo TemplateText(array('value' => $this->_tpl_vars['user']['vorname'],'cut' => 20), $this);?>
<br /><small><?php echo TemplateText(array('value' => $this->_tpl_vars['user']['strasse'],'cut' => 20), $this);?>
 <?php echo TemplateText(array('value' => $this->_tpl_vars['user']['hnr'],'cut' => 5), $this);?>
, <?php echo TemplateText(array('value' => $this->_tpl_vars['user']['plz'],'cut' => 8), $this);?>
 <?php echo TemplateText(array('value' => $this->_tpl_vars['user']['ort'],'cut' => 20), $this);?>
</small></td>
			<?php $_from = $this->_tpl_vars['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['fieldID'] => $this->_tpl_vars['field']):
?><?php if ($this->_tpl_vars['field']['checked']): ?>
			<td<?php if ($this->_tpl_vars['field']['typ'] == 2): ?> style="text-align:center;"<?php endif; ?>>
				<?php if ($this->_tpl_vars['field']['typ'] == 2): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php if ($this->_tpl_vars['user']['profileData'][$this->_tpl_vars['fieldID']]): ?>yes<?php else: ?>no<?php endif; ?>.png" border="0" alt="" width="16" height="16" />
				<?php elseif ($this->_tpl_vars['field']['typ'] == 32): ?><?php echo TemplateFieldDate(array('value' => $this->_tpl_vars['user']['profileData'][$this->_tpl_vars['fieldID']]), $this);?>

				<?php else: ?><?php echo TemplateText(array('value' => $this->_tpl_vars['user']['profileData'][$this->_tpl_vars['fieldID']]), $this);?>
<?php endif; ?>
			</td>
			<?php endif; ?><?php endforeach; endif; unset($_from); ?>
			<td><?php echo $this->_tpl_vars['user']['status']; ?>
<br /><small><?php echo TemplateLang(array('p' => 'group'), $this);?>
: <?php echo TemplateText(array('value' => $this->_tpl_vars['user']['groupName'],'cut' => 25), $this);?>
</small></td>
			<td>
				<a href="users.php?do=edit&id=<?php echo $this->_tpl_vars['user']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/edit.png" border="0" alt="<?php echo TemplateLang(array('p' => 'edit'), $this);?>
" width="16" height="16" /></a>
				<a href="javascript:singleAction('<?php if ($this->_tpl_vars['user']['gesperrt'] == 'no'): ?>lock<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'yes'): ?>unlock<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'locked'): ?>activate<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'delete'): ?>recover<?php endif; ?>', '<?php echo $this->_tpl_vars['user']['id']; ?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php if ($this->_tpl_vars['user']['gesperrt'] == 'no'): ?>lock<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'yes'): ?>unlock<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'locked'): ?>unlock<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'delete'): ?>recover<?php endif; ?>.png" border="0" alt="<?php if ($this->_tpl_vars['user']['gesperrt'] == 'no'): ?><?php echo TemplateLang(array('p' => 'lock'), $this);?>
<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'yes'): ?><?php echo TemplateLang(array('p' => 'unlock'), $this);?>
<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'locked'): ?><?php echo TemplateLang(array('p' => 'activate'), $this);?>
<?php elseif ($this->_tpl_vars['user']['gesperrt'] == 'delete'): ?><?php echo TemplateLang(array('p' => 'recover'), $this);?>
<?php endif; ?>" width="16" height="16" /></a>
				<a href="javascript:singleAction('delete', '<?php echo $this->_tpl_vars['user']['id']; ?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php if ($this->_tpl_vars['user']['gesperrt'] == 'delete'): ?>delete<?php else: ?>trash<?php endif; ?>.png" border="0" alt="<?php echo TemplateLang(array('p' => 'delete'), $this);?>
" width="16" height="16" /></a>
				<a href="users.php?do=login&id=<?php echo $this->_tpl_vars['user']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" target="_blank" onclick="return confirm('<?php echo TemplateLang(array('p' => 'loginwarning'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/login.png" border="0" alt="<?php echo TemplateLang(array('p' => 'login'), $this);?>
" width="16" height="16" /></a>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>

		<tr>
			<td class="footer" colspan="999">
				<div style="float:left;">
					<?php echo TemplateLang(array('p' => 'action'), $this);?>
: <select name="massAction" id="massAction" class="smallInput">
						<option value="-">------------</option>

						<optgroup label="<?php echo TemplateLang(array('p' => 'actions'), $this);?>
">
							<option value="delete"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option>
							<option value="lock"><?php echo TemplateLang(array('p' => 'lock'), $this);?>
</option>
							<option value="unlock"><?php echo TemplateLang(array('p' => 'unlock'), $this);?>
</option>
							<option value="restore"><?php echo TemplateLang(array('p' => 'restore'), $this);?>
</option>
						</optgroup>

						<optgroup label="<?php echo TemplateLang(array('p' => 'move'), $this);?>
">
						<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['groupID'] => $this->_tpl_vars['group']):
?>
							<option value="moveto_<?php echo $this->_tpl_vars['groupID']; ?>
"><?php echo TemplateLang(array('p' => 'moveto'), $this);?>
 &quot;<?php echo TemplateText(array('value' => $this->_tpl_vars['group']['title'],'cut' => 25), $this);?>
&quot;</option>
						<?php endforeach; endif; unset($_from); ?>
						</optgroup>
					</select>&nbsp;
				</div>
				<div style="float:left;">
					<input type="submit" name="executeMassAction" value=" <?php echo TemplateLang(array('p' => 'execute'), $this);?>
 " class="smallInput" />
				</div>
				<div style="float:right;padding-top:3px;">
					<?php echo TemplateLang(array('p' => 'pages'), $this);?>
: <?php echo TemplatePageNav(array('page' => $this->_tpl_vars['pageNo'],'pages' => $this->_tpl_vars['pageCount'],'on' => " <span class=\"pageNav\"><b>[.t]</b></span> ",'off' => " <span class=\"pageNav\"><a href=\"javascript:updatePage(.s);\">.t</a></span> "), $this);?>
&nbsp;
				</div>
			</td>
		</tr>
	</table>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'show'), $this);?>
</legend>

	<table width="100%">
		<tr>
			<td width="40" valign="top" rowspan="2"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/filter.png" border="0" alt="" width="32" height="32" /></td>
			<td class="td1" width="80"><?php echo TemplateLang(array('p' => 'status'), $this);?>
:</td>
			<td class="td2">
				<input type="checkbox" name="statusRegistered" id="statusRegistered"<?php if ($this->_tpl_vars['statusRegistered']): ?> checked="checked"<?php endif; ?> />
					<label for="statusRegistered"><b><?php echo TemplateLang(array('p' => 'registered'), $this);?>
</b></label><br />
				<input type="checkbox" name="statusActive" id="statusActive"<?php if ($this->_tpl_vars['statusActive']): ?> checked="checked"<?php endif; ?> />
					<label for="statusActive"><b><?php echo TemplateLang(array('p' => 'active'), $this);?>
</b></label><br />
				<input type="checkbox" name="statusLocked" id="statusLocked"<?php if ($this->_tpl_vars['statusLocked']): ?> checked="checked"<?php endif; ?> />
					<label for="statusLocked"><b><?php echo TemplateLang(array('p' => 'locked'), $this);?>
</b></label><br />
				<input type="checkbox" name="statusNotActivated" id="statusNotActivated"<?php if ($this->_tpl_vars['statusNotActivated']): ?> checked="checked"<?php endif; ?> />
					<label for="statusNotActivated"><b><?php echo TemplateLang(array('p' => 'notactivated'), $this);?>
</b></label><br />
				<input type="checkbox" name="statusDeleted" id="statusDeleted"<?php if ($this->_tpl_vars['statusDeleted']): ?> checked="checked"<?php endif; ?> />
					<label for="statusDeleted"><b><?php echo TemplateLang(array('p' => 'deleted'), $this);?>
</b></label><br />
			</td>
			<td class="td1" width="80"><?php echo TemplateLang(array('p' => 'groups'), $this);?>
:</td>
			<td class="td2">
				<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['groupID'] => $this->_tpl_vars['group']):
?>
					<input type="checkbox" name="group_<?php echo $this->_tpl_vars['groupID']; ?>
" id="group_<?php echo $this->_tpl_vars['groupID']; ?>
"<?php if ($this->_tpl_vars['group']['checked']): ?> checked="checked"<?php endif; ?> />
						<label for="group_<?php echo $this->_tpl_vars['groupID']; ?>
"><b><?php echo TemplateText(array('value' => $this->_tpl_vars['group']['title']), $this);?>
</b></label><br />
				<?php endforeach; endif; unset($_from); ?>
			</td>
		</tr>
		<?php if ($this->_tpl_vars['fields']): ?>
		<tr>
			<td class="td1" widt="80">
				<?php echo TemplateLang(array('p' => 'profilefields'), $this);?>
:
			</td>
			<td colspan="3" class="td2">
				<?php $_from = $this->_tpl_vars['fields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['fieldID'] => $this->_tpl_vars['field']):
?>
					<input type="checkbox" name="field_<?php echo $this->_tpl_vars['fieldID']; ?>
" id="field_<?php echo $this->_tpl_vars['fieldID']; ?>
"<?php if ($this->_tpl_vars['field']['checked']): ?> checked="checked"<?php endif; ?> />
						<label for="field_<?php echo $this->_tpl_vars['fieldID']; ?>
"><b><?php echo TemplateText(array('value' => $this->_tpl_vars['field']['feld']), $this);?>
</b></label><br />
				<?php endforeach; endif; unset($_from); ?>
			</td>
		</tr>
		<?php endif; ?>
	</table>

	<p align="right">
		<?php echo TemplateLang(array('p' => 'perpage'), $this);?>
:
		<input type="text" name="perPage" value="<?php echo $this->_tpl_vars['perPage']; ?>
" size="5" />
		<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'apply'), $this);?>
 " />
	</p>
</fieldset>
</form>