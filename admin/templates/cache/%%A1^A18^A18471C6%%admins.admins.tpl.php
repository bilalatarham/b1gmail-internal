<?php /* Smarty version 2.6.28, created on 2020-09-29 11:44:27
         compiled from admins.admins.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'admins.admins.tpl', 2, false),array('function', 'cycle', 'admins.admins.tpl', 15, false),array('function', 'text', 'admins.admins.tpl', 19, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'admins'), $this);?>
</legend>
	
	<form action="admins.php?action=admins&sid=<?php echo $this->_tpl_vars['sid']; ?>
" name="f1" method="post" onsubmit="spin(this)">
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="25" style="text-align:center;"><a href="javascript:invertSelection(document.forms.f1,'admin_');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/dot.png" border="0" alt="" width="10" height="8" /></a></th>
			<th><?php echo TemplateLang(array('p' => 'name'), $this);?>
</th>
			<th width="140"><?php echo TemplateLang(array('p' => 'status'), $this);?>
</th>
			<th width="60">&nbsp;</th>
		</tr>
		
		<?php $_from = $this->_tpl_vars['admins']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['admin']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/user_active.png" border="0" alt="" width="16" height="16" /></td>
			<td align="center"><?php if ($this->_tpl_vars['admin']['adminid'] != 1): ?><input type="checkbox" name="admin_<?php echo $this->_tpl_vars['admin']['adminid']; ?>
" /><?php endif; ?></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['admin']['username']), $this);?>
<br />
				<small><?php echo TemplateText(array('value' => $this->_tpl_vars['admin']['firstname']), $this);?>
 <?php echo TemplateText(array('value' => $this->_tpl_vars['admin']['lastname']), $this);?>
</small></td>
			<td>
				<?php if ($this->_tpl_vars['admin']['type'] == 0): ?><?php echo TemplateLang(array('p' => 'superadmin'), $this);?>
<?php else: ?><?php echo TemplateLang(array('p' => 'admin'), $this);?>
<?php endif; ?>
			</td>
			<td>
				<a href="admins.php?action=admins&do=edit&id=<?php echo $this->_tpl_vars['admin']['adminid']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/edit.png" border="0" alt="<?php echo TemplateLang(array('p' => 'edit'), $this);?>
" width="16" height="16" /></a>
				<?php if ($this->_tpl_vars['admin']['adminid'] != 1): ?><a href="admins.php?action=admins&delete=<?php echo $this->_tpl_vars['admin']['adminid']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onclick="return confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/delete.png" border="0" alt="<?php echo TemplateLang(array('p' => 'delete'), $this);?>
" width="16" height="16" /></a><?php endif; ?>
			</td>
		</tr>		
		<?php endforeach; endif; unset($_from); ?>
		
		<tr>
			<td class="footer" colspan="5">
				<div style="float:left;">
					<?php echo TemplateLang(array('p' => 'action'), $this);?>
: <select name="massAction" class="smallInput">
						<option value="-">------------</option>
						
						<optgroup label="<?php echo TemplateLang(array('p' => 'actions'), $this);?>
">
							<option value="delete"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option>
						</optgroup>
					</select>&nbsp;
				</div>
				<div style="float:left;">
					<input type="submit" name="executeMassAction" value=" <?php echo TemplateLang(array('p' => 'execute'), $this);?>
 " class="smallInput" />
				</div>
			</td>
		</tr>
	</table>
	</form>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'addadmin'), $this);?>
</legend>
	
	<form action="admins.php?action=admins&add=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
		<table width="100%">
			<tr>
				<td width="40" valign="top" rowspan="6"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_users.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="150"><?php echo TemplateLang(array('p' => 'username'), $this);?>
:</td>
				<td class="td2"><input type="text" size="28" id="username" name="username" value="" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'firstname'), $this);?>
:</td>
				<td class="td2"><input type="text" size="36" id="firstname" name="firstname" value="" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'lastname'), $this);?>
:</td>
				<td class="td2"><input type="text" size="36" id="lastname" name="lastname" value="" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'password'), $this);?>
:</td>
				<td class="td2"><input type="password" size="28" id="pw1" name="pw1" value="" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'password'), $this);?>
 (<?php echo TemplateLang(array('p' => 'repeat'), $this);?>
):</td>
				<td class="td2"><input type="password" size="28" id="pw2" name="pw2" value="" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'type'), $this);?>
:</td>
				<td class="td2"><select name="type">
					<option value="1"><?php echo TemplateLang(array('p' => 'admin'), $this);?>
</option>
					<option value="0"><?php echo TemplateLang(array('p' => 'superadmin'), $this);?>
</option>
				</select></td>
			</tr>
		</table>
	
		<p align="right">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'add'), $this);?>
 " />
		</p>
	</form>
</fieldset>