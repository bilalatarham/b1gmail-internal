<?php /* Smarty version 2.6.28, created on 2020-10-01 11:55:57
         compiled from page.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'page.tpl', 6, false),array('function', 'fileDateSig', 'page.tpl', 13, false),array('function', 'text', 'page.tpl', 44, false),array('function', 'pluginMenus', 'page.tpl', 115, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title><?php if ($this->_tpl_vars['title']): ?><?php echo $this->_tpl_vars['title']; ?>
 - <?php endif; ?>b1gMail <?php echo TemplateLang(array('p' => 'acp'), $this);?>
</title>

	<!-- meta -->
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $this->_tpl_vars['charset']; ?>
" />

	<!-- links -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link href="<?php echo $this->_tpl_vars['tpldir']; ?>
style/common.css?<?php echo TemplateFileDateSig(array('file' => "style/common.css"), $this);?>
" rel="stylesheet" type="text/css" />
	<link href="../clientlib/fontawesome/css/font-awesome.min.css?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/fontawesome/css/font-awesome.min.css"), $this);?>
" rel="stylesheet" type="text/css" />
	<link href="../clientlib/fontawesome/css/font-awesome-animation.min.css?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/fontawesome/css/font-awesome-animation.min.css"), $this);?>
" rel="stylesheet" type="text/css" />
<?php $_from = $this->_tpl_vars['_cssFiles']['admin']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_file']):
?>	<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['_file']; ?>
" />
<?php endforeach; endif; unset($_from); ?>

	<!-- client scripts -->
	<script language="javascript">
	<!--
		var currentSID = '<?php echo $this->_tpl_vars['sid']; ?>
';
	//-->
	</script>
	<script src="../clientlang.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
" type="text/javascript" language="javascript"></script>
	<script src="<?php echo $this->_tpl_vars['tpldir']; ?>
js/common.js?<?php echo TemplateFileDateSig(array('file' => "js/common.js"), $this);?>
" type="text/javascript" language="javascript"></script>
<?php $_from = $this->_tpl_vars['_jsFiles']['admin']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_file']):
?>	<script type="text/javascript" src="<?php echo $this->_tpl_vars['_file']; ?>
"></script>
<?php endforeach; endif; unset($_from); ?>

	<link href="<?php echo $this->_tpl_vars['tpldir']; ?>
style/print.css?<?php echo TemplateFileDateSig(array('file' => "style/print.css"), $this);?>
" rel="stylesheet" type="text/css" media="print" />
</head>

<body onload="documentLoader();preloadImages();" topmargin="0">

	<div id="navbar">
		<div id="navbar-first">
			<div id="navbar-logo">
				<div id="logo-right">
					<?php if ($this->_tpl_vars['adminRow']['type'] == 0): ?>
					<a href="welcome.php?action=phpinfo&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/phpinfo.png" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'phpinfo'), $this);?>
</a>
					<a href="prefs.common.php?action=license&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/license.png" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'license'), $this);?>
</a>
					<?php endif; ?>
					<a href="javascript:showHelp();"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/help.png" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'help'), $this);?>
</a>
					<a href="admins.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/user_active.png" border="0" alt="" /> <?php echo TemplateText(array('value' => $this->_tpl_vars['adminRow']['username']), $this);?>
</a>
					<a href="index.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=logout" onclick="return confirm('<?php echo TemplateLang(array('p' => 'logoutquestion'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/logout.png" border="0" alt="" /> <?php echo TemplateLang(array('p' => 'logout'), $this);?>
</a>
				</div>
				<a href="welcome.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
">
					<img src="./templates/images/logo_letter.png" border="0" alt="" id="logo" /><img src="./templates/images/logo_text<?php if (! $this->_tpl_vars['isGerman']): ?>_en<?php endif; ?>.png" border="0" alt="" />
				</a>
			</div>
		</div>
		<div id="navbar-second">
			<div id="navbar-nav">
				<ul id="nav">
					<li id="welcome-menu"><a href="#welcome-menu"><img src="./templates/images/ico_license.png" /><?php echo TemplateLang(array('p' => 'welcome'), $this);?>
</a>
						<ul>
							<li><a href="welcome.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/ico_license.png" /><?php echo TemplateLang(array('p' => 'welcome'), $this);?>
</a></li>
							<li><a href="admins.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/ico_users.png" /><?php echo TemplateLang(array('p' => 'admins'), $this);?>
</a></li>
						</ul>
					</li>
					<?php if ($this->_tpl_vars['adminRow']['type'] == 0): ?>
					<li id="prefs-menu"><a href="#prefs-menu"><img src="./templates/images/ico_prefs_misc.png" /><?php echo TemplateLang(array('p' => 'prefs'), $this);?>
</a>
						<ul>
							<li><a href="prefs.common.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/ico_prefs_common.png" /><?php echo TemplateLang(array('p' => 'common'), $this);?>
</a></li>
							<li><a href="prefs.email.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/ico_prefs_email.png" /><?php echo TemplateLang(array('p' => 'email'), $this);?>
</a></li>
							<li><a href="prefs.recvrules.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/rule32.png" /><?php echo TemplateLang(array('p' => 'recvrules'), $this);?>
</a></li>
							<li><a href="prefs.webdisk.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/ico_disk.png" /><?php echo TemplateLang(array('p' => 'webdisk'), $this);?>
</a></li>
							<li><a href="prefs.sms.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/gateway32.png" /><?php echo TemplateLang(array('p' => 'sms'), $this);?>
</a></li>
							<li><a href="prefs.abuse.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/abuse.png" /><?php echo TemplateLang(array('p' => 'abuseprotect'), $this);?>
</a></li>
							<li><a href="prefs.coupons.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/coupon.png" /><?php echo TemplateLang(array('p' => 'coupons'), $this);?>
</a></li>
							<li><a href="prefs.profilefields.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/field32.png" /><?php echo TemplateLang(array('p' => 'profilefields'), $this);?>
</a></li>
							<li><a href="prefs.languages.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/lang32.png" /><?php echo TemplateLang(array('p' => 'languages'), $this);?>
</a></li>
							<li><a href="prefs.templates.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/template.png" /><?php echo TemplateLang(array('p' => 'templates'), $this);?>
</a></li>
							<li><a href="prefs.ads.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/ad32.png" /><?php echo TemplateLang(array('p' => 'ads'), $this);?>
</a></li>
							<li><a href="prefs.faq.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/faq32.png" /><?php echo TemplateLang(array('p' => 'faq'), $this);?>
</a></li>
							<li><a href="prefs.countries.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/country.png" /><?php echo TemplateLang(array('p' => 'countries'), $this);?>
</a></li>
							<li><a href="prefs.widgetlayouts.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/wlayout_add.png" /><?php echo TemplateLang(array('p' => 'widgetlayouts'), $this);?>
</a></li>
							<li><a href="prefs.payments.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/ico_prefs_payments.png" /><?php echo TemplateLang(array('p' => 'payments'), $this);?>
</a></li>
							<li><a href="toolbox.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/toolbox.png" /><?php echo TemplateLang(array('p' => 'toolbox'), $this);?>
</a></li>
						</ul>
					</li>
					<?php endif; ?>
					<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['users'] || $this->_tpl_vars['adminRow']['privileges']['groups'] || $this->_tpl_vars['adminRow']['privileges']['workgroups'] || $this->_tpl_vars['adminRow']['privileges']['activity'] || $this->_tpl_vars['adminRow']['privileges']['newsletter'] || $this->_tpl_vars['adminRow']['privileges']['payments']): ?>
					<li id="users-menu"><a href="#users-menu"><img src="./templates/images/ico_users.png" /><?php echo TemplateLang(array('p' => 'usersgroups'), $this);?>
</a>
						<ul>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['users']): ?><li><a href="users.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/user_action.png" /><?php echo TemplateLang(array('p' => 'users'), $this);?>
</a></li><?php endif; ?>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['groups']): ?><li><a href="groups.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/ico_group.png" /><?php echo TemplateLang(array('p' => 'groups'), $this);?>
</a></li><?php endif; ?>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['workgroups']): ?><li><a href="workgroups.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/ico_workgroup.png" /><?php echo TemplateLang(array('p' => 'workgroups'), $this);?>
</a><?php endif; ?>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['activity']): ?><li><a href="activity.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/activity.png" /><?php echo TemplateLang(array('p' => 'activity'), $this);?>
</a></li><?php endif; ?>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['abuse']): ?><li><a href="abuse.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/abuse.png" /><?php echo TemplateLang(array('p' => 'abuseprotect'), $this);?>
</a></li><?php endif; ?>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['newsletter']): ?><li><a href="newsletter.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/newsletter.png" /><?php echo TemplateLang(array('p' => 'newsletter'), $this);?>
</a></li><?php endif; ?>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['payments']): ?><li><a href="payments.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/ico_prefs_payments.png" /><?php echo TemplateLang(array('p' => 'payments'), $this);?>
</a></li><?php endif; ?>
						</ul>
					</li>
					<?php endif; ?>
					<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['optimize'] || $this->_tpl_vars['adminRow']['privileges']['maintenance'] || $this->_tpl_vars['adminRow']['privileges']['stats'] || $this->_tpl_vars['adminRow']['privileges']['logs']): ?>
					<li id="tools-menu"><a href="#tools-menu"><img src="./templates/images/toolbox.png" /><?php echo TemplateLang(array('p' => 'tools'), $this);?>
</a>
						<ul>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['optimize']): ?><li><a href="optimize.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/db_optimize.png" /><?php echo TemplateLang(array('p' => 'optimize'), $this);?>
</a></li><?php endif; ?>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['maintenance']): ?><li><a href="maintenance.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/orphans32.png" /><?php echo TemplateLang(array('p' => 'maintenance'), $this);?>
</a></li><?php endif; ?>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['stats']): ?><li><a href="stats.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/stats.png" /><?php echo TemplateLang(array('p' => 'stats'), $this);?>
</a></li><?php endif; ?>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['logs']): ?><li><a href="logs.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/logs.png" /><?php echo TemplateLang(array('p' => 'logs'), $this);?>
</a></li><?php endif; ?>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0): ?><li><a href="updates.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/update.png" /><?php echo TemplateLang(array('p' => 'updates'), $this);?>
</a></li><?php endif; ?>
						</ul>
					</li>
					<?php endif; ?>
					<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['plugins']): ?>
					<li id="plugins-menu"><a href="#plugins-menu"><img src="./templates/images/plugin.png" /><?php echo TemplateLang(array('p' => 'plugins'), $this);?>
</a>
						<ul>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0): ?><li><a href="plugins.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="./templates/images/plugin.png" /><?php echo TemplateLang(array('p' => 'plugins'), $this);?>
</a></li><?php endif; ?>
							<?php $_from = $this->_tpl_vars['pluginMenuItems']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['plugin'] => $this->_tpl_vars['pluginInfo']):
?>
							<?php if ($this->_tpl_vars['adminRow']['type'] == 0 || $this->_tpl_vars['adminRow']['privileges']['plugins'][$this->_tpl_vars['plugin']]): ?>
							<li><a href="plugin.page.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&plugin=<?php echo $this->_tpl_vars['plugin']; ?>
" ><img src="<?php if ($this->_tpl_vars['pluginInfo']['icon']): ?>../plugins/templates/images/<?php echo $this->_tpl_vars['pluginInfo']['icon']; ?>
<?php else: ?>./templates/images/wlayout_add.png<?php endif; ?>" /><?php echo $this->_tpl_vars['pluginInfo']['title']; ?>
</a></li>
							<?php endif; ?>
							<?php endforeach; endif; unset($_from); ?><?php echo TemplatePluginMenus(array(), $this);?>

						</ul>
					</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>

	<?php if ($this->_tpl_vars['title']): ?><div id="breadcrumb">
		<?php echo $this->_tpl_vars['title']; ?>

	</div><?php endif; ?>

	<div id="content">
		<div id="pageTabs">
			<ul>
				<?php $_from = $this->_tpl_vars['tabs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tab']):
?>
				<li<?php if ($this->_tpl_vars['tab']['active']): ?> class="active"<?php endif; ?>>
					<a href="<?php echo $this->_tpl_vars['tab']['link']; ?>
sid=<?php echo $this->_tpl_vars['sid']; ?>
">
						<img src="<?php if ($this->_tpl_vars['tab']['relIcon']): ?>./templates/images/<?php echo $this->_tpl_vars['tab']['relIcon']; ?>
<?php elseif ($this->_tpl_vars['tab']['icon']): ?><?php echo $this->_tpl_vars['tab']['icon']; ?>
<?php else: ?>./templates/images/ico_prefs_misc.png<?php endif; ?>" border="0" alt="" />
						<?php echo $this->_tpl_vars['tab']['title']; ?>

					</a>
				</li>
				<?php endforeach; endif; unset($_from); ?>
			</ul>
		</div>
		<div id="pageContent">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['page']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<br class="clear" />
		</div>
	</div>

	<div id="footer">
		<a href="welcome.php?action=about&sid=<?php echo $this->_tpl_vars['sid']; ?>
">b1gMail <?php echo $this->_tpl_vars['bmver']; ?>
</a>
	</div>

</body>

</html>