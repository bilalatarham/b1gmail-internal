<?php /* Smarty version 2.6.28, created on 2020-09-29 11:33:49
         compiled from /var/www/html/plugins/templates/bms.admin.overview.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', '/var/www/html/plugins/templates/bms.admin.overview.tpl', 2, false),array('function', 'size', '/var/www/html/plugins/templates/bms.admin.overview.tpl', 44, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'license'), $this);?>
</legend>
	
	<table width="100%">
		<tr>
			<td rowspan="2" width="40" align="center" valign="top"><img src="../plugins/templates/images/bms_logo.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td1" width="24%"><?php echo TemplateLang(array('p' => 'version'), $this);?>
 (<?php echo TemplateLang(array('p' => 'bms_adminplugin'), $this);?>
):</td>
			<td class="td2" width="26%"><?php echo $this->_tpl_vars['adminVersion']; ?>
</td>
			
			<td rowspan="2" width="40" align="center" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_license.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td1" width="24%"><?php echo TemplateLang(array('p' => 'bms_licstatus'), $this);?>
:</td>
			<td class="td2" width="26%">					
					<a href="<?php echo $this->_tpl_vars['pageURL']; ?>
&action=common&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php if ($this->_tpl_vars['bms_prefs']['licstate'] == 2): ?>
					<font color="red"><?php echo TemplateLang(array('p' => 'bms_expired'), $this);?>
</font>
					<?php elseif ($this->_tpl_vars['bms_prefs']['licstate'] == 0): ?>
					<font color="red"><?php echo TemplateLang(array('p' => 'bms_invalid'), $this);?>
</font>
					<?php elseif ($this->_tpl_vars['bms_prefs']['licstate'] == 1): ?>
					<font color="darkgreen"><?php echo TemplateLang(array('p' => 'bms_valid'), $this);?>
</font>
					<?php else: ?>
					<?php echo TemplateLang(array('p' => 'bms_validating'), $this);?>

					<?php endif; ?></a>
			</td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'version'), $this);?>
 (<?php echo TemplateLang(array('p' => 'bms_core'), $this);?>
):</td>
			<td class="td2"><?php if ($this->_tpl_vars['coreVersion']): ?><?php echo $this->_tpl_vars['coreVersion']; ?>
<?php else: ?><i>(<?php echo TemplateLang(array('p' => 'unknown'), $this);?>
)</i><?php endif; ?></td>
			<td colspan="2">&nbsp;</td>
		</tr>
	</table>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'overview'), $this);?>
</legend>
	
	<table width="100%">
		<?php if (( $this->_tpl_vars['bms_prefs']['licfeatures'] & 4 ) != 0): ?>
		<tr>
			<td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/bms_common.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td1" width="24%"><?php echo TemplateLang(array('p' => 'bms_pop3today'), $this);?>
:</td>
			<td class="td2" width="26%"><?php echo $this->_tpl_vars['pop3Today']; ?>
</td>
			
			<td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/bms_stats.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td1" width="24%"><?php echo TemplateLang(array('p' => 'bms_pop3traffic'), $this);?>
:</td>
			<td class="td2" width="26%"><?php echo TemplateSize(array('bytes' => $this->_tpl_vars['pop3Traffic']), $this);?>
</td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'bms_imaptoday'), $this);?>
:</td>
			<td class="td2"><?php echo $this->_tpl_vars['imapToday']; ?>
</td>
			
			<td class="td1"><?php echo TemplateLang(array('p' => 'bms_imaptraffic'), $this);?>
:</td>
			<td class="td2"><?php echo TemplateSize(array('bytes' => $this->_tpl_vars['imapTraffic']), $this);?>
</td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'bms_smtptoday'), $this);?>
:</td>
			<td class="td2"><?php echo $this->_tpl_vars['smtpToday']; ?>
</td>
			
			<td class="td1"><?php echo TemplateLang(array('p' => 'bms_smtptraffic'), $this);?>
:</td>
			<td class="td2"><?php echo TemplateSize(array('bytes' => $this->_tpl_vars['smtpTraffic']), $this);?>
</td>
		</tr>
		
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>
		<?php endif; ?>
		<tr>
			<td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/bms_queue.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td1"><?php echo TemplateLang(array('p' => 'bms_queueentries'), $this);?>
:</td>
			<td class="td2" width="20%"><?php echo $this->_tpl_vars['queueEntries']; ?>
</td>
			
			<td rowspan="3" width="40" align="center" valign="top"><img src="../plugins/templates/images/bms_features.png" border="0" alt="" width="32" heigh="32" /></td>
			<td class="td1"><?php echo TemplateLang(array('p' => 'bms_queue'), $this);?>
?</td>
			<td class="td2" width="20%"><img src="templates/images/<?php if ($this->_tpl_vars['queueRunning']): ?>ok<?php else: ?>delete<?php endif; ?>.png" align="absmiddle" border="0" alt="" width="16" height="16" />
										<?php if ($this->_tpl_vars['queueRunning']): ?><?php echo TemplateLang(array('p' => 'bms_running'), $this);?>
 (<?php echo $this->_tpl_vars['threadCount']; ?>
 <?php echo TemplateLang(array('p' => 'bms_threads'), $this);?>
)<?php else: ?><?php echo TemplateLang(array('p' => 'bms_not_running'), $this);?>
<?php endif; ?></td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'bms_inbound'), $this);?>
:</td>
			<td class="td2"><?php echo $this->_tpl_vars['queueInbound']; ?>
</td>
			
			<td class="td1"><?php echo TemplateLang(array('p' => 'bms_feature_tls'), $this);?>
?</td>
			<td class="td2" width="20%"><img src="templates/images/<?php if (( $this->_tpl_vars['bms_prefs']['core_features'] & 1 ) != 0): ?>ok<?php else: ?>delete<?php endif; ?>.png" border="0" alt="" width="16" height="16" /></td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'bms_outbound'), $this);?>
:</td>
			<td class="td2"><?php echo $this->_tpl_vars['queueOutbound']; ?>
</td>
			
			<td class="td1"><?php echo TemplateLang(array('p' => 'bms_feature_sig'), $this);?>
?</td>
			<td class="td2"><img src="templates/images/<?php if (( $this->_tpl_vars['bms_prefs']['core_features'] & 2 ) != 0): ?>ok<?php else: ?>delete<?php endif; ?>.png" border="0" alt="" width="16" height="16" /></td>
		</tr>
	</table>
</fieldset>


<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'notices'), $this);?>
</legend>
	
	<table width="100%" id="noticeTable">
	<?php $_from = $this->_tpl_vars['notices']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['notice']):
?>
		<tr>
			<td width="20" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php echo $this->_tpl_vars['notice']['type']; ?>
.png" width="16" height="16" border="0" alt="" align="absmiddle" /></td>
			<td valign="top"><?php echo $this->_tpl_vars['notice']['text']; ?>
</td>
			<td align="right" valign="top" width="20"><?php if ($this->_tpl_vars['notice']['link']): ?><a href="<?php echo $this->_tpl_vars['notice']['link']; ?>
sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/go.png" border="0" alt="" width="16" height="16" /></a><?php else: ?>&nbsp;<?php endif; ?></td>
		</tr>
	<?php endforeach; endif; unset($_from); ?>
	</table>
</fieldset>

<script language="javascript" src="https://ssl.b1g.de/service.b1gmail.com/b1gmailserver/updates/?do=noticeJS&adminVersion=<?php echo $this->_tpl_vars['adminVersion']; ?>
&coreVersion=<?php echo $this->_tpl_vars['coreVersion']; ?>
&lang=<?php echo $this->_tpl_vars['lang']; ?>
"></script>