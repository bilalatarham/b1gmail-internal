<?php /* Smarty version 2.6.28, created on 2020-09-29 11:36:11
         compiled from prefs.taborder.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.taborder.tpl', 3, false),array('function', 'cycle', 'prefs.taborder.tpl', 13, false),array('function', 'text', 'prefs.taborder.tpl', 16, false),)), $this); ?>
<form action="prefs.common.php?action=taborder&save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" name="f1" method="post" onsubmit="spin(this)">
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'taborder'), $this);?>
</legend>
	
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th><?php echo TemplateLang(array('p' => 'title'), $this);?>
</th>
			<th width="80"><?php echo TemplateLang(array('p' => 'pos'), $this);?>
</th>
		</tr>
		
		<?php $_from = $this->_tpl_vars['pageTabs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tabKey'] => $this->_tpl_vars['tab']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td align="center"><img src="<?php if (! $this->_tpl_vars['tab']['iconDir']): ?><?php echo $this->_tpl_vars['usertpldir']; ?>
images/li/tab_ico_<?php else: ?>../<?php echo $this->_tpl_vars['tab']['iconDir']; ?>
<?php endif; ?><?php echo $this->_tpl_vars['tab']['icon']; ?>
.png" border="0" alt="" width="16" height="16" /></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['tab']['text']), $this);?>
</td>
			<td><input type="text" name="order[<?php echo $this->_tpl_vars['tabKey']; ?>
]" value="<?php echo $this->_tpl_vars['tab']['order']; ?>
" size="6" /></td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
	</table>
</fieldset>
<p>
	<div style="float:right;" class="buttons">
		<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
	</div>
</p>
</form>