<?php /* Smarty version 2.6.28, created on 2020-09-29 11:34:49
         compiled from /var/www/html/plugins/templates/bms.admin.common.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', '/var/www/html/plugins/templates/bms.admin.common.tpl', 3, false),array('function', 'text', '/var/www/html/plugins/templates/bms.admin.common.tpl', 15, false),array('function', 'date', '/var/www/html/plugins/templates/bms.admin.common.tpl', 147, false),)), $this); ?>
<form action="<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=common&save=true" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_userarea'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="4" valign="top" width="40"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_users.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_usershowlogin'), $this);?>
?</td>
				<td class="td2">
					<input type="checkbox" name="user_showlogin"<?php if ($this->_tpl_vars['bms_prefs']['user_showlogin']): ?> checked="checked"<?php endif; ?> />
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_pop3server'), $this);?>
:</td>
				<td class="td2"><input type="text" name="user_pop3server" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['user_pop3server'],'allowEmpty' => true), $this);?>
" size="32" />
					:
					<input type="text" name="user_pop3port" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['user_pop3port']), $this);?>
" size="5" />
					<input type="checkbox" name="user_pop3ssl" id="user_pop3ssl"<?php if ($this->_tpl_vars['bms_prefs']['user_pop3ssl']): ?> checked="checked"<?php endif; ?> />
					<label for="user_pop3ssl">SSL</label></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_smtpserver'), $this);?>
:</td>
				<td class="td2"><input type="text" name="user_smtpserver" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['user_smtpserver'],'allowEmpty' => true), $this);?>
" size="32" />
					:
					<input type="text" name="user_smtpport" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['user_smtpport']), $this);?>
" size="5" />
					<input type="checkbox" name="user_smtpssl" id="user_smtpssl"<?php if ($this->_tpl_vars['bms_prefs']['user_smtpssl']): ?> checked="checked"<?php endif; ?> />
					<label for="user_smtpssl">SSL</label></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_imapserver'), $this);?>
:</td>
				<td class="td2"><input type="text" name="user_imapserver" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['user_imapserver'],'allowEmpty' => true), $this);?>
" size="32" />
					:
					<input type="text" name="user_imapport" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['user_imapport']), $this);?>
" size="5" />
					<input type="checkbox" name="user_imapssl" id="user_imapssl"<?php if ($this->_tpl_vars['bms_prefs']['user_imapssl']): ?> checked="checked"<?php endif; ?> />
					<label for="user_imapssl">SSL</label></td>
			</tr>
	</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_logging'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="5" valign="top" width="40"><img src="../plugins/templates/images/bms_logging.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_logging_debug'), $this);?>
?</td>
				<td class="td2"><input type="checkbox" name="loglevel[8]"<?php if (( $this->_tpl_vars['bms_prefs']['loglevel'] & 8 ) != 0): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_logging_notices'), $this);?>
?</td>
				<td class="td2"><input type="checkbox" name="loglevel[1]"<?php if (( $this->_tpl_vars['bms_prefs']['loglevel'] & 1 ) != 0): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_logging_warnings'), $this);?>
?</td>
				<td class="td2"><input type="checkbox" name="loglevel[2]"<?php if (( $this->_tpl_vars['bms_prefs']['loglevel'] & 2 ) != 0): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_logging_errors'), $this);?>
?</td>
				<td class="td2"><input type="checkbox" name="loglevel[4]"<?php if (( $this->_tpl_vars['bms_prefs']['loglevel'] & 4 ) != 0): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_logging_autodelete'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" id="logs_autodelete" name="logs_autodelete"<?php if ($this->_tpl_vars['bms_prefs']['logs_autodelete']): ?> checked="checked"<?php endif; ?> />
					<label for="logs_autodelete"><?php echo TemplateLang(array('p' => 'bms_enableolder'), $this);?>
</label>
					<input type="text" name="logs_autodelete_days" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['logs_autodelete_days']), $this);?>
" size="4" />
					<?php echo TemplateLang(array('p' => 'days'), $this);?>
<br />
					<input type="checkbox" id="logs_autodelete_archive" name="logs_autodelete_archive"<?php if ($this->_tpl_vars['bms_prefs']['logs_autodelete_archive']): ?> checked="checked"<?php endif; ?> />
					<label for="logs_autodelete_archive"><?php echo TemplateLang(array('p' => 'savearc'), $this);?>
</label>
				</td>
			</tr>
		</table>
	</fieldset>
	
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_failban'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="5" valign="top" width="40"><img src="../plugins/templates/images/bms_untrusted.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_fb_activatefor'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" name="failban_types[1]" id="failban_types_1"<?php if (( $this->_tpl_vars['bms_prefs']['failban_types'] & 1 ) != 0): ?> checked="checked"<?php endif; ?> />
						<label for="failban_types_1"><?php echo TemplateLang(array('p' => 'bms_fb_1'), $this);?>
</label><br />
					<input type="checkbox" name="failban_types[2]" id="failban_types_2"<?php if (( $this->_tpl_vars['bms_prefs']['failban_types'] & 2 ) != 0): ?> checked="checked"<?php endif; ?> />
						<label for="failban_types_2"><?php echo TemplateLang(array('p' => 'bms_fb_2'), $this);?>
</label><br />
					<input type="checkbox" name="failban_types[4]" id="failban_types_4"<?php if (( $this->_tpl_vars['bms_prefs']['failban_types'] & 4 ) != 0): ?> checked="checked"<?php endif; ?> />
						<label for="failban_types_4"><?php echo TemplateLang(array('p' => 'bms_fb_4'), $this);?>
</label><br />
					<input type="checkbox" name="failban_types[8]" id="failban_types_8"<?php if (( $this->_tpl_vars['bms_prefs']['failban_types'] & 8 ) != 0): ?> checked="checked"<?php endif; ?> />
						<label for="failban_types_8"><?php echo TemplateLang(array('p' => 'bms_fb_8'), $this);?>
</label>				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_fb_attempts'), $this);?>
:</td>
				<td class="td2"><input type="text" size="6" name="failban_attempts" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['failban_attempts']), $this);?>
" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_fb_time'), $this);?>
:</td>
				<td class="td2"><input type="text" size="6" name="failban_time" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['failban_time']), $this);?>
" /> <?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_fb_bantime'), $this);?>
:</td>
				<td class="td2"><input type="text" size="6" name="failban_bantime" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['failban_bantime']), $this);?>
" /> <?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_list'), $this);?>
:</td>
				<td class="td2"><?php echo $this->_tpl_vars['banCount']; ?>
 <?php echo TemplateLang(array('p' => 'entries'), $this);?>
 <input class="button" type="button"<?php if ($this->_tpl_vars['banCount'] == 0): ?> disabled="disabled"<?php endif; ?> value=" <?php echo TemplateLang(array('p' => 'show'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=common&do=banlist';" /> <input<?php if ($this->_tpl_vars['banCount'] == 0): ?> disabled="disabled"<?php endif; ?> class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'reset'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&action=common&resetBanList=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
';" /></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_tls_ssl'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" valign="top" width="40" rowspan="2"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_ssl.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_ssl_cipher_list'), $this);?>
:</td>
				<td class="td2"><input type="text" name="ssl_cipher_list" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['ssl_cipher_list'],'allowEmpty' => true), $this);?>
" style="width:95%;" /></td>
			</tr>
			<tr>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_tlsarecord'), $this);?>
:</td>
				<td class="td2"><div id="tlsaRecord"><input<?php if (! $this->_tpl_vars['queueRunning'] || ( $this->_tpl_vars['bms_prefs']['core_features'] & 1 ) == 0): ?> disabled="disabled"<?php endif; ?> type="button" class="button" value="<?php echo TemplateLang(array('p' => 'show'), $this);?>
" onclick="bms_showTLSARecord()" /></div></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'license'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="5" valign="top" width="40"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_license.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'status'), $this);?>
:</td>
				<td class="td2">
					<?php if ($this->_tpl_vars['bms_prefs']['licstate'] == 2): ?>
					<font color="red"><?php echo TemplateLang(array('p' => 'bms_expired'), $this);?>
</font>
					<?php elseif ($this->_tpl_vars['bms_prefs']['licstate'] == 0): ?>
					<font color="red"><?php echo TemplateLang(array('p' => 'bms_invalid'), $this);?>
</font>
					<?php elseif ($this->_tpl_vars['bms_prefs']['licstate'] == 1): ?>
					<font color="darkgreen"><?php echo TemplateLang(array('p' => 'bms_valid'), $this);?>

						<?php if ($this->_tpl_vars['bms_prefs']['lic_valid_until'] <= 0): ?>
						(<?php echo TemplateLang(array('p' => 'unlimited'), $this);?>
)
						<?php else: ?>
						(<?php echo TemplateLang(array('p' => 'bms_until'), $this);?>
 <?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['bms_prefs']['lic_valid_until'],'dayonly' => true), $this);?>
)
						<?php endif; ?>
					</font>
					<?php else: ?>
					<?php echo TemplateLang(array('p' => 'bms_validating'), $this);?>

					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'features'), $this);?>
:</td>
				<td class="td2"><?php echo TemplateText(array('value' => $this->_tpl_vars['features']), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'version'), $this);?>
:</td>
				<td class="td2"><?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['core_version']), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'licensekey'), $this);?>
:</td>
				<td class="td2"><input type="text" name="license" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['license'],'allowEmpty' => true), $this);?>
" size="32" style="width:95%;" /></td>
			</tr>
		</table>
	</fieldset>
	
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_activation'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="3" valign="top" width="40"><img src="../plugins/templates/images/bms_activation.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'status'), $this);?>
:</td>
				<td class="td2">
					<?php if ($this->_tpl_vars['bms_prefs']['activation_status'] == 0): ?>
					<font color="darkorange"><?php echo TemplateLang(array('p' => 'bms_notyetactivated'), $this);?>
</font>
					<?php elseif ($this->_tpl_vars['bms_prefs']['activation_status'] == 2): ?>
					<font color="red"><?php echo TemplateLang(array('p' => 'bms_failed'), $this);?>
</font>
					<?php elseif ($this->_tpl_vars['bms_prefs']['activation_status'] == 1): ?>
					<font color="darkgreen"><?php echo TemplateLang(array('p' => 'bms_activated'), $this);?>
<?php else: ?>
					<?php echo TemplateLang(array('p' => 'bms_validating'), $this);?>

					<?php endif; ?>
					
					<?php if ($this->_tpl_vars['bms_prefs']['activation_status'] != 1 && $this->_tpl_vars['bms_prefs']['auto_activate'] == 0): ?>
					<input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'bms_activatenow'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=common&do=activate';" />
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_systemid'), $this);?>
:</td>
				<td class="td2"><?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['system_id']), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_autoactivate'), $this);?>
?</td>
				<td class="td2"><input type="checkbox" name="auto_activate"<?php if ($this->_tpl_vars['bms_prefs']['auto_activate']): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
		</table>
	</fieldset>
	
	<p>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>

<script language="javascript">
<?php echo '<!--
	function bms_showTLSARecord()
	{
		MakeXMLRequest(\''; ?>
<?php echo $this->_tpl_vars['pageURL']; ?>
<?php echo '&sid=\' + currentSID
							+ \'&action=common&do=tlsaRecord\',
			function(e)
			{
				if(e.readyState == 4)
				{
					var text = e.responseText;
					if(text.length > 0)
					{
						var div = EBID(\'tlsaRecord\');
						while(div.firstChild) div.removeChild(div.firstChild);

						var field = document.createElement(\'input\');
						field.style.width 	= \'95%\';
						field.readOnly 		= true;
						field.value 		= text;
						field.onclick 		= function() { field.select(); };
						EBID(\'tlsaRecord\').appendChild(field);

						field.select();
					}
				}
			});
	}
//-->'; ?>

</script>