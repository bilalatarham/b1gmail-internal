<?php /* Smarty version 2.6.28, created on 2020-09-29 15:50:04
         compiled from prefs.faq.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.faq.tpl', 2, false),array('function', 'cycle', 'prefs.faq.tpl', 16, false),array('function', 'text', 'prefs.faq.tpl', 21, false),array('function', 'fileDateSig', 'prefs.faq.tpl', 94, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'faq'), $this);?>
</legend>
	
	<form action="prefs.faq.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" name="f1" onsubmit="spin(this)">
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="25" style="text-align:center;"><a href="javascript:invertSelection(document.forms.f1,'faq_');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/dot.png" border="0" alt="" width="10" height="8" /></a></th>
			<th><?php echo TemplateLang(array('p' => 'question'), $this);?>
</th>
			<th width="100"><?php echo TemplateLang(array('p' => 'language'), $this);?>
</th>
			<th width="120"><?php echo TemplateLang(array('p' => 'type'), $this);?>
</th>
			<th width="60">&nbsp;</th>
		</tr>
		
		<?php $_from = $this->_tpl_vars['faqs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['faq']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/faq.png" border="0" alt="" width="16" height="16" /></td>
			<td align="center"><input type="checkbox" name="faq_<?php echo $this->_tpl_vars['faq']['id']; ?>
" /></td>
			<td><a href="prefs.faq.php?do=edit&id=<?php echo $this->_tpl_vars['faq']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo $this->_tpl_vars['faq']['frage']; ?>
</a><br /><small><?php echo TemplateLang(array('p' => 'requires'), $this);?>
: <?php if ($this->_tpl_vars['faq']['required']): ?><?php echo $this->_tpl_vars['requirements'][$this->_tpl_vars['faq']['required']]; ?>
<?php else: ?>-<?php endif; ?></small></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['faq']['lang']), $this);?>
</td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['faq']['typ']), $this);?>
</td>
			<td>
				<a href="prefs.faq.php?do=edit&id=<?php echo $this->_tpl_vars['faq']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/edit.png" border="0" alt="<?php echo TemplateLang(array('p' => 'edit'), $this);?>
" width="16" height="16" /></a>
				<a href="prefs.faq.php?delete=<?php echo $this->_tpl_vars['faq']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onclick="return confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/delete.png" border="0" alt="<?php echo TemplateLang(array('p' => 'edit'), $this);?>
" width="16" height="16" /></a>
			</td>
			
		</tr>
		<?php endforeach; endif; unset($_from); ?>
		
		<tr>
			<td class="footer" colspan="6">
				<div style="float:left;">
					<?php echo TemplateLang(array('p' => 'action'), $this);?>
: <select name="massAction" class="smallInput">
						<option value="-">------------</option>
						
						<optgroup label="<?php echo TemplateLang(array('p' => 'actions'), $this);?>
">
							<option value="delete"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option>
						</optgroup>
					</select>&nbsp;
				</div>
				<div style="float:left;">
					<input type="submit" name="executeMassAction" value=" <?php echo TemplateLang(array('p' => 'execute'), $this);?>
 " class="smallInput" />
				</div>
			</td>
		</tr>
	</table>
	</form>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'addfaq'), $this);?>
</legend>
	
	<form action="prefs.faq.php?add=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="editor.submit();spin(this);">
		<table width="100%">
			<tr>
				<td width="40" valign="top" rowspan="6"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/faq32.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="150"><?php echo TemplateLang(array('p' => 'question'), $this);?>
:</td>
				<td class="td2"><input type="text" style="width:85%;" name="frage" value="" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'type'), $this);?>
:</td>
				<td class="td2"><select name="typ">
					<option value="nli"><?php echo TemplateLang(array('p' => 'nli'), $this);?>
</option>
					<option value="li"><?php echo TemplateLang(array('p' => 'li'), $this);?>
</option>
					<option value="both"><?php echo TemplateLang(array('p' => 'both'), $this);?>
</option>
				</select></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'language'), $this);?>
:</td>
				<td class="td2"><select name="lang">
					<option value=":all:"><?php echo TemplateLang(array('p' => 'all'), $this);?>
</option>
					<optgroup label="<?php echo TemplateLang(array('p' => 'languages'), $this);?>
">
						<?php $_from = $this->_tpl_vars['languages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['langID'] => $this->_tpl_vars['lang']):
?>
						<option value="<?php echo $this->_tpl_vars['langID']; ?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['lang']['title']), $this);?>
</option>
						<?php endforeach; endif; unset($_from); ?>
					</optgroup>
				</select></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'requires'), $this);?>
:</td>
				<td class="td2"><select name="required">
					<option value="">------------</option>
					<optgroup label="<?php echo TemplateLang(array('p' => 'services'), $this);?>
">
						<?php $_from = $this->_tpl_vars['requirements']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['reqID'] => $this->_tpl_vars['req']):
?>
						<option value="<?php echo $this->_tpl_vars['reqID']; ?>
"><?php echo $this->_tpl_vars['req']; ?>
</option>
						<?php endforeach; endif; unset($_from); ?>
					</optgroup>
				</select></td>
			</tr>
			<tr>
				<td colspan="2" style="border: 1px solid #DDDDDD;background-color:#FFFFFF;">
					<textarea name="antwort" id="antwort" class="plainTextArea" style="width:100%;height:220px;"></textarea>
					<script language="javascript" src="../clientlib/wysiwyg.js?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/wysiwyg.js"), $this);?>
"></script>
					<script type="text/javascript" src="../clientlib/ckeditor/ckeditor.js?<?php echo TemplateFileDateSig(array('file' => "../../clientlib/ckeditor/ckeditor.js"), $this);?>
"></script>
					<script language="javascript">
					<!--
						var editor = new htmlEditor('antwort');
						editor.init();
						registerLoadAction('editor.start()');
					//-->
					</script>
				</td>
			</tr>
			<tr>
				<td>
					<select class="smallInput" onchange="editor.insertText(this.value);">
						<option value="">-- <?php echo TemplateLang(array('p' => 'vars'), $this);?>
 --</option>
						<option value="%%user%%">%%user%% (<?php echo TemplateLang(array('p' => 'email'), $this);?>
)</option>
						<option value="%%wddomain%%">%%wddomain%% (<?php echo TemplateLang(array('p' => 'wddomain'), $this);?>
)</option>
						<option value="%%selfurl%%">%%selfurl%% (<?php echo TemplateLang(array('p' => 'selfurl'), $this);?>
)</option>
						<option value="%%hostname%%">%%hostname%% (<?php echo TemplateLang(array('p' => 'hostname'), $this);?>
)</option>
					</select>
				</td>
				<td align="right">
					<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'add'), $this);?>
 " />
				</td>
			</tr>
		</table>
	</form>
</fieldset>