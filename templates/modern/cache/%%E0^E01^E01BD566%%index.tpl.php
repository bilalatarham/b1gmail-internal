<?php /* Smarty version 2.6.28, created on 2020-09-30 09:57:18
         compiled from nli/index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'nli/index.tpl', 2, false),array('function', 'text', 'nli/index.tpl', 10, false),array('function', 'fileDateSig', 'nli/index.tpl', 15, false),array('function', 'hook', 'nli/index.tpl', 32, false),array('function', 'domain', 'nli/index.tpl', 129, false),)), $this); ?>
<!DOCTYPE html>
<html lang="<?php echo TemplateLang(array('p' => 'langCode'), $this);?>
">

<head>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $this->_tpl_vars['charset']; ?>
" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php if ($this->_tpl_vars['robotsNoIndex']): ?><meta name="robots" content="noindex" /><?php endif; ?>

	<title><?php echo $this->_tpl_vars['service_title']; ?>
<?php if ($this->_tpl_vars['pageTitle']): ?> - <?php echo TemplateText(array('value' => $this->_tpl_vars['pageTitle']), $this);?>
<?php endif; ?></title>

	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

	<link href="<?php echo $this->_tpl_vars['tpldir']; ?>
bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['tpldir']; ?>
style/notloggedin.css?<?php echo TemplateFileDateSig(array('file' => "style/notloggedin.css"), $this);?>
" />

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<script language="javascript" type="text/javascript">
	<!--
		var tplDir = '<?php echo $this->_tpl_vars['tpldir']; ?>
', sslURL = '<?php echo $this->_tpl_vars['ssl_url']; ?>
', serverTZ = <?php echo $this->_tpl_vars['serverTZ']; ?>
;
	//-->
	</script>

	<script src="clientlang.php" language="javascript" type="text/javascript"></script>
	<script src="clientlib/jquery/jquery-1.8.2.min.js"></script>
	<script src="<?php echo $this->_tpl_vars['tpldir']; ?>
bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo $this->_tpl_vars['tpldir']; ?>
js/nli.main.js?<?php echo TemplateFileDateSig(array('file' => "js/nli.main.js"), $this);?>
"></script>
	<?php echo TemplateHook(array('id' => "nli:index.tpl:head"), $this);?>

</head>

<body>
	<?php echo TemplateHook(array('id' => "nli:index.tpl:beforeContent"), $this);?>


	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/logo.png" border="0" alt="" style="height:24px;" /> <?php echo $this->_tpl_vars['service_title']; ?>
</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li<?php if ($_REQUEST['action'] == 'login'): ?> class="active"<?php endif; ?>><a href="index.php"><?php echo TemplateLang(array('p' => 'home'), $this);?>
</a></li>
				<?php $_from = $this->_tpl_vars['pluginUserPages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?><?php if ($this->_tpl_vars['item']['top'] && $this->_tpl_vars['item']['after'] == 'login'): ?>
					<li<?php if ($this->_tpl_vars['item']['active']): ?> class="active"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['item']['link']; ?>
"><?php echo $this->_tpl_vars['item']['text']; ?>
</a></li>
				<?php endif; ?><?php endforeach; endif; unset($_from); ?>
					<?php if ($this->_tpl_vars['_regEnabled'] || ( ! $this->_tpl_vars['templatePrefs']['hideSignup'] )): ?><li<?php if ($_REQUEST['action'] == 'signup'): ?> class="active"<?php endif; ?>><a href="<?php if ($this->_tpl_vars['ssl_signup_enable']): ?><?php echo $this->_tpl_vars['ssl_url']; ?>
<?php endif; ?>index.php?action=signup"><?php echo TemplateLang(array('p' => 'signup'), $this);?>
</a></li><?php endif; ?>
				<?php $_from = $this->_tpl_vars['pluginUserPages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?><?php if ($this->_tpl_vars['item']['top'] && $this->_tpl_vars['item']['after'] == 'signup'): ?>
					<li<?php if ($this->_tpl_vars['item']['active']): ?> class="active"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['item']['link']; ?>
"><?php echo $this->_tpl_vars['item']['text']; ?>
</a></li>
				<?php endif; ?><?php endforeach; endif; unset($_from); ?>
					<li<?php if ($_REQUEST['action'] == 'faq'): ?> class="active"<?php endif; ?>><a href="index.php?action=faq"><?php echo TemplateLang(array('p' => 'faq'), $this);?>
</a></li>
				<?php $_from = $this->_tpl_vars['pluginUserPages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?><?php if ($this->_tpl_vars['item']['top'] && $this->_tpl_vars['item']['after'] == 'faq'): ?>
					<li<?php if ($this->_tpl_vars['item']['active']): ?> class="active"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['item']['link']; ?>
"><?php echo $this->_tpl_vars['item']['text']; ?>
</a></li>
				<?php endif; ?><?php endforeach; endif; unset($_from); ?>
					<li<?php if ($_REQUEST['action'] == 'tos'): ?> class="active"<?php endif; ?>><a href="index.php?action=tos"><?php echo TemplateLang(array('p' => 'tos'), $this);?>
</a></li>
				<?php $_from = $this->_tpl_vars['pluginUserPages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?><?php if ($this->_tpl_vars['item']['top'] && ( ! $this->_tpl_vars['item']['after'] || $this->_tpl_vars['item']['after'] == 'tos' )): ?>
					<li<?php if ($this->_tpl_vars['item']['active']): ?> class="active"<?php endif; ?>><a href="<?php echo $this->_tpl_vars['item']['link']; ?>
"><?php echo $this->_tpl_vars['item']['text']; ?>
</a></li>
				<?php endif; ?><?php endforeach; endif; unset($_from); ?>
					<li<?php if ($_REQUEST['action'] == 'imprint'): ?> class="active"<?php endif; ?>><a href="index.php?action=imprint"><?php echo TemplateLang(array('p' => 'contact'), $this);?>
</a></li>
				</ul>
				<form action="<?php if ($this->_tpl_vars['ssl_login_enable'] || ( $this->_tpl_vars['welcomeBack'] && $_COOKIE['bm_savedSSL'] )): ?><?php echo $this->_tpl_vars['ssl_url']; ?>
<?php endif; ?>index.php?action=login" method="post" id="loginFormPopover">
					<input type="hidden" name="do" value="login" />
					<input type="hidden" name="timezone" value="<?php echo $this->_tpl_vars['timezone']; ?>
" />

					<ul class="nav navbar-nav navbar-right">
						<?php if ($_REQUEST['action'] != 'login' || $this->_tpl_vars['welcomeBack']): ?><li class="login-li<?php if (! $this->_tpl_vars['welcomeBack']): ?> hidden-xs<?php endif; ?>">
							<?php if ($this->_tpl_vars['welcomeBack']): ?>
							<input type="hidden" name="email_full" value="<?php echo $_COOKIE['bm_savedUser']; ?>
" />
							<input type="hidden" name="password" value="" />
							<input type="hidden" name="savelogin" value="true" />
							<?php if ($_COOKIE['bm_savedSSL']): ?><input type="hidden" name="ssl" value="true" /><?php endif; ?>
							
							<div class="btn-group">
								<button type="submit" class="btn btn-primary navbar-btn">
									<span class="glyphicon glyphicon-user"></span>
									<?php echo TemplateText(array('value' => $_COOKIE['bm_savedUser'],'cut' => 18), $this);?>

								</button>
								<button type="button" class="btn btn-primary navbar-btn dropdown-toggle" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li><a href="index.php?action=forgetCookie"><?php echo TemplateLang(array('p' => 'logout'), $this);?>
</a></li>
								</ul>
							</div>
							<?php else: ?>
							<button type="button" class="btn btn-primary navbar-btn dropdown-toggle" data-toggle="popover" data-placement="bottom">
								<?php echo TemplateLang(array('p' => 'login'), $this);?>
 <span class="caret"></span>
							</button>
							<?php endif; ?>
						</li><?php endif; ?>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php $_from = $this->_tpl_vars['languageList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['langKey'] => $this->_tpl_vars['langInfo']):
?><?php if ($this->_tpl_vars['langInfo']['active']): ?><?php echo $this->_tpl_vars['langInfo']['title']; ?>
<?php endif; ?><?php endforeach; endif; unset($_from); ?> <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<?php $_from = $this->_tpl_vars['languageList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['langKey'] => $this->_tpl_vars['langInfo']):
?>
								<li<?php if ($this->_tpl_vars['langInfo']['active']): ?> class="active"<?php endif; ?>><a href="index.php?action=switchLanguage&amp;lang=<?php echo $this->_tpl_vars['langKey']; ?>
<?php if ($_GET['action']): ?>&amp;target=<?php echo TemplateText(array('value' => $_GET['action']), $this);?>
<?php endif; ?>"><?php echo $this->_tpl_vars['langInfo']['title']; ?>
</a></li>
								<?php endforeach; endif; unset($_from); ?>
							</ul>
						</li>
					</ul>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="lostPW" tabindex="-1" role="dialog" aria-labelledby="lostPWLabel" aria-hidden="true">
		<div class="modal-dialog">
			<form action="index.php?action=lostPassword" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo TemplateLang(array('p' => 'cancel'), $this);?>
</span></button>
					<h4 class="modal-title" id="lostPWLabel"><?php echo TemplateLang(array('p' => 'lostpw'), $this);?>
</h4>
				</div>
				<div class="modal-body">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					<?php if ($this->_tpl_vars['domain_combobox']): ?>
						<label class="sr-only" for="email_local_lpw"><?php echo TemplateLang(array('p' => 'email'), $this);?>
</label>
						<input type="text" name="email_local" id="email_local_lpw" class="form-control" placeholder="<?php echo TemplateLang(array('p' => 'email'), $this);?>
" required="true" />
						<div class="input-group-btn">
							<input type="hidden" name="email_domain" data-bind="email-domain" value="<?php echo TemplateDomain(array('value' => $this->_tpl_vars['domainList'][0]), $this);?>
" />
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span data-bind="label">@<?php echo TemplateDomain(array('value' => $this->_tpl_vars['domainList'][0]), $this);?>
</span> <span class="caret"></span></button>
							<ul class="dropdown-menu dropdown-menu-right domainMenu" role="menu">
								<?php $_from = $this->_tpl_vars['domainList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_key'] => $this->_tpl_vars['domain']):
?><li<?php if ($this->_tpl_vars['_key'] == 0): ?> class="active"<?php endif; ?>><a href="#">@<?php echo TemplateDomain(array('value' => $this->_tpl_vars['domain']), $this);?>
</a></li><?php endforeach; endif; unset($_from); ?>
							</ul>
						</div>
					<?php else: ?>
						<label class="sr-only" for="email_full_p"><?php echo TemplateLang(array('p' => 'email'), $this);?>
</label>
						<input type="email" name="email_full" id="email_full_lpw" class="form-control" placeholder="<?php echo TemplateLang(array('p' => 'email'), $this);?>
" required="true" />
					<?php endif; ?>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo TemplateLang(array('p' => 'cancel'), $this);?>
</button>
					<button type="submit" class="btn btn-success"><?php echo TemplateLang(array('p' => 'requestpw'), $this);?>
</button>
				</div>
			</div>
			</form>
		</div>
	</div>

	<div id="loginPopover" style="display: none;">
		<div class="alert alert-danger" style="display:none;"></div>

		<div class="form-group">
			<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
			<?php if ($this->_tpl_vars['domain_combobox']): ?>
				<label class="sr-only" for="email_local_p"><?php echo TemplateLang(array('p' => 'email'), $this);?>
</label>
				<input type="text" name="email_local" id="email_local_p" class="form-control" placeholder="<?php echo TemplateLang(array('p' => 'email'), $this);?>
" required="true" />
				<div class="input-group-btn">
					<input type="hidden" name="email_domain" data-bind="email-domain" value="<?php echo TemplateDomain(array('value' => $this->_tpl_vars['domainList'][0]), $this);?>
" />
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span data-bind="label">@<?php echo TemplateDomain(array('value' => $this->_tpl_vars['domainList'][0]), $this);?>
</span> <span class="caret"></span></button>
					<ul class="dropdown-menu dropdown-menu-right domainMenu" role="menu">
						<?php $_from = $this->_tpl_vars['domainList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_key'] => $this->_tpl_vars['domain']):
?><li<?php if ($this->_tpl_vars['_key'] == 0): ?> class="active"<?php endif; ?>><a href="#">@<?php echo TemplateDomain(array('value' => $this->_tpl_vars['domain']), $this);?>
</a></li><?php endforeach; endif; unset($_from); ?>
					</ul>
				</div>
			<?php else: ?>
				<label class="sr-only" for="email_full_p"><?php echo TemplateLang(array('p' => 'email'), $this);?>
</label>
				<input type="email" name="email_full" id="email_full_p" class="form-control" placeholder="<?php echo TemplateLang(array('p' => 'email'), $this);?>
" required="true" />
			<?php endif; ?>
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
				<label class="sr-only" for="password_p"><?php echo TemplateLang(array('p' => 'password'), $this);?>
</label>
				<input type="password" name="password" id="password_p" class="form-control" placeholder="<?php echo TemplateLang(array('p' => 'password'), $this);?>
" required="true" />
			</div>
		</div>
		<div class="checkbox">
			<label>
				<input type="checkbox" name="savelogin" id="savelogin_p" />
				<?php echo TemplateLang(array('p' => 'savelogin'), $this);?>

			</label>
		</div>
		<?php if ($this->_tpl_vars['ssl_login_option']): ?><div class="checkbox">
			<label>
				<input type="checkbox" id="ssl_p"<?php if ($this->_tpl_vars['ssl_login_enable']): ?> checked="checked"<?php endif; ?> onchange="updateFormSSL(this)" onclick="updateFormSSL(this)" />
				<?php echo TemplateLang(array('p' => 'ssl'), $this);?>

			</label>
		</div><?php endif; ?>
		<div class="form-group">
			<button type="submit" class="btn btn-success btn-block"><?php echo TemplateLang(array('p' => 'login'), $this);?>
</button>
		</div>

		<div class="login-lostpw">
			<a href="#" data-toggle="modal" data-target="#lostPW"><?php echo TemplateLang(array('p' => 'lostpw'), $this);?>
?</a>
		</div>
	</div>

	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['page']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

	<?php if ($this->_tpl_vars['page'] != 'nli/login.tpl'): ?><div class="container">
		<hr />

		<footer class="row">
			<div class="col-xs-4">
				&copy; <?php echo $this->_tpl_vars['year']; ?>
 <?php echo $this->_tpl_vars['service_title']; ?>

			</div>
			<div class="col-xs-4" style="text-align:center;">
				<a href="<?php echo $this->_tpl_vars['mobileURL']; ?>
"><?php echo TemplateLang(array('p' => 'mobilepda'), $this);?>
</a>
				<?php $_from = $this->_tpl_vars['pluginUserPages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?><?php if (! $this->_tpl_vars['item']['top']): ?>
				|	<a href="<?php echo $this->_tpl_vars['item']['link']; ?>
"><?php echo $this->_tpl_vars['item']['text']; ?>
</a>
				<?php endif; ?><?php endforeach; endif; unset($_from); ?>
			</div>
			<div class="col-xs-4" style="text-align:right;">
				<?php echo '&nbsp;'; ?>

			</div>
		</footer>

		<br />
	</div><?php endif; ?>

	<?php echo TemplateHook(array('id' => "nli:index.tpl:afterContent"), $this);?>

</body>

</html>