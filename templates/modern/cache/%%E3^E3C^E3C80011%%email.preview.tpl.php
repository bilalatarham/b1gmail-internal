<?php /* Smarty version 2.6.28, created on 2020-09-29 12:06:54
         compiled from li/email.preview.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'text', 'li/email.preview.tpl', 3, false),array('function', 'lng', 'li/email.preview.tpl', 10, false),array('function', 'addressList', 'li/email.preview.tpl', 11, false),array('function', 'date', 'li/email.preview.tpl', 15, false),array('function', 'size', 'li/email.preview.tpl', 112, false),)), $this); ?>
<div class="previewMailHeader">
	<div class="left">
		<h1><?php echo TemplateText(array('value' => $this->_tpl_vars['subject']), $this);?>
</h1>
		
		<a href="javascript:advancedOptions('mailHeaders', 'right', 'bottom', '<?php echo $this->_tpl_vars['tpldir']; ?>
');" style="float:left;margin-right:0.5em;">
			<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mini_arrow_<?php if ($this->_tpl_vars['narrow']): ?>bottom<?php else: ?>right<?php endif; ?>.png" width="13" height="13" align="absmiddle" border="0" alt="" id="advanced_mailHeaders_arrow" />
		</a>
		
		<div id="advanced_mailHeaders_body2" style="display:<?php if ($this->_tpl_vars['narrow']): ?>none<?php endif; ?>;">
			<?php echo TemplateLang(array('p' => 'from2'), $this);?>

				<?php echo TemplateAddressList(array('list' => $this->_tpl_vars['fromAddresses'],'short' => true), $this);?>

			<?php echo TemplateLang(array('p' => 'to2'), $this);?>

				<?php echo TemplateAddressList(array('list' => $this->_tpl_vars['toAddresses'],'short' => true), $this);?>

			<span class="date">
				<?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['date'],'nice' => true), $this);?>

			</span>
			
			<?php if ($this->_tpl_vars['attachments']): ?>
			<a href="javascript:advancedOptions('mailHeaders', 'right', 'bottom', '<?php echo $this->_tpl_vars['tpldir']; ?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_attachment.gif" border="0" alt="" align="absmiddle" /></a>
			<?php endif; ?>
		</div>
		
		<div id="advanced_mailHeaders_body" style="display:<?php if (! $this->_tpl_vars['narrow']): ?>none<?php endif; ?>;">
			<table class="lightTable">
				<tr>
					<th><?php echo TemplateLang(array('p' => 'from'), $this);?>
:</th>
					<td><?php echo TemplateAddressList(array('list' => $this->_tpl_vars['fromAddresses']), $this);?>
</td>
				</tr>
				<tr>
					<th><?php echo TemplateLang(array('p' => 'to'), $this);?>
:</th>
					<td><?php echo TemplateAddressList(array('list' => $this->_tpl_vars['toAddresses']), $this);?>
</td>
				</tr>
				<?php if ($this->_tpl_vars['ccAddresses']): ?>
				<tr>
					<th><?php echo TemplateLang(array('p' => 'cc'), $this);?>
:</th>
					<td><?php echo TemplateAddressList(array('list' => $this->_tpl_vars['ccAddresses']), $this);?>
</td>
				</tr>
				<?php endif; ?>
				<?php if ($this->_tpl_vars['replyToAddresses']): ?>
				<tr>
					<th><?php echo TemplateLang(array('p' => 'replyto'), $this);?>
:</th>
					<td>
						<?php echo TemplateAddressList(array('list' => $this->_tpl_vars['replyToAddresses']), $this);?>

					</td>
				</tr>
				<?php endif; ?>
				<?php if ($this->_tpl_vars['priority'] != 0): ?>
				<tr>
					<th><?php echo TemplateLang(array('p' => 'priority'), $this);?>
:</th>
					<td>
						<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_<?php if ($this->_tpl_vars['priority'] == -1): ?>low<?php elseif ($this->_tpl_vars['priority'] == 1): ?>high<?php endif; ?>.gif" border="0" alt="" align="absmiddle" />
						<?php echo TemplateLang(array('p' => "prio_".($this->_tpl_vars['priority'])), $this);?>

					</td>
				</tr>
				<?php endif; ?>
				<tr>
					<th><?php echo TemplateLang(array('p' => 'date'), $this);?>
:</th>
					<td><?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['date'],'elapsed' => true), $this);?>
</td>
				</tr>

				<?php if ($this->_tpl_vars['smimeStatus'] != 0 && ! ( $this->_tpl_vars['smimeStatus'] & 1 )): ?>
				<tr>
					<th><?php echo TemplateLang(array('p' => 'security'), $this);?>
:</th>
					<td>
						<?php if ($this->_tpl_vars['smimeStatus'] & 2): ?>
						<font color="#FF0000">
							<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_signed_bad.png" width="16" height="16" border="0" alt="" align="absmiddle" />
							<?php echo TemplateLang(array('p' => 'badsigned'), $this);?>

						</font>
						&nbsp;&nbsp;
						<?php endif; ?>
						<?php if ($this->_tpl_vars['smimeStatus'] & 4): ?>
						<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_signed_ok.png" width="16" height="16" border="0" alt="" align="absmiddle" />
						<a href="javascript:void(0);" onclick="showCertificate('<?php echo $this->_tpl_vars['smimeCertificateHash']; ?>
');"><?php echo TemplateLang(array('p' => 'signed'), $this);?>
</a>
						&nbsp;&nbsp;
						<?php endif; ?>
						<?php if ($this->_tpl_vars['smimeStatus'] & 8): ?>
						<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_signed_noverify.png" width="16" height="16" border="0" alt="" align="absmiddle" />
						<a href="javascript:void(0);" onclick="showCertificate('<?php echo $this->_tpl_vars['smimeCertificateHash']; ?>
');" style="color:#FF8C00;"><?php echo TemplateLang(array('p' => 'noverifysigned'), $this);?>
</a>
						&nbsp;&nbsp;
						<?php endif; ?>
						<?php if ($this->_tpl_vars['smimeStatus'] & 64): ?>
						<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_encrypted_error.png" width="16" height="16" border="0" alt="" align="absmiddle" />
								
						<font color="#FF0000">
							<?php echo TemplateLang(array('p' => 'decryptionfailed'), $this);?>

						</font>
						&nbsp;&nbsp;
						<?php endif; ?>
						<?php if ($this->_tpl_vars['smimeStatus'] & 128): ?>
						<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_encrypted.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'encrypted'), $this);?>

						&nbsp;&nbsp;
						<?php endif; ?>
					</td>
				</tr>
				<?php endif; ?>

				<?php if ($this->_tpl_vars['attachments']): ?>
				<tr>
					<th><?php echo TemplateLang(array('p' => 'attachments'), $this);?>
:</th>
					<td>
						<?php $_from = $this->_tpl_vars['attachments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['attID'] => $this->_tpl_vars['attachment']):
?>
						<i class="fa fa-file-o"></i>
						<?php if ($this->_tpl_vars['attachment']['mimetype'] == 'message/rfc822' || $this->_tpl_vars['attachment']['filetype'] == '.eml'): ?>
						<a href="javascript:showAttachedMail(<?php echo $this->_tpl_vars['mailID']; ?>
, '<?php echo $this->_tpl_vars['attID']; ?>
', '<?php echo TemplateText(array('value' => $this->_tpl_vars['attachment']['filename'],'cut' => 45,'escape' => true), $this);?>
');">
						<?php elseif ($this->_tpl_vars['attachment']['mimetype'] == 'application/zip' || $this->_tpl_vars['attachment']['filetype'] == '.zip'): ?>
						<a href="javascript:showAttachedZIP(<?php echo $this->_tpl_vars['mailID']; ?>
, '<?php echo $this->_tpl_vars['attID']; ?>
', '<?php echo TemplateText(array('value' => $this->_tpl_vars['attachment']['filename'],'cut' => 45,'escape' => true), $this);?>
');">
						<?php else: ?>
						<a href="email.read.php?id=<?php echo $this->_tpl_vars['mailID']; ?>
&action=downloadAttachment&attachment=<?php echo $this->_tpl_vars['attID']; ?>
<?php if ($this->_tpl_vars['attachment']['viewable']): ?>&view=true<?php endif; ?>&sid=<?php echo $this->_tpl_vars['sid']; ?>
" target="_blank">
						<?php endif; ?>
							<?php echo TemplateText(array('value' => $this->_tpl_vars['attachment']['filename'],'cut' => 45), $this);?>

							(<?php echo TemplateSize(array('bytes' => $this->_tpl_vars['attachment']['size']), $this);?>
)</a>
						&nbsp;
						<?php endforeach; endif; unset($_from); ?>
					</td>
				</tr>
				<?php endif; ?>

				<?php if ($this->_tpl_vars['notes']): ?>
				<tr>
					<th><?php echo TemplateLang(array('p' => 'notes'), $this);?>
:</th>
					<td>
						<textarea style="width:100%;height:60px;" readonly="readonly"><?php echo TemplateText(array('value' => $this->_tpl_vars['notes'],'allowEmpty' => true), $this);?>
</textarea>
					</td>
				</tr>
				<?php endif; ?>

				<?php if ($this->_tpl_vars['deliveryStatus']): ?>
				<tr>
					<th><?php echo TemplateLang(array('p' => 'deliverystatus'), $this);?>
:</th>
					<td>
						<?php if ($this->_tpl_vars['deliveryStatus']['exception']): ?><i class="fa fa-exclamation-triangle" style="color:orange;"></i>
						<?php elseif ($this->_tpl_vars['deliveryStatus']['allDelivered']): ?><i class="fa fa-check" style="color:green;"></i>
						<?php else: ?><i class="fa fa-refresh"></i><?php endif; ?>
						<a href="javascript:showDeliveryStatus(<?php echo $this->_tpl_vars['mailID']; ?>
);"><?php echo $this->_tpl_vars['deliveryStatus']['statusText']; ?>
</a>
					</td>
				<?php endif; ?>
			</table>
		</div>
	</div>
	<div class="right">
		<button onclick="currentID=<?php echo $this->_tpl_vars['mailID']; ?>
;showMailMenu(event,this);">
			<i class="fa fa-gears"></i>
			<?php echo TemplateLang(array('p' => 'actions'), $this);?>

			<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_btn_dropdown.png" border="0" alt="" align="absmiddle" />
		</button>
	</div>
</div>

<div id="bigFormToolbar">
	
	<button type="button" onclick="mailReply(<?php echo $this->_tpl_vars['mailID']; ?>
,false);">
		<i class="fa fa-mail-reply"></i>
		<?php echo TemplateLang(array('p' => 'reply'), $this);?>

	</button>
	
	<button type="button" onclick="mailReply(<?php echo $this->_tpl_vars['mailID']; ?>
,true);">
		<i class="fa fa-mail-reply-all"></i>
		<?php echo TemplateLang(array('p' => 'replyall'), $this);?>

	</button>
	
	<button type="button" onclick="document.location.href='email.compose.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&forward=<?php echo $this->_tpl_vars['mailID']; ?>
';">
		<i class="fa fa-mail-forward"></i>
		<?php echo TemplateLang(array('p' => 'forward'), $this);?>

	</button>
	
	<button type="button" onclick="printMail(<?php echo $this->_tpl_vars['mailID']; ?>
,'<?php echo $this->_tpl_vars['sid']; ?>
');">
		<i class="fa fa-print"></i>
		<?php echo TemplateLang(array('p' => 'print'), $this);?>

	</button>
	
	<?php if (! $this->_tpl_vars['folderInfo']['readonly']): ?><button type="button" onclick="<?php if ($this->_tpl_vars['folderID'] == -5): ?>if(confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
')) <?php endif; ?> deleteMail(<?php echo $this->_tpl_vars['mailID']; ?>
);">
		<i class="fa fa-remove"></i>
		<?php echo TemplateLang(array('p' => 'delete'), $this);?>

	</button><?php endif; ?>
	
</div>

<?php if ($this->_tpl_vars['folderID'] == -3): ?>
<div class="mailNote preview">
	&nbsp;
	<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_drafts.png" width="16" height="16" />
	<?php echo TemplateLang(array('p' => 'thisisadraft'), $this);?>

	<a href="email.compose.php?redirect=<?php echo $this->_tpl_vars['mailID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateLang(array('p' => 'editsend'), $this);?>
</a>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['flags'] & 128): ?>
<div class="mailWarning preview">
	&nbsp;
	<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/infected.png" width="16" height="16" />
	<?php echo TemplateLang(array('p' => 'infectedtext'), $this);?>
: <?php echo $this->_tpl_vars['infection']; ?>

</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['flags'] & 256): ?>
<div class="mailNote preview" id="spamQuestionDiv" style="display:;">
	&nbsp;
	<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/spam.png" width="16" height="16" />
	<?php echo TemplateLang(array('p' => 'spamtext'), $this);?>

	<?php if (! $this->_tpl_vars['trained']): ?><a href="javascript:setMailSpamStatus(<?php echo $this->_tpl_vars['mailID']; ?>
, false)"><?php echo TemplateLang(array('p' => 'isnotspam'), $this);?>
</a><?php endif; ?>
</div>
<?php elseif (! $this->_tpl_vars['trained']): ?>
<div class="mailNote preview" id="spamQuestionDiv" style="display:;">
	&nbsp;
	<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/spam_question.png" width="16" height="16" />
	<?php echo TemplateLang(array('p' => 'spamquestion'), $this);?>

	&nbsp;&nbsp;
	<a href="javascript:setMailSpamStatus(<?php echo $this->_tpl_vars['mailID']; ?>
, true, true)">
		<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/yes.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'yes'), $this);?>

	</a>
	&nbsp;&nbsp;
	<a href="javascript:setMailSpamStatus(<?php echo $this->_tpl_vars['mailID']; ?>
, false)">
		<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/no.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'no'), $this);?>

	</a>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['flags'] & 512): ?>
<div class="mailNote preview">
	&nbsp;
	<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/msg_small.png" width="16" height="16" />
	<?php echo TemplateLang(array('p' => 'certmailinfo'), $this);?>

</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['htmlAvailable']): ?>
<div class="mailNote preview">
	&nbsp;
	<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/msg_small.png" width="16" height="16" />
	<?php echo TemplateLang(array('p' => 'htmlavailable'), $this);?>

	<a href="email.read.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&id=<?php echo $this->_tpl_vars['mailID']; ?>
&htmlView=true"><?php echo TemplateLang(array('p' => 'view'), $this);?>
 &raquo;</a>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['noExternal']): ?>
<div class="mailNote preview" id="noExternalDiv" style="display:;">
	&nbsp;
	<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/msg_small.png" width="16" height="16" />
	<?php echo TemplateLang(array('p' => 'noexternal'), $this);?>

	<a href="email.read.php?action=inlineHTML&mode=<?php echo $this->_tpl_vars['textMode']; ?>
&id=<?php echo $this->_tpl_vars['mailID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&enableExternal=true" target="textArea" onclick="document.getElementById('noExternalDiv').style.display='none';"><?php echo TemplateLang(array('p' => 'showexternal'), $this);?>
 &raquo;</a>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['confirmationTo']): ?>
<div class="mailNote preview" id="confirmationDiv" style="display:;">
	&nbsp;
	<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_confirm.png" width="16" height="16" />
	<?php echo TemplateLang(array('p' => 'senderconfirmto'), $this);?>

	<b><?php echo TemplateText(array('value' => $this->_tpl_vars['confirmationTo']), $this);?>
</b>.
	<a href="javascript:sendMailConfirmation(<?php echo $this->_tpl_vars['mailID']; ?>
);"><?php echo TemplateLang(array('p' => 'sendconfirmation'), $this);?>
 &raquo;</a>
</div>
<?php elseif ($this->_tpl_vars['flags'] & 16384): ?>
<div class="mailNote preview" id="confirmationDiv" style="display:;">
	&nbsp;
	<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_confirm.png" width="16" height="16" />
	<?php echo TemplateLang(array('p' => 'confirmationsent'), $this);?>

</div>
<?php endif; ?>

<iframe width="100%" style="height:200px;" id="textArea" name="textArea" src="about:blank" frameborder="no"></iframe>
<textarea id="textArea_raw" style="display:none;"><?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['text'],'allowDoubleEnc' => true), $this);?>
</textarea>

<form id="quoteForm" action="email.compose.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&reply=<?php echo $this->_tpl_vars['mailID']; ?>
" method="post">
	<input type="hidden" name="text" id="quoteText" value="" />
</form>