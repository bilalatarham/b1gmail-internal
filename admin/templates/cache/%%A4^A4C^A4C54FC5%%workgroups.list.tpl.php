<?php /* Smarty version 2.6.28, created on 2020-09-30 13:04:25
         compiled from workgroups.list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'workgroups.list.tpl', 2, false),array('function', 'cycle', 'workgroups.list.tpl', 15, false),array('function', 'text', 'workgroups.list.tpl', 19, false),array('function', 'email', 'workgroups.list.tpl', 20, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'workgroups'), $this);?>
</legend>

	<form name="f1" action="workgroups.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post">
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="25" style="text-align:center;"><a href="javascript:invertSelection(document.forms.f1,'group_');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/dot.png" border="0" alt="" width="10" height="8" /></a></th>
			<th><?php echo TemplateLang(array('p' => 'title'), $this);?>
</th>
			<th><?php echo TemplateLang(array('p' => 'email'), $this);?>
</th>
			<th width="70">&nbsp;</th>
		</tr>

		<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['group']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_workgroup.png" border="0" width="16" height="16" alt="" /></td>
			<td align="center"><input type="checkbox" name="group_<?php echo $this->_tpl_vars['group']['id']; ?>
" /></td>
			<td><a href="workgroups.php?do=edit&id=<?php echo $this->_tpl_vars['group']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['group']['title']), $this);?>
</a><br /><small><?php echo $this->_tpl_vars['group']['members']; ?>
 <?php echo TemplateLang(array('p' => 'members'), $this);?>
</small></td>
			<td><?php echo TemplateEMail(array('value' => $this->_tpl_vars['group']['email']), $this);?>
</td>
			<td>
				<a href="workgroups.php?do=edit&id=<?php echo $this->_tpl_vars['group']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/edit.png" border="0" alt="<?php echo TemplateLang(array('p' => 'edit'), $this);?>
" width="16" height="16" /></a>
				<a href="workgroups.php?delete=<?php echo $this->_tpl_vars['group']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onclick="return confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/delete.png" border="0" alt="<?php echo TemplateLang(array('p' => 'delete'), $this);?>
" width="16" height="16" /></a>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>

		<tr>
			<td class="footer" colspan="5">
				<div style="float:left;">
					<?php echo TemplateLang(array('p' => 'action'), $this);?>
: <select name="massAction" class="smallInput">
						<option value="-">------------</option>

						<optgroup label="<?php echo TemplateLang(array('p' => 'actions'), $this);?>
">
							<option value="delete"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option>
						</optgroup>
					</select>&nbsp;
				</div>
				<div style="float:left;">
					<input type="submit" name="executeMassAction" value=" <?php echo TemplateLang(array('p' => 'execute'), $this);?>
 " class="smallInput" />
				</div>
			</td>
		</tr>
	</table>
	</form>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'add'), $this);?>
</legend>

	<form method="post" action="workgroups.php?create=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onsubmit="spin(this)">
		<table width="100%">
			<tr>
				<td width="40" valign="top" rowspan="2"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/workgroup_add32.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="130"><?php echo TemplateLang(array('p' => 'title'), $this);?>
:</td>
				<td class="td2"><input type="text" name="title" value="" style="width:85%;" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'email'), $this);?>
:</td>
				<td class="td2"><input type="text" name="email" value="" style="width:85%;" /></td>
			</tr>
		</table>

		<p align="right">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'add'), $this);?>
 " />
		</p>
	</form>
</fieldset>