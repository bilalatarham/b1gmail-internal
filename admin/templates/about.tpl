<p align="center">
	<img src="{$tpldir}images/about_logo.png" width="242" height="171" border="0" alt="" />
</p>

<p align="center">
	<br />
	<b>b1gMail</b><br />
	<i>
		{lng p="version"} {$version}
		{if $patchlevel}<small><br />{lng p="patchlevel"} {$patchlevel}</small>{/if}
	</i>
</p>

<p align="center">
	Copyright &copy; 2002-2018 <a target="_blank" href="http://www.b1g.de"><img src="./templates/images/b1gsoftware.png" align="absmiddle" style="padding-bottom:2px;" border="0" alt="B1G Software" width="87" height="22" /></a>
</p>

<p align="center">
	<small>
		{lng p="acpiconsfrom"} <a href="http://www.fatcow.com/free-icons" target="_blank" rel="noreferrer">FatCow Web Hosting</a><br />
		{lng p="acpbgfrom"} <a href="http://subtlepatterns.com" target="_blank" rel="noreferrer">subtlepatterns.com</a>
	</small>
</p>
