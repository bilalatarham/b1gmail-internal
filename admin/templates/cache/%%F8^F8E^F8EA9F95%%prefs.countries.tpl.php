<?php /* Smarty version 2.6.28, created on 2020-09-29 15:07:03
         compiled from prefs.countries.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.countries.tpl', 2, false),array('function', 'cycle', 'prefs.countries.tpl', 17, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'countries'), $this);?>
</legend>

	<form action="prefs.countries.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
" name="f1" method="post" onsubmit="spin(this)">
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="25" style="text-align:center;"><a href="javascript:invertSelection(document.forms.f1,'country_');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/dot.png" border="0" alt="" width="10" height="8" /></a></th>
			<th><?php echo TemplateLang(array('p' => 'country'), $this);?>
</th>
			<th width="80" style="text-align:center;"><?php echo TemplateLang(array('p' => 'plzdb'), $this);?>
?</th>
			<th width="80" style="text-align:center;"><?php echo TemplateLang(array('p' => 'eucountry'), $this);?>
?</th>
			<th width="80" style="text-align:center;"><?php echo TemplateLang(array('p' => 'vatrate'), $this);?>
</th>
			<th width="60">&nbsp;</th>
		</tr>
		
		<?php $_from = $this->_tpl_vars['countries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['country']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/country.png" border="0" alt="" width="16" height="16" /></td>
			<td align="center"><input type="checkbox" name="country_<?php echo $this->_tpl_vars['country']['id']; ?>
" /></td>
			<td><?php echo $this->_tpl_vars['country']['land']; ?>
</td>
			<td style="text-align:center;"><?php if ($this->_tpl_vars['country']['plzDB']): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ok.png" border="0" alt="" width="16" height="16" /><?php endif; ?></td>
			<td style="text-align:center;"><?php if ($this->_tpl_vars['country']['is_eu']): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ok.png" border="0" alt="" width="16" height="16" /><?php endif; ?></td>
			<td style="text-align:center;"><?php if ($this->_tpl_vars['country']['vat']): ?><?php echo $this->_tpl_vars['country']['vat']; ?>
 %<?php endif; ?></td>
			<td>
				<a href="prefs.countries.php?do=edit&id=<?php echo $this->_tpl_vars['country']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/edit.png" border="0" alt="<?php echo TemplateLang(array('p' => 'edit'), $this);?>
" width="16" height="16" /></a>
				<a href="prefs.countries.php?delete=<?php echo $this->_tpl_vars['country']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onclick="return confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/delete.png" border="0" alt="<?php echo TemplateLang(array('p' => 'delete'), $this);?>
" width="16" height="16" /></a>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
		
		<tr>
			<td class="footer" colspan="8">
				<div style="float:left;">
					<?php echo TemplateLang(array('p' => 'action'), $this);?>
: <select name="massAction" class="smallInput">
						<option value="-">------------</option>
						
						<optgroup label="<?php echo TemplateLang(array('p' => 'actions'), $this);?>
">
							<option value="delete"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option>
						</optgroup>
					</select>&nbsp;
				</div>
				<div style="float:left;">
					<input type="submit" name="executeMassAction" value=" <?php echo TemplateLang(array('p' => 'execute'), $this);?>
 " class="smallInput" />
				</div>
			</td>
		</tr>
	</table>
	</form>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'addcountry'), $this);?>
</legend>
	
	<form action="prefs.countries.php?add=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
		<table width="100%">
			<tr>
				<td width="40" valign="top" rowspan="1"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/country_add.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="150"><?php echo TemplateLang(array('p' => 'country'), $this);?>
:</td>
				<td class="td2"><input type="text" style="width:85%;" name="land" value="" /></td>
			</tr>
		</table>
	
		<p align="right">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'add'), $this);?>
 " />
		</p>
	</form>
</fieldset>