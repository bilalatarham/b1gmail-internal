<?php /* Smarty version 2.6.28, created on 2020-09-29 11:37:50
         compiled from li/email.sent.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'li/email.sent.tpl', 6, false),array('function', 'text', 'li/email.sent.tpl', 48, false),)), $this); ?>
<br />
<table width="95%">
	<tr>
		<td valign="top" width="64" align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/msg.png" width="48" height="48" border="0" alt="" /></td>
		<td valign="top">
			<b><?php echo TemplateLang(array('p' => 'sendmail'), $this);?>
</b>
			<br /><br /><?php echo TemplateLang(array('p' => 'mailsent'), $this);?>

			<br /><br />
			<input type="button" value="&laquo; <?php echo TemplateLang(array('p' => 'back'), $this);?>
" class="primary" onclick="document.location.href='email.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
';" />
			<br /><br /><br />
		</td>
	</tr>
	
	<?php if ($this->_tpl_vars['addrMails']): ?>
	<tr>
		<td valign="top" width="64" align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/addr48.png" width="48" height="48" border="0" alt="" /></td>
		<td valign="top">
			<b><?php echo TemplateLang(array('p' => 'addressbook'), $this);?>
</b>
			<br /><br />
			<?php echo TemplateLang(array('p' => 'addraddtext'), $this);?>

			<br /><br />
			<form action="organizer.addressbook.php?action=quickAdd&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="return ajaxFormSubmit(this);">
								
				<?php $_from = $this->_tpl_vars['addrMails']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['i'] => $this->_tpl_vars['item']):
?>
				<table class="listTable">
					<tr>
						<td class="listTableHead" colspan="2">
							<input type="checkbox" name="addr[<?php echo $this->_tpl_vars['i']; ?>
][email]" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['item']['email']), $this);?>
" id="addr_<?php echo $this->_tpl_vars['i']; ?>
" checked="checked" />
							<label for="addr_<?php echo $this->_tpl_vars['i']; ?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['item']['email']), $this);?>
</label>
						</td>
					</tr>
					<tr>
						<td class="listTableLeft"><label><?php echo TemplateLang(array('p' => 'firstname'), $this);?>
:</label></td>
						<td class="listTableRight">
							<input type="text" name="addr[<?php echo $this->_tpl_vars['i']; ?>
][firstname]" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['item']['firstname'],'allowEmpty' => true), $this);?>
" size="30" />
						</td>
					</tr>
					<tr>
						<td class="listTableLeft"><label><?php echo TemplateLang(array('p' => 'surname'), $this);?>
:</label></td>
						<td class="listTableRight">
							<input type="text" name="addr[<?php echo $this->_tpl_vars['i']; ?>
][lastname]" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['item']['lastname'],'allowEmpty' => true), $this);?>
" size="30" />
						</td>
					</tr>
					<tr>
						<td class="listTableLeft"><label><?php echo TemplateLang(array('p' => 'company'), $this);?>
:</label></td>
						<td class="listTableRight">
							<input type="text" name="addr[<?php echo $this->_tpl_vars['i']; ?>
][company]" size="30" />
						</td>
					</tr>
					<?php if ($this->_tpl_vars['groups']): ?><tr>
						<td class="listTableLeft"><label><?php echo TemplateLang(array('p' => 'groupmember'), $this);?>
:</label></td>
						<td class="listTableRight">
							<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['groupID'] => $this->_tpl_vars['group']):
?>
								<input type="checkbox" id="group_<?php echo $this->_tpl_vars['i']; ?>
_<?php echo $this->_tpl_vars['groupID']; ?>
" name="addr[<?php echo $this->_tpl_vars['i']; ?>
][groups][]" value="<?php echo $this->_tpl_vars['groupID']; ?>
" />
								<label for="group_<?php echo $this->_tpl_vars['i']; ?>
_<?php echo $this->_tpl_vars['groupID']; ?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['group']['title'],'cut' => 18), $this);?>
</label><br />
							<?php endforeach; endif; unset($_from); ?>
						</td>
					</tr><?php endif; ?>
				</table><br />
				<?php endforeach; endif; unset($_from); ?>
				
				<input type="submit" class="primary" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
			</form>
		</td>
	</tr>
	<?php endif; ?>
</table>