<?php /* Smarty version 2.6.28, created on 2020-09-29 11:27:49
         compiled from updates.prefs.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'updates.prefs.tpl', 3, false),)), $this); ?>
<form action="updates.php?action=prefs&save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">	
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'autoperms'), $this);?>
</legend>
	
		<p>
			<?php echo TemplateLang(array('p' => 'autoperms_desc'), $this);?>

		</p>
		
		<table width="90%">
			<tr>
				<td align="left" rowspan="7" valign="top" width="40"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/updates.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'enable'), $this);?>
?</td>
				<td class="td2"><input name="ftp_active"<?php if ($this->_tpl_vars['bm_prefs']['ftp_active'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'ftphost'), $this);?>
:</td>
				<td class="td2"><input type="text" name="ftp_host" value="<?php echo $this->_tpl_vars['bm_prefs']['ftp_host']; ?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'ftpport'), $this);?>
:</td>
				<td class="td2"><input type="text" name="ftp_port" value="<?php echo $this->_tpl_vars['bm_prefs']['ftp_port']; ?>
" size="4" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'ftpuser'), $this);?>
:</td>
				<td class="td2"><input type="text" name="ftp_user" value="<?php echo $this->_tpl_vars['bm_prefs']['ftp_user']; ?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'ftppass'), $this);?>
:</td>
				<td class="td2"><input type="password" name="ftp_pass" value="<?php echo $this->_tpl_vars['bm_prefs']['ftp_pass']; ?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'ftpdir'), $this);?>
:</td>
				<td class="td2"><input type="text" name="ftp_dir" value="<?php echo $this->_tpl_vars['bm_prefs']['ftp_dir']; ?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'ftpperms'), $this);?>
:</td>
				<td class="td2"><input type="text" name="ftp_permissions" value="<?php echo $this->_tpl_vars['bm_prefs']['ftp_permissions']; ?>
" size="4" /></td>
			</tr>
		</table>
		
		<p>
			<div style="float:right">
				<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
			</div>
		</p>
	</fieldset>
	
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'patchlevel'), $this);?>
</legend>
	
		<p>
			<?php echo TemplateLang(array('p' => 'patchlevel_desc'), $this);?>

		</p>
		
		<table width="90%">
			<tr>
				<td align="left" valign="top" width="40"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/updates_pl.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'patchlevel'), $this);?>
:</td>
				<td class="td2"><?php echo $this->_tpl_vars['bm_prefs']['patchlevel']; ?>

								<input<?php if ($this->_tpl_vars['bm_prefs']['patchlevel'] == 0): ?> disabled="disabled"<?php endif; ?> class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'reset'), $this);?>
 " onclick="document.location.href='updates.php?action=prefs&resetPL=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
';" /></td>
			</tr>
		</table>
	</fieldset>
</form>