<div class="innerWidget">
<table cellspacing="0" width="100%" style="table-layout:fixed;">
	{foreach from=$bmwidget_email_items item=folder key=folderID}
<tr>
	<td width="20" align="center"><img width="16" height="16" src="{$tpldir}images/li/menu_ico_{$folder.icon}.png" border="0" alt="" align="absmiddle" /></td>
	<td style="text-overflow:ellipsis;overflow:hidden;"><a href="email.php?folder={$folderID}&sid={$sid}">{$folder.text}</a></td>
	<td align="left" width="50"><i class="fa fa-envelope-o"></i> {$folder.allMails}</td>
	<td align="left" width="45"><i class="fa fa-flag-o"></i> {if $folder.flaggedMails>0}<b>{$folder.flaggedMails}</b>{else}-{/if}</td>
	<td align="left" width="45"><i class="fa fa-envelope"></i> {if $folder.unreadMails>0}<b>{$folder.unreadMails}</b>{else}-{/if}</td>
</tr>
	{/foreach}
</table>
</div>