<?php /* Smarty version 2.6.28, created on 2020-10-01 11:21:56
         compiled from users.search.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'users.search.tpl', 3, false),)), $this); ?>
<form action="users.php?action=search&do=search&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)" name="f1">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'search'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="3" valign="top" width="40"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/user_search.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="150"><?php echo TemplateLang(array('p' => 'searchfor'), $this);?>
:</td>
				<td class="td2"><input type="text" name="q" value="" size="36" style="width:85%;" /></td>
			</tr>
			<tr>
				<td class="td1" valign="top"><?php echo TemplateLang(array('p' => 'searchin'), $this);?>
:</td>
				<td class="td2">
					<table width="100%">
						<tr>
							<td width="33%"><input type="checkbox" name="searchIn[id]" id="searchIn_id" checked="checked" />
								<label for="searchIn_id"><b><?php echo TemplateLang(array('p' => 'id'), $this);?>
</b></label></td>
							<td width="33%"><input type="checkbox" name="searchIn[email]" id="searchIn_email" checked="checked" />
								<label for="searchIn_email"><b><?php echo TemplateLang(array('p' => 'email'), $this);?>
</b></label></td>
							<td><input type="checkbox" name="searchIn[altmail]" id="searchIn_altmail" checked="checked" />
								<label for="searchIn_altmail"><b><?php echo TemplateLang(array('p' => 'altmail'), $this);?>
</b></label></td>
						</tr>
						<tr>
							<td><input type="checkbox" name="searchIn[name]" id="searchIn_name" checked="checked" />
								<label for="searchIn_name"><b><?php echo TemplateLang(array('p' => 'name'), $this);?>
</b></label></td>
							<td><input type="checkbox" name="searchIn[address]" id="searchIn_address" checked="checked" />
								<label for="searchIn_address"><b><?php echo TemplateLang(array('p' => 'address'), $this);?>
</b></label></td>
							<td><input type="checkbox" name="searchIn[telfaxmobile]" id="searchIn_telfaxmobile" checked="checked" />
								<label for="searchIn_telfaxmobile"><b><?php echo TemplateLang(array('p' => 'tel'), $this);?>
 / <?php echo TemplateLang(array('p' => 'fax'), $this);?>
 / <?php echo TemplateLang(array('p' => 'cellphone'), $this);?>
</b></label></td>
						</tr>
						<tr>
							<td><input type="checkbox" id="searchIn_all" checked="checked" onchange="invertSelection(document.forms.f1,'searchIn',true,this.checked)" />
								<label for="searchIn_all"><b><?php echo TemplateLang(array('p' => 'all'), $this);?>
</b></label></td>
							<td colspan="2">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</fieldset>
	
	<p>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'search'), $this);?>
 " />
		</div>
	</p>
</form>