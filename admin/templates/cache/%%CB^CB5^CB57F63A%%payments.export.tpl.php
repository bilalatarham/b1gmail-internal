<?php /* Smarty version 2.6.28, created on 2020-09-30 13:03:32
         compiled from payments.export.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'payments.export.tpl', 2, false),array('function', 'html_select_date', 'payments.export.tpl', 10, false),array('function', 'text', 'payments.export.tpl', 41, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'accentries'), $this);?>
</legend>
	
	<form action="payments.php?action=export&do=exportAccEntries&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" target="_top">
		<table>
			<tr>
				<td width="40" valign="top" rowspan="4"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_accentries.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="120"><?php echo TemplateLang(array('p' => 'from'), $this);?>
:</td>
				<td class="td2">
					<?php echo smarty_function_html_select_date(array('prefix' => 'start','time' => $this->_tpl_vars['start'],'start_year' => "-5",'field_order' => 'DMY','field_separator' => "."), $this);?>
 
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'to'), $this);?>
:</td>
				<td class="td2">
					<?php echo smarty_function_html_select_date(array('prefix' => 'end','time' => $this->_tpl_vars['end'],'start_year' => "-5",'field_order' => 'DMY','field_separator' => "."), $this);?>

				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'account_debit'), $this);?>
:</td>
				<td class="td2">
					<table>
						<tr>
							<td><?php echo TemplateLang(array('p' => 'banktransfer'), $this);?>
:</td>
							<td><input type="text" name="accounts[0]" size="8" value="1100" /></td>
						</tr>
						<tr>
							<td><?php echo TemplateLang(array('p' => 'su'), $this);?>
:</td>
							<td><input type="text" name="accounts[2]" size="8" value="1100" /></td>
						</tr>
						<tr>
							<td><?php echo TemplateLang(array('p' => 'paypal'), $this);?>
:</td>
							<td><input type="text" name="accounts[1]" size="8" value="1101" /></td>
						</tr>
						<tr>
							<td><?php echo TemplateLang(array('p' => 'skrill'), $this);?>
:</td>
							<td><input type="text" name="accounts[3]" size="8" value="1102" /></td>
						</tr>
						<?php $_from = $this->_tpl_vars['paymentMethods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['methodID'] => $this->_tpl_vars['method']):
?>
						<tr>
							<td><?php echo TemplateText(array('value' => $this->_tpl_vars['method']['title']), $this);?>
</td>
							<td><input type="text" name="accounts[-<?php echo $this->_tpl_vars['methodID']; ?>
]" size="8" value="<?php echo $this->_tpl_vars['methodID']+1102; ?>
" /></td>
						</tr>
						<?php endforeach; endif; unset($_from); ?>
					</table>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'account_credit'), $this);?>
:</td>
				<td class="td2">
					<input type="text" name="account" size="8" value="8400" />
				</td>
			</tr>
		</table>
		
		<p align="right">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'export'), $this);?>
 " />
		</p>
	</form>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'invoices'), $this);?>
</legend>
	
	<form action="payments.php?action=export&do=exportInvoices&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" target="_top">
		<table>
			<tr>
				<td width="40" valign="top" rowspan="3"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_invoices.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="120"><?php echo TemplateLang(array('p' => 'from'), $this);?>
:</td>
				<td class="td2">
					<?php echo smarty_function_html_select_date(array('prefix' => 'start','time' => $this->_tpl_vars['start'],'start_year' => "-5",'field_order' => 'DMY','field_separator' => "."), $this);?>
 
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'to'), $this);?>
:</td>
				<td class="td2">
					<?php echo smarty_function_html_select_date(array('prefix' => 'end','time' => $this->_tpl_vars['end'],'start_year' => "-5",'field_order' => 'DMY','field_separator' => "."), $this);?>

				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'options'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" name="paidOnly" id="paidOnly" checked="checked" />
						<label for="paidOnly"><b><?php echo TemplateLang(array('p' => 'paidonly'), $this);?>
</b></label>
				</td>
			</tr>
		</table>
		
		<p align="right">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'export'), $this);?>
 " />
		</p>
	</form>
</fieldset>