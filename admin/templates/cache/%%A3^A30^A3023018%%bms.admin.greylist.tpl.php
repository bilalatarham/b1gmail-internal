<?php /* Smarty version 2.6.28, created on 2020-09-29 11:35:06
         compiled from /var/www/html/plugins/templates/bms.admin.greylist.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', '/var/www/html/plugins/templates/bms.admin.greylist.tpl', 5, false),array('function', 'cycle', '/var/www/html/plugins/templates/bms.admin.greylist.tpl', 16, false),array('function', 'date', '/var/www/html/plugins/templates/bms.admin.greylist.tpl', 23, false),array('function', 'pageNav', '/var/www/html/plugins/templates/bms.admin.greylist.tpl', 32, false),)), $this); ?>
<form action="<?php echo $this->_tpl_vars['pageURL']; ?>
&action=smtp&do=greylist&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)" name="f1">
	<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['pageNo']; ?>
" />
	
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_greylist'), $this);?>
</legend>
		
		<table class="list">
			<tr>
				<th><?php echo TemplateLang(array('p' => 'bms_ip'), $this);?>
</th>
				<th width="160"><?php echo TemplateLang(array('p' => 'bms_grey_date'), $this);?>
</th>
				<th width="100"><?php echo TemplateLang(array('p' => 'bms_grey_confirmed'), $this);?>
</th>
				<th width="70"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</th>
			</tr>
			
			<?php $_from = $this->_tpl_vars['greylist']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
			<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

			<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
				<?php if ($this->_tpl_vars['item']['ip6']): ?>
				<td><span id="ip_<?php echo $this->_tpl_vars['item']['ip6']; ?>
"><a href="javascript:bms_lookupIP('<?php echo $this->_tpl_vars['item']['ip6']; ?>
');"><?php echo $this->_tpl_vars['item']['ip6']; ?>
</a></span></td>
				<?php else: ?>
				<td><span id="ip_<?php echo $this->_tpl_vars['item']['ip']; ?>
"><a href="javascript:bms_lookupIP('<?php echo $this->_tpl_vars['item']['ip']; ?>
');"><?php echo $this->_tpl_vars['item']['ip']; ?>
</a></span></td>
				<?php endif; ?>
				<td><?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['item']['time']), $this);?>
</td>
				<td><input type="checkbox" disabled="disabled"<?php if ($this->_tpl_vars['item']['confirmed']): ?> checked="checked"<?php endif; ?> /></td>
				<td><input type="checkbox" name="delete[]" value="<?php echo $this->_tpl_vars['key']; ?>
" /></td>
			</tr>
			<?php endforeach; endif; unset($_from); ?>
			
			<tr>
				<td class="footer" colspan="4">
					<div style="float:right;padding-top:3px;padding-bottom:3px;">
						<?php echo TemplateLang(array('p' => 'pages'), $this);?>
: <?php echo TemplatePageNav(array('page' => $this->_tpl_vars['pageNo'],'pages' => $this->_tpl_vars['pageCount'],'on' => " <span class=\"pageNav\"><b>[.t]</b></span> ",'off' => " <span class=\"pageNav\"><a href=\"javascript:updatePage(.s);\">.t</a></span> "), $this);?>
&nbsp;
					</div>
				</td>
			</tr>
		</table>
	</fieldset>
	
	<p>
		<div style="float:left" class="buttons">
			<input class="button" type="button" value=" &laquo; <?php echo TemplateLang(array('p' => 'back'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&action=smtp&sid=<?php echo $this->_tpl_vars['sid']; ?>
';" />
		</div>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>

<script language="javascript">
<?php echo '<!--
	function _bms_lookupIP(e)
	{
		if(e.readyState == 4)
		{
			var text = e.responseText;
			text = text.split(\'/\');

			if(text.length == 2)
			{
				var ip = text[0], hostName = text[1];
				if(EBID(\'ip_\'+ip))
					EBID(\'ip_\'+ip).innerHTML = hostName + \' (\' + ip + \')\';
			}
		}
	}

	function bms_lookupIP(ip)
	{
		MakeXMLRequest(\''; ?>
<?php echo $this->_tpl_vars['pageURL']; ?>
<?php echo '&sid=\' + currentSID
							+ \'&action=lookupIP\'
							+ \'&ip=\' + escape(ip),
						_bms_lookupIP);
	}
//-->'; ?>

</script>