<?php /* Smarty version 2.6.28, created on 2020-09-29 11:36:10
         compiled from prefs.lockedusernames.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.lockedusernames.tpl', 2, false),array('function', 'cycle', 'prefs.lockedusernames.tpl', 14, false),array('function', 'text', 'prefs.lockedusernames.tpl', 18, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'lockedusernames'), $this);?>
</legend>
	
	<form action="prefs.common.php?action=lockedusernames&sid=<?php echo $this->_tpl_vars['sid']; ?>
" name="f1" method="post" onsubmit="spin(this)">
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="25" style="text-align:center;"><a href="javascript:invertSelection(document.forms.f1,'locked_');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/dot.png" border="0" alt="" width="10" height="8" /></a></th>
			<th><?php echo TemplateLang(array('p' => 'username'), $this);?>
</th>
			<th width="55">&nbsp;</th>
		</tr>
		
		<?php $_from = $this->_tpl_vars['lockedUsernames']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['locked']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/lockedusername.png" border="0" alt="" width="16" height="16" /></td>
			<td><input type="checkbox" name="locked_<?php echo $this->_tpl_vars['locked']['id']; ?>
" /></td>
			<td><?php echo $this->_tpl_vars['locked']['type']; ?>
 &quot;<?php echo TemplateText(array('value' => $this->_tpl_vars['locked']['username']), $this);?>
&quot;</td>
			<td>
				<a href="prefs.common.php?action=lockedusernames&delete=<?php echo $this->_tpl_vars['locked']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onclick="return confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/delete.png" border="0" alt="<?php echo TemplateLang(array('p' => 'edit'), $this);?>
" width="16" height="16" /></a>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
		
		<tr>
			<td class="footer" colspan="4">
				<div style="float:left;">
					<?php echo TemplateLang(array('p' => 'action'), $this);?>
: <select name="massAction" class="smallInput">
						<option value="-">------------</option>
						
						<optgroup label="<?php echo TemplateLang(array('p' => 'actions'), $this);?>
">
							<option value="delete"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option>
						</optgroup>
					</select>&nbsp;
				</div>
				<div style="float:left;">
					<input type="submit" name="executeMassAction" value=" <?php echo TemplateLang(array('p' => 'execute'), $this);?>
 " class="smallInput" />
				</div>
			</td>
		</tr>
	</table>
	</form>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'addlockedusername'), $this);?>
</legend>
	
	<form action="prefs.common.php?action=lockedusernames&add=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
		<table width="100%">
			<tr>
				<td width="40" valign="top" rowspan="2"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/lockedusername32.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="150"><?php echo TemplateLang(array('p' => 'type'), $this);?>
:</td>
				<td class="td2"><select name="typ">
					<?php $_from = $this->_tpl_vars['lockedTypeTable']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['text']):
?>
						<option value="<?php echo $this->_tpl_vars['id']; ?>
"><?php echo $this->_tpl_vars['text']; ?>
</option>
					<?php endforeach; endif; unset($_from); ?>
					</select></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'username'), $this);?>
:</td>
				<td class="td2"><input type="text" style="width:85%;" name="benutzername" value="" /></td>
			</tr>
		</table>
	
		<p align="right">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'add'), $this);?>
 " />
		</p>
	</form>
</fieldset>