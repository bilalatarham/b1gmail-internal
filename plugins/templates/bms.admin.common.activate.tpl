<fieldset>
	<legend>{lng p="bms_activation"}</legend>
	
	<form action="https://ssl.b1g.de/service.b1gmail.com/b1gmailserver/activate/" method="post" id="activationForm" onsubmit="spin(this)">
		<input type="hidden" name="ReturnURL"	value="{$returnURL}" />
		<input type="hidden" name="Mode" 		value="acp" />
		<input type="hidden" name="LicenseKey" 	value="{text value=$bms_prefs.license}" />
		<input type="hidden" name="SystemID" 	value="{text value=$bms_prefs.system_id}" />
		<input type="hidden" name="Version" 	value="{text value=$bmsVersion}" />
		<input type="hidden" name="Arch" 		value="{text value=$bmsArch}" />
		<input type="hidden" name="Lang" 		value="{text value=$lang}" />
	
		<table>
			<tr>
				<td width="36" valign="top"><img src="../plugins/templates/images/bms_activation.png" border="0" alt="" width="32" height="32" /></td>
				<td valign="top">
					{lng p="bms_activation_ask"}<br /><br />
			
					<table>
						<tr>
							<td class="td1" width="150">{lng p="licensekey"}:</td>
							<td class="td2">{text value=$bms_prefs.license}</td>
						</tr>
						<tr>
							<td class="td1">{lng p="bms_systemid"}:</td>
							<td class="td2">{text value=$bms_prefs.system_id} <small>{lng p="bms_systemid_note"}</small></td>
						</tr>
						<tr>
							<td class="td1">{lng p="version"}:</td>
							<td class="td2">{text value=$bmsVersion}</td>
						</tr>
						<tr>
							<td class="td1">{lng p="bms_arch"}:</td>
							<td class="td2">{text value=$bmsArch}</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<p align="right" class="buttons">
			<input class="button" type="submit" value=" {lng p="bms_activatenow"} " />
		</p>
	</form>
</fieldset>
