<?php /* Smarty version 2.6.28, created on 2020-09-29 11:35:46
         compiled from prefs.common.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.common.tpl', 3, false),array('function', 'text', 'prefs.common.tpl', 9, false),array('function', 'email', 'prefs.common.tpl', 74, false),)), $this); ?>
<form action="prefs.common.php?save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'common'), $this);?>
</legend>

		<table>
			<tr>
				<td width="40" valign="top" rowspan="14"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_common.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'projecttitle'), $this);?>
:</td>
				<td class="td2"><input type="text" name="titel" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['titel']), $this);?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'hostname'), $this);?>
:</td>
				<td class="td2"><input type="text" name="b1gmta_host" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['b1gmta_host']), $this);?>
" size="28" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'selffolder'), $this);?>
:</td>
				<td class="td2"><input type="text" name="selffolder" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['selffolder']), $this);?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'selfurl'), $this);?>
:</td>
				<td class="td2"><input type="text" name="selfurl" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['selfurl']), $this);?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'mobile_url'), $this);?>
:</td>
				<td class="td2"><input type="text" name="mobile_url" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['mobile_url']), $this);?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'searchengine'), $this);?>
:</td>
				<td class="td2"><input type="text" name="search_engine" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['search_engine']), $this);?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'croninterval'), $this);?>
:</td>
				<td class="td2"><input type="number" min="1" step="1" name="cron_interval" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['cron_interval']), $this);?>
" size="6" /> <?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'auto_tz'), $this);?>
?</td>
				<td class="td2"><input name="auto_tz"<?php if ($this->_tpl_vars['bm_prefs']['auto_tz'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'sessioniplock'), $this);?>
?</td>
				<td class="td2"><input name="ip_lock"<?php if ($this->_tpl_vars['bm_prefs']['ip_lock'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'sessioncookielock'), $this);?>
?</td>
				<td class="td2"><input name="cookie_lock"<?php if ($this->_tpl_vars['bm_prefs']['cookie_lock'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'compresspages'), $this);?>
?</td>
				<td class="td2"><input name="compress_pages"<?php if ($this->_tpl_vars['bm_prefs']['compress_pages'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'maintmode'), $this);?>
?</td>
				<td class="td2"><input name="wartung"<?php if ($this->_tpl_vars['bm_prefs']['wartung'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'nliarea'), $this);?>
</legend>

		<table>
			<tr>
				<td width="40" valign="top" rowspan="5"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/template32.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'domain_combobox'), $this);?>
?</td>
				<td class="td2"><input name="domain_combobox"<?php if ($this->_tpl_vars['bm_prefs']['domain_combobox'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'redirectmobile'), $this);?>
?</td>
				<td class="td2"><input name="redirect_mobile"<?php if ($this->_tpl_vars['bm_prefs']['redirect_mobile'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'contactform'), $this);?>
?</td>
				<td class="td2"><input name="contactform" id="contactform"<?php if ($this->_tpl_vars['bm_prefs']['contactform'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" />
					<label for="contactform"> <?php echo TemplateLang(array('p' => 'to2'), $this);?>
: </label><input type="text" name="contactform_to" value="<?php echo TemplateEMail(array('value' => $this->_tpl_vars['bm_prefs']['contactform_to']), $this);?>
" size="24" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'contactform_name'), $this);?>
?</td>
				<td class="td2"><input name="contactform_name"<?php if ($this->_tpl_vars['bm_prefs']['contactform_name'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'contactform_subject'), $this);?>
?</td>
				<td class="td2"><input name="contactform_subject"<?php if ($this->_tpl_vars['bm_prefs']['contactform_subject'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" />
					&nbsp;
					<small><?php echo TemplateLang(array('p' => 'cfs_note'), $this);?>

						<a href="prefs.languages.php?action=texts&sid=<?php echo $this->_tpl_vars['sid']; ?>
#contact_subjects">&raquo; <?php echo TemplateLang(array('p' => 'customtexts'), $this);?>
</a></small></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'ssl'), $this);?>
</legend>

		<table>
			<tr>
				<td width="40" valign="top" rowspan="4"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_ssl.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'ssl_url'), $this);?>
:</td>
				<td class="td2"><input type="text" name="ssl_url" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['ssl_url']), $this);?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'ssl_login_option'), $this);?>
?</td>
				<td class="td2"><input name="ssl_login_option"<?php if ($this->_tpl_vars['bm_prefs']['ssl_login_option'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'ssl_login_enable'), $this);?>
?</td>
				<td class="td2"><input name="ssl_login_enable"<?php if ($this->_tpl_vars['bm_prefs']['ssl_login_enable'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'ssl_signup_enable'), $this);?>
?</td>
				<td class="td2"><input name="ssl_signup_enable"<?php if ($this->_tpl_vars['bm_prefs']['ssl_signup_enable'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'datastorage'), $this);?>
</legend>

		<table>
			<tr>
				<td width="40" valign="top" rowspan="2"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_storage.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'datafolder'), $this);?>
:</td>
				<td class="td2"><input type="text" name="datafolder" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['datafolder']), $this);?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'structstorage'), $this);?>
:</td>
				<td class="td2"><input type="checkbox" name="structstorage"<?php if ($this->_tpl_vars['bm_prefs']['structstorage'] == 'yes' && ! $this->_tpl_vars['safemode']): ?> checked="checked"<?php endif; ?><?php if ($this->_tpl_vars['safemode']): ?> disabled="disabled"<?php endif; ?> /></td>
			</tr>
		</table>

		<?php if ($this->_tpl_vars['safemode']): ?>
		<p>
			<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/warning.png" border="0" alt="" width="16" height="16" align="absmiddle" />
			<?php echo TemplateLang(array('p' => 'structsafewarn'), $this);?>

		</p>
		<?php endif; ?>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'logs'), $this);?>
</legend>
		<table>
			<tr>
				<td width="40" valign="top" rowspan="5"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/filter.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'log_autodelete'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" id="logs_autodelete" name="logs_autodelete"<?php if ($this->_tpl_vars['bm_prefs']['logs_autodelete'] == 'yes'): ?> checked="checked"<?php endif; ?> />
					<label for="logs_autodelete"><?php echo TemplateLang(array('p' => 'enableolder'), $this);?>
</label>
					<input type="number" name="logs_autodelete_days" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bm_prefs']['logs_autodelete_days']), $this);?>
" size="4" min="1" step="1" />
					<?php echo TemplateLang(array('p' => 'days'), $this);?>
<br />
					<input type="checkbox" id="logs_autodelete_archive" name="logs_autodelete_archive"<?php if ($this->_tpl_vars['bm_prefs']['logs_autodelete_archive'] == 'yes'): ?> checked="checked"<?php endif; ?> />
					<label for="logs_autodelete_archive"><?php echo TemplateLang(array('p' => 'savearc'), $this);?>
</label>
				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'users'), $this);?>
</legend>
		<table>
			<tr>
				<td width="40" valign="top" rowspan="5"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_users.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'logouturl'), $this);?>
:</td>
				<td class="td2"><input type="text" name="logouturl" value="<?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['bm_prefs']['logouturl']), $this);?>
" size="36" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'savehistory'), $this);?>
?</td>
				<td class="td2"><input name="contact_history"<?php if ($this->_tpl_vars['bm_prefs']['contact_history'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'gutregged'), $this);?>
?</td>
				<td class="td2"><input name="gut_regged"<?php if ($this->_tpl_vars['bm_prefs']['gut_regged'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'autocancel'), $this);?>
?</td>
				<td class="td2"><input name="autocancel"<?php if ($this->_tpl_vars['bm_prefs']['autocancel'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'notifications'), $this);?>
</legend>
		<table>
			<tr>
				<td width="40" valign="top" rowspan="2"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_notify.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'notifyinterval'), $this);?>
:</td>
				<td class="td2"><input type="number" name="notify_interval" value="<?php echo $this->_tpl_vars['bm_prefs']['notify_interval']; ?>
" size="6" min="1" step="1" /> <?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'notifylifetime'), $this);?>
:</td>
				<td class="td2"><input type="number" name="notify_lifetime" value="<?php echo $this->_tpl_vars['bm_prefs']['notify_lifetime']; ?>
" size="6" min="1" step="1" /> <?php echo TemplateLang(array('p' => 'days2'), $this);?>
</td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'defaults'), $this);?>
</legend>

		<table>
			<tr>
				<td width="40" valign="top" rowspan="7"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_defaults.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'language'), $this);?>
:</td>
				<td class="td2"><select name="language">
				<?php $_from = $this->_tpl_vars['languages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['langID'] => $this->_tpl_vars['lang']):
?>
					<option value="<?php echo $this->_tpl_vars['langID']; ?>
"<?php if ($this->_tpl_vars['langID'] == $this->_tpl_vars['bm_prefs']['language']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['lang']['title']), $this);?>
</option>
				<?php endforeach; endif; unset($_from); ?>
				</select></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'country'), $this);?>
:</td>
				<td class="td2"><select name="std_land">
				<?php $_from = $this->_tpl_vars['countries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['countryID'] => $this->_tpl_vars['country']):
?>
					<option value="<?php echo $this->_tpl_vars['countryID']; ?>
"<?php if ($this->_tpl_vars['countryID'] == $this->_tpl_vars['bm_prefs']['std_land']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['country']), $this);?>
</option>
				<?php endforeach; endif; unset($_from); ?>
				</select></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'dateformat'), $this);?>
:</td>
				<td class="td2"><input type="text" name="datumsformat" value="<?php echo $this->_tpl_vars['bm_prefs']['datumsformat']; ?>
" size="16" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'itemsperpage'), $this);?>
:</td>
				<td class="td2"><input type="number" min="1" step="1" name="ordner_proseite" value="<?php echo $this->_tpl_vars['bm_prefs']['ordner_proseite']; ?>
" size="6" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'mail_groupmode'), $this);?>
:</td>
				<td class="td2"><select name="mail_groupmode">
					<option value="-"<?php if ($this->_tpl_vars['bm_prefs']['mail_groupmode'] == '-'): ?> selected="selected"<?php endif; ?>>------------</option>

					<optgroup label="<?php echo TemplateLang(array('p' => 'props'), $this);?>
">
						<option value="fetched"<?php if ($this->_tpl_vars['bm_prefs']['mail_groupmode'] == 'fetched'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'date'), $this);?>
</option>
						<option value="von"<?php if ($this->_tpl_vars['bm_prefs']['mail_groupmode'] == 'von'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'from'), $this);?>
</option>
					</optgroup>

					<optgroup label="<?php echo TemplateLang(array('p' => 'flags'), $this);?>
">
						<option value="gelesen"<?php if ($this->_tpl_vars['bm_prefs']['mail_groupmode'] == 'gelesen'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'read'), $this);?>
</option>
						<option value="beantwortet"<?php if ($this->_tpl_vars['bm_prefs']['mail_groupmode'] == 'beantwortet'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'answered'), $this);?>
</option>
						<option value="weitergeleitet"<?php if ($this->_tpl_vars['bm_prefs']['mail_groupmode'] == 'weitergeleitet'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'forwarded'), $this);?>
</option>
						<option value="flagged"<?php if ($this->_tpl_vars['bm_prefs']['mail_groupmode'] == 'flagged'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'flagged'), $this);?>
</option>
						<option value="done"<?php if ($this->_tpl_vars['bm_prefs']['mail_groupmode'] == 'done'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'done'), $this);?>
</option>
						<option value="attach"<?php if ($this->_tpl_vars['bm_prefs']['mail_groupmode'] == 'attach'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'attachment'), $this);?>
</option>
						<option value="color"<?php if ($this->_tpl_vars['bm_prefs']['mail_groupmode'] == 'color'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'color'), $this);?>
</option>
					</optgroup>
				</select></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'calendarviewmode'), $this);?>
:</td>
				<td class="td2"><select name="calendar_defaultviewmode">
					<option value="day"<?php if ($this->_tpl_vars['bm_prefs']['calendar_defaultviewmode'] == 'day'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'day'), $this);?>
</option>
					<option value="week"<?php if ($this->_tpl_vars['bm_prefs']['calendar_defaultviewmode'] == 'week'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'week'), $this);?>
</option>
					<option value="month"<?php if ($this->_tpl_vars['bm_prefs']['calendar_defaultviewmode'] == 'month'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'month'), $this);?>
</option>
				</select></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'hotkeys'), $this);?>
?</td>
				<td class="td2"><input name="hotkeys_default"<?php if ($this->_tpl_vars['bm_prefs']['hotkeys_default'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
		</table>
	</fieldset>

	<p>
		<div style="float:right;" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>