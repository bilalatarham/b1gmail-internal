<div class="sidebarHeading">{lng p="prefs"}</div>
<div class="contentMenuIcons">
	<a href="prefs.php?sid={$sid}"><img src="{$tpldir}images/li/ico_overview.png" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="overview"}</a><br />
	{foreach from=$prefsItems item=null key=item}
	<a href="prefs.php?action={$item}&sid={$sid}"><img src="{if $prefsIcons[$item]}{$prefsIcons[$item]}{else}{$tpldir}images/li/{if $item=='autoresponder'}mail_reply{elseif $item=='contact'}addr_common{else}ico_{$item}{/if}.png{/if}" width="16" height="16" border="0" alt="" align="absmiddle" /> {lng p="$item"}</a><br />
	{/foreach}
</div>
