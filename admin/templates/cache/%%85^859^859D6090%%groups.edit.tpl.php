<?php /* Smarty version 2.6.28, created on 2020-10-01 12:34:26
         compiled from groups.edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'groups.edit.tpl', 6, false),array('function', 'text', 'groups.edit.tpl', 11, false),)), $this); ?>
<form method="post" action="groups.php?<?php if ($this->_tpl_vars['create']): ?>action=create&create=true<?php else: ?>do=edit&id=<?php echo $this->_tpl_vars['group']['id']; ?>
&save=true<?php endif; ?>&sid=<?php echo $this->_tpl_vars['sid']; ?>
" onsubmit="spin(this)">
<table width="100%" cellspacing="2" cellpadding="0">
	<tr>
		<td valign="top" width="50%">
			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'common'), $this);?>
</legend>
				
				<table width="100%">
					<tr>
						<td class="td1" width="160"><?php echo TemplateLang(array('p' => 'title'), $this);?>
:</td>
						<td class="td2"><input type="text" name="titel" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['group']['titel'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'htmlview'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="soforthtml"<?php if ($this->_tpl_vars['group']['soforthtml'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'monthasset'), $this);?>
:</td>
						<td class="td2"><input type="text" name="sms_monat" value="<?php echo $this->_tpl_vars['group']['sms_monat']; ?>
" size="8" /> <?php echo TemplateLang(array('p' => 'credits'), $this);?>
</td>
					</tr>
				</table>
			</fieldset>
			
			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'storage'), $this);?>
</legend>
				
				<table width="100%">
					<tr>
						<td class="td1" width="160"><?php echo TemplateLang(array('p' => 'email'), $this);?>
:</td>
						<td class="td2"><input type="text" name="storage" value="<?php echo $this->_tpl_vars['group']['storage']/1024/1024; ?>
" size="8" /> MB</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'webdisk'), $this);?>
:</td>
						<td class="td2"><input type="text" name="webdisk" value="<?php echo $this->_tpl_vars['group']['webdisk']/1024/1024; ?>
" size="8" /> MB</td>
					</tr>
				</table>
			</fieldset>
			
			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'limits'), $this);?>
</legend>
				
				<table width="100%">
					<tr>
						<td class="td1" width="160"><?php echo TemplateLang(array('p' => 'emailin'), $this);?>
:</td>
						<td class="td2"><input type="text" name="maxsize" value="<?php echo $this->_tpl_vars['group']['maxsize']/1024; ?>
" size="8" /> KB</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'emailout'), $this);?>
:</td>
						<td class="td2"><input type="text" name="anlagen" value="<?php echo $this->_tpl_vars['group']['anlagen']/1024; ?>
" size="8" /> KB</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'wdtraffic'), $this);?>
:</td>
						<td class="td2"><input type="text" name="traffic" value="<?php if ($this->_tpl_vars['group']['traffic'] > 0): ?><?php echo $this->_tpl_vars['group']['traffic']/1024/1024; ?>
<?php else: ?><?php echo $this->_tpl_vars['group']['traffic']; ?>
<?php endif; ?>" size="8" /> MB</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'wdspeed'), $this);?>
:</td>
						<td class="td2"><input type="text" name="wd_member_kbs" value="<?php echo $this->_tpl_vars['group']['wd_member_kbs']; ?>
" size="8" /> KB/s</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'sharespeed'), $this);?>
:</td>
						<td class="td2"><input type="text" name="wd_open_kbs" value="<?php echo $this->_tpl_vars['group']['wd_open_kbs']; ?>
" size="8" /> KB/s</td>
					</tr>
					<tr>
						<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'maxrecps'), $this);?>
:</td>
						<td class="td2"><input type="text" name="max_recps" value="<?php echo $this->_tpl_vars['group']['max_recps']; ?>
" size="8" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'sendlimit'), $this);?>
:</td>
						<td class="td2"><input type="text" name="send_limit_count" value="<?php echo $this->_tpl_vars['group']['send_limit_count']; ?>
" size="8" />
										<?php echo TemplateLang(array('p' => 'emailsin'), $this);?>

										<input type="text" name="send_limit_time" value="<?php echo $this->_tpl_vars['group']['send_limit_time']; ?>
" size="8" />
										<?php echo TemplateLang(array('p' => 'minutes'), $this);?>
</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'ownpop3'), $this);?>
:</td>
						<td class="td2"><input type="text" name="ownpop3" value="<?php echo $this->_tpl_vars['group']['ownpop3']; ?>
" size="8" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'ownpop3interval'), $this);?>
:</td>
						<td class="td2"><input type="text" name="ownpop3_interval" value="<?php echo $this->_tpl_vars['group']['ownpop3_interval']; ?>
" size="8" /> <?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'selfpop3_check'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="selfpop3_check"<?php if ($this->_tpl_vars['group']['selfpop3_check'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'aliases'), $this);?>
:</td>
						<td class="td2"><input type="text" name="aliase" value="<?php echo $this->_tpl_vars['group']['aliase']; ?>
" size="8" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'smspre'), $this);?>
:</td>
						<td class="td2">
							<textarea style="width:100%;height:80px;" name="sms_pre"><?php echo TemplateText(array('value' => $this->_tpl_vars['group']['sms_pre'],'allowEmpty' => true), $this);?>
</textarea>
							<small><?php echo TemplateLang(array('p' => 'sepby'), $this);?>
</small>
						</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'smsvalidation'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="smsvalidation"<?php if ($this->_tpl_vars['group']['smsvalidation'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'allownewsoptout'), $this);?>
?</td>
						<td class="td2"><input name="allow_newsletter_optout"<?php if ($this->_tpl_vars['group']['allow_newsletter_optout'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'mail_send_code'), $this);?>
?</td>
						<td class="td2"><input name="mail_send_code"<?php if ($this->_tpl_vars['group']['mail_send_code'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'sms_send_code'), $this);?>
?</td>
						<td class="td2"><input name="sms_send_code"<?php if ($this->_tpl_vars['group']['sms_send_code'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'abuseprotect'), $this);?>
?</td>
						<td class="td2"><input name="abuseprotect"<?php if ($this->_tpl_vars['group']['abuseprotect'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
					</tr>
				</table>
			</fieldset>
		</td>
		<td valign="top">
			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'services'), $this);?>
</legend>
				
				<table width="100%">
					<tr>
						<td class="td1" width="150"><?php echo TemplateLang(array('p' => 'autoresponder'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="responder"<?php if ($this->_tpl_vars['group']['responder'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
						<td class="td1" width="150"><?php echo TemplateLang(array('p' => 'forward'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="forward"<?php if ($this->_tpl_vars['group']['forward'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'ads'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="ads"<?php if ($this->_tpl_vars['group']['ads'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
						<td class="td2" colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'mail2sms'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="mail2sms"<?php if ($this->_tpl_vars['group']['mail2sms'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
						<td class="td1"><?php echo TemplateLang(array('p' => 'ownfrom'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="sms_ownfrom"<?php if ($this->_tpl_vars['group']['sms_ownfrom'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'mobileaccess'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="wap"<?php if ($this->_tpl_vars['group']['wap'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
						<td class="td1"><?php echo TemplateLang(array('p' => 'sync'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="syncml"<?php if ($this->_tpl_vars['group']['syncml'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'wdshare'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="share"<?php if ($this->_tpl_vars['group']['share'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
						<td class="td1"><?php echo TemplateLang(array('p' => 'webdav'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="webdav"<?php if (! $this->_tpl_vars['davSupport']): ?> disabled="disabled"<?php else: ?><?php if ($this->_tpl_vars['group']['webdav'] == 'yes'): ?> checked="checked"<?php endif; ?><?php endif; ?> /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'organizerdav'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="organizerdav"<?php if (! $this->_tpl_vars['davSupport']): ?> disabled="disabled"<?php else: ?><?php if ($this->_tpl_vars['group']['organizerdav'] == 'yes'): ?> checked="checked"<?php endif; ?><?php endif; ?> /></td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'smtp'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="smtp"<?php if ($this->_tpl_vars['group']['smtp'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
						<td class="td1"><?php echo TemplateLang(array('p' => 'pop3'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="pop3"<?php if ($this->_tpl_vars['group']['pop3'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'imap'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="imap"<?php if ($this->_tpl_vars['group']['imap'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'smime'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="smime"<?php if (! $this->_tpl_vars['smimeSupport']): ?> disabled="disabled"<?php else: ?><?php if ($this->_tpl_vars['group']['smime'] == 'yes'): ?> checked="checked"<?php endif; ?><?php endif; ?> /></td>
						<td class="td1"><?php echo TemplateLang(array('p' => 'issue_certificates'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="issue_certificates"<?php if (! $this->_tpl_vars['smimeSupport']): ?> disabled="disabled"<?php else: ?><?php if ($this->_tpl_vars['group']['issue_certificates'] == 'yes'): ?> checked="checked"<?php endif; ?><?php endif; ?> /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'upload_certificates'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="upload_certificates"<?php if (! $this->_tpl_vars['smimeSupport']): ?> disabled="disabled"<?php else: ?><?php if ($this->_tpl_vars['group']['upload_certificates'] == 'yes'): ?> checked="checked"<?php endif; ?><?php endif; ?> /></td>
						<td class="td1"><?php echo TemplateLang(array('p' => 'sender_aliases'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="sender_aliases"<?php if ($this->_tpl_vars['group']['sender_aliases'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'ftsearch'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="ftsearch"<?php if (! $this->_tpl_vars['ftsSupport']): ?> disabled="disabled"<?php else: ?><?php if ($this->_tpl_vars['group']['ftsearch'] == 'yes'): ?> checked="checked"<?php endif; ?><?php endif; ?> /></td>
						<td class="td1"><?php echo TemplateLang(array('p' => 'notifications'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="notifications"<?php if ($this->_tpl_vars['group']['notifications'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'deliverystatus'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="maildeliverystatus"<?php if ($this->_tpl_vars['group']['maildeliverystatus'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
						<td class="td1"><?php echo TemplateLang(array('p' => 'auto_save_drafts'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="auto_save_drafts"<?php if ($this->_tpl_vars['group']['auto_save_drafts'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
					</tr>
				</table>
			</fieldset>
			
			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'bmtoolbox'), $this);?>
</legend>
				
				<table width="100%">
					<tr>
						<td class="td1" width="150"><?php echo TemplateLang(array('p' => 'tbx_enable'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="checker"<?php if ($this->_tpl_vars['group']['checker'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
						<td class="td2" width="150">&nbsp;</td>
						<td class="td2">&nbsp;</td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'tbx_webdisk'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="tbx_webdisk"<?php if ($this->_tpl_vars['group']['tbx_webdisk'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
						<td class="td1"><?php echo TemplateLang(array('p' => 'tbx_smsmanager'), $this);?>
?</td>
						<td class="td2"><input type="checkbox" name="tbx_smsmanager"<?php if ($this->_tpl_vars['group']['tbx_smsmanager'] == 'yes'): ?> checked="checked"<?php endif; ?> /></td>
					</tr>
				</table>
			</fieldset>
			
			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'aliasdomains'), $this);?>
</legend>
				
				<textarea style="width:100%;height:80px;" name="saliase"><?php echo TemplateText(array('value' => $this->_tpl_vars['group']['saliase'],'allowEmpty' => true), $this);?>
</textarea>
				<small><?php echo TemplateLang(array('p' => 'sepby'), $this);?>
</small>
			</fieldset>
			
			<fieldset>
				<legend><?php echo TemplateLang(array('p' => 'misc'), $this);?>
</legend>
				
				<table width="100%">
					<tr>
						<td class="td1" width="160"><?php echo TemplateLang(array('p' => 'creditprice'), $this);?>
:</td>
						<td class="td2"><input type="text" name="sms_price_per_credit" value="<?php echo $this->_tpl_vars['group']['sms_price_per_credit']; ?>
" size="6" /> (1/100 <?php echo $this->_tpl_vars['currency']; ?>
)</td>
					</tr>
					<tr>
						<td class="td1" width="160"><?php echo TemplateLang(array('p' => 'smsfrom'), $this);?>
:</td>
						<td class="td2"><input type="text" name="sms_from" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['group']['sms_from'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'smssig'), $this);?>
:</td>
						<td class="td2"><input type="text" name="sms_sig" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['group']['sms_sig'],'allowEmpty' => true), $this);?>
" style="width:85%;" /></td>
					</tr>
					<tr>
						<td class="td1"><?php echo TemplateLang(array('p' => 'mailsig'), $this);?>
:</td>
						<td class="td2"><textarea style="width:100%;height:80px;" name="signatur"><?php echo TemplateText(array('value' => $this->_tpl_vars['group']['signatur'],'allowEmpty' => true), $this);?>
</textarea></td>
					</tr>
					
					<?php $_from = $this->_tpl_vars['groupOptions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['fieldKey'] => $this->_tpl_vars['fieldInfo']):
?>
					<tr>
						<td class="td1"><?php echo $this->_tpl_vars['fieldInfo']['desc']; ?>
</td>
						<td class="td2">
							<?php if ($this->_tpl_vars['fieldInfo']['type'] == 16): ?>
								<textarea style="width:100%;height:80px;" name="<?php echo $this->_tpl_vars['fieldKey']; ?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['fieldInfo']['value'],'allowEmpty' => true), $this);?>
</textarea></td>
							<?php elseif ($this->_tpl_vars['fieldInfo']['type'] == 8): ?>
								<?php $_from = $this->_tpl_vars['fieldInfo']['options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['optionKey'] => $this->_tpl_vars['optionValue']):
?>
								<input type="radio" name="<?php echo $this->_tpl_vars['fieldKey']; ?>
" id="<?php echo $this->_tpl_vars['fieldKey']; ?>
_<?php echo $this->_tpl_vars['optionKey']; ?>
" value="<?php echo $this->_tpl_vars['optionKey']; ?>
"<?php if ($this->_tpl_vars['fieldInfo']['value'] == $this->_tpl_vars['optionKey']): ?> checked="checked"<?php endif; ?> />
									<label for="<?php echo $this->_tpl_vars['fieldKey']; ?>
_<?php echo $this->_tpl_vars['optionKey']; ?>
"><?php echo TemplateText(array('value' => $this->_tpl_vars['optionValue']), $this);?>
</label>
								<?php endforeach; endif; unset($_from); ?>
							<?php elseif ($this->_tpl_vars['fieldInfo']['type'] == 4): ?>
								<select name="<?php echo $this->_tpl_vars['fieldKey']; ?>
">
								<?php $_from = $this->_tpl_vars['fieldInfo']['options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['optionKey'] => $this->_tpl_vars['optionValue']):
?>
									<option value="<?php echo $this->_tpl_vars['optionKey']; ?>
"<?php if ($this->_tpl_vars['fieldInfo']['value'] == $this->_tpl_vars['optionKey']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['optionValue']), $this);?>
</option>
								<?php endforeach; endif; unset($_from); ?>									
								</select>
							<?php elseif ($this->_tpl_vars['fieldInfo']['type'] == 2): ?>
								<input type="checkbox" name="<?php echo $this->_tpl_vars['fieldKey']; ?>
" value="1"<?php if ($this->_tpl_vars['fieldInfo']['value']): ?> checked="checked"<?php endif; ?> />
							<?php elseif ($this->_tpl_vars['fieldInfo']['type'] == 1): ?>
								<input type="text" style="width:85%;" name="<?php echo $this->_tpl_vars['fieldKey']; ?>
" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['fieldInfo']['value'],'allowEmpty' => true), $this);?>
" />
							<?php endif; ?>
					</tr>
					<?php endforeach; endif; unset($_from); ?>
				</table>
			</fieldset>
		</td>
	</tr>
</table>
<p>
	<?php if (! $this->_tpl_vars['create']): ?><div style="float:left" class="buttons">
		&nbsp;<?php echo TemplateLang(array('p' => 'action'), $this);?>
:
		<select name="groupAction" id="groupAction">
			<optgroup label="<?php echo TemplateLang(array('p' => 'actions'), $this);?>
">
				<option value="newsletter.php?toGroup=<?php echo $this->_tpl_vars['group']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateLang(array('p' => 'sendmail'), $this);?>
</option>
				<option value="groups.php?singleAction=delete&singleID=<?php echo $this->_tpl_vars['group']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option>
			</optgroup>
		</select>
	</div>
	<div style="float:left">
		<input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'ok'), $this);?>
 " onclick="executeAction('groupAction');" />
	</div><?php endif; ?>
	<div style="float:right" class="buttons">
		<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
	</div>
</p>
</form>
<br /><br />