<?php /* Smarty version 2.6.28, created on 2020-09-30 10:05:03
         compiled from nli/signup.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'nli/signup.tpl', 2, false),array('function', 'hook', 'nli/signup.tpl', 13, false),array('function', 'domain', 'nli/signup.tpl', 75, false),array('function', 'html_select_date', 'nli/signup.tpl', 302, false),array('function', 'fileDateSig', 'nli/signup.tpl', 441, false),)), $this); ?>
<div class="container">
	<div class="page-header"><h1><?php echo TemplateLang(array('p' => 'signup'), $this);?>
</h1></div>

	<p><?php if ($this->_tpl_vars['signupText']): ?><?php echo $this->_tpl_vars['signupText']; ?>
<?php else: ?><?php echo TemplateLang(array('p' => 'signuptxt'), $this);?>
<?php endif; ?> <?php if ($this->_tpl_vars['code']): ?><?php echo TemplateLang(array('p' => 'signuptxt_code'), $this);?>
<?php endif; ?></p>

	<div class="row"><div class="col-md-8"><form action="<?php if ($this->_tpl_vars['ssl_signup_enable']): ?><?php echo $this->_tpl_vars['ssl_url']; ?>
<?php endif; ?>index.php?action=signup" method="post" id="signupForm">
		<input type="hidden" name="do" value="createAccount" />
		<input type="hidden" name="transPostVars" value="true" />
		<input type="hidden" name="codeID" value="<?php echo $this->_tpl_vars['codeID']; ?>
" />
	
		<?php if ($this->_tpl_vars['errorStep']): ?><div class="alert alert-danger" role="alert"><strong><?php echo TemplateLang(array('p' => 'error'), $this);?>
:</strong> <?php echo $this->_tpl_vars['errorInfo']; ?>
</div><?php endif; ?>

		<?php echo TemplateHook(array('id' => "nli:signup.tpl:formStart"), $this);?>


		<div class="panel-group" id="signup">

		<?php echo TemplateHook(array('id' => "nli:signup.tpl:panelGroupStart"), $this);?>


		<div class="panel panel-primary">
			<div class="panel-heading panel-title">
				<span class="glyphicon glyphicon-user"></span>
				<a data-toggle="collapse" data-parent="#signup" href="#signup-account"><?php echo TemplateLang(array('p' => 'wishaddressandpw'), $this);?>
</a>
			</div>
			<div class="panel-collapse collapse in" id="signup-account">
				<div class="panel-body">
					<?php if ($this->_tpl_vars['f_anrede'] != 'n'): ?>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label" for="salutation">
									<?php echo TemplateLang(array('p' => 'salutation'), $this);?>

									<?php if ($this->_tpl_vars['f_anrede'] == 'p'): ?><span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span><?php endif; ?>
								</label>
								<select<?php if ($this->_tpl_vars['f_anrede'] == 'p'): ?> required="required"<?php endif; ?> class="form-control" name="salutation" id="salutation">
									<option value="">&nbsp;</option>
									<option value="herr"<?php if ($this->_tpl_vars['_safePost']['salutation'] == 'herr'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'mr'), $this);?>
</option>
									<option value="frau"<?php if ($this->_tpl_vars['_safePost']['salutation'] == 'frau'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'mrs'), $this);?>
</option>
								</select>
							</div>
						</div>
					</div>
					<?php endif; ?>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="firstname">
									<?php echo TemplateLang(array('p' => 'firstname'), $this);?>

									<span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span>
								</label>
								<input type="text" class="form-control" required="true" name="firstname" id="firstname" value="<?php echo $this->_tpl_vars['_safePost']['firstname']; ?>
" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="surname">
									<?php echo TemplateLang(array('p' => 'surname'), $this);?>

									<span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span>
								</label>
								<input type="text" class="form-control" required="true" name="surname" id="surname" value="<?php echo $this->_tpl_vars['_safePost']['surname']; ?>
" />
							</div>
						</div>
					</div>

					<hr />

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label" for="email_local"><?php echo TemplateLang(array('p' => 'wishaddress'), $this);?>
</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
									<input type="text" name="email_local" id="email_local" class="form-control" required="true" value="<?php echo $this->_tpl_vars['_safePost']['email_local']; ?>
" />
									<div class="input-group-btn">
										<input type="hidden" name="email_domain" id="email_domain" data-bind="email-domain" value="<?php if ($this->_tpl_vars['_safePost']['email_domain']): ?><?php echo $this->_tpl_vars['_safePost']['email_domain']; ?>
<?php else: ?><?php echo TemplateDomain(array('value' => $this->_tpl_vars['domainListSignup'][0]), $this);?>
<?php endif; ?>" />
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span data-bind="label">@<?php if (! $this->_tpl_vars['_safePost']['email_domain']): ?><?php echo TemplateDomain(array('value' => $this->_tpl_vars['domainListSignup'][0]), $this);?>
<?php else: ?><?php echo $this->_tpl_vars['_safePost']['email_domain']; ?>
<?php endif; ?></span> <span class="caret"></span></button>
										<ul class="dropdown-menu dropdown-menu-right domainMenu" role="menu">
											<?php $_from = $this->_tpl_vars['domainListSignup']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['domain']):
?><li<?php if (( ! $this->_tpl_vars['_safePost']['email_domain'] && $this->_tpl_vars['key'] == 0 ) || $this->_tpl_vars['_safePost']['email_domain'] == $this->_tpl_vars['domain']): ?> class="active"<?php endif; ?>><a href="#">@<?php echo TemplateDomain(array('value' => $this->_tpl_vars['domain']), $this);?>
</a></li><?php endforeach; endif; unset($_from); ?>
										</ul>
									</div>
								</div>
							</div>

							<div class="alert alert-info" style="display:none;" role="alert" id="email_alert"></div>
						</div>
						<div class="col-md-6">
							
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="pass1">
									<?php echo TemplateLang(array('p' => 'password'), $this);?>

									<span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
, <?php echo $this->_tpl_vars['minPassText']; ?>
</span>
								</label>
								<input type="password" data-min-length="<?php echo $this->_tpl_vars['minPassLength']; ?>
" class="form-control" required="true" autocomplete="off" name="pass1" id="pass1" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="pass2">
									<?php echo TemplateLang(array('p' => 'repeat'), $this);?>

									<span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span>
								</label>
								<input type="password" class="form-control" required="true" autocomplete="off" name="pass2" id="pass2" />
							</div>
						</div>
					</div>

					<?php if (! $this->_tpl_vars['errorStep']): ?><button class="btn btn-success pull-right" data-role="next-block" type="button">
						<?php echo TemplateLang(array('p' => 'next'), $this);?>
 <span class="glyphicon glyphicon-chevron-right"></span>
					</button><?php endif; ?>
				</div>
			</div>
		</div>

		<?php if ($this->_tpl_vars['f_strasse'] != 'n'): ?>
		<div class="panel panel-default">
			<div class="panel-heading panel-title">
				<span class="glyphicon glyphicon-home"></span>
				<a data-toggle="collapse" data-parent="#signup" href="#signup-address"><?php echo TemplateLang(array('p' => 'address'), $this);?>
</a>
			</div>
			<div class="panel-collapse collapse" id="signup-address">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<label class="control-label" for="street">
									<?php echo TemplateLang(array('p' => 'street'), $this);?>

									<?php if ($this->_tpl_vars['f_strasse'] == 'p'): ?><span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span><?php endif; ?>
								</label>
								<input type="text" class="form-control"<?php if ($this->_tpl_vars['f_strasse'] == 'p'): ?> required="true"<?php endif; ?> name="street" id="street" value="<?php echo $this->_tpl_vars['_safePost']['street']; ?>
" />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label" for="no">
									<?php echo TemplateLang(array('p' => 'nr'), $this);?>

									<?php if ($this->_tpl_vars['f_strasse'] == 'p'): ?><span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span><?php endif; ?>
								</label>
								<input type="text" class="form-control"<?php if ($this->_tpl_vars['f_strasse'] == 'p'): ?> required="true"<?php endif; ?> name="no" id="no" value="<?php echo $this->_tpl_vars['_safePost']['no']; ?>
" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label" for="zip">
									<?php echo TemplateLang(array('p' => 'zip'), $this);?>

									<?php if ($this->_tpl_vars['f_strasse'] == 'p'): ?><span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span><?php endif; ?>
								</label>
								<input type="text" class="form-control"<?php if ($this->_tpl_vars['f_strasse'] == 'p'): ?> required="true"<?php endif; ?> name="zip" id="zip" value="<?php echo $this->_tpl_vars['_safePost']['zip']; ?>
" />
							</div>
						</div>
						<div class="col-md-8">
							<div class="form-group">
								<label class="control-label" for="city">
									<?php echo TemplateLang(array('p' => 'city'), $this);?>

									<?php if ($this->_tpl_vars['f_strasse'] == 'p'): ?><span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span><?php endif; ?>
								</label>
								<input type="text" class="form-control"<?php if ($this->_tpl_vars['f_strasse'] == 'p'): ?> required="true"<?php endif; ?> name="city" id="city" value="<?php echo $this->_tpl_vars['_safePost']['city']; ?>
" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label" for="country">
									<?php echo TemplateLang(array('p' => 'country'), $this);?>

									<?php if ($this->_tpl_vars['f_strasse'] == 'p'): ?><span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span><?php endif; ?>
								</label>
								<select class="form-control" name="country" id="country">
									<?php $_from = $this->_tpl_vars['countryList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['country']):
?>
									<option value="<?php echo $this->_tpl_vars['id']; ?>
"<?php if (( ! $this->_tpl_vars['_safePost']['country'] && $this->_tpl_vars['id'] == $this->_tpl_vars['defaultCountry'] ) || ( $this->_tpl_vars['_safePost']['country'] == $this->_tpl_vars['id'] )): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['country']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>
							</div>
						</div>
					</div>

					<?php if (! $this->_tpl_vars['errorStep'] && ( $this->_tpl_vars['f_safecode'] != 'n' || $this->_tpl_vars['code'] || $this->_tpl_vars['profilfelder'] || $this->_tpl_vars['f_telefon'] != 'n' || $this->_tpl_vars['f_fax'] != 'n' || $this->_tpl_vars['f_mail2sms_nummer'] != 'n' || $this->_tpl_vars['f_alternativ'] != 'n' )): ?><button class="btn btn-success pull-right" data-role="next-block" type="button">
						<?php echo TemplateLang(array('p' => 'next'), $this);?>
 <span class="glyphicon glyphicon-chevron-right"></span>
					</button><?php endif; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<?php if ($this->_tpl_vars['f_telefon'] != 'n' || $this->_tpl_vars['f_fax'] != 'n' || $this->_tpl_vars['f_mail2sms_nummer'] != 'n' || $this->_tpl_vars['f_alternativ'] != 'n'): ?>
		<div class="panel panel-default">
			<div class="panel-heading panel-title">
				<span class="glyphicon glyphicon-earphone"></span>
				<a data-toggle="collapse" data-parent="#signup" href="#signup-contact"><?php echo TemplateLang(array('p' => 'contactinfo'), $this);?>
</a>
			</div>
			<div class="panel-collapse collapse" id="signup-contact">
				<div class="panel-body">
					<?php if ($this->_tpl_vars['f_telefon'] != 'n' || $this->_tpl_vars['f_fax'] != 'n' || $this->_tpl_vars['f_mail2sms_nummer'] != 'n'): ?>
					<div class="row">
						<?php if ($this->_tpl_vars['f_telefon'] != 'n'): ?>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="phone">
									<?php echo TemplateLang(array('p' => 'phone'), $this);?>

									<?php if ($this->_tpl_vars['f_telefon'] == 'p'): ?><span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span><?php endif; ?>
								</label>
								<input type="text" class="form-control"<?php if ($this->_tpl_vars['f_telefon'] == 'p'): ?> required="true"<?php endif; ?> name="phone" id="phone" value="<?php echo $this->_tpl_vars['_safePost']['phone']; ?>
" />
							</div>
						</div>
						<?php endif; ?>
						<?php if ($this->_tpl_vars['f_fax'] != 'n'): ?>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="fax">
									<?php echo TemplateLang(array('p' => 'fax'), $this);?>

									<?php if ($this->_tpl_vars['f_fax'] == 'p'): ?><span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span><?php endif; ?>
								</label>
								<input type="text" class="form-control"<?php if ($this->_tpl_vars['f_fax'] == 'p'): ?> required="true"<?php endif; ?> name="fax" id="fax" value="<?php echo $this->_tpl_vars['_safePost']['fax']; ?>
" />
							</div>
						</div>
						<?php endif; ?>
						<?php if ($this->_tpl_vars['f_mail2sms_nummer'] != 'n'): ?>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="mail2sms_nummer">
									<?php echo TemplateLang(array('p' => 'mobile'), $this);?>

									<?php if ($this->_tpl_vars['f_mail2sms_nummer'] == 'p'): ?><span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span><?php endif; ?>
								</label>
								<input type="text" class="form-control"<?php if ($this->_tpl_vars['f_mail2sms_nummer'] == 'p'): ?> required="true"<?php endif; ?> name="mail2sms_nummer" id="mail2sms_nummer" value="<?php echo $this->_tpl_vars['_safePost']['mail2sms_nummer']; ?>
" />
							</div>
						</div>
						<?php endif; ?>
					</div>
					<?php endif; ?>

					<?php if ($this->_tpl_vars['f_alternativ'] != 'n'): ?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="altmail">
									<?php echo TemplateLang(array('p' => 'altmail2'), $this);?>

									<?php if ($this->_tpl_vars['f_alternativ'] == 'p'): ?><span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span><?php endif; ?>
								</label>
								<input type="email" class="form-control"<?php if ($this->_tpl_vars['f_alternativ'] == 'p'): ?> required="true"<?php endif; ?> name="altmail" id="altmail" value="<?php echo $this->_tpl_vars['_safePost']['altmail']; ?>
" />
							</div>
						</div>
					</div>
					<?php endif; ?>

					<?php if (! $this->_tpl_vars['errorStep'] && ( $this->_tpl_vars['f_safecode'] != 'n' || $this->_tpl_vars['code'] || $this->_tpl_vars['profilfelder'] )): ?><button class="btn btn-success pull-right" data-role="next-block" type="button">
						<?php echo TemplateLang(array('p' => 'next'), $this);?>
 <span class="glyphicon glyphicon-chevron-right"></span>
					</button><?php endif; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<?php if ($this->_tpl_vars['profilfelder']): ?>
		<div class="panel panel-default">
			<div class="panel-heading panel-title">
				<span class="glyphicon glyphicon-list-alt"></span>
				<a data-toggle="collapse" data-parent="#signup" href="#signup-misc"><?php echo TemplateLang(array('p' => 'misc'), $this);?>
</a>
			</div>
			<div class="panel-collapse collapse" id="signup-misc">
				<div class="panel-body">
					<?php $_from = $this->_tpl_vars['profilfelder']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['feld']):
?>
					<?php $this->assign('fieldID', $this->_tpl_vars['feld']['id']); ?>
					<?php $this->assign('fieldName', "field_".($this->_tpl_vars['fieldID'])); ?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<?php if ($this->_tpl_vars['feld']['typ'] != 2): ?><label class="control-label" for="<?php echo $this->_tpl_vars['fieldName']; ?>
">
									<?php echo $this->_tpl_vars['feld']['feld']; ?>

									<?php if ($this->_tpl_vars['feld']['pflicht']): ?><span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span><?php endif; ?>
								</label><?php endif; ?>

								<?php if ($this->_tpl_vars['feld']['typ'] == 1): ?>
								<input<?php if ($this->_tpl_vars['feld']['pflicht']): ?> required="true"<?php endif; ?> class="form-control" name="<?php echo $this->_tpl_vars['fieldName']; ?>
" id="<?php echo $this->_tpl_vars['fieldName']; ?>
" value="<?php echo $this->_tpl_vars['_safePost'][$this->_tpl_vars['fieldName']]; ?>
" type="text" />
								<?php elseif ($this->_tpl_vars['feld']['typ'] == 2): ?>
								<label class="control-label">
									<input type="checkbox" name="<?php echo $this->_tpl_vars['fieldName']; ?>
" id="<?php echo $this->_tpl_vars['fieldName']; ?>
"<?php if ($this->_tpl_vars['_safePost'][$this->_tpl_vars['fieldName']]): ?> checked="checked"<?php endif; ?> />
									<?php echo $this->_tpl_vars['feld']['feld']; ?>

								</label>
								<?php elseif ($this->_tpl_vars['feld']['typ'] == 4): ?>
								<select class="form-control" name="<?php echo $this->_tpl_vars['fieldName']; ?>
" id="<?php echo $this->_tpl_vars['fieldName']; ?>
">
									<?php $_from = $this->_tpl_vars['feld']['extra']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									<option value="<?php echo $this->_tpl_vars['item']; ?>
"<?php if ($this->_tpl_vars['_safePost'][$this->_tpl_vars['fieldName']] == $this->_tpl_vars['item']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['item']; ?>
</option>
									<?php endforeach; endif; unset($_from); ?>
								</select>
								<?php elseif ($this->_tpl_vars['feld']['typ'] == 8): ?>
									<?php $_from = $this->_tpl_vars['feld']['extra']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
									<div class="radio">
										<label>
											<input type="radio" id="<?php echo $this->_tpl_vars['fieldName']; ?>
_<?php echo $this->_tpl_vars['item']; ?>
" name="<?php echo $this->_tpl_vars['fieldName']; ?>
" value="<?php echo $this->_tpl_vars['item']; ?>
"<?php if ($this->_tpl_vars['_safePost'][$this->_tpl_vars['fieldName']] == $this->_tpl_vars['item']): ?> checked="checked"<?php endif; ?> />
											<?php echo $this->_tpl_vars['item']; ?>

										</label> 
									</div>
									<?php endforeach; endif; unset($_from); ?>
								<?php else: ?>
									<div><?php if ($this->_tpl_vars['feld']['pflicht']): ?><?php $this->assign('all_extra', 'required="true"'); ?><?php else: ?><?php $this->assign('all_extra', ""); ?><?php endif; ?><?php if ($this->_tpl_vars['dateFields'][$this->_tpl_vars['fieldName']]): ?>
									<?php echo smarty_function_html_select_date(array('time' => $this->_tpl_vars['dateFields'][$this->_tpl_vars['fieldName']],'year_empty' => "---",'day_empty' => "---",'month_empty' => "---",'start_year' => "-120",'end_year' => "+0",'prefix' => ($this->_tpl_vars['fieldName']),'field_order' => 'DMY','class' => "form-control",'style' => "width:auto;display:inline-block;",'all_extra' => ($this->_tpl_vars['all_extra'])), $this);?>

									<?php else: ?>
									<?php echo smarty_function_html_select_date(array('time' => "---",'year_empty' => "---",'day_empty' => "---",'month_empty' => "---",'start_year' => "-120",'end_year' => "+0",'prefix' => ($this->_tpl_vars['fieldName']),'field_order' => 'DMY','class' => "form-control",'style' => "width:auto;display:inline-block;",'all_extra' => ($this->_tpl_vars['all_extra'])), $this);?>

									<?php endif; ?></div>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<?php endforeach; endif; unset($_from); ?>

					<?php if (! $this->_tpl_vars['errorStep'] && ( $this->_tpl_vars['f_safecode'] != 'n' || $this->_tpl_vars['code'] )): ?><button class="btn btn-success pull-right" data-role="next-block" type="button">
						<?php echo TemplateLang(array('p' => 'next'), $this);?>
 <span class="glyphicon glyphicon-chevron-right"></span>
					</button><?php endif; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<?php if ($this->_tpl_vars['code']): ?>
		<div class="panel panel-default">
			<div class="panel-heading panel-title">
				<span class="glyphicon glyphicon-barcode"></span>
				<a data-toggle="collapse" data-parent="#signup" href="#signup-code"><?php echo TemplateLang(array('p' => 'code'), $this);?>
</a>
			</div>
			<div class="panel-collapse collapse" id="signup-code">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="code">
									<?php echo TemplateLang(array('p' => 'code'), $this);?>

								</label>
								<input type="text" class="form-control" value="<?php echo $this->_tpl_vars['_safePost']['code']; ?>
" name="code" id="code" data-toggle="tooltip" data-placement="bottom" title="<?php echo TemplateLang(array('p' => 'signuptxt_code'), $this);?>
" />
							</div>
						</div>
					</div>

					<?php if (! $this->_tpl_vars['errorStep'] && ( $this->_tpl_vars['f_safecode'] != 'n' )): ?><button class="btn btn-success pull-right" data-role="next-block" type="button">
						<?php echo TemplateLang(array('p' => 'next'), $this);?>
 <span class="glyphicon glyphicon-chevron-right"></span>
					</button><?php endif; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<?php if ($this->_tpl_vars['f_safecode'] != 'n'): ?>
		<div class="panel panel-default">
			<div class="panel-heading panel-title">
				<span class="glyphicon glyphicon-flag"></span>
				<a data-toggle="collapse" data-parent="#signup" href="#signup-finish"><?php echo TemplateLang(array('p' => 'completesignup'), $this);?>
</a>
			</div>
			<div class="panel-collapse collapse" id="signup-finish">
				<div class="panel-body">
					<div class="row">
						<?php if ($this->_tpl_vars['captchaInfo']['hasOwnInput']): ?>
						<div class="col-md-12">
							<div class="form-group" id="captchaContainer">
								<label class="control-label">
									<?php echo TemplateLang(array('p' => 'safecode'), $this);?>

									<span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span>
								</label>
								<?php echo $this->_tpl_vars['captchaHTML']; ?>

							</div>
						</div>
						<?php else: ?>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="safecode">
									<?php echo TemplateLang(array('p' => 'safecode'), $this);?>

									<span class="required"><?php echo TemplateLang(array('p' => 'required'), $this);?>
</span>
								</label>
								<input type="text" class="form-control" required="true" name="safecode" id="safecode" />
							</div>
						</div>
						<div class="col-md-6" id="captchaContainer">
							<?php echo $this->_tpl_vars['captchaHTML']; ?>

						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>

		</div>

		<div class="alert alert-info">
			<span class="glyphicon glyphicon-info-sign"></span>
			<?php echo TemplateLang(array('p' => 'iprecord'), $this);?>

		</div>

		<div class="form-group">
			<label class="control-label">
				<input type="checkbox" name="tos" value="true"<?php if ($this->_tpl_vars['_safePost']['tos'] == 'true'): ?> checked="checked"<?php endif; ?> />
				<?php echo TemplateLang(array('p' => 'accepttos'), $this);?>

				<a href="#" data-toggle="modal" data-target="#tosModal"><?php echo TemplateLang(array('p' => 'tos'), $this);?>
</a>
			</label>
			<button type="submit" id="signupSubmit" class="btn btn-success pull-right" data-loading-text="<?php echo TemplateLang(array('p' => 'pleasewait'), $this);?>
">
				<span class="glyphicon glyphicon-ok"></span> <?php echo TemplateLang(array('p' => 'submit'), $this);?>

			</button>
		</div>
	</form></div></div>
</div>

<div class="modal fade" id="tosModal" tabindex="-1" role="dialog" aria-labelledby="tosLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo TemplateLang(array('p' => 'close'), $this);?>
</span></button>
				<h4 class="modal-title" id="tosLabel"><?php echo TemplateLang(array('p' => 'tos'), $this);?>
</h4>
			</div>
			<div class="modal-body" style="max-height:400px;overflow-y:auto;">
				<?php echo $this->_tpl_vars['tos_html']; ?>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo TemplateLang(array('p' => 'close'), $this);?>
</button>
			</div>
		</div>
	</div>
</div>

<?php if ($this->_tpl_vars['signupSuggestions']): ?><div class="modal fade" id="suggestionsModal" tabindex="-1" role="dialog" aria-labelledby="suggestionsLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo TemplateLang(array('p' => 'close'), $this);?>
</span></button>
				<h4 class="modal-title" id="suggestionsLabel"><?php echo TemplateLang(array('p' => 'suggestions'), $this);?>
</h4>
			</div>
			<div class="modal-body" id="suggestionsBody"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span>
					<?php echo TemplateLang(array('p' => 'nothanks'), $this);?>

				</button>
			</div>
		</div>
	</div>
</div><?php endif; ?>

<script src="<?php echo $this->_tpl_vars['tpldir']; ?>
js/nli.signup.js?<?php echo TemplateFileDateSig(array('file' => "js/nli.signup.js"), $this);?>
"></script>
<script language="javascript">
<!--
	$(document).ready(function() {
	<?php if ($this->_tpl_vars['errorStep']): ?>
	<?php $_from = $this->_tpl_vars['invalidFields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['field']):
?>
	markFieldAsInvalid('<?php echo $this->_tpl_vars['field']; ?>
');
	<?php endforeach; endif; unset($_from); ?>
	<?php if ($this->_tpl_vars['f_safecode'] != 'n'): ?>
	markFieldAsInvalid('safecode');
	<?php endif; ?>
	markFieldAsInvalid('pass1');
	markFieldAsInvalid('pass2');
	checkEMailAvailability();
	<?php else: ?>
	if($('#salutation').length) $('#salutation').focus();
	else if($('#firstname').length) $('#firstname').focus();
	<?php endif; ?>
	});
//-->
</script>