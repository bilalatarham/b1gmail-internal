<?php /* Smarty version 2.6.28, created on 2020-09-30 10:10:46
         compiled from li/notifications.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'text', 'li/notifications.tpl', 6, false),array('function', 'date', 'li/notifications.tpl', 7, false),array('function', 'lng', 'li/notifications.tpl', 13, false),)), $this); ?>
<?php if ($this->_tpl_vars['bmNotifications']): ?>
<ul>
<?php $_from = $this->_tpl_vars['bmNotifications']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['_item']):
?>
	<li><a href="#"<?php if ($this->_tpl_vars['_item']['link']): ?> onmousedown="<?php if ($this->_tpl_vars['_item']['flags'] & 2): ?><?php echo $this->_tpl_vars['_item']['link']; ?>
<?php else: ?>document.location.href='<?php echo $this->_tpl_vars['_item']['link']; ?>
sid=<?php echo $this->_tpl_vars['sid']; ?>
';<?php endif; ?>"<?php endif; ?><?php if (! $this->_tpl_vars['_item']['read']): ?> class="unread"<?php endif; ?><?php if ($this->_tpl_vars['_item']['old']): ?> style="opacity:0.5;"<?php endif; ?>>
		<?php if ($this->_tpl_vars['_item']['icon']): ?><table><tr><td style="width:40px;"><img src="<?php echo $this->_tpl_vars['_item']['icon']; ?>
" /></td><td><?php endif; ?>
		<?php echo TemplateText(array('noentities' => true,'value' => $this->_tpl_vars['_item']['text'],'cut' => 150), $this);?>

		<div class="date"><?php echo TemplateDate(array('nice' => true,'timestamp' => $this->_tpl_vars['_item']['date']), $this);?>
</div>
		<?php if ($this->_tpl_vars['_item']['icon']): ?></td></tr></table><?php endif; ?>
	</a></li>
<?php endforeach; endif; unset($_from); ?>
</ul>
<?php else: ?>
	<center style="margin-top:1em;color:#999;"><em>(<?php echo TemplateLang(array('p' => 'nonotifications'), $this);?>
)</em></center>
<?php endif; ?>