<?php /* Smarty version 2.6.28, created on 2020-09-29 11:41:09
         compiled from /var/www/html/plugins/templates/bms.admin.imap.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', '/var/www/html/plugins/templates/bms.admin.imap.tpl', 3, false),array('function', 'text', '/var/www/html/plugins/templates/bms.admin.imap.tpl', 9, false),array('function', 'date', '/var/www/html/plugins/templates/bms.admin.imap.tpl', 124, false),)), $this); ?>
<form action="<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=imap&save=true" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'common'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="6" valign="top" width="40"><img src="../plugins/templates/images/bms_common.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_greeting'), $this);?>
:</td>
				<td class="td2"><input type="text" name="imapgreeting" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['imapgreeting'],'allowEmpty' => true), $this);?>
" size="32" style="width:95%;" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_timeout'), $this);?>
:</td>
				<td class="td2"><input type="text" name="imap_timeout" value="<?php echo $this->_tpl_vars['bms_prefs']['imap_timeout']; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_idle_poll'), $this);?>
:</td>
				<td class="td2"><input type="text" name="imap_idle_poll" value="<?php echo $this->_tpl_vars['bms_prefs']['imap_idle_poll']; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_mysqlconnection'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" name="imap_mysqlclose"<?php if ($this->_tpl_vars['bms_prefs']['imap_mysqlclose'] == 1): ?> checked="checked"<?php endif; ?> id="imap_mysqlclose" />
					<label for="imap_mysqlclose"><b><?php echo TemplateLang(array('p' => 'bms_closewhenidle'), $this);?>
</b></label><br />
					
					<input type="checkbox" name="imap_idle_mysqlclose"<?php if ($this->_tpl_vars['bms_prefs']['imap_idle_mysqlclose'] == 1): ?> checked="checked"<?php endif; ?> id="imap_idle_mysqlclose" />
					<label for="imap_idle_mysqlclose"><b><?php echo TemplateLang(array('p' => 'bms_closeduringidle'), $this);?>
</b></label>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_intfolders'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" name="imap_intelligentfolders"<?php if ($this->_tpl_vars['bms_prefs']['imap_intelligentfolders'] == 1): ?> checked="checked"<?php endif; ?> id="imap_intelligentfolders" />
					<label for="imap_intelligentfolders"><b><?php echo TemplateLang(array('p' => 'show'), $this);?>
</b></label>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_autoexpunge'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" name="imap_autoexpunge"<?php if ($this->_tpl_vars['bms_prefs']['imap_autoexpunge'] == 1): ?> checked="checked"<?php endif; ?> id="imap_autoexpunge" />
					<label for="imap_autoexpunge"><b><?php echo TemplateLang(array('p' => 'enable'), $this);?>
</b></label>
				</td>
			</tr>
		</table>
	</fieldset>
	
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_foldernames'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="5" valign="top" width="40"><img src="../plugins/templates/images/bms_folders.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_folder_inbox'), $this);?>
:</td>
				<td class="td2"><input type="text" value="INBOX" disabled="disabled" size="32" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_folder_sent'), $this);?>
:</td>
				<td class="td2"><input type="text" name="imap_folder_sent" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['imap_folder_sent'],'allowEmpty' => true), $this);?>
" size="32" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_folder_spam'), $this);?>
:</td>
				<td class="td2"><input type="text" name="imap_folder_spam" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['imap_folder_spam'],'allowEmpty' => true), $this);?>
" size="32" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_folder_drafts'), $this);?>
:</td>
				<td class="td2"><input type="text" name="imap_folder_drafts" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['imap_folder_drafts'],'allowEmpty' => true), $this);?>
" size="32" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_folder_trash'), $this);?>
:</td>
				<td class="td2"><input type="text" name="imap_folder_trash" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['imap_folder_trash'],'allowEmpty' => true), $this);?>
" size="32" /></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_imaplimit'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="2" valign="top" width="40"><img src="../plugins/templates/images/bms_imaplimit.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_user_choseimaplimit'), $this);?>
?</td>
				<td class="td2"><input type="checkbox" name="user_choseimaplimit"<?php if ($this->_tpl_vars['bms_prefs']['user_choseimaplimit']): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_imaplimit'), $this);?>
:</td>
				<td class="td2"><input type="text" name="imap_limit" value="<?php echo $this->_tpl_vars['bms_prefs']['imap_limit']; ?>
" size="6" />
								<?php echo TemplateLang(array('p' => 'emails'), $this);?>

								<small>(<?php echo TemplateLang(array('p' => 'bms_zerolimit'), $this);?>
)</small></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_apns'), $this);?>
</legend>
	
		<table width="100%">
			<tr>
				<td align="left" rowspan="3" valign="top" width="40"><img src="../plugins/templates/images/bms_apns.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_apns'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox" name="apns_enable"<?php if ($this->_tpl_vars['bms_prefs']['apns_enable'] == 1): ?> checked="checked"<?php endif; ?> id="apns_enable"<?php if (! $this->_tpl_vars['apnsSet']): ?> disabled="disabled"<?php endif; ?> />
					<label for="apns_enable"><b><?php echo TemplateLang(array('p' => 'enable'), $this);?>
</b></label>
					<?php if (! $this->_tpl_vars['apnsSet']): ?>
						<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/warning.png" border="0" alt="" width="16" height="16" align="absmiddle" />
						<?php echo TemplateLang(array('p' => 'bms_apnsnote'), $this);?>

					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td class="td1" width="200"><?php echo TemplateLang(array('p' => 'bms_serverport'), $this);?>
:</td>
				<td class="td2">
					<input type="text" name="apns_host" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['apns_host'],'allowEmpty' => true), $this);?>
" size="32" />
					:
					<input type="text" name="apns_port" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['bms_prefs']['apns_port'],'allowEmpty' => true), $this);?>
" size="6" />
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_pushcertificate'), $this);?>
:</td>
				<td class="td2">
					<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php if (! $this->_tpl_vars['apnsSet'] || ! $this->_tpl_vars['apnsValid']): ?>delete<?php else: ?>yes<?php endif; ?>.png" border="0" alt="" width="16" height="16" align="absmiddle" />
					<?php if ($this->_tpl_vars['apnsSet']): ?>
						<?php echo TemplateLang(array('p' => 'bms_setvaliduntil'), $this);?>

						<?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['apnsValidUntil'],'dayonly' => true), $this);?>

					<?php else: ?>
						<?php echo TemplateLang(array('p' => 'bms_notset'), $this);?>

					<?php endif; ?>
					<input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'setedit'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=imap&do=apns';" />
				</td>
			</tr>
		</table>
	</fieldset>

	<p>
		<div style="float:left" class="buttons">
			<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/warning.png" border="0" alt="" width="16" height="16" align="absmiddle" />
			<?php echo TemplateLang(array('p' => 'bms_apnsqueuerestartnote'), $this);?>

		</div>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>