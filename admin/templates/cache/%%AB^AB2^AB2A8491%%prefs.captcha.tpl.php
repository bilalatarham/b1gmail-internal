<?php /* Smarty version 2.6.28, created on 2020-09-29 11:36:00
         compiled from prefs.captcha.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.captcha.tpl', 3, false),array('function', 'text', 'prefs.captcha.tpl', 11, false),)), $this); ?>
<form action="prefs.common.php?action=captcha&save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'safecode'), $this);?>
</legend>
	
		<table>
			<tr>
				<td width="40" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/captcha32.png" border="0" alt="" width="32" height="32" /></td>				
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'captchaprovider'), $this);?>
:</td>
				<td class="td2"><select name="captcha_provider" onchange="changeCaptchaProvider(this)">
					<?php $_from = $this->_tpl_vars['providers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['prov']):
?>
					<option value="<?php echo $this->_tpl_vars['key']; ?>
"<?php if ($this->_tpl_vars['defaultProvider'] == $this->_tpl_vars['key']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['prov']['title']), $this);?>
</option>
					<?php endforeach; endif; unset($_from); ?>
				</select></td>
			</tr>
		</table>
	</fieldset>

	<?php $_from = $this->_tpl_vars['providers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['prov']):
?><?php if ($this->_tpl_vars['prov']['configFields']): ?>
	<fieldset id="cp_<?php echo $this->_tpl_vars['key']; ?>
" style="display:<?php if ($this->_tpl_vars['key'] != $this->_tpl_vars['defaultProvider']): ?>none<?php endif; ?>;">
		<legend><?php echo TemplateLang(array('p' => 'prefs'), $this);?>
: <?php echo TemplateText(array('value' => $this->_tpl_vars['prov']['title']), $this);?>
</legend>

		<table width="100%">
		<?php $_from = $this->_tpl_vars['prov']['configFields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['fieldKey'] => $this->_tpl_vars['fieldInfo']):
?>
			<tr>
				<td width="220" class="td1"><?php echo $this->_tpl_vars['fieldInfo']['title']; ?>
</td>
				<td class="td2">
					<?php if ($this->_tpl_vars['fieldInfo']['type'] == 16): ?>
						<textarea style="width:100%;height:80px;" name="prefs[<?php echo $this->_tpl_vars['key']; ?>
][<?php echo $this->_tpl_vars['fieldKey']; ?>
]"><?php echo TemplateText(array('value' => $this->_tpl_vars['fieldInfo']['value'],'allowEmpty' => true), $this);?>
</textarea></td>
					<?php elseif ($this->_tpl_vars['fieldInfo']['type'] == 8): ?>
						<?php $_from = $this->_tpl_vars['fieldInfo']['options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['optionKey'] => $this->_tpl_vars['optionValue']):
?>
						<label>
							<input type="radio" name="prefs[<?php echo $this->_tpl_vars['key']; ?>
][<?php echo $this->_tpl_vars['fieldKey']; ?>
]" value="<?php echo $this->_tpl_vars['optionKey']; ?>
"<?php if ($this->_tpl_vars['fieldInfo']['value'] == $this->_tpl_vars['optionKey']): ?> checked="checked"<?php endif; ?> />
							<?php echo TemplateText(array('value' => $this->_tpl_vars['optionValue']), $this);?>

						</label>
						<?php endforeach; endif; unset($_from); ?>
					<?php elseif ($this->_tpl_vars['fieldInfo']['type'] == 4): ?>
						<select name="prefs[<?php echo $this->_tpl_vars['key']; ?>
][<?php echo $this->_tpl_vars['fieldKey']; ?>
]">
						<?php $_from = $this->_tpl_vars['fieldInfo']['options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['optionKey'] => $this->_tpl_vars['optionValue']):
?>
							<option value="<?php echo $this->_tpl_vars['optionKey']; ?>
"<?php if ($this->_tpl_vars['fieldInfo']['value'] == $this->_tpl_vars['optionKey']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['optionValue']), $this);?>
</option>
						<?php endforeach; endif; unset($_from); ?>									
						</select>
					<?php elseif ($this->_tpl_vars['fieldInfo']['type'] == 2): ?>
						<input type="checkbox" name="prefs[<?php echo $this->_tpl_vars['key']; ?>
][<?php echo $this->_tpl_vars['fieldKey']; ?>
]" value="1"<?php if ($this->_tpl_vars['fieldInfo']['value']): ?> checked="checked"<?php endif; ?> />
					<?php elseif ($this->_tpl_vars['fieldInfo']['type'] == 1): ?>
						<input type="text" style="width:85%;" name="prefs[<?php echo $this->_tpl_vars['key']; ?>
][<?php echo $this->_tpl_vars['fieldKey']; ?>
]" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['fieldInfo']['value'],'allowEmpty' => true), $this);?>
" />
					<?php endif; ?>
				</td>
			</tr>
		<?php endforeach; endif; unset($_from); ?>
		</table>
	</fieldset>
	<?php endif; ?><?php endforeach; endif; unset($_from); ?>
	
	<p>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>