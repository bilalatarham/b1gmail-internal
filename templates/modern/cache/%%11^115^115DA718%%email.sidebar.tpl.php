<?php /* Smarty version 2.6.28, created on 2020-09-29 11:27:05
         compiled from li/email.sidebar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'hook', 'li/email.sidebar.tpl', 1, false),array('function', 'lng', 'li/email.sidebar.tpl', 3, false),)), $this); ?>
<?php echo TemplateHook(array('id' => "email.sidebar.tpl:head"), $this);?>


<div class="sidebarHeading"><?php echo TemplateLang(array('p' => 'email'), $this);?>
</div>
<div class="contentMenuIcons">
	<a href="email.compose.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/send_mail.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'sendmail'), $this);?>
</a><br />
	<a href="email.folders.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_folder.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'folderadmin'), $this);?>
</a><br />
	<?php echo TemplateHook(array('id' => "email.sidebar.tpl:email"), $this);?>

</div>

<div class="sidebarHeading"><?php echo TemplateLang(array('p' => 'folders'), $this);?>
</div>
<div class="contentMenuIcons" id="folderList">
</div>
<script language="javascript">
<!--
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "li/email.folderlist.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	EBID('folderList').innerHTML = d;
	enableFolderDragTargets();
//-->
</script>

<?php echo TemplateHook(array('id' => "email.sidebar.tpl:foot"), $this);?>
