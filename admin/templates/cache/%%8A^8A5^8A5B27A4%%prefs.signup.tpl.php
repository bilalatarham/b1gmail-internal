<?php /* Smarty version 2.6.28, created on 2020-09-29 11:36:08
         compiled from prefs.signup.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'prefs.signup.tpl', 3, false),array('function', 'text', 'prefs.signup.tpl', 22, false),array('function', 'email', 'prefs.signup.tpl', 45, false),)), $this); ?>
<form action="prefs.common.php?action=signup&save=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'common'), $this);?>
</legend>

		<table>
			<tr>
				<td width="40" valign="top" rowspan="10"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_signup.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'enablereg'), $this);?>
?</td>
				<td class="td2"><input name="regenabled"<?php if ($this->_tpl_vars['bm_prefs']['regenabled'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'stateafterreg'), $this);?>
:</td>
				<td class="td2"><select name="usr_status">
					<option value="no"<?php if ($this->_tpl_vars['bm_prefs']['usr_status'] == 'no'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'active'), $this);?>
</option>
					<option value="locked"<?php if ($this->_tpl_vars['bm_prefs']['usr_status'] == 'locked'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'notactivated'), $this);?>
</option>
				</select></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'stdgroup'), $this);?>
:</td>
				<td class="td2"><select name="std_gruppe">
				<?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['group']):
?>
					<option value="<?php echo $this->_tpl_vars['group']['id']; ?>
"<?php if ($this->_tpl_vars['bm_prefs']['std_gruppe'] == $this->_tpl_vars['group']['id']): ?> selected="selected"<?php endif; ?>><?php echo TemplateText(array('value' => $this->_tpl_vars['group']['title']), $this);?>
</option>
				<?php endforeach; endif; unset($_from); ?>
				</select></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'usercountlimit'), $this);?>
:</td>
				<td class="td2"><input type="checkbox" name="user_count_limit_enable" id="user_count_limit_enable" onclick="if(!this.checked)EBID('user_count_limit').value='0';"<?php if ($this->_tpl_vars['bm_prefs']['user_count_limit'] > 0): ?> checked="checked"<?php endif; ?> />
								<input type="text" name="user_count_limit" id="user_count_limit" value="<?php echo $this->_tpl_vars['bm_prefs']['user_count_limit']; ?>
" size="6" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'minaddrlength'), $this);?>
:</td>
				<td class="td2"><input type="text" name="minuserlength" value="<?php echo $this->_tpl_vars['bm_prefs']['minuserlength']; ?>
" size="6" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'minpasslength'), $this);?>
:</td>
				<td class="td2"><input type="text" name="min_pass_length" value="<?php echo $this->_tpl_vars['bm_prefs']['min_pass_length']; ?>
" size="6" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'signupsuggestions'), $this);?>
?</td>
				<td class="td2"><input id="signup_suggestions" name="signup_suggestions"<?php if ($this->_tpl_vars['bm_prefs']['signup_suggestions'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'regnotify'), $this);?>
?</td>
				<td class="td2"><input id="notify_mail" name="notify_mail"<?php if ($this->_tpl_vars['bm_prefs']['notify_mail'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /><label for="notify_mail"> <?php echo TemplateLang(array('p' => 'to2'), $this);?>
: </label><input type="text" name="notify_to" value="<?php echo TemplateEMail(array('value' => $this->_tpl_vars['bm_prefs']['notify_to']), $this);?>
" size="24" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'sendwelcomemail'), $this);?>
?</td>
				<td class="td2"><input id="welcome_mail" name="welcome_mail"<?php if ($this->_tpl_vars['bm_prefs']['welcome_mail'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'nosignupautodel'), $this);?>
?</td>
				<td class="td2"><input id="nosignup_autodel" name="nosignup_autodel"<?php if ($this->_tpl_vars['bm_prefs']['nosignup_autodel'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" />
									<?php echo TemplateLang(array('p' => 'after'), $this);?>

									<input type="text" name="nosignup_autodel_days" value="<?php echo $this->_tpl_vars['bm_prefs']['nosignup_autodel_days']; ?>
" size="6" />
									<?php echo TemplateLang(array('p' => 'days2'), $this);?>
</td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'reg_validation'), $this);?>
</legend>

		<table>
			<tr>
				<td width="40" valign="top" rowspan="3"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/user_active32.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'reg_validation'), $this);?>
?</td>
				<td class="td2"><select name="reg_validation">
					<option value="off"<?php if ($this->_tpl_vars['bm_prefs']['reg_validation'] == 'off'): ?> selected="selected" <?php endif; ?>><?php echo TemplateLang(array('p' => 'no'), $this);?>
</option>
					<option value="email"<?php if ($this->_tpl_vars['bm_prefs']['reg_validation'] == 'email'): ?> selected="selected" <?php endif; ?>><?php echo TemplateLang(array('p' => 'byemail'), $this);?>
</option>
					<option value="sms"<?php if ($this->_tpl_vars['bm_prefs']['reg_validation'] == 'sms'): ?> selected="selected" <?php endif; ?>><?php echo TemplateLang(array('p' => 'bysms'), $this);?>
</option>
				</select></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'max_resend_times'), $this);?>
:</td>
				<td class="td2">
					<input type="text" name="reg_validation_max_resend_times" value="<?php echo $this->_tpl_vars['bm_prefs']['reg_validation_max_resend_times']; ?>
" size="6" />
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'min_resend_interval'), $this);?>
:</td>
				<td class="td2">
					<input type="text" name="reg_validation_min_resend_interval" value="<?php echo $this->_tpl_vars['bm_prefs']['reg_validation_min_resend_interval']; ?>
" size="6" />
					<?php echo TemplateLang(array('p' => 'seconds'), $this);?>

				</td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'fields'), $this);?>
</legend>

		<table class="list">
			<tr>
				<th width="20">&nbsp;</th>
				<th><?php echo TemplateLang(array('p' => 'field'), $this);?>
</th>
				<th width="110"><?php echo TemplateLang(array('p' => 'oblig'), $this);?>
</th>
				<th width="110"><?php echo TemplateLang(array('p' => 'available'), $this);?>
</th>
				<th width="110"><?php echo TemplateLang(array('p' => 'notavailable'), $this);?>
</th>
			</tr>

			<tr class="td1">
				<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/field.png" border="0" alt="" width="16" height="16" /></td>
				<td><?php echo TemplateLang(array('p' => 'salutation'), $this);?>
</td>
				<td style="text-align:center;"><input type="radio" name="f_anrede" value="p"<?php if ($this->_tpl_vars['bm_prefs']['f_anrede'] == 'p'): ?> checked="checked"<?php endif; ?> /></td>
				<td style="text-align:center;"><input type="radio" name="f_anrede" value="v"<?php if ($this->_tpl_vars['bm_prefs']['f_anrede'] == 'v'): ?> checked="checked"<?php endif; ?> /></td>
				<td style="text-align:center;"><input type="radio" name="f_anrede" value="n"<?php if ($this->_tpl_vars['bm_prefs']['f_anrede'] == 'n'): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr class="td2">
				<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/field.png" border="0" alt="" width="16" height="16" /></td>
				<td><?php echo TemplateLang(array('p' => 'address'), $this);?>
</td>
				<td style="text-align:center;"><input type="radio" name="f_strasse" value="p"<?php if ($this->_tpl_vars['bm_prefs']['f_strasse'] == 'p'): ?> checked="checked"<?php endif; ?> /></td>
				<td style="text-align:center;"><input type="radio" name="f_strasse" value="v"<?php if ($this->_tpl_vars['bm_prefs']['f_strasse'] == 'v'): ?> checked="checked"<?php endif; ?> /></td>
				<td style="text-align:center;"><input type="radio" name="f_strasse" value="n"<?php if ($this->_tpl_vars['bm_prefs']['f_strasse'] == 'n'): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr class="td1">
				<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/field.png" border="0" alt="" width="16" height="16" /></td>
				<td><?php echo TemplateLang(array('p' => 'tel'), $this);?>
</td>
				<td style="text-align:center;"><input type="radio" name="f_telefon" value="p"<?php if ($this->_tpl_vars['bm_prefs']['f_telefon'] == 'p'): ?> checked="checked"<?php endif; ?> /></td>
				<td style="text-align:center;"><input type="radio" name="f_telefon" value="v"<?php if ($this->_tpl_vars['bm_prefs']['f_telefon'] == 'v'): ?> checked="checked"<?php endif; ?> /></td>
				<td style="text-align:center;"><input type="radio" name="f_telefon" value="n"<?php if ($this->_tpl_vars['bm_prefs']['f_telefon'] == 'n'): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr class="td2">
				<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/field.png" border="0" alt="" width="16" height="16" /></td>
				<td><?php echo TemplateLang(array('p' => 'fax'), $this);?>
</td>
				<td style="text-align:center;"><input type="radio" name="f_fax" value="p"<?php if ($this->_tpl_vars['bm_prefs']['f_fax'] == 'p'): ?> checked="checked"<?php endif; ?> /></td>
				<td style="text-align:center;"><input type="radio" name="f_fax" value="v"<?php if ($this->_tpl_vars['bm_prefs']['f_fax'] == 'v'): ?> checked="checked"<?php endif; ?> /></td>
				<td style="text-align:center;"><input type="radio" name="f_fax" value="n"<?php if ($this->_tpl_vars['bm_prefs']['f_fax'] == 'n'): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr class="td1">
				<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/field.png" border="0" alt="" width="16" height="16" /></td>
				<td><?php echo TemplateLang(array('p' => 'altmail'), $this);?>
</td>
				<td style="text-align:center;"><input type="radio" name="f_alternativ" value="p"<?php if ($this->_tpl_vars['bm_prefs']['f_alternativ'] == 'p'): ?> checked="checked"<?php endif; ?> /></td>
				<td style="text-align:center;"><input type="radio" name="f_alternativ" value="v"<?php if ($this->_tpl_vars['bm_prefs']['f_alternativ'] == 'v'): ?> checked="checked"<?php endif; ?> /></td>
				<td style="text-align:center;"><input type="radio" name="f_alternativ" value="n"<?php if ($this->_tpl_vars['bm_prefs']['f_alternativ'] == 'n'): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr class="td2">
				<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/field.png" border="0" alt="" width="16" height="16" /></td>
				<td><?php echo TemplateLang(array('p' => 'cellphone'), $this);?>
</td>
				<td style="text-align:center;"><input type="radio" name="f_mail2sms_nummer" value="p"<?php if ($this->_tpl_vars['bm_prefs']['f_mail2sms_nummer'] == 'p'): ?> checked="checked"<?php endif; ?> /></td>
				<td style="text-align:center;"><input type="radio" name="f_mail2sms_nummer" value="v"<?php if ($this->_tpl_vars['bm_prefs']['f_mail2sms_nummer'] == 'v'): ?> checked="checked"<?php endif; ?> /></td>
				<td style="text-align:center;"><input type="radio" name="f_mail2sms_nummer" value="n"<?php if ($this->_tpl_vars['bm_prefs']['f_mail2sms_nummer'] == 'n'): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
			<tr class="td1">
				<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/field.png" border="0" alt="" width="16" height="16" /></td>
				<td><?php echo TemplateLang(array('p' => 'safecode'), $this);?>
</td>
				<td style="text-align:center;"><input type="radio" name="f_safecode" value="p"<?php if ($this->_tpl_vars['bm_prefs']['f_safecode'] == 'p'): ?> checked="checked"<?php endif; ?> /></td>
				<td style="text-align:center;"><input type="radio" disabled="disabled" /></td>
				<td style="text-align:center;"><input type="radio" name="f_safecode" value="n"<?php if ($this->_tpl_vars['bm_prefs']['f_safecode'] == 'n'): ?> checked="checked"<?php endif; ?> /></td>
			</tr>
		</table>

		<p align="center">
			<?php echo TemplateLang(array('p' => 'customfieldsat'), $this);?>
 <a href="prefs.profilefields.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
">&raquo; <?php echo TemplateLang(array('p' => 'profilefields'), $this);?>
</a>.
		</p>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'datavalidation'), $this);?>
</legend>

		<table>
			<tr>
				<td width="40" valign="top" rowspan="6"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_validation.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'regiplock'), $this);?>
:</td>
				<td class="td2"><input type="text" name="reg_iplock" value="<?php echo $this->_tpl_vars['bm_prefs']['reg_iplock']; ?>
" size="6" /> <?php echo TemplateLang(array('p' => 'seconds'), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'plzcheck'), $this);?>
?</td>
				<td class="td2"><input name="plz_check"<?php if ($this->_tpl_vars['bm_prefs']['plz_check'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'altcheck'), $this);?>
?</td>
				<td class="td2"><input name="alt_check"<?php if ($this->_tpl_vars['bm_prefs']['alt_check'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'lockedaltmails'), $this);?>
:</td>
				<td class="td2">
					<textarea style="width:100%;height:80px;" name="locked_altmail"><?php echo TemplateText(array('value' => $this->_tpl_vars['bm_prefs']['locked_altmail'],'allowEmpty' => true), $this);?>
</textarea>
					<small><?php echo TemplateLang(array('p' => 'altmailsepby'), $this);?>
</small>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'check_double_altmail'), $this);?>
?</td>
				<td class="td2"><input name="check_double_altmail"<?php if ($this->_tpl_vars['bm_prefs']['check_double_altmail'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'check_double_cellphone'), $this);?>
?</td>
				<td class="td2"><input name="check_double_cellphone"<?php if ($this->_tpl_vars['bm_prefs']['check_double_cellphone'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
		</table>
	</fieldset>

	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'signupdnsbl'), $this);?>
</legend>

		<table width="90%">
			<tr>
				<td align="left" rowspan="3" valign="top" width="40"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/antispam_dnsbl.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'enable'), $this);?>
?</td>
				<td class="td2"><input name="signup_dnsbl_enable"<?php if ($this->_tpl_vars['bm_prefs']['signup_dnsbl_enable'] == 'yes'): ?> checked="checked"<?php endif; ?> type="checkbox" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'dnsblservers'), $this);?>
:</td>
				<td class="td2">
					<textarea style="width:100%;height:80px;" name="signup_dnsbl"><?php echo TemplateText(array('value' => $this->_tpl_vars['bm_prefs']['signup_dnsbl'],'allowEmpty' => true), $this);?>
</textarea>
					<small><?php echo TemplateLang(array('p' => 'sepby'), $this);?>
</small>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'action'), $this);?>
:</td>
				<td class="td2"><select name="signup_dnsbl_action">
					<option value="block"<?php if ($this->_tpl_vars['bm_prefs']['signup_dnsbl_action'] == 'block'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'blocksignup'), $this);?>
</option>
					<option value="lock"<?php if ($this->_tpl_vars['bm_prefs']['signup_dnsbl_action'] == 'lock'): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'activatemanually'), $this);?>
</option>
				</select></td>
			</tr>
		</table>
	</fieldset>

	<p>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>