<?php
/*
 * b1gMail
 * (c) 2002-2016 B1G Software
 *
 * Redistribution of this code without explicit permission
 * is forbidden!
 *
 */

if(!defined('B1GMAIL_INIT'))
	die('Directly calling this file is not supported');

$apTypes = array(
	BMAP_SEND_RECP_LIMIT			=> array(
		'title'					=> $lang_admin['ap_type1'],
		'defaultPoints'			=> 5,
		'prefs'					=> array()
	),
	BMAP_SEND_FREQ_LIMIT			=> array(
		'title'					=> $lang_admin['ap_type2'],
		'defaultPoints'			=> 25,
		'prefs'					=> array()
	),
	BMAP_SEND_RECP_BLOCKED			=> array(
		'title'					=> $lang_admin['ap_type3'],
		'defaultPoints'			=> 15,
		'prefs'					=> array()
	),
	BMAP_SEND_RECP_LOCAL_INVALID	=> array(
		'title'					=> $lang_admin['ap_type4'],
		'defaultPoints'			=> 10,
		'prefs'					=> array()
	),
	BMAP_SEND_RECP_DOMAIN_INVALID	=> array(
		'title'					=> $lang_admin['ap_type5'],
		'defaultPoints'			=> 10,
		'prefs'					=> array()
	),
	BMAP_SEND_WITHOUT_RECEIVE		=> array(
		'title'					=> $lang_admin['ap_type6'],
		'defaultPoints'			=> 20,
		'prefs'					=> array(
			'interval'				=> array(
				'title'				=> $lang_admin['limit_interval_m'].':',
				'type'				=> FIELD_TEXT,
				'default'			=> '60'
			)
		)
	),
	BMAP_SEND_FAST					=> array(
		'title'					=> $lang_admin['ap_type7'],
		'defaultPoints'			=> 20,
		'prefs'					=> array(
			'interval'			=> array(
				'title'				=> $lang_admin['min_resend_interval_s'].':',
				'type'				=> FIELD_TEXT,
				'default'			=> '5'
			)
		)
	),
	BMAP_RECV_FREQ_LIMIT			=> array(
		'title'					=> $lang_admin['ap_type21'],
		'defaultPoints'			=> 5,
		'prefs'					=> array(
			'amount'				=> array(
				'title'				=> $lang_admin['limit_amount_count'].':',
				'type'				=> FIELD_TEXT,
				'default'			=> '50'
			),
			'interval'				=> array(
				'title'				=> $lang_admin['limit_interval_m'].':',
				'type'				=> FIELD_TEXT,
				'default'			=> '5'
			)
		)
	),
	BMAP_RECV_TRAFFIC_LIMIT			=> array(
		'title'					=> $lang_admin['ap_type22'],
		'defaultPoints'			=> 5,
		'prefs'					=> array(
			'amount'				=> array(
				'title'				=> $lang_admin['limit_amount_mb'].':',
				'type'				=> FIELD_TEXT,
				'default'			=> '100'
			),
			'interval'				=> array(
				'title'				=> $lang_admin['limit_interval_m'].':',
				'type'				=> FIELD_TEXT,
				'default'			=> '5'
			)
		)
	)
);
