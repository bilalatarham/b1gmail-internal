<?php /* Smarty version 2.6.28, created on 2020-09-29 12:06:54
         compiled from li/email.read.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'li/email.read.tpl', 4, false),array('function', 'text', 'li/email.read.tpl', 4, false),array('function', 'hook', 'li/email.read.tpl', 19, false),array('function', 'addressList', 'li/email.read.tpl', 25, false),array('function', 'date', 'li/email.read.tpl', 33, false),array('function', 'email', 'li/email.read.tpl', 264, false),array('function', 'cycle', 'li/email.read.tpl', 302, false),array('function', 'size', 'li/email.read.tpl', 309, false),array('modifier', 'count', 'li/email.read.tpl', 286, false),)), $this); ?>
<div id="contentHeader">
	<div class="left">
		<i class="fa fa-envelope"></i>
		<?php echo TemplateLang(array('p' => 'mail_read'), $this);?>
: <?php echo TemplateText(array('value' => $this->_tpl_vars['subject'],'cut' => 45), $this);?>

	</div>
	<div class="right">
		<button type="button" onclick="document.location.href='email.php?folder=<?php echo $this->_tpl_vars['folderID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
';">
			<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_<?php echo $this->_tpl_vars['folderInfo']['type']; ?>
.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			<?php echo $this->_tpl_vars['folderInfo']['title']; ?>

		</button>
		<?php if (! $this->_tpl_vars['folderInfo']['readonly']): ?><button type="button" onclick="moveMail('<?php echo $this->_tpl_vars['mailID']; ?>
');">
			<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_move.png" width="16" height="16" border="0" alt="" align="absmiddle" />
			<?php echo TemplateLang(array('p' => 'move'), $this);?>

		</button><?php endif; ?>
	</div>
</div>

<div class="scrollContainer withBottomBar<?php if ($_GET['openConversationView'] || $this->_tpl_vars['attachments'] || $this->_tpl_vars['notes']): ?>AndLayer<?php endif; ?>" id="mailReadScrollContainer">
	<?php echo TemplateHook(array('id' => "email.read.tpl:head"), $this);?>


	<div class="previewMailHeader" id="mailHeader">
		<table class="lightTable" width="100%">
			<tr>
				<th width="120"><?php echo TemplateLang(array('p' => 'from'), $this);?>
:</th>
				<td><?php echo TemplateAddressList(array('list' => $this->_tpl_vars['fromAddresses']), $this);?>
</td>
			</tr>
			<tr>
				<th><?php echo TemplateLang(array('p' => 'subject'), $this);?>
:</th>
				<td><strong><?php echo TemplateText(array('value' => $this->_tpl_vars['subject']), $this);?>
</strong></td>
			</tr>
			<tr>
				<th><?php echo TemplateLang(array('p' => 'date'), $this);?>
:</th>
				<td><?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['date'],'elapsed' => true), $this);?>
</td>
			</tr>
			<tr>
				<th><?php echo TemplateLang(array('p' => 'to'), $this);?>
:</th>
				<td><?php echo TemplateAddressList(array('list' => $this->_tpl_vars['toAddresses']), $this);?>
</td>
			</tr>
			<?php if ($this->_tpl_vars['ccAddresses']): ?><tr>
				<th><?php echo TemplateLang(array('p' => 'cc'), $this);?>
:</th>
				<td><?php echo TemplateAddressList(array('list' => $this->_tpl_vars['ccAddresses']), $this);?>
</td>
			</tr><?php endif; ?>
			<?php if ($this->_tpl_vars['replyToAddresses']): ?><tr>
				<th><?php echo TemplateLang(array('p' => 'replyto'), $this);?>
:</th>
				<td><?php echo TemplateAddressList(array('list' => $this->_tpl_vars['replyToAddresses']), $this);?>
</td>
			</tr><?php endif; ?>
			<?php if ($this->_tpl_vars['priority'] != 0): ?><tr>
				<th><?php echo TemplateLang(array('p' => 'priority'), $this);?>
:</th>
				<td>
					<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_<?php if ($this->_tpl_vars['priority'] == -1): ?>low<?php elseif ($this->_tpl_vars['priority'] == 1): ?>high<?php endif; ?>.gif" border="0" alt="" align="absmiddle" />
					<?php echo TemplateLang(array('p' => "prio_".($this->_tpl_vars['priority'])), $this);?>

				</td>
			</tr><?php endif; ?>

			<?php if ($this->_tpl_vars['smimeStatus'] != 0 && ! ( $this->_tpl_vars['smimeStatus'] & 1 )): ?>
			<tr>
				<th><?php echo TemplateLang(array('p' => 'security'), $this);?>
:</th>
				<td>
					<?php if ($this->_tpl_vars['smimeStatus'] & 2): ?>
					<font color="#FF0000">
						<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_signed_bad.png" width="16" height="16" border="0" alt="" align="absmiddle" />
						<?php echo TemplateLang(array('p' => 'badsigned'), $this);?>

					</font>
					&nbsp;&nbsp;
					<?php endif; ?>
					<?php if ($this->_tpl_vars['smimeStatus'] & 4): ?>
					<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_signed_ok.png" width="16" height="16" border="0" alt="" align="absmiddle" />
					<a href="javascript:void(0);" onclick="showCertificate('<?php echo $this->_tpl_vars['smimeCertificateHash']; ?>
');"><?php echo TemplateLang(array('p' => 'signed'), $this);?>
</a>
					&nbsp;&nbsp;
					<?php endif; ?>
					<?php if ($this->_tpl_vars['smimeStatus'] & 8): ?>
					<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_signed_noverify.png" width="16" height="16" border="0" alt="" align="absmiddle" />
					<a href="javascript:void(0);" onclick="showCertificate('<?php echo $this->_tpl_vars['smimeCertificateHash']; ?>
');" style="color:#FF8C00;"><?php echo TemplateLang(array('p' => 'noverifysigned'), $this);?>
</a>
					&nbsp;&nbsp;
					<?php endif; ?>
					<?php if ($this->_tpl_vars['smimeStatus'] & 64): ?>
					<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_encrypted_error.png" width="16" height="16" border="0" alt="" align="absmiddle" />

					<font color="#FF0000">
						<?php echo TemplateLang(array('p' => 'decryptionfailed'), $this);?>

					</font>
					&nbsp;&nbsp;
					<?php endif; ?>
					<?php if ($this->_tpl_vars['smimeStatus'] & 128): ?>
					<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mailico_encrypted.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'encrypted'), $this);?>

					&nbsp;&nbsp;
					<?php endif; ?>
				</td>
			</tr>
			<?php endif; ?>

			<?php if ($this->_tpl_vars['deliveryStatus']): ?>
			<tr>
				<th><?php echo TemplateLang(array('p' => 'deliverystatus'), $this);?>
:</th>
				<td>
					<?php if ($this->_tpl_vars['deliveryStatus']['exception']): ?><i class="fa fa-exclamation-triangle" style="color:orange;"></i>
					<?php elseif ($this->_tpl_vars['deliveryStatus']['allDelivered']): ?><i class="fa fa-check" style="color:green;"></i>
					<?php else: ?><i class="fa fa-refresh"></i><?php endif; ?>
					<a href="javascript:showDeliveryStatus(<?php echo $this->_tpl_vars['mailID']; ?>
);"><?php echo $this->_tpl_vars['deliveryStatus']['statusText']; ?>
</a>
				</td>
			<?php endif; ?>

			<?php echo TemplateHook(array('id' => "email.read.tpl:metaTable"), $this);?>

		</table>
	</div>

	<div id="bigFormToolbar">

		<?php if ($this->_tpl_vars['prevID']): ?><button type="button" onclick="document.location.href='email.read.php?id=<?php echo $this->_tpl_vars['prevID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
';">
			&laquo;
		</button><?php endif; ?>

		<button type="button" onclick="mailReply(<?php echo $this->_tpl_vars['mailID']; ?>
,false);">
			<i class="fa fa-mail-reply"></i>
			<?php echo TemplateLang(array('p' => 'reply'), $this);?>

		</button>

		<button type="button" onclick="mailReply(<?php echo $this->_tpl_vars['mailID']; ?>
,true);">
			<i class="fa fa-mail-reply-all"></i>
			<?php echo TemplateLang(array('p' => 'replyall'), $this);?>

		</button>

		<button type="button" onclick="document.location.href='email.compose.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&forward=<?php echo $this->_tpl_vars['mailID']; ?>
';">
			<i class="fa fa-mail-forward"></i>
			<?php echo TemplateLang(array('p' => 'forward'), $this);?>

		</button>

		<button type="button" onclick="document.location.href='email.compose.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&redirect=<?php echo $this->_tpl_vars['mailID']; ?>
';">
			<i class="fa fa-envelope-o"></i>
			<?php echo TemplateLang(array('p' => 'redirect'), $this);?>

		</button>

		<button type="button" onclick="document.location.href='email.read.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=download&id=<?php echo $this->_tpl_vars['mailID']; ?>
';">
			<i class="fa fa-download"></i>
			<?php echo TemplateLang(array('p' => 'download'), $this);?>

		</button>

		<button type="button" onclick="printMail(<?php echo $this->_tpl_vars['mailID']; ?>
,'<?php echo $this->_tpl_vars['sid']; ?>
');">
			<i class="fa fa-print"></i>
			<?php echo TemplateLang(array('p' => 'print'), $this);?>

		</button>

		<?php if (! $this->_tpl_vars['folderInfo']['readonly']): ?><button type="button" onclick="<?php if ($this->_tpl_vars['folderID'] == -5): ?>if(confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
')) <?php endif; ?> document.location.href='email.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&do=deleteMail&id=<?php echo $this->_tpl_vars['mailID']; ?>
&folder=<?php echo $this->_tpl_vars['folderID']; ?>
';">
			<i class="fa fa-remove"></i>
			<?php echo TemplateLang(array('p' => 'delete'), $this);?>

		</button><?php endif; ?>

		<?php if ($this->_tpl_vars['nextID']): ?><button type="button" onclick="document.location.href='email.read.php?id=<?php echo $this->_tpl_vars['nextID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
';">
			&raquo;
		</button><?php endif; ?>

	</div>


<div class="pad">

<?php echo TemplateHook(array('id' => "email.read.tpl:beforeText"), $this);?>

<div>
		<?php if ($this->_tpl_vars['flags'] & 128): ?>
		<div class="mailWarning">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/infected.png" width="16" height="16" />
			<?php echo TemplateLang(array('p' => 'infectedtext'), $this);?>
: <?php echo $this->_tpl_vars['infection']; ?>

		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['flags'] & 256): ?>
		<div class="mailNote" id="spamQuestionDiv" style="display:;">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/spam.png" width="16" height="16" />
			<?php echo TemplateLang(array('p' => 'spamtext'), $this);?>

			<?php if (! $this->_tpl_vars['trained']): ?><a href="javascript:setMailSpamStatus(<?php echo $this->_tpl_vars['mailID']; ?>
, false)"><?php echo TemplateLang(array('p' => 'isnotspam'), $this);?>
</a><?php endif; ?>
		</div>
		<?php elseif (! $this->_tpl_vars['trained']): ?>
		<div class="mailNote" id="spamQuestionDiv" style="display:;">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/spam_question.png" width="16" height="16" />
			<?php echo TemplateLang(array('p' => 'spamquestion'), $this);?>

			&nbsp;&nbsp;
			<a href="javascript:setMailSpamStatus(<?php echo $this->_tpl_vars['mailID']; ?>
, true)">
				<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/yes.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'yes'), $this);?>

			</a>
			&nbsp;&nbsp;
			<a href="javascript:setMailSpamStatus(<?php echo $this->_tpl_vars['mailID']; ?>
, false)">
				<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/no.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'no'), $this);?>

			</a>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['flags'] & 512): ?>
		<div class="mailNote">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/msg_small.png" width="16" height="16" />
			<?php echo TemplateLang(array('p' => 'certmailinfo'), $this);?>

		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['htmlAvailable']): ?>
		<div class="mailNote">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/msg_small.png" width="16" height="16" />
			<?php echo TemplateLang(array('p' => 'htmlavailable'), $this);?>

			<a href="email.read.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&id=<?php echo $this->_tpl_vars['mailID']; ?>
&htmlView=true"><?php echo TemplateLang(array('p' => 'view'), $this);?>
 &raquo;</a>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['noExternal']): ?>
		<div class="mailNote" id="noExternalDiv" style="display:;">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/msg_small.png" width="16" height="16" />
			<?php echo TemplateLang(array('p' => 'noexternal'), $this);?>

			<a href="email.read.php?action=inlineHTML&mode=<?php echo $this->_tpl_vars['textMode']; ?>
&id=<?php echo $this->_tpl_vars['mailID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&enableExternal=true" target="mailFrame" onclick="document.getElementById('noExternalDiv').style.display='none';"><?php echo TemplateLang(array('p' => 'showexternal'), $this);?>
 &raquo;</a>
		</div>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['confirmationTo']): ?>
		<div class="mailNote" id="confirmationDiv" style="display:;">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_confirm.png" width="16" height="16" />
			<?php echo TemplateLang(array('p' => 'senderconfirmto'), $this);?>

			<b><?php echo TemplateText(array('value' => $this->_tpl_vars['confirmationTo']), $this);?>
</b>.
			<a href="javascript:sendMailConfirmation(<?php echo $this->_tpl_vars['mailID']; ?>
);"><?php echo TemplateLang(array('p' => 'sendconfirmation'), $this);?>
 &raquo;</a>
		</div>
		<?php elseif ($this->_tpl_vars['flags'] & 16384): ?>
		<div class="mailNote preview" id="confirmationDiv" style="display:;">
			&nbsp;
			<img align="absmiddle" border="0" alt="" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mail_confirm.png" width="16" height="16" />
			<?php echo TemplateLang(array('p' => 'confirmationsent'), $this);?>

		</div>
		<?php endif; ?>
		<?php echo TemplateHook(array('id' => "email.read.tpl:mailNotes"), $this);?>

	</div>

	<iframe name="mailFrame" width="100%" style="height:200px;" id="textArea" src="about:blank" class="mailHTMLText" frameborder="no"></iframe>
	<textarea id="textArea_raw" style="display:none;"><?php echo TemplateText(array('allowEmpty' => true,'value' => $this->_tpl_vars['text'],'allowDoubleEnc' => true), $this);?>
</textarea>

	<script language="javascript">
	<!--
		initEMailTextArea(EBID('textArea_raw').value);
	//-->
	</script>
</p>
<?php echo TemplateHook(array('id' => "email.read.tpl:afterText"), $this);?>


<div id="afterText">
<?php if ($this->_tpl_vars['vcards']): ?>
<p>
<?php $_from = $this->_tpl_vars['vcards']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['card']):
?>
	<div class="mailBox"><table width="100%">
			<tr>
				<td rowspan="4" width="66" align="center"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/vcf.png" width="48" height="48" border="0" alt="" /></td>
				<td width="100"><b><?php echo TemplateLang(array('p' => 'firstname'), $this);?>
:</td>
				<td><?php echo TemplateText(array('value' => $this->_tpl_vars['card']['vorname']), $this);?>
</td>
				<td rowspan="4" width="140" background="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/box_bg.gif">
					<a href="email.read.php?id=<?php echo $this->_tpl_vars['mailID']; ?>
&action=importVCF&attachment=<?php echo $this->_tpl_vars['key']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/import_vcf.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'importvcf'), $this);?>
</a><br />
					<a href="email.read.php?id=<?php echo $this->_tpl_vars['mailID']; ?>
&action=downloadAttachment&attachment=<?php echo $this->_tpl_vars['key']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_download.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'download'), $this);?>
</a>
				</td>
			</tr>
			<tr>
				<td><b><?php echo TemplateLang(array('p' => 'surname'), $this);?>
:</td>
				<td><?php echo TemplateText(array('value' => $this->_tpl_vars['card']['nachname']), $this);?>
</td>
			</tr>
			<tr>
				<td><b><?php echo TemplateLang(array('p' => 'company'), $this);?>
:</td>
				<td><?php echo TemplateText(array('value' => $this->_tpl_vars['card']['firma']), $this);?>
</td>
			</tr>
			<tr>
				<td><b><?php echo TemplateLang(array('p' => 'email'), $this);?>
:</td>
				<td><?php echo TemplateEMail(array('value' => $this->_tpl_vars['card']['email']), $this);?>
</td>
			</tr>
		</table></div>
<?php endforeach; endif; unset($_from); ?>
</p>
<?php endif; ?>

<?php echo TemplateHook(array('id' => "email.read.tpl:foot"), $this);?>


<form id="quoteForm" action="email.compose.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&reply=<?php echo $this->_tpl_vars['mailID']; ?>
" method="post">
	<input type="hidden" name="text" id="quoteText" value="" />
</form>

</div>

</div></div>

<div class="contentBottomLayer" id="bottomLayer_attachments" style="display:<?php if ($_GET['openConversationView'] || $this->_tpl_vars['notes'] || ! $this->_tpl_vars['attachments']): ?>none<?php endif; ?>;">
	<div class="contentHeader">
		<div class="left">
			<input type="checkbox" style="margin-left:-0.5em;" id="allChecker" onclick="checkAll(this.checked, document.forms.attachmentsForm, 'att');" />
			<i class="fa fa-paperclip"></i>
			<?php echo TemplateLang(array('p' => 'attachments'), $this);?>
 (<?php echo count($this->_tpl_vars['attachments']); ?>
)
		</div>
		<div class="right">
			<button onclick="readMailHideBottomLayers()">
				<i class="fa fa-close"></i>
			</button>
		</div>
	</div>

	<form name="attachmentsForm" method="get" action="email.read.php">
	<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['mailID']; ?>
" />
	<input type="hidden" name="sid" value="<?php echo $this->_tpl_vars['sid']; ?>
" />

	<div class="scrollContainer withBottomBar">
		<table class="listTable" style="border:none;border-radius:0;">
			<?php $_from = $this->_tpl_vars['attachments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['attID'] => $this->_tpl_vars['attachment']):
?>
			<?php echo smarty_function_cycle(array('values' => "listTableTD,listTableTD2",'assign' => 'class'), $this);?>

			<tr>
				<td class="<?php echo $this->_tpl_vars['class']; ?>
" width="26"><input type="checkbox" name="att[]" id="att_<?php echo $this->_tpl_vars['attID']; ?>
" value="<?php echo $this->_tpl_vars['attID']; ?>
" /></td>
				<td class="<?php echo $this->_tpl_vars['class']; ?>
"><i class="fa fa-paperclip"></i>
										<a href="javascript:advancedOptions('<?php echo $this->_tpl_vars['attID']; ?>
', 'right', 'bottom', '<?php echo $this->_tpl_vars['tpldir']; ?>
');"><img id="advanced_<?php echo $this->_tpl_vars['attID']; ?>
_arrow" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/mini_arrow_right.png" width="13" height="13" border="0" alt="" align="absmiddle" />
										<?php echo TemplateText(array('value' => $this->_tpl_vars['attachment']['filename'],'cut' => 45), $this);?>
</a></td>
				<td class="<?php echo $this->_tpl_vars['class']; ?>
" width="20%"><?php echo TemplateText(array('value' => $this->_tpl_vars['attachment']['mimetype'],'cut' => 45), $this);?>
</td>
				<td class="<?php echo $this->_tpl_vars['class']; ?>
" width="20%" style="text-align:right;"><?php echo TemplateLang(array('p' => 'approx'), $this);?>
 <?php echo TemplateSize(array('bytes' => $this->_tpl_vars['attachment']['size']), $this);?>
&nbsp;&nbsp;
											<a href="email.read.php?id=<?php echo $this->_tpl_vars['mailID']; ?>
&action=downloadAttachment&attachment=<?php echo $this->_tpl_vars['attID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><i class="fa fa-download"></i></a>
											&nbsp;</td>
			</tr>
			<tbody id="advanced_<?php echo $this->_tpl_vars['attID']; ?>
_body" style="display:none;">
			<tr>
				<td class="attDiv">&nbsp;</td>
				<td colspan="3" class="attDiv">
					<input type="button" value=" <?php echo TemplateLang(array('p' => 'download'), $this);?>
 " onclick="document.location.href='email.read.php?id=<?php echo $this->_tpl_vars['mailID']; ?>
&action=downloadAttachment&attachment=<?php echo $this->_tpl_vars['attID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
';" />
					<input type="button" value=" <?php echo TemplateLang(array('p' => 'savetowebdisk'), $this);?>
 " onclick="saveAttachmentToWebdisk(<?php echo $this->_tpl_vars['mailID']; ?>
, '<?php echo $this->_tpl_vars['attID']; ?>
', '<?php echo $this->_tpl_vars['attachment']['filename']; ?>
', '<?php echo $this->_tpl_vars['sid']; ?>
')" />
					<?php if ($this->_tpl_vars['attachment']['viewable']): ?><input type="button" value=" <?php echo TemplateLang(array('p' => 'view'), $this);?>
 " onclick="javascript:window.open('email.read.php?id=<?php echo $this->_tpl_vars['mailID']; ?>
&action=downloadAttachment&attachment=<?php echo $this->_tpl_vars['attID']; ?>
&view=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
');" /><?php endif; ?>
					<?php if ($this->_tpl_vars['attachment']['mimetype'] == 'message/rfc822' || $this->_tpl_vars['attachment']['filetype'] == '.eml'): ?><input type="button" value=" <?php echo TemplateLang(array('p' => 'view'), $this);?>
 " onclick="javascript:showAttachedMail(<?php echo $this->_tpl_vars['mailID']; ?>
, '<?php echo $this->_tpl_vars['attID']; ?>
', '<?php echo TemplateText(array('value' => $this->_tpl_vars['attachment']['filename'],'cut' => 45,'escape' => true), $this);?>
');" /><?php endif; ?>
					<?php if ($this->_tpl_vars['attachment']['mimetype'] == 'application/zip' || $this->_tpl_vars['attachment']['filetype'] == '.zip'): ?><input type="button" value=" <?php echo TemplateLang(array('p' => 'view'), $this);?>
 " onclick="javascript:showAttachedZIP(<?php echo $this->_tpl_vars['mailID']; ?>
, '<?php echo $this->_tpl_vars['attID']; ?>
', '<?php echo TemplateText(array('value' => $this->_tpl_vars['attachment']['filename'],'cut' => 45,'escape' => true), $this);?>
');" /><?php endif; ?>
				</td>
			</tr>
			</tbody>
			<?php endforeach; endif; unset($_from); ?>
		</table>
	</div>

	<div class="contentFooter">
	 	<div class="left">
			<select class="smallInput" name="do">
				<option value="-">------ <?php echo TemplateLang(array('p' => 'selaction'), $this);?>
 ------</option>
				<option value="downloadAttachments"><?php echo TemplateLang(array('p' => 'download'), $this);?>
</option>
				<?php echo TemplateHook(array('id' => "email.read.tpl:attachSelect"), $this);?>

			</select>
			<input class="smallInput" type="submit" value="<?php echo TemplateLang(array('p' => 'ok'), $this);?>
" />
		</div>
	</div>
	</form>
</div>

<?php if ($this->_tpl_vars['conversationView']): ?>
<div class="contentBottomLayer" id="bottomLayer_conversation" style="display:<?php if (! $_GET['openConversationView']): ?>none<?php endif; ?>;">
	<div class="contentHeader">
		<div class="left">
			<i class="fa fa-comment"></i>
			<?php echo TemplateLang(array('p' => 'conversation'), $this);?>

		</div>
		<div class="right">
			<button onclick="readMailHideBottomLayers()">
				<i class="fa fa-close"></i>
			</button>
		</div>
	</div>

	<div class="bigForm">
		<iframe id="conversationIFrame" style="width:100%;height:100%;" src="email.read.php?action=showThread&id=<?php echo $this->_tpl_vars['mailID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" border="0" frameborder="0"></iframe>
	</div>
</div>
<?php endif; ?>

<div class="contentBottomLayer" id="bottomLayer_props" style="display:<?php if (! $this->_tpl_vars['notes'] || $_GET['openConversationView']): ?>none<?php endif; ?>;">
	<div class="contentHeader">
		<div class="left">
			<i class="fa fa-tags"></i>
			<?php echo TemplateLang(array('p' => 'props'), $this);?>

		</div>
		<div class="right">
			<button onclick="readMailHideBottomLayers()">
				<i class="fa fa-close"></i>
			</button>
		</div>
	</div>

	<form method="post" action="email.read.php?id=<?php echo $this->_tpl_vars['mailID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
">
	<input type="hidden" name="do" value="saveMeta" />

	<div class="bigForm">
		<table class="listTable" style="border:none;border-radius:0;">
			<tr>
				<th class="listTableHead"><?php echo TemplateLang(array('p' => 'color'), $this);?>
</th>
				<th class="listTableHead"><?php echo TemplateLang(array('p' => 'flags'), $this);?>
</th>
				<th class="listTableHead"><?php echo TemplateLang(array('p' => 'notes'), $this);?>
</th>
			</tr>
			<tr>
				<td width="160" style="padding:5px;">
					<table>
						<tr>
							<td><input type="radio"<?php if ($this->_tpl_vars['color'] == 1): ?> checked="checked"<?php endif; ?> name="color" value="1" /></td>
							<td><div class="calendarDate_0" style="padding:0px;margin:0px;margin-left:5px;width:12px;height:12px;"></div></td>

							<td><input type="radio"<?php if ($this->_tpl_vars['color'] == 2): ?> checked="checked"<?php endif; ?> name="color" value="2" /></td>
							<td><div class="calendarDate_1" style="padding:0px;margin:0px;margin-left:5px;width:12px;height:12px;"></div></td>

							<td><input type="radio"<?php if ($this->_tpl_vars['color'] == 3): ?> checked="checked"<?php endif; ?> name="color" value="3" /></td>
							<td><div class="calendarDate_2" style="padding:0px;margin:0px;margin-left:5px;width:12px;height:12px;"></div></td>
						</tr>
						<tr>
							<td><input type="radio"<?php if ($this->_tpl_vars['color'] == 4): ?> checked="checked"<?php endif; ?> name="color" value="4" /></td>
							<td><div class="calendarDate_3" style="padding:0px;margin:0px;margin-left:5px;width:12px;height:12px;"></div></td>

							<td><input type="radio"<?php if ($this->_tpl_vars['color'] == 5): ?> checked="checked"<?php endif; ?> name="color" value="5" /></td>
							<td><div class="calendarDate_4" style="padding:0px;margin:0px;margin-left:5px;width:12px;height:12px;"></div></td>

							<td><input type="radio"<?php if ($this->_tpl_vars['color'] == 6): ?> checked="checked"<?php endif; ?> name="color" value="6" /></td>
							<td><div class="calendarDate_5" style="padding:0px;margin:0px;margin-left:5px;width:12px;height:12px;"></div></td>
						</tr>
						<tr>
							<td><input type="radio"<?php if ($this->_tpl_vars['color'] == 0): ?> checked="checked"<?php endif; ?> name="color" value="0" /></td>
							<td colspan="5">&nbsp;<?php echo TemplateLang(array('p' => 'none'), $this);?>
</td>
						</tr>
					</table>
				</td>
				<td width="150" class="listTableTDActive" style="padding:5px;padding-left:10px;">
					<i class="fa fa-envelope"></i>
					<input type="checkbox" name="flags[1]" id="flags1"<?php if ($_POST['do'] == 'saveMeta' && ( $this->_tpl_vars['flags'] & 1 )): ?> checked="checked"<?php endif; ?> />
					<label for="flags1"><?php echo TemplateLang(array('p' => 'unread'), $this);?>
</label><br />

					<i class="fa fa-flag"></i>
					<input type="checkbox" name="flags[16]" id="flags16"<?php if ($this->_tpl_vars['flags'] & 16): ?> checked="checked"<?php endif; ?> />
					<label for="flags16"><?php echo TemplateLang(array('p' => 'marked'), $this);?>
</label><br />

					<i class="fa fa-check"></i>
					<input type="checkbox" name="flags[4096]" id="flags4096"<?php if ($this->_tpl_vars['flags'] & 4096): ?> checked="checked"<?php endif; ?> />
					<label for="flags4096"><?php echo TemplateLang(array('p' => 'done'), $this);?>
</label><br />
				</td>
				<td style="padding:5px;">
					<textarea style="width:100%;height:65px;box-sizing:border-box;" name="notes"><?php echo TemplateText(array('value' => $this->_tpl_vars['notes'],'allowEmpty' => true), $this);?>
</textarea>
				</td>
			</tr>
		</table>
	</div>

	<div class="contentFooter">
	 	<div class="right">
			<button class="primary" type="submit"<?php if ($this->_tpl_vars['folderInfo']['readonly']): ?> disabled="disabled" style="color:grey;"<?php endif; ?>>
				<i class="fa fa-check"></i>
				<?php echo TemplateLang(array('p' => 'save'), $this);?>

			</button>
		</div>
	</div>

	</form>
</div>

<div id="contentFooter">
	<div class="left">
		<?php if ($this->_tpl_vars['attachments']): ?>
		<button type="button" onclick="readMailShowBottomLayer('attachments')">
			<i class="fa fa-paperclip"></i>
			<?php echo TemplateLang(array('p' => 'attachments'), $this);?>
 <strong>(<?php echo count($this->_tpl_vars['attachments']); ?>
)</strong>
		</button>
		<?php endif; ?>

		<?php if ($this->_tpl_vars['conversationView']): ?>
		<button type="button" onclick="readMailShowBottomLayer('conversation')">
			<i class="fa fa-comment"></i>
			<?php echo TemplateLang(array('p' => 'conversation'), $this);?>

		</button>
		<?php endif; ?>

		<button type="button" onclick="readMailShowBottomLayer('props')">
			<i class="fa fa-tags"></i>
			<?php echo TemplateLang(array('p' => 'props'), $this);?>

		</button>
	</div>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "li/email.addressmenu.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>