<?php /* Smarty version 2.6.28, created on 2020-09-29 11:38:45
         compiled from /var/www/html/plugins/templates/bms.admin.logs.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', '/var/www/html/plugins/templates/bms.admin.logs.tpl', 2, false),array('function', 'date', '/var/www/html/plugins/templates/bms.admin.logs.tpl', 2, false),array('function', 'cycle', '/var/www/html/plugins/templates/bms.admin.logs.tpl', 14, false),array('function', 'text', '/var/www/html/plugins/templates/bms.admin.logs.tpl', 17, false),array('function', 'pageNav', '/var/www/html/plugins/templates/bms.admin.logs.tpl', 29, false),array('function', 'html_select_date', '/var/www/html/plugins/templates/bms.admin.logs.tpl', 45, false),array('function', 'html_select_time', '/var/www/html/plugins/templates/bms.admin.logs.tpl', 46, false),)), $this); ?>
<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'logs'), $this);?>
 (<?php echo TemplateDate(array('nice' => true,'timestamp' => $this->_tpl_vars['start']), $this);?>
 - <?php echo TemplateDate(array('nice' => true,'timestamp' => $this->_tpl_vars['end']), $this);?>
)</legend>
	
	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="90"><?php echo TemplateLang(array('p' => 'bms_component'), $this);?>
</th>
			<th><?php echo TemplateLang(array('p' => 'entry'), $this);?>
</th>
			<th width="150"><?php echo TemplateLang(array('p' => 'date'), $this);?>

				<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/sort_desc.png" border="0" alt="" width="7" height="6" align="absmiddle" /></th>
		</tr>
		
		<?php $_from = $this->_tpl_vars['entries']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['entry']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/<?php echo $this->_tpl_vars['entry']['prioImg']; ?>
.png" border="0" alt="" width="16" height="16" /></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['entry']['componentName']), $this);?>
</td>
			<td><code><?php echo $this->_tpl_vars['entry']['szEntry']; ?>
</code></td>
			<td><?php echo TemplateDate(array('nice' => true,'timestamp' => $this->_tpl_vars['entry']['iDate']), $this);?>
</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>
		
		<tr>
			<td class="footer" colspan="4">
				<div style="float:left;">
					<input class="button" type="button" value=" <?php echo TemplateLang(array('p' => 'export'), $this);?>
 " onclick="parent.frames['top'].location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&action=logs&sid=<?php echo $this->_tpl_vars['sid']; ?>
&do=export&start=<?php echo $this->_tpl_vars['start']; ?>
&end=<?php echo $this->_tpl_vars['end']; ?>
&page=<?php echo $this->_tpl_vars['pageNo']; ?>
&q=<?php echo $this->_tpl_vars['ueQ']; ?>
<?php echo $this->_tpl_vars['prioQ']; ?>
';" />
				</div>
				<div style="float:right;padding-top:3px;">
					<?php echo TemplateLang(array('p' => 'pages'), $this);?>
: <?php echo TemplatePageNav(array('page' => $this->_tpl_vars['pageNo'],'pages' => $this->_tpl_vars['pageCount'],'on' => " <span class=\"pageNav\"><b>[.t]</b></span> ",'off' => " <span class=\"pageNav\"><a href=\"".($this->_tpl_vars['pageURL'])."&action=logs&start=".($this->_tpl_vars['start'])."&end=".($this->_tpl_vars['end'])."&q=".($this->_tpl_vars['ueQ']).($this->_tpl_vars['prioQ'])."&page=.s&sid=".($this->_tpl_vars['sid'])."\">.t</a></span> "), $this);?>
&nbsp;
				</div>
			</td>
		</tr>
	</table>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'filter'), $this);?>
</legend>
	
	<form action="<?php echo $this->_tpl_vars['pageURL']; ?>
&action=logs&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)">
		<table>
			<tr>
				<td width="40" valign="top" rowspan="5"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/filter.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="100"><?php echo TemplateLang(array('p' => 'from'), $this);?>
:</td>
				<td class="td2">
						<?php echo smarty_function_html_select_date(array('prefix' => 'start','time' => $this->_tpl_vars['start'],'start_year' => "-5",'field_order' => 'DMY','field_separator' => "."), $this);?>
, 
						<?php echo smarty_function_html_select_time(array('prefix' => 'start','time' => $this->_tpl_vars['start'],'display_seconds' => false), $this);?>

				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'to'), $this);?>
:</td>
				<td class="td2">
						<?php echo smarty_function_html_select_date(array('prefix' => 'end','time' => $this->_tpl_vars['end'],'start_year' => "-5",'field_order' => 'DMY','field_separator' => "."), $this);?>
, 
						<?php echo smarty_function_html_select_time(array('prefix' => 'end','time' => $this->_tpl_vars['end'],'display_seconds' => false), $this);?>

				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'bms_component'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox"<?php if ($this->_tpl_vars['component'][1]): ?> checked="checked"<?php endif; ?> name="component[1]" id="component1" />
					<label for="component1">Core</label> &nbsp;

					<input type="checkbox"<?php if ($this->_tpl_vars['component'][2]): ?> checked="checked"<?php endif; ?> name="component[2]" id="component2" />
					<label for="component2">POP3</label> &nbsp;

					<input type="checkbox"<?php if ($this->_tpl_vars['component'][4]): ?> checked="checked"<?php endif; ?> name="component[4]" id="component4" />
					<label for="component4">IMAP</label> &nbsp;

					<input type="checkbox"<?php if ($this->_tpl_vars['component'][8]): ?> checked="checked"<?php endif; ?> name="component[8]" id="component8" />
					<label for="component8">HTTP</label> &nbsp;

					<input type="checkbox"<?php if ($this->_tpl_vars['component'][16]): ?> checked="checked"<?php endif; ?> name="component[16]" id="component16" />
					<label for="component16">SMTP</label> &nbsp;

					<input type="checkbox"<?php if ($this->_tpl_vars['component'][32]): ?> checked="checked"<?php endif; ?> name="component[32]" id="component32" />
					<label for="component32">MSGQueue</label> &nbsp;

					<input type="checkbox"<?php if ($this->_tpl_vars['component'][64]): ?> checked="checked"<?php endif; ?> name="component[64]" id="component64" />
					<label for="component64">Plugin</label>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'priority'), $this);?>
:</td>
				<td class="td2">
					<input type="checkbox"<?php if ($this->_tpl_vars['prio'][8]): ?> checked="checked"<?php endif; ?> name="prio[8]" id="prio8" />
					<label for="prio8"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/debug.png" border="0" alt="" width="16" height="16" /></label> &nbsp;

					<input type="checkbox"<?php if ($this->_tpl_vars['prio'][1]): ?> checked="checked"<?php endif; ?> name="prio[1]" id="prio1" />
					<label for="prio1"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/info.png" border="0" alt="" width="16" height="16" /></label> &nbsp;

					<input type="checkbox"<?php if ($this->_tpl_vars['prio'][2]): ?> checked="checked"<?php endif; ?> name="prio[2]" id="prio2" />
					<label for="prio2"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/warning.png" border="0" alt="" width="16" height="16" /></label> &nbsp;

					<input type="checkbox"<?php if ($this->_tpl_vars['prio'][4]): ?> checked="checked"<?php endif; ?> name="prio[4]" id="prio4" />
					<label for="prio4"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/error.png" border="0" alt="" width="16" height="16" /></label>
				</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'search'), $this);?>
:</td>
				<td class="td2">
						<input type="text" name="q" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['q'],'allowEmpty' => true), $this);?>
" size="36" style="width:85%;" />
				</td>
			</tr>
		</table>
		
		<p align="right">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'apply'), $this);?>
 " />
		</p>
	</form>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'archiving'), $this);?>
</legend>
		
	<form action="<?php echo $this->_tpl_vars['pageURL']; ?>
&action=logs&do=archive&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="if(EBID('saveCopy').checked || confirm('<?php echo TemplateLang(array('p' => 'reallynotarc'), $this);?>
')) spin(this); else return(false);">
		<p>
			<?php echo TemplateLang(array('p' => 'logarc_desc'), $this);?>

		</p>
		
		<table>
			<tr>
				<td width="40" valign="top"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/archiving.png" border="0" alt="" width="32" height="32" /></td>
				<td class="td1" width="80"><?php echo TemplateLang(array('p' => 'date'), $this);?>
:</td>
				<td class="td2">
					<?php echo smarty_function_html_select_date(array('prefix' => 'date','start_year' => "-5",'field_order' => 'DMY','field_separator' => "."), $this);?>
, 
					<?php echo smarty_function_html_select_time(array('prefix' => 'date','display_seconds' => false), $this);?>

				</td>
			</tr>
		</table>
		
		<p align="right">
			<input type="checkbox" name="saveCopy" id="saveCopy" checked="checked" />
			<label for="saveCopy"><b><?php echo TemplateLang(array('p' => 'savearc'), $this);?>
</label>
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'execute'), $this);?>
 " />
		</p>
	</form>
</fieldset>