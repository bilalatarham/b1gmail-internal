<?php /* Smarty version 2.6.28, created on 2020-09-30 10:11:37
         compiled from li/email.folders.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'li/email.folders.tpl', 3, false),array('function', 'cycle', 'li/email.folders.tpl', 45, false),array('function', 'text', 'li/email.folders.tpl', 48, false),array('function', 'size', 'li/email.folders.tpl', 51, false),)), $this); ?>
<div id="contentHeader">
	<div class="left">
		<img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_folder.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateLang(array('p' => 'folderadmin'), $this);?>

	</div>
</div>

<form name="f1" method="post" action="email.folders.php?action=action&sid=<?php echo $this->_tpl_vars['sid']; ?>
">

<div class="scrollContainer withBottomBar">
<table class="bigTable">
	<thead>
	<tr>
		<th width="20"><input type="checkbox" id="allChecker" onclick="checkAll(this.checked, document.forms.f1, 'folder');" /></th>
		<th class="listTableHead">
			<a href="email.folders.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&sort=titel&order=<?php echo $this->_tpl_vars['sortOrderInv']; ?>
"><?php echo TemplateLang(array('p' => 'title'), $this);?>
</a>
			<?php if ($this->_tpl_vars['sortColumn'] == 'titel'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/<?php echo $this->_tpl_vars['sortOrder']; ?>
.gif" border="0" alt="" align="absmiddle" /><?php endif; ?>
		</th>
		<th width="160">
			<a href="email.folders.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&sort=parent&order=<?php echo $this->_tpl_vars['sortOrderInv']; ?>
"><?php echo TemplateLang(array('p' => 'parentfolder'), $this);?>
</a>
			<?php if ($this->_tpl_vars['sortColumn'] == 'parent'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/<?php echo $this->_tpl_vars['sortOrder']; ?>
.gif" border="0" alt="" align="absmiddle" /><?php endif; ?>
		</th>
		<th width="70">
			<?php echo TemplateLang(array('p' => 'size'), $this);?>

		</th>
		<th width="140">
			<?php echo TemplateLang(array('p' => 'status'), $this);?>

		</th>
		<th width="70">
			<a href="email.folders.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&sort=subscribed&order=<?php echo $this->_tpl_vars['sortOrderInv']; ?>
"><?php echo TemplateLang(array('p' => 'subscribed'), $this);?>
</a>
			<?php if ($this->_tpl_vars['sortColumn'] == 'subscribed'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/<?php echo $this->_tpl_vars['sortOrder']; ?>
.gif" border="0" alt="" align="absmiddle" /><?php endif; ?>
		</th>
		<th width="55">&nbsp;</th>
	</tr>
	</thead>
	
	<?php if ($this->_tpl_vars['sysFolderList']): ?>
	<tr>
		<td colspan="7" class="folderGroup">
			<a style="display:block;" href="javascript:toggleGroup('sys');">&nbsp;<img id="groupImage_sys" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/contract.png" width="11" height="11" border="0" align="absmiddle" alt="" />
			&nbsp;<?php echo TemplateLang(array('p' => 'sysfolders'), $this);?>
</a>
		</td>
	</tr>
	<tbody id="group_sys" style="display:;">
	<?php $_from = $this->_tpl_vars['sysFolderList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['folderID'] => $this->_tpl_vars['folder']):
?>
	<?php echo smarty_function_cycle(array('values' => "listTableTD,listTableTD2",'assign' => 'class'), $this);?>

	<tr>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap"><input type="checkbox" disabled="disabled" /></td>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap">&nbsp;<a href="email.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&folder=<?php echo $this->_tpl_vars['folderID']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_<?php echo $this->_tpl_vars['folder']['type']; ?>
.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateText(array('value' => $this->_tpl_vars['folder']['titel'],'cut' => 25), $this);?>
</a></td>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap">&nbsp;<?php echo TemplateText(array('value' => $this->_tpl_vars['folder']['parent'],'cut' => 15), $this);?>
</td>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap" style="text-align:center;">
			<?php echo TemplateSize(array('bytes' => $this->_tpl_vars['folder']['size']), $this);?>

		</td>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap" style="text-align:center;">
			<table>
				<tr>
					<td width="45" align="left"><i class="fa fa-envelope-o"></i>
						<?php echo $this->_tpl_vars['folder']['allMails']; ?>
</td>
					<td width="45" align="left"><i class="fa fa-envelope"></i>
						<?php echo $this->_tpl_vars['folder']['unreadMails']; ?>
</td>
					<td width="45" align="left"><i class="fa fa-flag-o"></i>
						<?php echo $this->_tpl_vars['folder']['flaggedMails']; ?>
</td>
				</tr>
			</table>
		</td>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap"><center><input type="checkbox" checked="checked" disabled="disabled" /></center></td>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap">
			<a href="email.folders.php?action=editFolder&id=<?php echo $this->_tpl_vars['folderID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_edit.png" width="16" height="16" border="0" alt="<?php echo TemplateLang(array('p' => 'edit'), $this);?>
" align="absmiddle" /></a>
		</td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>
	</tbody>
	<?php endif; ?>
	
	<?php if ($this->_tpl_vars['theFolderList']): ?>
	<tr>
		<td colspan="7" class="folderGroup">
			<a style="display:block;" href="javascript:toggleGroup('own');">&nbsp;<img id="groupImage_own" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/contract.png" width="11" height="11" border="0" align="absmiddle" alt="" />
			&nbsp;<?php echo TemplateLang(array('p' => 'ownfolders'), $this);?>
</a>
		</td>
	</tr>
	<tbody id="group_own" style="display:;">
	<?php $_from = $this->_tpl_vars['theFolderList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['folderID'] => $this->_tpl_vars['folder']):
?>
	<?php echo smarty_function_cycle(array('values' => "listTableTD,listTableTD2",'assign' => 'class'), $this);?>

	<tr>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap"><input type="checkbox" id="folder_<?php echo $this->_tpl_vars['folderID']; ?>
" name="folder_<?php echo $this->_tpl_vars['folderID']; ?>
" /></td>
		<td class="<?php if ($this->_tpl_vars['sortColumn'] == 'titel'): ?>listTableTDActive<?php else: ?><?php echo $this->_tpl_vars['class']; ?>
<?php endif; ?>" nowrap="nowrap">&nbsp;<a href="email.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&folder=<?php echo $this->_tpl_vars['folderID']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_<?php if ($this->_tpl_vars['folder']['intelligent'] == 1): ?>intelli<?php endif; ?>folder.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateText(array('value' => $this->_tpl_vars['folder']['titel'],'cut' => 25), $this);?>
</a></td>
		<td class="<?php if ($this->_tpl_vars['sortColumn'] == 'parent'): ?>listTableTDActive<?php else: ?><?php echo $this->_tpl_vars['class']; ?>
<?php endif; ?>" nowrap="nowrap">&nbsp;<?php echo TemplateText(array('value' => $this->_tpl_vars['folder']['parent'],'cut' => 15), $this);?>
</td>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap" style="text-align:center;">
			<?php if ($this->_tpl_vars['folder']['intelligent']): ?>-<?php else: ?><?php echo TemplateSize(array('bytes' => $this->_tpl_vars['folder']['size']), $this);?>
<?php endif; ?>
		</td>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap" style="text-align:center;">
			<table>
				<tr>
					<td width="45" align="left"><i class="fa fa-envelope-o"></i>
						<?php echo $this->_tpl_vars['folder']['allMails']; ?>
</td>
					<td width="45" align="left"><i class="fa fa-envelope"></i>
						<?php echo $this->_tpl_vars['folder']['unreadMails']; ?>
</td>
					<td width="45" align="left"><i class="fa fa-flag-o"></i>
						<?php echo $this->_tpl_vars['folder']['flaggedMails']; ?>
</td>
				</tr>
			</table>
		</td>
		<td class="<?php if ($this->_tpl_vars['sortColumn'] == 'subscribed'): ?>listTableTDActive<?php else: ?><?php echo $this->_tpl_vars['class']; ?>
<?php endif; ?>" nowrap="nowrap"><center><input type="checkbox" <?php if ($this->_tpl_vars['folder']['subscribed'] == 1): ?>checked="checked" <?php endif; ?> onchange="updateFolderSubscription('<?php echo $this->_tpl_vars['folderID']; ?>
', this, '<?php echo $this->_tpl_vars['sid']; ?>
')" /></center></td>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap">
			<a href="email.folders.php?action=editFolder&id=<?php echo $this->_tpl_vars['folderID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_edit.png" width="16" height="16" border="0" alt="<?php echo TemplateLang(array('p' => 'edit'), $this);?>
" align="absmiddle" /></a>
			<a onclick="return confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
');" href="email.folders.php?action=deleteFolder&id=<?php echo $this->_tpl_vars['folderID']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/ico_delete.png" width="16" height="16" border="0" alt="<?php echo TemplateLang(array('p' => 'delete'), $this);?>
" align="absmiddle" /></a>
		</td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>
	</tbody>
	<?php endif; ?>

	<?php if ($this->_tpl_vars['sharedFolderList']): ?>
	<tr>
		<td colspan="7" class="folderGroup">
			<a style="display:block;" href="javascript:toggleGroup('shared');">&nbsp;<img id="groupImage_shared" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/contract.png" width="11" height="11" border="0" align="absmiddle" alt="" />
			&nbsp;<?php echo TemplateLang(array('p' => 'sharedfolders'), $this);?>
</a>
		</td>
	</tr>
	<tbody id="group_shared" style="display:;">
	<?php $_from = $this->_tpl_vars['sharedFolderList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['folderID'] => $this->_tpl_vars['folder']):
?>
	<?php echo smarty_function_cycle(array('values' => "listTableTD,listTableTD2",'assign' => 'class'), $this);?>

	<tr>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap"><input type="checkbox" id="folder_<?php echo $this->_tpl_vars['folderID']; ?>
" name="folder_<?php echo $this->_tpl_vars['folderID']; ?>
" /></td>
		<td class="<?php if ($this->_tpl_vars['sortColumn'] == 'titel'): ?>listTableTDActive<?php else: ?><?php echo $this->_tpl_vars['class']; ?>
<?php endif; ?>" nowrap="nowrap">&nbsp;<a href="email.php?sid=<?php echo $this->_tpl_vars['sid']; ?>
&folder=<?php echo $this->_tpl_vars['folderID']; ?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/menu_ico_sharedfolder.png" width="16" height="16" border="0" alt="" align="absmiddle" /> <?php echo TemplateText(array('value' => $this->_tpl_vars['folder']['titel'],'cut' => 25), $this);?>
</a>
			<?php if ($this->_tpl_vars['folder']['readonly']): ?><small>(<?php echo TemplateLang(array('p' => 'readonly'), $this);?>
)</small><?php endif; ?></td>
		<td class="<?php if ($this->_tpl_vars['sortColumn'] == 'parent'): ?>listTableTDActive<?php else: ?><?php echo $this->_tpl_vars['class']; ?>
<?php endif; ?>" nowrap="nowrap">&nbsp;<?php echo TemplateText(array('value' => $this->_tpl_vars['folder']['parent'],'cut' => 15), $this);?>
</td>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap" style="text-align:center;">
			<?php echo TemplateSize(array('bytes' => $this->_tpl_vars['folder']['size']), $this);?>

		</td>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap" style="text-align:center;">
			<table>
				<tr>
					<td width="45" align="left"><i class="fa fa-envelope-o"></i>
						<?php echo $this->_tpl_vars['folder']['allMails']; ?>
</td>
					<td width="45" align="left"><i class="fa fa-envelope"></i>
						<?php echo $this->_tpl_vars['folder']['unreadMails']; ?>
</td>
					<td width="45" align="left"><i class="fa fa-flag-o"></i>
						<?php echo $this->_tpl_vars['folder']['flaggedMails']; ?>
</td>
				</tr>
			</table>
		</td>
		<td class="<?php if ($this->_tpl_vars['sortColumn'] == 'subscribed'): ?>listTableTDActive<?php else: ?><?php echo $this->_tpl_vars['class']; ?>
<?php endif; ?>" nowrap="nowrap"><center><input type="checkbox" <?php if ($this->_tpl_vars['folder']['subscribed'] == 1): ?>checked="checked" <?php endif; ?> disabled="disabled" /></center></td>
		<td class="<?php echo $this->_tpl_vars['class']; ?>
" nowrap="nowrap">
			&nbsp;
		</td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>
	</tbody>
	<?php endif; ?>
</table>

</div>

<div id="contentFooter">
	<div class="left">
		<select class="smallInput" name="do">
			<option value="-">------ <?php echo TemplateLang(array('p' => 'selaction'), $this);?>
 ------</option>
			<option value="delete"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option>
		</select>
		<input class="smallInput" type="submit" value="<?php echo TemplateLang(array('p' => 'ok'), $this);?>
" />
	</div>
	
	<div class="right">
		<button class="primary" onclick="document.location.href='email.folders.php?action=addFolder&sid=<?php echo $this->_tpl_vars['sid']; ?>
';" type="button">
			<i class="fa fa-plus-circle"></i>
			<?php echo TemplateLang(array('p' => 'addfolder'), $this);?>

		</button>
	</div>
</div>

</form>