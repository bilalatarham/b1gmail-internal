<?php /* Smarty version 2.6.28, created on 2020-09-29 12:12:43
         compiled from /var/www/html/plugins/templates/bms.admin.deliveryrules.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', '/var/www/html/plugins/templates/bms.admin.deliveryrules.tpl', 3, false),array('function', 'cycle', '/var/www/html/plugins/templates/bms.admin.deliveryrules.tpl', 18, false),array('function', 'text', '/var/www/html/plugins/templates/bms.admin.deliveryrules.tpl', 29, false),)), $this); ?>
<form action="<?php echo $this->_tpl_vars['pageURL']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
&action=msgqueue&do=deliveryRules&save=true" method="post" onsubmit="spin(this)">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'bms_deliveryrules'), $this);?>
</legend>
		
		<table class="list">
			<tr>
				<th><?php echo TemplateLang(array('p' => 'type'), $this);?>
</th>
				<th><?php echo TemplateLang(array('p' => 'field'), $this);?>
</th>
				<th><?php echo TemplateLang(array('p' => 'bms_rule'), $this);?>
</th>
				<th><?php echo TemplateLang(array('p' => 'bms_target'), $this);?>
</th>
				<th><?php echo TemplateLang(array('p' => 'bms_param'), $this);?>
</th>
				<th><?php echo TemplateLang(array('p' => 'options'), $this);?>
</th>
				<th width="80"><?php echo TemplateLang(array('p' => 'pos'), $this);?>
</th>
				<th><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</th>
			</tr>
			
			<?php $_from = $this->_tpl_vars['rules']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ruleID'] => $this->_tpl_vars['rule']):
?>
			<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

			<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
				<td><select name="rules[<?php echo $this->_tpl_vars['ruleID']; ?>
][mail_type]">
						<option value="0"<?php if ($this->_tpl_vars['rule']['mail_type'] == 0): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_inbound'), $this);?>
</option>
						<option value="1"<?php if ($this->_tpl_vars['rule']['mail_type'] == 1): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_outbound'), $this);?>
</option>
					</select></td>
				<td><select name="rules[<?php echo $this->_tpl_vars['ruleID']; ?>
][rule_subject]">
						<option value="1"<?php if ($this->_tpl_vars['rule']['rule_subject'] == 1): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_sender'), $this);?>
</option>
						<option value="2"<?php if ($this->_tpl_vars['rule']['rule_subject'] == 2): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_recipient'), $this);?>
</option>
						<option value="0"<?php if ($this->_tpl_vars['rule']['rule_subject'] == 0): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_recpdomain'), $this);?>
</option>
					</select></td>
				<td><input type="text" name="rules[<?php echo $this->_tpl_vars['ruleID']; ?>
][rule]" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['rule']['rule'],'allowEmpty' => true), $this);?>
" size="16" /></td>
				<td><select name="rules[<?php echo $this->_tpl_vars['ruleID']; ?>
][target]">
						<option value="0"<?php if ($this->_tpl_vars['rule']['target'] == 0): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_target_0'), $this);?>
</option>
						<option value="1"<?php if ($this->_tpl_vars['rule']['target'] == 1): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_redirecttosendmail'), $this);?>
</option>
						<option value="2"<?php if ($this->_tpl_vars['rule']['target'] == 2): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_redirecttosmtprelay'), $this);?>
</option>
						<option value="3"<?php if ($this->_tpl_vars['rule']['target'] == 3): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bms_target_3'), $this);?>
</option>
					</select></td>
				<td><input type="text" name="rules[<?php echo $this->_tpl_vars['ruleID']; ?>
][target_param]" value="<?php echo TemplateText(array('value' => $this->_tpl_vars['rule']['target_param'],'allowEmpty' => true), $this);?>
" size="16" /></td>
				<td>
					<input type="checkbox" name="rules[<?php echo $this->_tpl_vars['ruleID']; ?>
][flags][]" value="1" id="rule<?php echo $this->_tpl_vars['ruleID']; ?>
_flag1"<?php if ($this->_tpl_vars['rule']['flags'] & 1): ?> checked="checked"<?php endif; ?> />
						<label for="rule<?php echo $this->_tpl_vars['ruleID']; ?>
_flag1"><?php echo TemplateLang(array('p' => 'bms_flag_ci'), $this);?>
</label><br />
					<input type="checkbox" name="rules[<?php echo $this->_tpl_vars['ruleID']; ?>
][flags][]" value="2" id="rule<?php echo $this->_tpl_vars['ruleID']; ?>
_flag2"<?php if ($this->_tpl_vars['rule']['flags'] & 2): ?> checked="checked"<?php endif; ?> />
						<label for="rule<?php echo $this->_tpl_vars['ruleID']; ?>
_flag2"><?php echo TemplateLang(array('p' => 'bms_flag_regexp'), $this);?>
</label>
				</td>
				<td><input type="text" name="rules[<?php echo $this->_tpl_vars['ruleID']; ?>
][pos]" value="<?php echo $this->_tpl_vars['rule']['pos']; ?>
" size="6" /></td>
				<td><input type="checkbox" name="rules[<?php echo $this->_tpl_vars['ruleID']; ?>
][delete]" /></td>
			</tr>
			<?php endforeach; endif; unset($_from); ?>
			
			<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

			<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
				<td><select name="rules[0][mail_type]">
					<option value="0"><?php echo TemplateLang(array('p' => 'bms_inbound'), $this);?>
</option>
					<option value="1"><?php echo TemplateLang(array('p' => 'bms_outbound'), $this);?>
</option>
				</select></td>
				<td><select name="rules[0][rule_subject]">
						<option value="1"><?php echo TemplateLang(array('p' => 'bms_sender'), $this);?>
</option>
						<option value="2"><?php echo TemplateLang(array('p' => 'bms_recipient'), $this);?>
</option>
						<option value="0"><?php echo TemplateLang(array('p' => 'bms_recpdomain'), $this);?>
</option>
					</select></td>
				<td><input type="text" name="rules[0][rule]" value="" size="16" /></td>
				<td><select name="rules[0][target]">
						<option value="0"><?php echo TemplateLang(array('p' => 'bms_target_0'), $this);?>
</option>
						<option value="1"><?php echo TemplateLang(array('p' => 'bms_redirecttosendmail'), $this);?>
</option>
						<option value="2"><?php echo TemplateLang(array('p' => 'bms_redirecttosmtprelay'), $this);?>
</option>
						<option value="3"><?php echo TemplateLang(array('p' => 'bms_target_3'), $this);?>
</option>
					</select></td>
				<td><input type="text" name="rules[0][target_param]" value="" size="16" /></td>
				<td>
					<input type="checkbox" name="rules[0][flags][]" value="1" id="rule0_flag1" />
						<label for="rule0_flag1"><?php echo TemplateLang(array('p' => 'bms_flag_ci'), $this);?>
</label><br />
					<input type="checkbox" name="rules[0][flags][]" value="2" id="rule0_flag2" />
						<label for="rule0_flag2"><?php echo TemplateLang(array('p' => 'bms_flag_regexp'), $this);?>
</label>
				</td>
				<td><input type="text" name="rules[0][pos]" value="<?php echo $this->_tpl_vars['nextPos']; ?>
" size="6" /></td>
				<td>&nbsp;</td>
			</tr>
		</table>
	</fieldset>
	
	<p>
		<div style="float:left" class="buttons">
			<input class="button" type="button" value=" &laquo; <?php echo TemplateLang(array('p' => 'back'), $this);?>
 " onclick="document.location.href='<?php echo $this->_tpl_vars['pageURL']; ?>
&action=msgqueue&sid=<?php echo $this->_tpl_vars['sid']; ?>
';" />
		</div>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>