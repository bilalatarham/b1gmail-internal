<?php /* Smarty version 2.6.28, created on 2020-09-29 11:44:24
         compiled from admins.account.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'admins.account.tpl', 3, false),array('function', 'text', 'admins.account.tpl', 9, false),)), $this); ?>
<form action="admins.php?changePassword=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="spin(this)" autocomplete="off">
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'loggedinas'), $this);?>
</legend>
	
		<table>
			<tr>
				<td width="40" valign="top" rowspan="3"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_users.png" border="0" alt="" width="32" height="32" /></td>				
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'username'), $this);?>
:</td>
				<td class="td2"><?php echo TemplateText(array('value' => $this->_tpl_vars['adminRow']['username']), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'name'), $this);?>
:</td>
				<td class="td2"><?php echo TemplateText(array('value' => $this->_tpl_vars['adminRow']['firstname']), $this);?>

								<?php echo TemplateText(array('value' => $this->_tpl_vars['adminRow']['lastname']), $this);?>
</td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'status'), $this);?>
:</td>
				<td class="td2"><?php if ($this->_tpl_vars['adminRow']['type'] == 0): ?><?php echo TemplateLang(array('p' => 'superadmin'), $this);?>
<?php else: ?><?php echo TemplateLang(array('p' => 'admin'), $this);?>
<?php endif; ?></td>
			</tr>
		</table>
	</fieldset>
	
	<fieldset>
		<legend><?php echo TemplateLang(array('p' => 'password'), $this);?>
</legend>
	
		<table>
			<tr>
				<td width="40" valign="top" rowspan="2"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_login.png" border="0" alt="" width="32" height="32" /></td>				
				<td class="td1" width="220"><?php echo TemplateLang(array('p' => 'newpassword'), $this);?>
:</td>
				<td class="td2"><input type="password" name="newpw1" size="36" /></td>
			</tr>
			<tr>
				<td class="td1"><?php echo TemplateLang(array('p' => 'newpassword'), $this);?>
 (<?php echo TemplateLang(array('p' => 'repeat'), $this);?>
):</td>
				<td class="td2"><input type="password" name="newpw2" size="36" /></td>
			</tr>
		</table>
	</fieldset>
	
	<p>
		<div style="float:right" class="buttons">
			<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'save'), $this);?>
 " />
		</div>
	</p>
</form>