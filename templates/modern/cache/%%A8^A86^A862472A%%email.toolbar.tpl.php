<?php /* Smarty version 2.6.28, created on 2020-09-29 11:27:05
         compiled from li/email.toolbar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'hook', 'li/email.toolbar.tpl', 3, false),array('function', 'comment', 'li/email.toolbar.tpl', 4, false),array('function', 'lng', 'li/email.toolbar.tpl', 6, false),array('function', 'progressBar', 'li/email.toolbar.tpl', 7, false),array('function', 'size', 'li/email.toolbar.tpl', 8, false),)), $this); ?>
<table cellspacing="0" cellpadding="0">
	<tr>
		<?php echo TemplateHook(array('id' => "email.toolbar.tpl:firstColumn"), $this);?>

		<?php echo TemplateComment(array('text' => 'space'), $this);?>

		<td><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/tb_sep.gif" border="0" alt="" /></td>
		<td><small>&nbsp; <?php echo TemplateLang(array('p' => 'space'), $this);?>
: &nbsp;</small></td>
		<td><?php echo TemplateProgressBar(array('value' => $this->_tpl_vars['spaceUsed'],'max' => $this->_tpl_vars['spaceLimit'],'width' => 100), $this);?>
</td>
		<td><small>&nbsp; <?php echo TemplateSize(array('bytes' => $this->_tpl_vars['spaceUsed']), $this);?>
 / <?php echo TemplateSize(array('bytes' => $this->_tpl_vars['spaceLimit']), $this);?>
 <?php echo TemplateLang(array('p' => 'used'), $this);?>
</small></td>
		
		<?php if ($this->_tpl_vars['enablePreview']): ?>
		<td width="15">&nbsp;</td>
		<td><img align="absmiddle" src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/li/tb_sep.gif" border="0" alt="" /></td>
		<td><small>&nbsp; <?php echo TemplateLang(array('p' => 'preview'), $this);?>
: &nbsp;</small></td>
		<td><select class="smallInput" onchange="updatePreviewPosition(this)">
			<option value="bottom"<?php if (! $this->_tpl_vars['narrow']): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'bottom'), $this);?>
</option>
			<option value="right"<?php if ($this->_tpl_vars['narrow']): ?> selected="selected"<?php endif; ?>><?php echo TemplateLang(array('p' => 'right'), $this);?>
</option>
		</select></td>
		<?php endif; ?>
		
		<?php echo TemplateHook(array('id' => "email.toolbar.tpl:lastColumn"), $this);?>

	</tr>
</table>