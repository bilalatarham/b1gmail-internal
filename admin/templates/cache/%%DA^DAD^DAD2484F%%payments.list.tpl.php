<?php /* Smarty version 2.6.28, created on 2020-09-30 13:03:12
         compiled from payments.list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'lng', 'payments.list.tpl', 9, false),array('function', 'cycle', 'payments.list.tpl', 27, false),array('function', 'email', 'payments.list.tpl', 31, false),array('function', 'text', 'payments.list.tpl', 32, false),array('function', 'date', 'payments.list.tpl', 44, false),array('function', 'pageNav', 'payments.list.tpl', 70, false),)), $this); ?>
<form action="payments.php?filter=true&sid=<?php echo $this->_tpl_vars['sid']; ?>
" method="post" onsubmit="if(EBID('massAction').value!='download') spin(this)" name="f1">
<input type="hidden" name="page" id="page" value="<?php echo $this->_tpl_vars['pageNo']; ?>
" />
<input type="hidden" name="sortBy" id="sortBy" value="<?php echo $this->_tpl_vars['sortBy']; ?>
" />
<input type="hidden" name="sortOrder" id="sortOrder" value="<?php echo $this->_tpl_vars['sortOrder']; ?>
" />
<input type="hidden" name="singleAction" id="singleAction" value="" />
<input type="hidden" name="singleID" id="singleID" value="" />

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'payments'), $this);?>
</legend>

	<table class="list">
		<tr>
			<th width="20">&nbsp;</th>
			<th width="25" style="text-align:center;"><a href="javascript:invertSelection(document.forms.f1,'payment[');"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/dot.png" border="0" alt="" width="10" height="8" /></a></th>
			<th><a href="javascript:updateSort('userid');"><?php echo TemplateLang(array('p' => 'user'), $this);?>

				<?php if ($this->_tpl_vars['sortBy'] == 'userid'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/sort_<?php echo $this->_tpl_vars['sortOrder']; ?>
.png" border="0" alt="" width="7" height="6" align="absmiddle" /><?php endif; ?></a></th>
			<th><a href="javascript:updateSort('orderid');"><?php echo TemplateLang(array('p' => 'orderno'), $this);?>

				<?php if ($this->_tpl_vars['sortBy'] == 'orderid'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/sort_<?php echo $this->_tpl_vars['sortOrder']; ?>
.png" border="0" alt="" width="7" height="6" align="absmiddle" /><?php endif; ?></a></th>
			<th width="135"><a href="javascript:updateSort('amount');"><?php echo TemplateLang(array('p' => 'amount'), $this);?>

				<?php if ($this->_tpl_vars['sortBy'] == 'amount'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/sort_<?php echo $this->_tpl_vars['sortOrder']; ?>
.png" border="0" alt="" width="7" height="6" align="absmiddle" /><?php endif; ?></a></th>
			<th width="145"><a href="javascript:updateSort('created');"><?php echo TemplateLang(array('p' => 'date'), $this);?>

				<?php if ($this->_tpl_vars['sortBy'] == 'created'): ?><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/sort_<?php echo $this->_tpl_vars['sortOrder']; ?>
.png" border="0" alt="" width="7" height="6" align="absmiddle" /><?php endif; ?></a></th>
			<th width="65">&nbsp;</th>
		</tr>

		<?php $_from = $this->_tpl_vars['payments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['payment']):
?>
		<?php echo smarty_function_cycle(array('name' => 'class','values' => "td1,td2",'assign' => 'class'), $this);?>

		<tr class="<?php echo $this->_tpl_vars['class']; ?>
">
			<td align="center"><img src="templates/images/<?php if ($this->_tpl_vars['payment']['status'] == 1): ?>yes<?php else: ?>no<?php endif; ?>.png" border="0" alt="" width="16" height="16" /></td>
			<td align="center"><input type="checkbox" name="payment[<?php echo $this->_tpl_vars['payment']['orderid']; ?>
]" /></td>
			<td><a href="users.php?do=edit&id=<?php echo $this->_tpl_vars['payment']['user']['id']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
"><?php echo TemplateEMail(array('value' => $this->_tpl_vars['payment']['user']['email'],'cut' => 25), $this);?>
</a><br />
				<small><?php echo TemplateText(array('value' => $this->_tpl_vars['payment']['user']['nachname'],'cut' => 20), $this);?>
, <?php echo TemplateText(array('value' => $this->_tpl_vars['payment']['user']['vorname'],'cut' => 20), $this);?>
</small></td>
			<td><?php echo TemplateText(array('value' => $this->_tpl_vars['payment']['invoiceNo']), $this);?>
<br /><small><?php echo TemplateText(array('value' => $this->_tpl_vars['payment']['customerNo']), $this);?>
</small></td>
			<td>
				<div style="float:left;">
					<?php echo $this->_tpl_vars['payment']['amount']; ?>
<br /><small><?php echo $this->_tpl_vars['payment']['method']; ?>
</small>
				</div>
				<?php if ($this->_tpl_vars['payment']['paymethod'] < 0): ?>
				<div style="float:right;">
					<a href="payments.php?do=details&orderid=<?php echo $this->_tpl_vars['payment']['orderid']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
" title="<?php echo TemplateLang(array('p' => 'details'), $this);?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/ico_prefs_payments.png" border="0" alt="<?php echo TemplateLang(array('p' => 'details'), $this);?>
" width="16" height="16" /></a>
				</div>
				<?php endif; ?>
			</td>
			<td><?php echo TemplateDate(array('timestamp' => $this->_tpl_vars['payment']['created'],'nice' => true), $this);?>
</td>
			<td>
				<?php if ($this->_tpl_vars['payment']['hasInvoice']): ?><a href="javascript:void(0);" onclick="openWindow('payments.php?action=showInvoice&orderID=<?php echo $this->_tpl_vars['payment']['orderid']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
','invoice_<?php echo $this->_tpl_vars['payment']['orderid']; ?>
',640,480);" title="<?php echo TemplateLang(array('p' => 'showinvoice'), $this);?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/file.png" border="0" alt="<?php echo TemplateLang(array('p' => 'showinvoice'), $this);?>
" width="16" height="16" /></a><?php endif; ?>
				<?php if ($this->_tpl_vars['payment']['status'] == 0): ?><a href="<?php if ($this->_tpl_vars['payment']['paymethod'] < 0): ?>payments.php?do=details&orderid=<?php echo $this->_tpl_vars['payment']['orderid']; ?>
&sid=<?php echo $this->_tpl_vars['sid']; ?>
<?php else: ?>javascript:singleAction('activate', '<?php echo $this->_tpl_vars['payment']['orderid']; ?>
');<?php endif; ?>" title="<?php echo TemplateLang(array('p' => 'activatepayment'), $this);?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/unlock.png" border="0" alt="<?php echo TemplateLang(array('p' => 'activatepayment'), $this);?>
" width="16" height="16" /></a><?php endif; ?>
				<a href="javascript:singleAction('delete', '<?php echo $this->_tpl_vars['payment']['orderid']; ?>
');" onclick="return confirm('<?php echo TemplateLang(array('p' => 'realdel'), $this);?>
');" title="<?php echo TemplateLang(array('p' => 'delete'), $this);?>
"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/delete.png" border="0" alt="<?php echo TemplateLang(array('p' => 'delete'), $this);?>
" width="16" height="16" /></a>
			</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>

		<tr>
			<td class="footer" colspan="7">
				<div style="float:left;">
					<?php echo TemplateLang(array('p' => 'action'), $this);?>
: <select name="massAction" id="massAction" class="smallInput">
						<option value="-">------------</option>

						<optgroup label="<?php echo TemplateLang(array('p' => 'actions'), $this);?>
">
							<option value="download"><?php echo TemplateLang(array('p' => 'downloadinvoices'), $this);?>
</option>
							<option value="activate"><?php echo TemplateLang(array('p' => 'activatepayment'), $this);?>
</option>
							<option value="delete"><?php echo TemplateLang(array('p' => 'delete'), $this);?>
</option>
						</optgroup>
					</select>&nbsp;
				</div>
				<div style="float:left;">
					<input type="submit" name="executeMassAction" value=" <?php echo TemplateLang(array('p' => 'execute'), $this);?>
 " class="smallInput" />
				</div>
				<div style="float:right;padding-top:3px;">
					<?php echo TemplateLang(array('p' => 'pages'), $this);?>
: <?php echo TemplatePageNav(array('page' => $this->_tpl_vars['pageNo'],'pages' => $this->_tpl_vars['pageCount'],'on' => " <span class=\"pageNav\"><b>[.t]</b></span> ",'off' => " <span class=\"pageNav\"><a href=\"javascript:updatePage(.s);\">.t</a></span> "), $this);?>
&nbsp;
				</div>
			</td>
		</tr>
	</table>
</fieldset>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'filter'), $this);?>
</legend>

	<table width="100%">
		<tr>
			<td width="40" valign="top" rowspan="2"><img src="<?php echo $this->_tpl_vars['tpldir']; ?>
images/filter.png" border="0" alt="" width="32" height="32" /></td>
			<td class="td1" width="100"><?php echo TemplateLang(array('p' => 'status'), $this);?>
:</td>
			<td class="td2">
				<input type="checkbox" name="status[0]" id="status_0" <?php if ($this->_tpl_vars['status'][0]): ?> checked="checked"<?php endif; ?> />
					<label for="status_0"><b><?php echo TemplateLang(array('p' => 'orderstatus_0'), $this);?>
</b></label><br />
				<input type="checkbox" name="status[1]" id="status_1" <?php if ($this->_tpl_vars['status'][1]): ?> checked="checked"<?php endif; ?> />
					<label for="status_1"><b><?php echo TemplateLang(array('p' => 'orderstatus_1'), $this);?>
</b></label><br />
			</td>
			<td class="td1" width="130"><?php echo TemplateLang(array('p' => 'paymentmethods'), $this);?>
:</td>
			<td class="td2">
				<input type="checkbox" name="paymentMethod[0]" id="paymentMethod_0" <?php if ($this->_tpl_vars['paymentMethod'][0]): ?> checked="checked"<?php endif; ?> />
					<label for="paymentMethod_0"><b><?php echo TemplateLang(array('p' => 'banktransfer'), $this);?>
</b></label><br />
				<input type="checkbox" name="paymentMethod[1]" id="paymentMethod_1" <?php if ($this->_tpl_vars['paymentMethod'][1]): ?> checked="checked"<?php endif; ?> />
					<label for="paymentMethod_1"><b><?php echo TemplateLang(array('p' => 'paypal'), $this);?>
</b></label><br />
				<input type="checkbox" name="paymentMethod[2]" id="paymentMethod_2" <?php if ($this->_tpl_vars['paymentMethod'][2]): ?> checked="checked"<?php endif; ?> />
					<label for="paymentMethod_2"><b><?php echo TemplateLang(array('p' => 'su'), $this);?>
</b></label><br />
				<input type="checkbox" name="paymentMethod[3]" id="paymentMethod_3" <?php if ($this->_tpl_vars['paymentMethod'][3]): ?> checked="checked"<?php endif; ?> />
					<label for="paymentMethod_3"><b><?php echo TemplateLang(array('p' => 'skrill'), $this);?>
</b></label><br />
				<?php $_from = $this->_tpl_vars['payMethods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['methodID'] => $this->_tpl_vars['method']):
?>
				<input type="checkbox" name="paymentMethod[-<?php echo $this->_tpl_vars['methodID']; ?>
]" id="paymentMethod_-<?php echo $this->_tpl_vars['methodID']; ?>
" <?php if ($this->_tpl_vars['paymentMethod'][$this->_tpl_vars['method']['negID']]): ?> checked="checked"<?php endif; ?> />
					<label for="paymentMethod_-<?php echo $this->_tpl_vars['methodID']; ?>
"><b><?php echo TemplateText(array('value' => $this->_tpl_vars['method']['title']), $this);?>
</b></label><br />
				<?php endforeach; endif; unset($_from); ?>
			</td>
		</tr>
	</table>

	<p align="right">
		<?php echo TemplateLang(array('p' => 'perpage'), $this);?>
:
		<input type="text" name="perPage" value="<?php echo $this->_tpl_vars['perPage']; ?>
" size="5" />
		<input class="button" type="submit" value=" <?php echo TemplateLang(array('p' => 'apply'), $this);?>
 " />
	</p>
</fieldset>

</form>

<fieldset>
	<legend><?php echo TemplateLang(array('p' => 'activatepayment'), $this);?>
</legend>

	<table>
		<tr>
			<td align="left" rowspan="3" valign="top" width="40"><img src="templates/images/ico_prefs_payments.png" border="0" alt="" width="32" height="32" /></td>
			<td colspan="2"><?php echo TemplateLang(array('p' => 'activate_desc'), $this);?>
</td>
		</tr>
		<tr>
			<td class="td1" width="120"><?php echo TemplateLang(array('p' => 'vkcode'), $this);?>
:</td>
			<td class="td2"><input type="text" name="vkCode" id="vkCode" value="VK-" size="26" onkeypress="return handleActivatePaymentInput(event, 0);"  /></td>
		</tr>
		<tr>
			<td class="td1"><?php echo TemplateLang(array('p' => 'amount'), $this);?>
:</td>
			<td class="td2"><input type="text" name="amount" id="amount" value="" size="10" onkeypress="return handleActivatePaymentInput(event, 1);" /> <?php echo TemplateText(array('value' => $this->_tpl_vars['bm_prefs']['currency']), $this);?>
</td>
		</tr>
	</table>

	<p>
		<div style="float:left;font-weight:bold;padding-top:4px;" id="activationResult">&nbsp;</div>
		<div style="float:right">
			<input class="button" type="button" onclick="activatePayment()" id="activateButton" value=" <?php echo TemplateLang(array('p' => 'activate'), $this);?>
 " />
		</div>
	</p>
</fieldset>